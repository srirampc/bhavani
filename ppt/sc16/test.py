import sys

str_db = ['TGAATTCGATTACATAACGA',
                'CGATTACAGAACTATCGGT',
          'TCAATGCGATTACACTGACG',
            'GATCCGATTACAAAACC']

#          12345678
str_db = ['ATAAATTGATTACATGTAGAT',
                 'GATTACAGGTATATGACT',
          'ATCAATGGATTACACTCACG',
             'GATCGATTACAAGTAC']



def gen_kmers(in_db, klen):
    kmer_dict = {}
    for sx in in_db:
        if len(sx) < klen:
            continue
        for ix in range(len(sx) - klen + 1):
            kmr = sx[ix:ix+klen]
            if kmr in kmer_dict:
                kmer_dict[kmr] += 1
            else:
                kmer_dict[kmr] = 1
    return kmer_dict

def rev_strings(in_db):
    return [sx[::-1] for sx in in_db]

def print_kdict(kmer_dict):
    for x in sorted(kmer_dict.keys()):
        print x, kmer_dict[x]

#123456789012
#    TAGATGTACATTAGTTAAATA
#TCAGTATATGGACATTAG
#     GCACTCACATTAGGTAACTA
#      CATGAACATTAGCTAG
