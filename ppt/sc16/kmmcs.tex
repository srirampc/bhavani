\documentclass[t]{beamer}
\usetheme[theme=navy]{sc16}
%\setmathsfont[Set=Greek,Uppercase=Italic,Lowercase=Italic]{Minion Pro}


\usepackage[MnSymbol]{mathspec}
\usepackage{fontspec}%
\usefonttheme[onlymath]{serif}
%\newfontfamily\Light{Concourse T2}%
%\newfontfamily\Book{FiraSans-Book}%
%\newfontfamily\Medium{FiraSans-Medium}%
%\setprimaryfont{Minion Pro}
\setsansfont[Mapping=tex-text]{Concourse T4}
\setmainfont[Mapping=tex-text]{Minion Pro}%
\setmathsfont[Set=Greek,Uppercase=Italic,Lowercase=Italic]{Minion Pro}
\setmonofont[Mapping=tex-text]{Source Code Pro}

\usepackage{tikz}

%\title{A Customized Beamer Template 2008}
%\author{Sriram P C\\ srirampc}
\title{\LARGE\bf A Parallel Algorithm for Finding All Pairs $k$-Mismatch Maximal Common Substrings}
\author{{\textbf{Sriram Chockalingam}${}^1$ \and \\ Sharma Thankachan ${}^2$ \and \\ Srinivas Aluru ${}^{1,2}$}}
\institute{${}^1$ Institute for Data Engineering and Science. \\ ${}^2$ School of Computational Science and Engg. \\ Georgia Institute of Technology, Atlanta.}

\newcommand{\D}{{\mathcal D}}
\newcommand{\occ}{\mathsf{occ}}
\newcommand{\sdepth}{\mathsf{string\textnormal{-}depth}}
\newcommand{\srev}{\mathsf{string\textnormal{-}reverse}}
\newcommand{\plabel}{\mathsf{path\textnormal{-}label}}
\newcommand{\match}{\mathsf{match\textnormal{-}so\textnormal{-}far}}
\newcommand{\trx}{\texttt{\$}}
\newcommand{\try}{\texttt{€}} % ₹
\newcommand{\tra}{\texttt{\$}}
\newcommand{\trb}{\texttt{€}} %
\newcommand{\trc}{\texttt{₹}}
\newcommand{\trd}{\texttt{¥}} %
\newcommand\inhighlight[1]{%
  \tikz[baseline=-0.7ex,outer sep=0,
    inner sep=0,inner ysep=1pt]{%
    \node[fill=gtBuzz]{\texttt{#1}};
  }%
}
\newcommand\innode[2]{%
  {
    \tikz[remember picture,baseline=#2]{%
      \node[inner sep=0pt,outer sep=0pt,minimum width=0mm,minimum height=0mm] (#1) {};
    }%
  }
}


\begin{document}
\dept{}
\statustext{}
\begin{titleframe}
% You can insert additional text for the title frame here if you want
\end{titleframe}
%\begin{frame}
%  \frametitle{Suffix Trees}
%\end{frame}

\begin{frame}
  \frametitle{Problem Definition}
    \vspace*{0.5cm}
\begin{center}
  \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
  \item Given
    \begin{itemize} \itemsep=2ex
      \vspace*{0.1cm}
      \item Collection $\D=\{S_1,S_2,\dots, S_n\}$ of $n$ sequences with $\sum_i |S_i| = N$,
      \item Length threshold $\phi > 0$ and
      \item Mismatch threshold $k \geq 0$.
    \end{itemize}
  \item Report
    \begin{itemize}
    \item All $k$-mismatch maximal common substrings (MCS) of length  $\geq \phi$ of any pair of sequences in $\D$.
    \end{itemize}
  \end{itemize}
\end{center}
\end{frame}

\begin{slide}
  \frametitle{Generalized Suffix Trees (GST)}
  \begin{center}
  \begin{itemize} \itemsep=4ex
    \vspace{0.1cm}
    \item Index data structure for a given set of sequences.
    \item Tree of suffixes satisfying thee properties.
       \begin{enumerate}\itemsep=1.5ex
         \vspace*{1.5ex}
       \item<2-> Every suffix is a leaf in the GST.
       \item<2-> Labelling of root to leaf path.
       \item<2-> Lowest Common Ancestor (LCA) property.
      \end{enumerate}
    \item<3-> A example with two sequences: $\{ E_1, E_2\}$, where
      \vspace{1mm}

      \begin{center}
      \begin{tabular}{c}
        $E_1$ = \verb|cabana|\trx  \\
        $E_2$ = \verb|banana|\try  \\
      \end{tabular}
      \end{center}
  \end{itemize}
  \end{center}
\end{slide}

\begin{slide}
  \frametitle{P1: Every Suffix is a Leaf}
  \includegraphics[width=1.01\linewidth]{../graphics/graphics-suffix-tree.pdf}
  \begin{center}
    \begin{tabular}{c}
    $E_1$ = \verb|cabana|\trx  \\
    $E_2$ = \verb|banana|\try  \\
   \end{tabular}
  \end{center}
\end{slide}
\begin{slide}
  \frametitle{P2: Labelling of Root to Leaf Path}
  \includegraphics[width=1.01\linewidth]{../graphics/graphics-suffix-tree-leaf-path.pdf}
  \begin{center}
    \begin{tabular}{c}
    $E_1$ = \verb|cabana|\trx  \\
    $E_2$ = \verb|b|\tikz[baseline=-0.7ex,outer sep=0,inner sep=0,inner ysep=1pt]{\node[draw,fill=gtBuzz]{\verb|anana|\try}}  \\
   \end{tabular}
  \end{center}
\end{slide}
\begin{slide}
  \frametitle{P3: Lowest Common Ancestor (LCA) of two Leaves}
  \includegraphics[width=1.01\linewidth]{../graphics/graphics-suffix-tree-lca.pdf}
  \begin{center}
    \begin{tabular}{c}
    $E_1$ = \verb|ca|\tikz[baseline=-0.7ex,outer sep=0,inner sep=0,inner ysep=1pt]{\node[draw,fill=gtBuzz]{\verb|bana|}}\trx  \\
    $E_2$ = \tikz[baseline=-0.7ex,outer sep=0,inner sep=0,inner ysep=1pt]{\node[draw,fill=gtBuzz]{\verb|bana|}}\verb|na|\try  \\
   \end{tabular}
  \end{center}
\end{slide}
\begin{slide}
  \frametitle{Generalized Suffix Tree: Known Results}
  \begin{center}
    \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
  \item Construction : $O(N)$ time (McCrieght, 1976).
      \begin{itemize}\itemsep=1.5ex
        \vspace*{0.1cm}
      \item were $N$ is the sum of the lengths of input sequences.
      \end{itemize}
    \item LCA query : $O(1)$ time (Harel and Tarjan, 1984).
    \item For any internal node $u \in \mathsf{GST}$,
      \begin{itemize}\itemsep=1.5ex
        \vspace*{0.1cm}
      \item $\sdepth(u) = |\plabel(u)|$ : $O(1)$ time.
      \item List of suffixes under $u$  : $O(\mathsf{subtree}\,\mathsf{size})$ time.
      \end{itemize}
  \end{itemize}
  \end{center}
\end{slide}
\begin{frame}
  \frametitle{Compact Suffix Trie}
  \vspace*{-3mm}
  \begin{center}
  \includegraphics[width=0.8\linewidth]{../graphics/graphics-compact-suffix-trie.pdf}
  \begin{itemize}
    \item Constructed for any subset of the suffixes.
    \item Given GST, construction can be in linear time (Weiner, 1973).
    \item Height of Compact trie $\leq H$, Height of the suffix tree.
  \end{itemize}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Problem Definition}
    \vspace*{0.5cm}
\begin{center}
  \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
  \item Given
    \begin{itemize} \itemsep=2ex
      \vspace*{0.1cm}
      \item Collection $\D=\{S_1,S_2,\dots, S_n\}$ of $n$ sequences with $\sum_i |S_i| = N$,
      \item Length threshold $\phi > 0$ and
      \item Mismatch threshold $k \geq 0$.
    \end{itemize}
  \item Report
    \begin{itemize}
    \item All $k$-mismatch maximal common substrings (MCS) of length  $\geq \phi$ of any pair of sequences in $\D$.
    \end{itemize}
  \end{itemize}
\end{center}
\end{frame}

\begin{frame}
  \frametitle{Properties of Output Pairs}
  \begin{center}
  \begin{itemize}\itemsep=4ex
    \vspace*{1cm}
  \item Output produced as pairs: $(S_a[x..], S_b[y..])$.
  \item<2-> A pair $(S_a[x..], S_b[y..])$ is a valid output if:
    \begin{enumerate} \itemsep=2ex
      \vspace*{0.2cm}
      \item $S_a[x..]$ and $S_b[y..]$ share a common prefix of length $\geq \phi$ while allowing $k$ mismatches.
      \item $x = 1$ or $y = 1$ or $S_a[x - 1] \neq S_b[y - 1]$.
      \item $a \neq b$.
      \end{enumerate}
  \end{itemize}
  \end{center}
\end{frame}
\begin{frame}
  \frametitle{Length of Maximal Match Segments}
  \begin{center}
    \includegraphics[width=\linewidth]{../graphics/graphics-maximal-match.pdf}
  \end{center}
  \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
  \item<1-> A $k$ mismatch maximal common substring has $k + 1$ maximal matches.
  \item<2-> Longest maximal match is at least
    \[ \tau = \left\lceil \frac{\phi - k}{ k + 1} \right\rceil \]
  \item<3-> Find the $\geq \tau$ length maximal match segment first.
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Our Approach}
  \vspace*{5mm}
 \only<1>{
  \begin{center}
    \includegraphics[width=\linewidth]{../graphics/graphics-maximal-match-olap.pdf}
  \end{center}
  }
\only<2>{
  \begin{center}
    \includegraphics[width=\linewidth]{../graphics/graphics-maximal-match-fwd.pdf}
  \end{center}
  }
\only<3>{
  \begin{center}
    \includegraphics[width=\linewidth]{../graphics/graphics-maximal-match-rev.pdf}
  \end{center}
}
\only<4->{
  \begin{center}
    \includegraphics[width=\linewidth]{../graphics/graphics-maximal-match-fwdrev.pdf}
  \end{center}
    }
    \vspace*{0.3cm}
    \begin{itemize}\itemsep=3ex
    \item<2-> Start with $\geq \tau$ length match, search towards the right of the match.
    \item<3-> Start with $\geq \tau$ length match, search towards the left of the match.
    \item<4-> For every rightward search, search leftward ($k+1$ possible ways).
    \item<5-> Using distributed GST, we search simultaneously in parallel.
    \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Primary Nodes}
 \vspace{-4mm}
  \only<1>{
  \begin{center}
    \includegraphics[width=0.8\linewidth]{../graphics/graphics-gst-main.pdf}
  \end{center}
  }
  \only<2>{
  \begin{center}
    \includegraphics[width=0.8\linewidth]{../graphics/graphics-gst-primary.pdf}
  \end{center}
  }
  \only<3>{
  \begin{center}
    \includegraphics[width=0.8\linewidth]{../graphics/graphics-gst-distribute.pdf}
  \end{center}
  }
 \vspace{-4mm}
  \begin{center}
  \begin{itemize}
  \item<1-> Construct distributed GST of $\mathcal{D} = \{S_1,\ldots,S_n\}$.
  \item<2-> Identify Primary Nodes: $u$ such that
    \[ \sdepth(u) \geq \tau > \sdepth(parent(u)) \]
  \item<3-> Distribute Primary Nodes (Assuming the \# of $\tau$-length strings $\leq N/p$ ).
  \end{itemize}
  \end{center}
\end{frame}
\begin{slide}
  \frametitle{Rightward Search}
  \vspace{-0.3mm}
  \only<1-2>{
    \begin{center}
      Rightwards Search
    \end{center}
    \includegraphics[width=0.92\linewidth]{../graphics/graphics-maximal-match-fwd.pdf}

    \vspace{0.3cm}

    Example with four sequences: \\
  }
  \only<3>{
    \includegraphics[width=0.92\linewidth]{../graphics/graphics-fwd-stree.pdf}
  }
  \only<4>{
    \includegraphics[width=0.92\linewidth]{../graphics/graphics-fwd-stree-path.pdf}
  }
  \only<5>{
    \includegraphics[width=0.92\linewidth]{../graphics/graphics-fwd-stree-chop.pdf}
  }
  \only<6>{
    \includegraphics[width=0.92\linewidth]{../graphics/graphics-fwd-stree-trie.pdf}
  }
  \begin{center}
    \begin{onlyenv}<2>

   \begin{tabular}{ll}
    $S_1$ & \verb|ATAAATTGATTACATGTAGAT|\tra \\
    $S_2$ & \verb|GATTACAGGTATATGACT|\trb \\
    $S_3$ & \verb|ATCAATGGATTACACTCACG|\trc \\
    $S_4$ & \verb|GATCGATTACAAGTAC|\trd \\
   \end{tabular}
   \vspace{0.5cm}

   $\phi = 14, k = 2 \Rightarrow \tau = 4$
   \end{onlyenv}
  \begin{onlyenv}<3>
    \vspace{-2mm}
   \begin{tabular}{ll}
    $S_1$ & \verb|ATAAATTGATTACATGTAGAT|\tra \\
    $S_2$ & \verb|GATTACAGGTATATGACT|\trb \\
    $S_3$ & \verb|ATCAATGGATTACACTCACG|\trc \\
    $S_4$ & \verb|GATCGATTACAAGTAC|\trd \\
   \end{tabular}
  \end{onlyenv}
  \begin{onlyenv}<4>
    \vspace{-2mm}
   \begin{tabular}{ll}
    $S_1$ & \verb|ATAAATT|\inhighlight{GATTACA}\verb|TGTAGAT|\tra \\
    $S_2$ & \verb|       |\inhighlight{GATTACA}\verb|GGTATATGACT|\trb \\
    $S_3$ & \verb|ATCAATG|\inhighlight{GATTACA}\verb|CTCACG|\trc \\
    $S_4$ & \verb|   GATC|\inhighlight{GATTACA}\verb|AGTAC|\trd \\
   \end{tabular}
  \end{onlyenv}
  \begin{onlyenv}<5->
    \vspace{-2mm}
   \begin{tabular}{ll}
    $S_1$ & \verb|ATAAATT|\inhighlight{GATTACA}\verb|TGTAGAT|\tra \\
    $S_2$ & \verb|       |\inhighlight{GATTACA}\verb|GGTATATGACT|\trb \\
    $S_3$ & \verb|ATCAATG|\inhighlight{GATTACA}\verb|CTCACG|\trc \\
    $S_4$ & \verb|   GATC|\inhighlight{GATTACA}\verb|AGTAC|\trd \\
   \end{tabular}
  \end{onlyenv}
  \end{center}
\end{slide}
\begin{frame}
  \frametitle{Time Complexity}
  \vspace{-1.2mm}
  \begin{columns}[T]
    \begin{column}{0.62\linewidth}
  \only<1>{ %
    \includegraphics[width=\linewidth]{../graphics/graphics-algo-recursion-lzero.pdf} %
    } %
  \only<2>{ %
    \includegraphics[width=\linewidth]{../graphics/graphics-algo-recursion-lone.pdf} %
    } %
  \only<3->{ %
    \includegraphics[width=\linewidth]{../graphics/graphics-algo-recursion.pdf}
  } %
  \end{column}
    \begin{column}{0.37\linewidth}
      \vspace{0.3cm}
      Let
      \begin{itemize}
      \item $\tau$ be long enough.
        \item $m = |$A Primary Node$|$.
        \item $H = $ Height of the GST.
      \end{itemize}
      \uncover<4->{ %
        Then
          \[ \sum_{j = 0}^{k} O(mH^{\,j}) = O(mH^k) \]
      }
      \uncover<5>{%
        Since $m \leq N/p$ and
         $E(H) = \log N$ (Devroye '92),
        \[ O((N/p) \log^k N) \]
        is the expected runtime.
      }
  \end{column}
  \end{columns}
\end{frame}
\begin{frame}
  \frametitle{Pair Generation}
  \begin{center}
    \vspace*{1.5cm}
    \begin{itemize}
    \item \begin{tabular}{ll} For $v \in T_k^{\,z}$ & s.t. \\
      & $\match + \sdepth(v) \geq \phi$ and \\
      & $\match + \sdepth(parent(v)) < \phi$ \\ \end{tabular}
   \vspace*{0.2cm}
   \begin{enumerate}\itemsep=2.5ex
   \item Sort the suffixes into $|\Sigma|+1$ buckets.
   \item Sort suffixes in each bucket by sequence identifier.
   \item Generate suffix pairs satisfying maximality conditions.
   \end{enumerate}
   \end{itemize}
  \end{center}
\end{frame}
\begin{slide}
  \frametitle{Leftward Search}
  \only<1>{
    \begin{center}
      Leftward Search
    \end{center}
    \includegraphics[width=0.92\linewidth]{../graphics/graphics-maximal-match-rev.pdf}
    \vspace{0.5cm}

    In our example,  \\
    }
 \only<2->{
  \includegraphics[width=0.9\linewidth]{../graphics/graphics-rev-stree-trie.pdf}
 }
 \begin{center}

   \begin{tabular}{ll}
    $S_1$ & \verb|ATA|\inhighlight{AAT}\verb|T|\inhighlight{GATTACA}\verb|TGTAGAT|\tra \\
    $S_3$ & \verb|ATC|\inhighlight{AAT}\verb|G|\inhighlight{GATTACA}\verb|CTCACG|\trc \\
   \end{tabular}
 \end{center}
\end{slide}
\begin{frame}
  \frametitle{\only<1>{Leftward Search}\only<2->{Right-Left Search}}
  \only<1>{ %
    \begin{center}
      \includegraphics[width=0.8\linewidth]{../graphics/graphics-algo-recursion-reverse.pdf} %
    \end{center}
  } %
  \only<2>{ %
  \vspace*{2cm}
    \begin{center}
      Right-Left Search
    \end{center}
    \begin{center}
    \includegraphics[width=0.8\linewidth]{../graphics/graphics-maximal-match-fwdrev.pdf} %
    \end{center}
  }
  \only<3->{ %
    \begin{center}
    \includegraphics[width=0.8\linewidth]{../graphics/graphics-algo-recursion-reversejx.pdf} %
    \end{center}
  }
    \begin{center}
  \only<1>{
    Parallel runtime for construction remain $O((N/p) H^k)$.
  }
  \only<3>{
     Parallel runtime for construction remain $O((N/p) H^k)$.
  }
  \only<4>{
      Each output pair processed $O(H^k)$ times. Hence $O((N/p + \occ) H^k)$ time.
  }
    \end{center}
\end{frame}
\begin{frame}
  \frametitle{Implementation Details}
  \vspace{1cm}
  \begin{center}
    \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
    \item Use distributed Suffix Array (SA) and Longest Common Prefix (LCP) arrays (Flick and Aluru, 2015) for GST for $\D$ and $\overleftarrow{\D}$ .
    \item Tries represented by corresponding SA and LCP arrays.
    \item Distribution via shifting SA, LCP appropriately.
    \item Tries construction by \emph{all2all} query on Inverse Suffix Array (ISA).
    \end{itemize}
  \end{center}
\end{frame}
%\begin{frame}
%  \frametitle{Communication Complexity}
%\end{frame}
\begin{frame}
  \frametitle{Experiments : Datasets}
  \vspace*{0.5cm}
  \begin{center}
\renewcommand{\arraystretch}{1.4}
\begin{tabular}{lrrr}
\hline
            & D1          & D2            & D3          \\
\hline
Type        & RNA         & DNA           & RNA         \\
Organism    & H. sapiens  & S. cerevisiae & H. sapiens  \\
Sequencer   & NextSeq 500 & HiSeq 2000    & HiSeq 2500  \\
No. Sequences   & 60 M  & 18 M    & 272 M \\
Sequence Length & 75          & 101           & 151         \\
Total Size  & 4.5 Gbp    & 1.8 Gbp      & 38.4 Gbp   \\
Input Size  & 3.2 Gbp   & 1.7 Gbp  & 35.1 Gbp   \\
\hline
\end{tabular}
  \end{center}
\end{frame}
%% \begin{frame}
%%   \frametitle{Experiments : Pre-processing}
%%   \vspace*{0.5cm}
%%   \begin{center}
%% \renewcommand{\arraystretch}{1.3}
%%   \begin{tabular}{lrrr}
%%     \hline
%% \multicolumn{4}{c}{{\textsc{Dataset Sizes before/after pre-processing}}} \\
%% \hline
%%            & D1         & D2        & D3          \\
%% \hline
%% No. Reads  Before PP  & 60,100,561  & 18,415,332    & 272,462,716 \\
%% No. Reads  After PP & 21,682,850 & 8,263,882 & 116,295,542 \\
%% Size Before PP  & 4.507Gbp    & 1.860Gbp      & 38.417Gbp   \\
%% Size After PP  & 1.626Gbp   & 0.835Gbp  & 17.560Gbp   \\
%% Input Size & 3.252Gbp   & 1.669Gbp  & 35.120Gbp   \\
%% \hline
%% \end{tabular}
%%   \end{center}
%% \end{frame}

\begin{frame}
  \frametitle{Results for D1}

  \vspace{4mm}
\renewcommand{\arraystretch}{1.3}
\begin{center}
\begin{tabular}{l|r|r|r|r|r}
\hline
 & \multicolumn{5}{c}{$\tau = 17$} \\
 \cline{2-6}
 & \multicolumn{2}{c|}{$k = 1$}     & \multicolumn{2}{c|}{$k = 2$} & $k = 3$ \\
 \cline{2-6}
No. of & Runtime & Relative & Runtime  & Relative & Runtime \\
Cores  & (sec)   & Speedup  & (sec)    & Speedup  & (sec) \\
\hline
128    & \innode{tx1}{-1.5ex}1607.69\innode{tx2}{0ex} & 1.00X    & \innode{ty1}{-1.5ex}14991.90\innode{ty2}{0ex} & 1.00X    & -- \\
256    & 826.71\innode{tx3}{0ex} & 1.94X    & 7952.84\innode{ty3}{0ex} & 1.88X    & -- \\
512    & 406.32\innode{tx4}{0ex} & 3.95X    & 4317.62\innode{ty4}{0ex} & 3.47X    & -- \\
1024   & 213.29\innode{tx5}{0ex} & 7.53X    & 2338.47\innode{ty5}{0ex} & 6.41X    & 29676.70 \\
\hline
\end{tabular}
\end{center}
\only<2>{
\begin{tikzpicture}[remember picture,overlay]
  \fill[gtBuzz] (tx1.north west) rectangle (tx5.south east);
  \fill[gtBuzz] (ty1.north west) rectangle (ty5.south east);
\end{tikzpicture}
}
\only<3>{
\begin{tikzpicture}[remember picture,overlay]
  \fill[gtBuzz] (tx1.north west) rectangle (tx4.south east);
  \fill[gtBuzz] (ty1.north west) rectangle (ty4.south east);
\end{tikzpicture}
}
\end{frame}
\begin{frame}
  \frametitle{D1 vs. D2}
  \vspace*{-1mm}
\begin{center}
  \includegraphics[width=\linewidth]{Rplot01.pdf}
\end{center}
\end{frame}
\begin{frame}
  \frametitle{Results for D3 (k = 1)}

  \vspace{2cm}
\renewcommand{\arraystretch}{1.5}
\begin{center}
\begin{tabular}{l|r|r}
\hline
 No. of & Time     & Relative \\
 Cores  & (sec)    & Speedup  \\
\hline
 512    & 26914.10 & 1.00X    \\
 1024   & 15031.40 & 1.79X    \\
\hline
\end{tabular}
\end{center}
\end{frame}
\begin{frame}
   \vspace{0.5cm}
  \begin{center}
    \begin{itemize}\itemsep=4ex
      \vspace*{0.1cm}
    \item Expected time complexity
      \[ O\left(\left(\frac{N}{p} \log N + \occ\right) \log^k N\right) \]
    \item Only other sub-quaratic sequential algorithm is by Thankachan {\it et. al.}, 2016
    \item Currently, exploring the use in Genome Mapping and Assembly.
    \end{itemize}
  \end{center}
 \frametitle{Conclusions}
\end{frame}

\end{document}
