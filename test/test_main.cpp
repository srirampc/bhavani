#include <mpi.h>

#include "gtest/gtest.h"


class MPITestPrinter : public  ::testing::EmptyTestEventListener {
private:
    int nsucc_parts, nparts;
    int ntest_fails, ncase_fails;
    int nproc,  rank;
    MPI_Comm _comm;
    bool case_fail;
    // Called before any test activity starts.
    virtual void OnTestProgramStart(const ::testing::UnitTest& unit_test) {
        _comm = MPI_COMM_WORLD;
        MPI_Comm_size(_comm, &nproc);
        MPI_Comm_rank(_comm, &rank);
        ntest_fails = 0;
        ncase_fails = 0;
        case_fail = false;
        if(rank != 0)
            return;
        std::cout << "==> Running "
                  << unit_test.test_to_run_count()
                  << " tests from "
                  << unit_test.test_case_to_run_count()
                  << " test cases. "
                  << "<==" << std::endl;
        std::cout.flush();
    }

    // Called after all test activities have ended.
    virtual void OnTestProgramEnd(const ::testing::UnitTest& unit_test) {
        // TODO : unit test passing based on the total no. of failures
        if(rank != 0)
            return;
        if(ntest_fails > 0)
            std::cout << "==> "
                      << ntest_fails
                      << " tests from "
                      << ncase_fails
                      << " test cases failed. "
                      << "<==" << std::endl;
        else
            std::cout << "==> ALL OK "
                      << "<==" << std::endl;
        std::cout.flush();
    }

    virtual void OnTestCaseStart(const ::testing::TestCase& t_case){
        MPI_Barrier(_comm);
        case_fail = false;
    }

    virtual void OnTestCaseEnd(const ::testing::TestCase& t_case) {
        MPI_Barrier(_comm);
        if(case_fail)
            ncase_fails += 1;
    }

    // Called before a test starts.
    virtual void OnTestStart(const  ::testing::TestInfo& test_info) {
        nparts = nsucc_parts = 0;
        if(rank == 0)
            std::cout << "[ RUN      ] "
                      << test_info.test_case_name() << "."
                      << test_info.name()
                      << std::endl;
        std::cout.flush();
        MPI_Barrier(_comm);
    }

    // Called after a failed assertion or a SUCCEED() invocation.
    virtual void OnTestPartResult(const ::testing::TestPartResult& test_part_result) {
        nparts++;
        if(test_part_result.failed())
            std::cout << "[     FAIL ] "
                      << " AT RANK "
                      << rank
                      << " FILE "
                      << test_part_result.file_name() << ":"
                      << test_part_result.line_number()
                      << std::endl
                      << test_part_result.summary()
                      << std::endl;
        else
            nsucc_parts++;
        std::cout.flush();
    }

    // Called after a test ends.
    virtual void OnTestEnd(const ::testing::TestInfo& test_info) {
        int lfail = (nparts == nsucc_parts) ? 0 : 1;
        int sum_fail = 0;
        // MPI_Reduce the total number of failures
        MPI_Reduce(&lfail, &sum_fail, 1, MPI_INT, MPI_SUM, 0, _comm);
        // update failure counts
        ntest_fails += (sum_fail > 0) ? 1 : 0;
        case_fail = (sum_fail > 0) ? true : false;
        if(rank == 0)
            std::cout << ((sum_fail == 0) ? "[       OK ] " :
                          "[     FAIL ] ")
                      << test_info.test_case_name() << "."
                      << test_info.name()
                      << std::endl;
        std::cout.flush();
        MPI_Barrier(_comm);
    }
};


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    MPI_Init(&argc, &argv);

    // Gets hold of the event listener list.
    ::testing::TestEventListeners& listeners =
      ::testing::UnitTest::GetInstance()->listeners();
    delete listeners.Release(listeners.default_result_printer());
    // Adds a listener to the end.  Google Test takes the ownership.
    listeners.Append(new MPITestPrinter);

    int rv = RUN_ALL_TESTS();

    MPI_Finalize();
    return rv;
}
