#include <vector>
#include <string>
#include <algorithm>
#include <mpi.h>
#include "gtest/gtest.h"
#include "parallel_utils.hpp"
#include "mpi_util.hpp"

class MPIUtilTest : public ::testing::Test {
protected:
    static int nproc, rank;
    static MPI_Comm _comm;

    static void SetUpTestCase() {
        _comm = MPI_COMM_WORLD;
        MPI_Comm_rank(_comm, &rank);
        MPI_Comm_size(_comm, &nproc);
    }

    static void TearDownTestCase() {
    }

    void load_balance_test(bool lStable = true);
    void load_compact_test(bool lStable = true);
    void oracle_test(bool lPerm = true);

};

int MPIUtilTest::nproc = 1;
int MPIUtilTest::rank = 0;
MPI_Comm MPIUtilTest::_comm = MPI_COMM_WORLD;

void MPIUtilTest::load_balance_test(bool lStable){
    // generate random
    int lcount = (rank % 2 == 0) ? 200 : 100,
        tcount = 0, lpfxsum = 0;
    MPI_Allreduce(&lcount, &tcount, 1, MPI_INT, MPI_SUM, _comm);
    int exp_count = block_size(rank, nproc, tcount);
    MPI_Scan(&lcount, &lpfxsum, 1, MPI_INT, MPI_SUM, _comm);
    std::vector<int> larray(lcount);
    for(int i = 0; i < lcount; i++)
        larray[i] = (lpfxsum - lcount) + i;
    if(lStable)
        load_balance_stable(larray, MPI_INT, _comm);
    else
        load_balance(larray, MPI_INT, _comm);
    ASSERT_EQ(exp_count, larray.size());
}

void MPIUtilTest::load_compact_test(bool lStable){
    // generate random
    int lcount = (rank > (nproc/2 - 1)) ? 200 : 0,
        tcount = 0, lpfxsum = 0;
    MPI_Allreduce(&lcount, &tcount, 1, MPI_INT, MPI_SUM, _comm);
    int exp_count = block_size(rank, nproc, tcount);
    MPI_Scan(&lcount, &lpfxsum, 1, MPI_INT, MPI_SUM, _comm);
    std::vector<int> larray;
    if(lcount > 0){
        larray.resize(lcount);
        for(int i = 0; i < lcount; i++)
            larray[i] = (lpfxsum - lcount) + i;
    }
    if(lStable)
        load_balance_stable(larray, MPI_INT, _comm);
    else
        load_balance(larray, MPI_INT, _comm);
    ASSERT_EQ(exp_count, larray.size());
}

TEST_F(MPIUtilTest, load_balance_stable){
    load_balance_test();
}

TEST_F(MPIUtilTest, load_compact_stable){
    load_compact_test();
}

TEST_F(MPIUtilTest, load_balance){
    load_balance_test(false);
}

TEST_F(MPIUtilTest, load_compact){
    load_compact_test(false);
}

class TestOracle:LocalOracle<int,int>{
public:
    const std::vector<int>& local_data;
    int dist_size;
    int rank, nproc, _comm;

    TestOracle(const std::vector<int>& dx,
               int gsz, int i, int p, int cx):
        local_data(dx), dist_size(gsz), rank(i), nproc(p), _comm(cx){
        //
    }

    void get(std::vector<int>& queries, std::vector<int>& results) const{
        results.resize(queries.size());
        int mbegin = block_low(rank, nproc, dist_size);
        for(int i = 0; i < (int)queries.size(); i++){
            assert(mbegin <= queries[i]);
            assert((queries[i] - mbegin) < (int)local_data.size());
            results[i] = local_data[queries[i] - mbegin];
        }
    }

    inline int size() const{
        return (int)local_data.size();
    }

    inline int global_size() const{
        return dist_size;
    }

    // the owning process
    inline int owner(const int& x) const{
        return block_owner(x, dist_size, nproc);
    }

    inline int comm_size() const{
        return nproc;
    }

    inline int comm_rank() const{
        return rank;
    }

    MPI_Comm comm() const{
        return _comm;
    }

};

void MPIUtilTest::oracle_test(bool lPerm){
    int lsize = 200, tsize = lsize * nproc;

    std::vector<int> queries(nproc * 2), data(lsize), results,
        expt_results(nproc * 2);

    // prepare data
    int mbegin = block_low(rank, nproc, tsize);
    for(unsigned i = 0; i < data.size();i++)
        data[i] = mbegin + 100 + i;
    TestOracle tcl(data, tsize, rank, nproc, _comm);

    // prepare queries
    int j = rank;
    for(unsigned i = 0; i < queries.size(); i++){
        j = (j + 1) == nproc ? 0 : (j + 1);
        int nbegin = block_low(j, nproc, tsize);
        queries[i] = nbegin + j;
        expt_results[i] = queries[i] + 100;
    }

    // get results
    if(!lPerm)
        get_query_results(tcl, queries, results);
    else
        get_query_results_perm(tcl, queries, results);
    ASSERT_EQ(results, expt_results);
}

TEST_F(MPIUtilTest, default_queries_test){
    oracle_test(false);
}

TEST_F(MPIUtilTest, perm_queries_test){
    oracle_test();
}
