#include <vector>
#include <string>
#include <algorithm>
#include <mpi.h>
#include <prettyprint.hpp>
#include "run_stats.hpp"
#include "gtest/gtest.h"
#include "util.hpp"
#include "suffix_ds.hpp"
#include "dist_query.hpp"
#include "trie_ds.hpp"
#include "mismatch_sfx.hpp"
#include "gen_pairs.hpp"


#include <sdsl/suffix_trees.hpp>
// typedef sdsl::rmq_support_sparse_table<
//     std::vector<unsigned>, true > rmq_table_t;

#include "psac_rmq.hpp"
typedef psac_rmq<unsigned, int> rmq_table_t;

typedef SuffixDS<char, int, unsigned, rmq_table_t> suffix_ds_t;
typedef RegionSuffixDS<char, int, unsigned> region_ds_t;
typedef BaseRegionSuffixDS<suffix_ds_t> base_region_ds_t;
typedef RootInternalNodeDS<suffix_ds_t, suffix_ds_t> root_nodes_t;
typedef RootInternalNodeDS<suffix_ds_t, base_region_ds_t> bs_root_nodes_t;
typedef RootInternalNodeDS<suffix_ds_t, region_ds_t> rg_root_nodes_t;
typedef SuffixTrieDS<suffix_ds_t> suffix_trie_t;
typedef TrieInternalNodeDS<suffix_ds_t> trie_nodes_t;

void read_fasta(const std::string& fname,
                std::vector<std::string>& all_headers,
                std::vector<std::string>& all_reads) {
    std::ifstream fs(fname);
    std::string line = "";
    std::string read = "";
    std::string header = "";

    while(getline(fs, line)) {
        if(line[0] == '>') {
            if(read.length() > 0){
                all_reads.push_back(read);
                all_headers.push_back(header);
            }
            read = "";
            line = line.substr(1);
            header = trim(line);
        } else if(is_valid_alpha(line[0])) {
            line  = trim(line);
            read += line;
        }
    }
    if(read.length() > 0){
        all_reads.push_back(read);
        all_headers.push_back(header);
    }
}

void make_reads_const(std::vector<std::string>& all_reads,
                      unsigned min_val = 0){
    unsigned rlen = 0;
    if(all_reads.size() > 0)
        rlen = all_reads[0].length();
    for(auto sx : all_reads)
        if(sx.length() < rlen)
            rlen = sx.length();
    if(min_val > 0)
        rlen = std::min(min_val, rlen);

    for(unsigned i = 0; i < all_reads.size();i++)
        if(all_reads[i].length() > rlen)
            all_reads[i] = all_reads[i].substr(0, rlen);
}

struct pcfg_t {
    unsigned K;
    int tau;
    unsigned read_length;
    uint64_t batch_width;
    std::ostream& ofs;
    pcfg_t():ofs(std::cout){}
};


class TestDS: public AbstractSuffixDS< std::vector<char>::const_iterator,
                                       std::vector<int>::const_iterator,
                                       std::vector<unsigned>::const_iterator > {
public:
    typedef sdsl::rmq_support_sparse_table< std::vector<unsigned>,
                                            true > drmq_t;
    std::vector<dchar_t> m_txt, m_prt;
    std::vector<dsize_t> m_sa, m_isa;
    std::vector<dcount_t> m_lcp;
    typedef std::vector<dchar_t>::const_iterator txt_iter_t;
    typedef std::vector<dsize_t>::const_iterator sa_iter_t;
    typedef std::vector<dsize_t>::const_iterator isa_iter_t;
    typedef std::vector<dcount_t>::const_iterator lcp_iter_t;
    drmq_t m_rmqt;
    int rank, nproc;
    MPI_Comm _comm;

    void print(std::ostream& ots) const{
        assert(size() == (int)m_sa.size());
        assert(size() == (int)m_lcp.size());
        assert(size() == (int)m_txt.size());
        if(rank == 0)
            for(int i = 0; i < size(); i++){
                ots << std::setw(6) << i
                    << std::setw(6) << m_txt[i]
                    << std::setw(6) << ((i < m_prt.size()) ? m_prt[i] : '$')
                    << std::setw(6) << ((i < m_isa.size()) ? m_isa[i] : 0)
                    << std::setw(6) << m_sa[i]
                    << std::setw(6) << m_lcp[i] << std::endl;
            }
        MPI_Barrier(comm());
    }

    virtual inline dsize_t size() const{
        return m_sa.size();
    }

    virtual inline dsize_t global_size() const{
        return m_sa.size();
    }

    virtual inline sa_iter_t sa() const{
        return m_sa.cbegin();
    }
    virtual inline lcp_iter_t lcp() const{
        return  m_lcp.cbegin();
    }
    virtual inline isa_iter_t isa() const{
        return m_isa.cbegin();
    }
    virtual inline txt_iter_t prt() const{
        return m_prt.cbegin();
    }
    virtual inline txt_iter_t sa_precede() const{
        return m_prt.cbegin();
    }
    virtual inline const std::vector<dsize_t>& sa_vec() const{
        return m_sa;
    }
    virtual inline const std::vector<dcount_t>& lcp_vec() const{
        return  m_lcp;
    }
    virtual inline const std::vector<dsize_t>& isa_vec() const{
        return m_isa;
    }
    virtual inline const std::vector<dchar_t>& txt_vec() const {
        return m_txt;
    };
    virtual inline const drmq_t& rmq_table() const {
        return m_rmqt;
    };
    inline const std::vector<dchar_t>& prt_vec() const{
        return m_prt;
    }
    inline unsigned rmq(int i, int j) const{
        assert(j < size());
        assert(i <= j);
        assert(i >= 0);
        // std::cout << "X" << i << " " << j << " " << m_rmqt(i, j) << std::endl;
        return m_lcp[m_rmqt(i, j)];
    }
    virtual inline MPI_Comm comm() const{
        return _comm;
    }
    virtual inline int comm_rank() const{
        return rank;
    }
    virtual inline int comm_size() const{
        return nproc;
    }

    // the owning process
    inline int owner(dsize_t x) const{
        return block_owner(x, global_size(), comm_size());
    }

    inline dcount_t proc_rmq(int px, int py) const{
        assert(comm_size() == 1);
        return 0;
    }

    void init(int r, int p, MPI_Comm cx){
        _comm = cx;
        rank = r;
        nproc = p;
    }

};


class SuffixDSTest : public ::testing::Test {

protected:
    // functions being over-ridden
    static void SetUpTestCase() {
        _comm = MPI_COMM_WORLD;
        MPI_Comm_rank(_comm, &rank);
        MPI_Comm_size(_comm, &nproc);
        init_txt();
        init_gst();
    }

    static void TearDownTestCase() {
    }

    virtual void SetUp(){
        init_sfx_ds();
    }

    static TestDS ban_ds, mis_ds, test_ds, rev_test_ds;
    static std::string test_txt, ban_txt, mis_txt, rev_test_txt;
    static std::vector<int> test_sa, rev_test_sa;
    static std::vector<unsigned> test_lcp, rev_test_lcp;
    static suffix_ds_t sfx_ds, rev_sfx_ds;

    static void init_txt();
    static void init_gst();
    static void init_sa_txt(std::string& in_txt, TestDS& in_ds);
    static void init_sfx_ds(std::string& in_txt,
                            std::vector<int>& in_sa,
                            std::vector<unsigned>& in_lcp,
                            suffix_ds_t& out_ds);
    static void init_sfx_ds();
    static void init_sa();

    template<typename CharPtrItr>
    static void init_sa_lcp_ds(std::string& in_txt,
                               CharPtrItr in_reads_itr,
                               //const char *in_reads[],
                               int nreads, int rlen,
                               std::vector<int>& in_sa,
                               std::vector<unsigned>& in_lcp,
                               TestDS& in_ds);
    static void concat_sep(std::vector<std::string>& all_reads,
                           std::string& out_str);

    void test_internal_nodes();
    void test_init();
    void test_range_min();
    void test_trie_lcp();
    void add_internal_nodes(root_nodes_t& rdin, suffix_trie_t& rtrie,
                            std::vector<int>& in_ext,
                            std::vector<unsigned>& exp_rslt);
    void test_boundary_nodes();
    void test_remote_regions();
    void test_left_shift_regions();
    void test_base_regions();
    void ext_suffixes(TestDS& tds,
                      RootInternalNodeDS<TestDS, TestDS>& inds,
                      std::vector<int>& exp_leaves);
    void extend_sfxs_trie();
    void test_pair_gen();
    void test_large_str();
    void run_naive(std::string& sx, std::string& sy,
                   std::vector<unsigned>& px, std::vector<int>& lx,
                   int tau, int kval, bool lbegin = false,
                   bool lend = false);
    void test_right(std::vector<std::string>& reads,
                    pcfg_t& param,
                    suffix_ds_t& sfx_lgp_ds,
                    suffix_ds_t& rev_sfx_lgp_ds,
                    root_nodes_t& rnodes);
    void test_left(std::vector<std::string>& reads,
                   pcfg_t& param,
                   suffix_ds_t& sfx_lgp_ds,
                   suffix_ds_t& rev_sfx_lgp_ds,
                   root_nodes_t& rnodes);
    void test_full(std::vector<std::string>& reads,
                   pcfg_t& param,
                   suffix_ds_t& sfx_lgp_ds,
                   suffix_ds_t& rev_sfx_lgp_ds,
                   bs_root_nodes_t& rnodes);
    void test_pairs();

public:
    static const int   nt_reads;
    static const int   reads_len;
    static const char *reads_sep;
    static const char *test_reads[];
    static const char *rev_test_reads[];
    static int nproc, rank;
    static MPI_Comm _comm;

};


// SuffixDSTest static variables
const int   SuffixDSTest::nt_reads = 6;
const int   SuffixDSTest::reads_len = 30;
const char *SuffixDSTest::reads_sep = "$";
const char *SuffixDSTest::test_reads[nt_reads] = {
    "CTCAACTCCACCTTCTTCCCCGAGGGACCC",
    "ACCACCTTCTTCCCCGACCCCATTGACCTG",
    "CTTCCCCGACCCCAGCCGGAGATTTGAAGT",
    "GGGTCCCTCGGGGAAGAAGGTGGAGTTGAG",
    "CAGGTCAATGGGGTCGGGGAAGAAGGTGGT",
    "ACTTCAAATCTCCGGCTGGGGTCGGGGAAG"
};

const char *SuffixDSTest::rev_test_reads[nt_reads] = {
    "GAAGGGGCTGGGGTCGGCCTCTAAACTTCA",
    "TGGTGGAAGAAGGGGCTGGGGTAACTGGAC",
    "GAGTTGAGGTGGAAGAAGGGGCTCCCTGGG",
    "TGAAGTTTAGAGGCCGACCCCAGCCCCTTC",
    "GTCCAGTTACCCCAGCCCCTTCTTCCACCA",
    "CCCAGGGAGCCCCTTCTTCCACCTCAACTC"
};



int SuffixDSTest::nproc = 1;
int SuffixDSTest::rank = 0;
MPI_Comm SuffixDSTest::_comm = MPI_COMM_WORLD;
std::string SuffixDSTest::test_txt;
std::string SuffixDSTest::rev_test_txt;
std::string SuffixDSTest::ban_txt;
std::string SuffixDSTest::mis_txt;
std::vector<int> SuffixDSTest::test_sa;
std::vector<unsigned> SuffixDSTest::test_lcp;
std::vector<int> SuffixDSTest::rev_test_sa;
std::vector<unsigned> SuffixDSTest::rev_test_lcp;
TestDS SuffixDSTest::ban_ds;
TestDS SuffixDSTest::mis_ds;
TestDS SuffixDSTest::test_ds;
TestDS SuffixDSTest::rev_test_ds;
suffix_ds_t SuffixDSTest::sfx_ds;
suffix_ds_t SuffixDSTest::rev_sfx_ds;

void SuffixDSTest::concat_sep(std::vector<std::string>& all_reads,
                              std::string& out_read){
    for(std::string& sx : all_reads) {
        out_read += sx;
        out_read += "$";
    }
}


template<typename CharPtrItr>
void SuffixDSTest::init_sa_lcp_ds(std::string& in_txt,
                                  CharPtrItr in_reads_itr,
                                  // const char *in_reads[],
                                  int nreads, int rlen,
                                  std::vector<int>& in_sa,
                                  std::vector<unsigned>& in_lcp,
                                  TestDS& in_ds){
    sdsl::cst_sct3<> gst;
    construct_im(gst, in_txt.c_str() , 1);
    in_sa.resize(in_txt.length());
    in_lcp.resize(in_txt.length());
    for(int k = 1u;k < (int)gst.lcp.size(); k++){
        int px = gst.csa[k];
        int lx = gst.lcp[k];
        int rmn = rlen - (px % (rlen + 1));
        rmn = (lx > rmn ? rmn : lx);
        // if(rank == 0)
        //     std::cout << (k - 1) << " " << in_txt[px] << " "
        //               << px << " " << rmn << std::endl;
        in_lcp[k - 1] = rmn;
        in_sa[k - 1] = px;
    }

    // Init the in_ds
    in_ds.m_txt.resize((nreads * rlen) + 1);
    for(int i = 0; i < nreads; i++)
        for(int j = 0; j < rlen; j++)
            in_ds.m_txt[i * rlen + j] = in_reads_itr[i][j];
    in_ds.m_sa.resize(in_ds.m_txt.size());
    in_ds.m_prt.resize(in_ds.m_sa.size());
    in_ds.m_lcp.resize(in_ds.m_sa.size());
    in_ds.m_txt[(nreads * rlen)] = '$';
    in_ds.m_sa[0] = (nreads * rlen);
    in_ds.m_lcp[0] = 0;
    in_ds.m_prt[0] = in_ds.m_txt[in_ds.m_sa[0] - 1];
    for(int k = nreads, j = 1; k < (int) in_sa.size(); k++, j++){
        in_ds.m_sa[j] = in_sa[k] - (in_sa[k] / (rlen + 1));
        in_ds.m_lcp[j] = in_lcp[k];
        in_ds.m_prt[j] = in_sa[k] > 0 ? in_txt[in_sa[k] - 1] : '$';
    }
    in_ds.m_isa.resize(in_ds.m_sa.size());
    for(int k = 0; k < (int) in_ds.m_sa.size(); k++)
        in_ds.m_isa[in_ds.m_sa[k]] = k;
    TestDS::drmq_t tmp_rq(&in_ds.m_lcp);
    in_ds.m_rmqt = tmp_rq;
    in_ds.init(rank, nproc, _comm);
}

void SuffixDSTest::init_gst(){
    init_sa_lcp_ds(test_txt, test_reads, nt_reads, reads_len,
                   test_sa, test_lcp, test_ds);
    init_sa_lcp_ds(rev_test_txt, rev_test_reads, nt_reads, reads_len,
                   rev_test_sa, rev_test_lcp, rev_test_ds);
}

void SuffixDSTest::init_txt(){
    test_txt = std::string("");
    for(int i = 0; i < nt_reads; i++){
        test_txt += test_reads[i];
        test_txt += reads_sep;
    }

    rev_test_txt = std::string();
    for(int i = 0; i < nt_reads; i++){
        rev_test_txt += rev_test_reads[i];
        rev_test_txt += reads_sep;
    }
    // for banana
    ban_txt = "BANANA$$";
    ban_ds.init(rank, nproc, _comm);
    ban_ds.m_txt.reserve(ban_txt.length());
    std::copy(ban_txt.begin(), ban_txt.end(),
              std::back_inserter(ban_ds.m_txt));
    ban_ds.m_prt.resize(ban_txt.length(), '$');
    // for missippi
    mis_txt = "MISSISSIPPI$$";
    mis_ds.init(rank, nproc, _comm);
    mis_ds.m_txt.reserve(mis_txt.length());
    std::copy(mis_txt.begin(), mis_txt.end(),
              std::back_inserter(mis_ds.m_txt));
    mis_ds.m_prt.resize(mis_txt.length(), '$');
}


void SuffixDSTest::init_sa_txt(std::string& in_txt, TestDS& in_ds){
    // construct SA, LCP using the sdsl-lite library
    sdsl::cst_sct3<> in_st;
    construct_im(in_st, in_txt.c_str(), 1);
    in_ds.m_sa.resize(in_txt.length());
    in_ds.m_lcp.resize(in_txt.length());
    for(int k = 1u; k < (int)in_st.lcp.size(); k++){
        // std::cout << (k - 1) << " "
        //           << in_ds.m_txt[k - 1] << " "
        //           << (int)in_st.csa[k] << " "
        //           << (int)in_st.lcp[k] << std::endl;
        in_ds.m_sa[k - 1] = (int)in_st.csa[k];
        in_ds.m_lcp[k - 1] = (unsigned)in_st.lcp[k];
    }
}

void SuffixDSTest::init_sfx_ds(std::string& in_txt,
                               std::vector<int>& in_sa,
                               std::vector<unsigned>& in_lcp,
                               suffix_ds_t& out_ds){
    std::vector<char> in_txtc;
    in_txtc.reserve(in_txt.length());
    std::copy(in_txt.begin(), in_txt.end(),
              std::back_inserter(in_txtc));
    int gsize = in_txt.length();
    std::vector<int> bp_size = block_partition(gsize, nproc),
        bp_ptr =  get_displacements(bp_size);
    suffix_ds_t tmp_ds(in_txtc.begin() + bp_ptr[rank],
                       in_sa.begin() + bp_ptr[rank],
                       in_lcp.begin() + bp_ptr[rank],
                       gsize, MPI_COMM_WORLD);
    out_ds.swap(tmp_ds);
    // out_ds = tmp_ds;
}

void SuffixDSTest::init_sfx_ds(){
    init_sfx_ds(test_txt, test_sa, test_lcp, sfx_ds);
    init_sfx_ds(rev_test_txt, rev_test_sa,
                rev_test_lcp, rev_sfx_ds);
}

void SuffixDSTest::init_sa(){
    init_sa_txt(ban_txt, ban_ds);
    init_sa_txt(mis_txt, mis_ds);
}

void SuffixDSTest::ext_suffixes(TestDS& tds,
                                RootInternalNodeDS<TestDS, TestDS>& inds,
                                std::vector<int>& exp_leaves){
    int leafcts = std::accumulate(inds.width(),
                                  inds.width() + inds.size(), 0);
    exp_leaves.resize(leafcts);
    for(int i = 0, k = 0; i < inds.size(); i++){
        auto left_end = inds.left()[i];
        for(int j = 0; j < inds.width()[i]; j++){
            exp_leaves[k] = (tds.sa()[left_end + j] + inds.depth()[i] + 1);
            exp_leaves[k] = (exp_leaves[k] < tds.size())
                ?  exp_leaves[k] : (tds.size());
            k++;
        }
    }
}

void SuffixDSTest::test_internal_nodes(){
    std::vector<int> node_width, node_left, exp_leaves;
    std::vector<int> lbounds, rbounds, lsa, rsa;
    std::vector<unsigned> node_depth;

    int nodects = 5;

    RootInternalNodeDS<TestDS, TestDS> ban_inds(ban_ds, ban_ds, 0, false);
    //ban_inds.print(std::cout);
    InternalNodeDS<TestDS>::ansv_bounds(ban_ds, lbounds, rbounds, lsa, rsa);
    std::cout << ban_ds.lcp_vec() << std::endl;
    std::cout << lbounds << std::endl;
    std::cout << rbounds << std::endl;
    std::cout << ban_inds.size() << std::endl;
    std::cout << ban_inds.left_vec() << std::endl;
    std::cout << ban_inds.width_vec() << std::endl;
    std::cout << ban_inds.depth_vec() << std::endl;

    node_left =  {0, 0, 2, 3, 6};
    node_width = {8, 2, 3, 2, 2};
    node_depth = {0, 1, 1, 3, 2};
    ASSERT_EQ(ban_inds.size(), nodects);
    ASSERT_EQ(ban_inds.left_vec(), node_left);
    ASSERT_EQ(ban_inds.width_vec(), node_width);
    ASSERT_EQ(ban_inds.depth_vec(),  node_depth);

    std::vector<int> parent_nodes(node_width.size());
    InternalNodeDS<TestDS>::max_ansv(node_width.begin(), node_width.end(),
                                     parent_nodes.begin());
    std::cout << parent_nodes << std::endl;

    ext_suffixes(ban_ds, ban_inds, exp_leaves);
    //DefaultBoundsChecker<int, int> read_bounds(6);
    SuffixTrieDS<TestDS> ban_trie(ban_ds, ban_inds);
    ASSERT_EQ(exp_leaves, ban_trie.sa_vec());

    nodects = 8;
    node_left =  { 0, 0, 2, 4, 7, 9, 9, 11};
    node_width = {13, 2, 4, 2, 2, 4, 2,  2};
    node_depth = { 0, 1, 1, 4, 1, 1, 2,  3};

    RootInternalNodeDS<TestDS, TestDS> mis_inds(mis_ds, mis_ds, 0, false);
    //mis_inds.print(std::cout);

    ASSERT_EQ(mis_inds.size(), nodects);
    ASSERT_EQ(mis_inds.left_vec(), node_left);
    ASSERT_EQ(mis_inds.width_vec(), node_width);
    ASSERT_EQ(mis_inds.depth_vec(),  node_depth);

    parent_nodes.resize(node_width.size());
    InternalNodeDS<TestDS>::max_ansv(node_width.begin(), node_width.end(),
                                     parent_nodes.begin());
    std::cout << node_left << std::endl;
    std::cout << node_width << std::endl;
    std::cout << parent_nodes << std::endl;
    parent_nodes.clear();
    mis_inds.trie_parents(parent_nodes);
    std::cout << parent_nodes << std::endl;
    return;

    exp_leaves.clear();
    ext_suffixes(mis_ds, mis_inds, exp_leaves);
    SuffixTrieDS<TestDS> mis_trie(mis_ds, mis_inds);
    ASSERT_EQ(exp_leaves, mis_trie.sa_vec());

    std::vector<int> batch_ptr;
    root_schedule(mis_inds, 5, batch_ptr);
    // std::cout << batch_ptr << std::endl;
}


void SuffixDSTest::test_init(){
    // sfx_ds.print(std::cout);
    // Check if the local and global size is good!
    ASSERT_EQ(sfx_ds.global_size(), test_txt.length() - nt_reads + 1);
    ASSERT_EQ(sfx_ds.size(),
              block_size(rank, nproc, sfx_ds.global_size()));
    std::vector<int> local_sac(sfx_ds.size()),
        local_isac(sfx_ds.size());
    std::vector<unsigned> local_lcpc(sfx_ds.size());
    std::vector<char> local_prtc(sfx_ds.size()),
        local_txtc(sfx_ds.size());
    // SA Compacted
    std::copy(test_ds.m_sa.begin() + sfx_ds.block_begin(),
              test_ds.m_sa.begin() + sfx_ds.block_begin() + sfx_ds.size(),
              local_sac.begin());
    ASSERT_EQ(sfx_ds.sa_vec(), local_sac);
    // LCP compacted
    std::copy(test_ds.m_lcp.begin() + sfx_ds.block_begin(),
              test_ds.m_lcp.begin() + sfx_ds.block_begin() + sfx_ds.size(),
              local_lcpc.begin());
    ASSERT_EQ(sfx_ds.lcp_vec(), local_lcpc);
    // ISA compacted
    std::copy(test_ds.m_isa.begin() + sfx_ds.block_begin(),
              test_ds.m_isa.begin() + sfx_ds.block_begin() + sfx_ds.size(),
              local_isac.begin());
    ASSERT_EQ(sfx_ds.isa_vec(), local_isac);
    // PRT
    std::copy(test_ds.m_prt.begin() + sfx_ds.block_begin(),
              test_ds.m_prt.begin() + sfx_ds.block_begin() + sfx_ds.size(),
              local_prtc.begin());
    // ASSERT_EQ(sfx_ds.prt_vec(), local_prtc);
    // txt
    std::copy(test_ds.m_txt.begin() + sfx_ds.block_begin(),
              test_ds.m_txt.begin() + sfx_ds.block_begin() + sfx_ds.size(),
              local_txtc.begin());
    ASSERT_EQ(sfx_ds.txt_vec(), local_txtc);
}


void SuffixDSTest::test_range_min(){
    std::vector<int> queries(1);
    std::vector<unsigned> results(1), exp_results(1);
    int qleft, qright;
    qleft = (sfx_ds.block_end() + 1) % sfx_ds.global_size();
    queries[0] = qleft;
    exp_results[0] = test_ds.lcp()[qleft];
    query_rmq_left(sfx_ds, queries, results);
    ASSERT_EQ(results,exp_results);

    qright = (rank > 0 ? sfx_ds.block_begin() : sfx_ds.global_size()) - 1;
    queries[0] = qright;
    exp_results[0] = test_ds.lcp()[qright];
    query_rmq_right(sfx_ds, queries, results);
    ASSERT_EQ(results,exp_results);

    qleft = (sfx_ds.block_end() + 2) % sfx_ds.global_size();
    queries[0] = qleft;
    exp_results[0] = test_ds.rmq(qleft - 1, qleft);
    query_rmq_left(sfx_ds, queries, results);
    ASSERT_EQ(results,exp_results);

    qright = (rank > 0 ? sfx_ds.block_begin() : sfx_ds.global_size()) - 2;
    queries[0] = qright;
    exp_results[0] = test_ds.rmq(qright, qright + 1);
    query_rmq_right(sfx_ds, queries, results);
    ASSERT_EQ(results,exp_results);

    qright = (sfx_ds.block_end() + 3) % sfx_ds.global_size();
    qleft = qright - 2;
    queries.resize(2);
    exp_results.resize(2);
    queries[0] = qleft;
    queries[1] = qright;
    exp_results[0] = 0;
    exp_results[1] = test_ds.rmq(qleft + 1, qright);
    query_rmq_pair(sfx_ds, queries, results);
    ASSERT_EQ(results,exp_results);

    qleft = (rank > 0 ? sfx_ds.block_begin() : sfx_ds.global_size()) - 3;
    qright = qleft + 2;
    queries[0] = qleft;
    queries[1] = qright;
    exp_results[0] = 0;
    exp_results[1] = test_ds.rmq(qleft + 1, qright);
    query_rmq_pair(sfx_ds, queries, results);
    ASSERT_EQ(results,exp_results);

    int qx = sfx_ds.block_end(),
        qy = (sfx_ds.block_end() + 1) % sfx_ds.global_size();
    unsigned rmx = test_ds.rmq(qx, qx),
        rmy = test_ds.rmq(qy, qy);
    if(qx <= qy){
        unsigned tx = rmq_span_min(sfx_ds, qx, qy, rmx, rmy);
        ASSERT_EQ(tx, test_ds.rmq(qx, qy));
    }

    qx = sfx_ds.block_begin() - ((rank  > 0) ?  1 : 0);
    qy = (sfx_ds.block_end() + 1) % sfx_ds.global_size();
    rmx = test_ds.rmq(qx, qx);
    rmy = test_ds.rmq(qy, qy);
    if(qx > 0 && qx <= qy){
        unsigned tx = rmq_span_min(sfx_ds, qx, qy, rmx, rmy);
        ASSERT_EQ(tx, test_ds.rmq(qx, qy));
    }
}

void SuffixDSTest::add_internal_nodes(root_nodes_t& rdin, suffix_trie_t& rtrie,
                                      std::vector<int>& in_ext,
                                      std::vector<unsigned>& exp_rslt){
    for(int k = 0, idx = 0; k < rdin.size(); k++){
        auto left_end = rdin.left()[k],
            right_end = rdin.left()[k] + rdin.width()[k] - 1;
        int ptgt = -1;
        for(int i = left_end; i <= right_end; i++){
            int tgt = test_ds.sa()[i] + rdin.depth()[k] + 1;
            tgt = tgt < test_ds.size() ? test_ds.isa()[tgt] : (test_ds.size());
            std::stringstream outs;
            outs << std::setw(6) << rank << std::setw(6) << k
                 << std::setw(6) << idx
                 << std::setw(6) << test_ds.sa()[i] << std::setw(6) << tgt
                 << std::setw(6) << ptgt
                 << std::setw(6)
                 << test_ds.txt_vec()[test_ds.sa()[i]]
                 << test_ds.txt_vec()[test_ds.sa()[i] + 1]
                 << test_ds.txt_vec()[test_ds.sa()[i] + 2]
                 << std::endl;
            // if(rank == 2)
            //     std::cout << outs.str();
            in_ext.push_back(tgt);
            if(i == left_end || tgt == test_ds.size())
                exp_rslt.push_back(0);
            else if(ptgt >= 0 && ptgt <= tgt && tgt < test_ds.size())
                exp_rslt.push_back(test_ds.rmq(ptgt == tgt ? tgt : (ptgt+1),
                                               tgt));
            else
                exp_rslt.push_back(0);
            ptgt = in_ext.back();
            idx++;
        }
    }
}

void SuffixDSTest::test_trie_lcp(){
    std::vector<unsigned> exp_rslt2, out_lcp2;
    std::vector<int> in_leaves2;
    root_nodes_t rdin(sfx_ds, sfx_ds, 0, true);
    suffix_trie_t rtrie(sfx_ds, rdin);
    // rtrie.print(std::cout);
    // std::vector<int> tmp{3};
    // std::vector<int> tmp2{89, 90, 65, 165};
    add_internal_nodes(rdin, rtrie, in_leaves2, exp_rslt2);
    // add_internal_nodes(rdin, rtrie, tmp2, exp_rslt2);
    // suffix_trie_lcp(sfx_ds, tmp, tmp2, out_lcp2);
    suffix_trie_lcp(sfx_ds, rdin.width(), rdin.size(), in_leaves2, out_lcp2);

    // if(rank == 4){
    //     std::cout << rank << " " << out_lcp2.size() << std::endl;
    //     for(int i = 0; i < out_lcp2.size();i++)
    //         if(exp_rslt2[i] != out_lcp2[i])
    //             std::cout << i << " " << exp_rslt2[i]
    //                       << " " << out_lcp2[i] << std::endl;
    // }
    //rdin.print(std::cout);
    ASSERT_EQ(out_lcp2, exp_rslt2);
    //return;
    rtrie.init_lcp();

    // rtrie.print(std::cout);
    std::vector<int> exp_isa(rtrie.size());
    for(int i = 0; i < rtrie.size(); i++){
        if(rtrie.sa()[i] < test_ds.size())
            exp_isa[i] = test_ds.isa()[rtrie.sa()[i]];
        else
            exp_isa[i] = rtrie.sa()[i];
    }
    ASSERT_EQ(rtrie.isa_vec(), exp_isa);

    std::vector<unsigned> exp_lcp(rtrie.size());
    for(int k = 0, i = 0; k < rtrie.parent_size(); k++){
        for(int j = 0; j < rtrie.parent_width()[k]; j++){
            if(j == 0)
                exp_lcp[i] = 0;
            else if(exp_isa[i] >= test_ds.size() || exp_isa[i-1] >= test_ds.size())
                exp_lcp[i] = 0;
            else
                exp_lcp[i] = test_ds.rmq(exp_isa[i - 1] == exp_isa[i] ?
                                         exp_isa[i] : (exp_isa[i - 1] + 1),
                                         exp_isa[i]);
            i++;
        }
    }
    ASSERT_EQ(rtrie.lcp_vec(), exp_lcp);

}


void SuffixDSTest::extend_sfxs_trie(){
    // only for one processor!
    const char *stest2_reads[2] = {"NTXATLAANTA", "NTYATLABNTA"};
    const char *rev_stest2_reads[2] = {"ATNBALTAYTN", "ATNAALTAXTN"};
    std::string stest2(""), rev_stest2("");
    for(int i = 0; i < 2; i++){
        stest2 += stest2_reads[i];
        rev_stest2 += rev_stest2_reads[i];
        stest2 += reads_sep;
        rev_stest2 += reads_sep;
    }
    std::vector<int> twx;
    std::vector<unsigned> twy;
    std::vector<int> stest2_sa, rev_stest2_sa;
    std::vector<unsigned> stest2_lcp, rev_stest2_lcp;
    TestDS stest2_ds, rev_stest2_ds;

    init_sa_lcp_ds(stest2, stest2_reads, 2, 11,
                   stest2_sa, stest2_lcp, stest2_ds);
    init_sa_lcp_ds(rev_stest2, rev_stest2_reads, 2, 11,
                   rev_stest2_sa, rev_stest2_lcp, rev_stest2_ds);
    suffix_ds_t sfx_stest2_ds, rev_sfx_stest2_ds;
    init_sfx_ds(stest2, stest2_sa, stest2_lcp, sfx_stest2_ds);
    init_sfx_ds(rev_stest2, rev_stest2_sa, rev_stest2_lcp, rev_sfx_stest2_ds);
    return;
    sfx_stest2_ds.print(std::cout);
    rev_sfx_stest2_ds.print(std::cout);
    DefaultBoundsChecker<int, int> read_bounds(11);
    base_region_ds_t brt(sfx_stest2_ds, 3);
    brt.print(std::cout);
    bs_root_nodes_t rdin(sfx_stest2_ds, brt, 3, false);
    rdin.print(std::cout);
    suffix_trie_t *rtrie = new suffix_trie_t(sfx_stest2_ds, rdin, read_bounds, true);
    rtrie->print(std::cout);
    trie_nodes_t *rdin_1m = new trie_nodes_t(sfx_stest2_ds, *rtrie);
    rdin_1m->print(std::cout);
    std::vector<int> rev_suf, suf;
    rdin_1m->suffixes(suf);
    rdin_1m->reverse_suffixes(rev_suf);
    std::cout << suf << std::endl;
    std::cout << rev_suf << std::endl;
    suffix_trie_t *rev_trie =
        new suffix_trie_t(rev_sfx_stest2_ds, *rdin_1m, read_bounds, false);
    rev_trie->print(std::cout);
    trie_nodes_t *rev_rdin_1m = new trie_nodes_t(rev_sfx_stest2_ds, *rev_trie);
    rev_rdin_1m->print(std::cout);
    ASSERT_EQ(rdin.size(), 3);
    twx = {2, 2, 2};
    ASSERT_EQ(rdin.width_vec(), twx);
    twy = {4, 3, 3};
    ASSERT_EQ(rdin.depth_vec(), twy);
    twy = {0, 0, 0};
    ASSERT_EQ(rdin.delta_vec(), twy);
    twx = {0, 3, 6};
    ASSERT_EQ(rdin.left_vec(), twx);

    ASSERT_EQ(rtrie->size(), 6);
    twx = {19, 8, 12, 23, 19, 8};
    ASSERT_EQ(rtrie->sa_vec(), twx);
    twx = {11, 12, 20, 23, 11, 12};
    ASSERT_EQ(rtrie->isa_vec(), twx);
    twy = {0, 3, 0, 0, 0, 3};
    ASSERT_EQ(rtrie->lcp_vec(), twy);

    ASSERT_EQ(rdin_1m->size(), 3);
    twx = {0, 2, 4};
    ASSERT_EQ(rdin_1m->left_vec(), twx);
    twx = {2, 2, 2};
    ASSERT_EQ(rdin_1m->width_vec(), twx);
    twy = {3, 0, 3};
    ASSERT_EQ(rdin_1m->depth_vec(), twy);
    twy = {5, 4, 4};
    ASSERT_EQ(rdin_1m->delta_vec(), twy);

    ASSERT_EQ(rev_trie->size(), 6);
    twx = {20, 9, 15, 23, 19, 8};
    ASSERT_EQ(rev_trie->sa_vec(), twx);
    twx = {17, 18, 2, 23, 21, 22};
    ASSERT_EQ(rev_trie->isa_vec(), twx);
    twy = {0, 2, 0, 0, 0, 0};
    ASSERT_EQ(rev_trie->lcp_vec(), twy);

    ASSERT_EQ(rev_rdin_1m->size(), 3);
    twx = {0, 2, 4};
    ASSERT_EQ(rev_rdin_1m->left_vec(), twx);
    twx = {2, 2, 2};
    ASSERT_EQ(rev_rdin_1m->width_vec(), twx);
    twy = {2, 0, 0};
    ASSERT_EQ(rev_rdin_1m->depth_vec(), twy);
    twy = {9, 5, 8};
    ASSERT_EQ(rev_rdin_1m->delta_vec(), twy);
}

void SuffixDSTest::test_boundary_nodes(){
    //
    root_nodes_t rdin(sfx_ds, sfx_ds, 3, true);
    std::vector<int> lbounds2(rdin.size()), exp_lbounds2(rdin.size()),
        rbounds2(rdin.size()), exp_rbounds2(rdin.size());
    // if(rank == 0)
    //     rdin.print(std::cout);
    for(int i = 0; i < rdin.size(); i++){
        lbounds2[i] = rdin.left()[i];
        rbounds2[i] = lbounds2[i] + rdin.width()[i] - 1;
        exp_lbounds2[i] = 0;
        exp_rbounds2[i] = sfx_ds.size() - 1;
    }
    ASSERT_NE(exp_lbounds2, lbounds2);
    ASSERT_NE(exp_rbounds2, rbounds2);
}

void SuffixDSTest::test_left_shift_regions(){
    region_ds_t rmt_ds(sfx_ds.global_size(),
                       sfx_ds.comm_rank(),
                       sfx_ds.comm_size(),
                       sfx_ds.comm());
    region_ds_t::left_shift_boundary(sfx_ds, 3, rmt_ds);
    // std::cout << rmt_ds.size() << std::endl;
    // rmt_ds.print(std::cout);
    int left_idx, right_idx;
    std::vector<char> exp_prt;
    std::vector<int> exp_sa;
    std::vector<unsigned> exp_lcp;
    left_idx = sfx_ds.block_end();
    right_idx = sfx_ds.block_end() + 1;
    while(test_ds.lcp()[left_idx] >= 3)
        left_idx--;
    while(right_idx < test_ds.size() && test_ds.lcp()[right_idx] >= 3)
        right_idx++;

    if(right_idx > left_idx + 1)
        for(int i = left_idx; i < right_idx; i++){
            exp_prt.push_back(test_ds.prt_vec()[i]);
            exp_sa.push_back(test_ds.sa()[i]);
            exp_lcp.push_back(test_ds.lcp()[i]);
        }

    ASSERT_EQ(rmt_ds.sa_vec(), exp_sa);
    // ASSERT_EQ(rmt_ds.prt_vec(), exp_prt);
    ASSERT_EQ(rmt_ds.lcp_vec(), exp_lcp);
}

void SuffixDSTest::test_base_regions(){
    sfx_ds.print(std::cout);
    base_region_ds_t brt(sfx_ds, 3);
    brt.print_summary(std::cout);
    brt.print(std::cout);
    bs_root_nodes_t rdin(sfx_ds, brt, 3, false);
}

void SuffixDSTest::test_pair_gen(){
    DefaultBoundsChecker<unsigned, int> rcheck(reads_len);

    root_nodes_t rnodes(sfx_ds, sfx_ds, 3, false);
    std::vector<suffix_trie_t*> tries(2);
    std::vector<trie_nodes_t*> trie_nodes(2);
    tries[0] = new suffix_trie_t(sfx_ds, rnodes, rcheck, true);
    trie_nodes[0] = new trie_nodes_t(sfx_ds, *(tries[0]));
    tries[1] = new suffix_trie_t(sfx_ds, *(trie_nodes[0]), rcheck, true);
    trie_nodes[1] = new trie_nodes_t(sfx_ds, *(tries[1]));

    CSPairGenerator<suffix_ds_t, root_nodes_t> rp_gen(sfx_ds, reads_len);
    rp_gen.generate(rnodes, std::cout);
    // TODO
    tries[0]->print_summary("", std::cout);
    trie_nodes[0]->print_summary(std::cout);

    CSPairGenerator<suffix_ds_t, trie_nodes_t> tp_gen(sfx_ds, reads_len);
    tp_gen.generate(*trie_nodes[0], std::cout);
    tries[1]->print_summary("", std::cout);
    trie_nodes[1]->print_summary(std::cout);

    // CSPairGenerator<suffix_ds_t, trie_nodes_t> pgen(sfx_ds, *trie_nodes[1],
    //                                                reads_len);
    // pgen.generate();

    std::vector<suffix_ds_t::dsize_t> suf, rev_suf;
    rnodes.suffixes(suf);
    rnodes.reverse_suffixes(rev_suf);

    std::cout << suf << std::endl;
    std::cout << rev_suf << std::endl;
    std::vector<suffix_trie_t*> rev_tries(2);
    std::vector<trie_nodes_t*> rev_trie_nodes(2);
    rev_tries[0] = new suffix_trie_t(rev_sfx_ds, rnodes, rcheck, false);
    rev_trie_nodes[0] = new trie_nodes_t(rev_sfx_ds, *(rev_tries[0]));
    rev_tries[0]->print(std::cout);
    rev_trie_nodes[0]->print(std::cout);

    std::cout << rev_trie_nodes[0]->sa_global_size() << std::endl;

    CSPairGenerator<suffix_ds_t, trie_nodes_t> rpgen(sfx_ds, reads_len,false);
    rpgen.generate(*(rev_trie_nodes[0]), std::cout);
    rev_tries[1] = new suffix_trie_t(rev_sfx_ds, *(rev_trie_nodes[0]), rcheck, true);
    rev_tries[1]->print(std::cout);
    rev_trie_nodes[1] = new trie_nodes_t(rev_sfx_ds, *(rev_tries[1]));
    rev_trie_nodes[1]->print(std::cout);
    std::cout << rev_trie_nodes[1]->sa_global_size() << std::endl;
    CSPairGenerator<suffix_ds_t, trie_nodes_t> rpgen2(sfx_ds, reads_len, false);
    rpgen2.generate(*rev_trie_nodes[1], std::cout);

    delete tries[0];
    delete tries[1];
    delete trie_nodes[0];
    delete trie_nodes[1];
}

void SuffixDSTest::run_naive(std::string& sx, std::string& sy,
                             std::vector<unsigned>& px, std::vector<int>& lx,
                             int tau, int kval, bool lbegin, bool lend) {
    px.resize(sx.size());
    lx.resize(sx.size());
    for(unsigned xstart = 0; xstart < sx.size(); xstart++) {
        px[xstart] = sy.size();
        for(unsigned ystart = 0; ystart < sy.size(); ystart++){
            // match k characters with at least three matches
            unsigned howfar = 0, kdx = 0, mrun = 0, crun = 0;
            if(lbegin) { // match at the beginning
                while((ystart + howfar < sy.size()) &&
                      (xstart + howfar < sx.size())){
                    if(sx[xstart + howfar] != sy[ystart + howfar]){
                        break;
                    }
                    mrun++;
                    howfar++;
                }
                if(mrun < (unsigned)tau)
                    continue;
            }
            while((ystart + howfar < sy.size()) &&
                  (xstart + howfar < sx.size())){
                if(sx[xstart + howfar] != sy[ystart + howfar]){
                    kdx += 1;
                }

                if(kval < (int)kdx)
                    break;

                if(sx[xstart + howfar] != sy[ystart + howfar]){
                    if(crun > mrun)
                        mrun = crun;
                    crun = 0;
                } else {
                    crun += 1;
                }
                howfar++;
            }
            if(lend && crun < (unsigned)tau)
                continue;
            if(crun > mrun)
                mrun = crun;
            if(mrun >= (unsigned)tau && lx[xstart] < (int) howfar){
                px[xstart] = ystart;
                lx[xstart] = (int) howfar;
            }
        }
    }
}

void SuffixDSTest::test_right(std::vector<std::string>& reads,
                              pcfg_t& param,
                              suffix_ds_t& sfx_lgp_ds,
                              suffix_ds_t& rev_sfx_lgp_ds,
                              root_nodes_t& rnodes){
    //  run a exhaustive algorithm for first two reads
    std::string& sx = reads[0]; std::string& sy = reads[1];
    std::vector<unsigned> pos;
    std::vector<int> length;

    run_naive(sx, sy, pos, length, param.tau, param.K, true);
    std::vector< std::pair<unsigned, unsigned> > pos_pairs;
    for(unsigned i = 0; i < length.size();i++){
        auto v = pos[i];
        if(length[i] > 0)
            pos_pairs.push_back(std::make_pair(i, v));
    }
    std::cout << pos_pairs.size() << " " << pos_pairs << std::endl;

    RunStats rstats(MPI_COMM_WORLD);
    MismatchSuffixTries< true, true, pcfg_t, RunStats,
                         suffix_ds_t, root_nodes_t > mst(sfx_lgp_ds,
                                                         rev_sfx_lgp_ds,
                                                         rnodes,
                                                         param, rstats);
    DefaultPairGenerator<suffix_ds_t, trie_nodes_t> dpgen(sfx_lgp_ds,
                                                          param.read_length,
                                                          true, 0);
    mst.extend_right(dpgen);
    // mst.trie_sa[0]->print_summary(std::cout);
    // mst.trie_nodes[0]->print_summary(std::cout);
    // mst.trie_sa[1]->print_summary(std::cout);
    // mst.trie_nodes[1]->print_summary(std::cout);
    CSPairGenerator<suffix_ds_t, trie_nodes_t> rpgen(sfx_lgp_ds,
                                                     param.read_length,
                                                     true);
    rpgen.generate(*mst.trie_nodes[1], std::cout);
}

void SuffixDSTest::test_left(std::vector<std::string>& reads,
                             pcfg_t& param,
                             suffix_ds_t& sfx_lgp_ds,
                             suffix_ds_t& rev_sfx_lgp_ds,
                             root_nodes_t& rnodes){
    std::string& sx = reads[0]; std::string& sy = reads[1];
    std::vector<unsigned> pos;
    std::vector<int> length;

    run_naive(sx, sy, pos, length, param.tau, param.K, false, true);
    std::vector< std::pair<unsigned, unsigned> > pos_pairs;
    for(unsigned i = 0; i < length.size();i++){
        auto v = pos[i];
        if(length[i] > 0)
            pos_pairs.push_back(std::make_pair(i, v));
    }
    std::cout << pos_pairs.size() << " " << pos_pairs << std::endl;
    std::cout << sx.substr(pos_pairs[0].first, length[pos_pairs[0].first]) << std::endl;
    std::cout << sy.substr(pos_pairs[0].second, length[pos_pairs[0].first]) << std::endl;

    RunStats rstats(MPI_COMM_WORLD);
    MismatchSuffixTries< true, true, pcfg_t, RunStats,
                         suffix_ds_t, root_nodes_t > mst(sfx_lgp_ds,
                                                         rev_sfx_lgp_ds,
                                                         rnodes,
                                                         param, rstats);
    DefaultPairGenerator<suffix_ds_t, trie_nodes_t> dpgen(sfx_lgp_ds,
                                                          param.read_length,
                                                          true, 0);
    mst.extend_left(dpgen);
    mst.rev_trie_sa[0]->print(std::cout);
    mst.rev_trie_nodes[0]->print(std::cout);
    mst.rev_trie_sa[1]->print(std::cout);
    mst.rev_trie_nodes[1]->print(std::cout);
    CSPairGenerator<suffix_ds_t, trie_nodes_t> rpgen(sfx_lgp_ds,
                                                     param.read_length,
                                                     false);
    rpgen.generate(*mst.rev_trie_nodes[1], std::cout);

}

void SuffixDSTest::test_full(std::vector<std::string>& reads,
                             pcfg_t& param,
                             suffix_ds_t& sfx_lgp_ds,
                             suffix_ds_t& rev_sfx_lgp_ds,
                             bs_root_nodes_t& rnodes){
    std::string& sx = reads[0]; std::string& sy = reads[1];
    std::vector<unsigned> pos;
    std::vector<int> length;

    run_naive(sx, sy, pos, length, param.tau, param.K, false, false);
    std::vector< std::pair<unsigned, unsigned> > pos_pairs;
    for(unsigned i = 0; i < length.size();i++){
        auto v = pos[i];
        if(length[i] > 0)
            pos_pairs.push_back(std::make_pair(i, v));
    }
    std::cout << pos_pairs.size() << " " << pos_pairs << std::endl;
    RunStats rstats(MPI_COMM_WORLD);
    CSPairGenerator<suffix_ds_t, trie_nodes_t> rpgen(sfx_lgp_ds,
                                                     param.read_length,
                                                     true);
    MismatchSuffixTries< true, true, pcfg_t, RunStats,
                         suffix_ds_t, bs_root_nodes_t > mst(sfx_lgp_ds,
                                                            rev_sfx_lgp_ds,
                                                            rnodes,
                                                            param, rstats);
    mst.extend_right(rpgen);
    rpgen.set_forward(false);
    mst.extend_right_left(rpgen);
    // mst.extend_left(&rpgen);
    // mst.rev_trie_sa[0]->print(std::cout);
    // mst.rev_trie_nodes[0]->print(std::cout);
    // mst.rev_trie_sa[1]->print(std::cout);
    // mst.rev_trie_nodes[1]->print(std::cout);
    // rpgen.generate(*mst.rev_trie_nodes[1]);
}

void SuffixDSTest::test_large_str(){
    std::vector<std::string> reads, headers;
    std::string out_str;
    // 1. read fasta - take the shortest to be the one
    read_fasta("primates.fa", headers, reads);
    make_reads_const(reads);

    pcfg_t param;
    param.tau = 120; param.K = 2; param.read_length = reads[0].size();

    std::vector<std::string> rev_reads(2);
    rev_reads[0] = reads[1];
    rev_reads[1] = reads[0];
    std::reverse(rev_reads[0].begin(), rev_reads[0].end());
    std::reverse(rev_reads[1].begin(), rev_reads[1].end());

    const char *in_reads[2] = { reads[0].c_str(), reads[1].c_str() };
    const char *in_rev_reads[2] = { rev_reads[1].c_str(),
                                    rev_reads[0].c_str() };

    std::string lgp, rev_lgp;
    concat_sep(reads, lgp);
    concat_sep(rev_reads, rev_lgp);

    std::vector<int> lgp_sa, rev_lgp_sa;
    std::vector<unsigned> lgp_lcp, rev_lgp_lcp;
    TestDS lgp_ds, rev_lgp_ds;
    init_sa_lcp_ds(lgp, in_reads, 2, reads[0].length(),
                   lgp_sa, lgp_lcp, lgp_ds);
    init_sa_lcp_ds(rev_lgp, in_rev_reads, 2, reads[0].length(),
                   rev_lgp_sa, rev_lgp_lcp, rev_lgp_ds);

    suffix_ds_t sfx_lgp_ds, rev_sfx_lgp_ds;
    init_sfx_ds(lgp, lgp_sa, lgp_lcp, sfx_lgp_ds);
    init_sfx_ds(rev_lgp, rev_lgp_sa, rev_lgp_lcp, rev_sfx_lgp_ds);

    sfx_lgp_ds.print_summary(std::cout);
    rev_sfx_lgp_ds.print_summary(std::cout);
    base_region_ds_t base_ds(sfx_lgp_ds, param.tau);
    base_ds.print_summary(std::cout);

    bs_root_nodes_t rnodes(sfx_lgp_ds, base_ds, param.tau, false);
    rnodes.print_summary(std::cout);

    root_nodes_t rnodes2(sfx_lgp_ds, sfx_lgp_ds, param.tau, false);
    rnodes2.print_summary(std::cout);
    // for(int i = 0; i < rnodes2.size(); i++)
    //     for(int j = 0; j < rnodes2.width()[i]; j++)
    //         std::cout << i << " " << j << " "
    //                   << rnodes2.sa(i, j) << std::endl;
    root_nodes_t rnodes3(rev_sfx_lgp_ds, rev_sfx_lgp_ds, param.tau, false);
    rnodes3.print_summary(std::cout);
    // for(int i = 0; i < rnodes3.size(); i++)
    //     for(int j = 0; j < rnodes3.width()[i]; j++)
    //         std::cout << i << " " << j << " "
    //                   << rnodes3.sa(i, j) << std::endl;
    // test_right(reads);
    // test_right(reads, param, sfx_lgp_ds, rev_sfx_lgp_ds, rnodes);
    // test_left(reads, param, sfx_lgp_ds, rev_sfx_lgp_ds, rnodes);
    test_full(reads, param, sfx_lgp_ds, rev_sfx_lgp_ds, rnodes);
}


void SuffixDSTest::test_pairs(){
    std::vector<std::string> reads, headers;
    std::string out_str;
    // 1. read fasta - take the shortest to be the one
    read_fasta("rand_reads.fa", headers, reads);

    pcfg_t param;
    param.tau = 24; param.K = 1; param.read_length = reads[0].size();
    param.batch_width = 1600;
    std::cout << param.read_length << std::endl;

    std::vector<std::string> rev_reads(reads.size());
    for(unsigned i = 0, j = (reads.size() - 1); i < rev_reads.size(); i++, j--) {
        rev_reads[i] = reads[j];
        std::reverse(rev_reads[i].begin(), rev_reads[i].end());
    }

    std::string lgp, rev_lgp;
    concat_sep(reads, lgp);
    concat_sep(rev_reads, rev_lgp);

    std::vector<int> lgp_sa, rev_lgp_sa;
    std::vector<unsigned> lgp_lcp, rev_lgp_lcp;
    TestDS lgp_ds, rev_lgp_ds;
    init_sa_lcp_ds(lgp, reads.begin(), reads.size(), reads[0].length(),
                   lgp_sa, lgp_lcp, lgp_ds);
    init_sa_lcp_ds(rev_lgp, rev_reads.begin(), reads.size(), reads[0].length(),
                   rev_lgp_sa, rev_lgp_lcp, rev_lgp_ds);

    suffix_ds_t sfx_lgp_ds, rev_sfx_lgp_ds;
    init_sfx_ds(lgp, lgp_sa, lgp_lcp, sfx_lgp_ds);
    init_sfx_ds(rev_lgp, rev_lgp_sa, rev_lgp_lcp, rev_sfx_lgp_ds);


    sfx_lgp_ds.print_summary(std::cout);
    rev_sfx_lgp_ds.print_summary(std::cout);
    base_region_ds_t base_ds(sfx_lgp_ds, param.tau);
    base_ds.print_summary(std::cout);

    bs_root_nodes_t rnodes(sfx_lgp_ds, base_ds, param.tau, false);
    rnodes.print_summary(std::cout);

    root_nodes_t rnodes2(sfx_lgp_ds, sfx_lgp_ds, param.tau, false);
    rnodes2.print_summary(std::cout);

    RunStats rstats(MPI_COMM_WORLD);
    std::vector<char> vtxt;
    heuristic_mismatch_suffixes(sfx_lgp_ds, param, rstats, vtxt, true);
    heuristic_mismatch_suffixes(rev_sfx_lgp_ds, param, rstats, vtxt, false);
    heuristic_mismatch_suffixes_0(sfx_lgp_ds, param, rstats);
}

TEST_F(SuffixDSTest, sfx_trie_lcp){
    if(nproc > 1)
        test_trie_lcp();
}

TEST_F(SuffixDSTest, internal_nodes){
    if(nproc == 1){
        init_sa();
        test_internal_nodes();
    }
}

TEST_F(SuffixDSTest, init_suffixds){
    test_init();
}

TEST_F(SuffixDSTest, rmq_test){
    // test_ds.print(std::cout);
    test_range_min();
}

TEST_F(SuffixDSTest, extend_sfxs){
    if(nproc == 1)
        extend_sfxs_trie();
}

TEST_F(SuffixDSTest, boundary_nodes){
    test_boundary_nodes();
}

TEST_F(SuffixDSTest, regions_test){
    // test_left_shift_regions();
    // test_base_regions();
}

// TEST_F(SuffixDSTest, pair_gen){
//     if(nproc == 1)
//         test_pair_gen();
// }

// TEST_F(SuffixDSTest, large_str){
//     if(nproc == 1)
//        test_large_str();
// }

TEST_F(SuffixDSTest, rand_pairs){
    //test_pairs();
}
