# Par-kMCS

Parallel all-pairs k-mismatch maximal common substrings for a database of sequences.


## Dependencies

* A modern, C++11 ready compiler such as `g++` version 4.7 or higher or `clang` version 3.2 or higher.
* The [cmake](www.cmake.org) build system (Version >= 2.8.11).
* A 64-bit operating system. Either Mac OS X or Linux are currently supported.
* [sdsl-lite](https://github.com/simongog/sdsl-lite) is included as a submodule.
* [googletest](https://github.com/google/googletest) is included as a submodule.
* For increased performance of SDSL, the processor of the system should support fast bit operations available in `SSE4.2`


## Compilation

[sdsl-lite](https://github.com/simongog/sdsl-lite) and [googletest](https://github.com/google/googletest)  librares are included as a submodules. Initialize the submodules as below, if they are not already initialized.

    git submodule update --init --recursive

Next, create a build directory outside of source directory. For example,

     mkdir build
     cd build

Finally, build the executable.

     cmake ../
     make
