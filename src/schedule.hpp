#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <mpi.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

template<typename RDSType, typename SizeType>
void root_schedule(RDSType& rnodes, SizeType batch_width,
                   std::vector<SizeType>& batch_ptr){
    // count the number of batches
    SizeType run_width = 0, nbatches = 0;
    for(SizeType i = 0; i < rnodes.size();i++){
        run_width += rnodes.width()[i];
        if(run_width >= batch_width) {
            nbatches += 1; run_width = 0;
        }
    }
    if(run_width > 0)
        nbatches += 1;
    run_width = 0;

    SizeType j = 0;
    batch_ptr.resize(nbatches + 1);
    for(SizeType i = 0; i < rnodes.size();i++){
        if(run_width == 0){
            batch_ptr[j++] = i;
        }
        run_width += rnodes.width()[i];
        if(run_width >= batch_width){
            run_width = 0;
        }
    }
    batch_ptr[j] = rnodes.size();
}


// root schedule based on string depth
template<typename RDSType, typename SizeType>
void root_schedule_sd(RDSType& rnodes, SizeType batch_width,
                      std::vector<SizeType>& batch_ptr,
                      MPI_Comm _comm){
    int rank, nproc;
    MPI_Comm_rank(_comm, &rank);
    MPI_Comm_size(_comm, &nproc);

    typedef typename RDSType::dcount_t dcount_t;

    // sort root nodes by string depth - descending
    rnodes.sort_by_depth(0, rnodes.size());

    // get the depths in currently
    std::vector<dcount_t> local_depths;
    std::vector<SizeType> batch_sizes;
    dcount_t last_seen = 250; int local_size = 0, j = 0;
    if(rnodes.size()  > 0){
        last_seen = rnodes.depth()[0]; local_size++;
    }
    for(SizeType i = 1; i < rnodes.size(); i++){
        if(rnodes.depth()[i] != last_seen){
            last_seen = rnodes.depth()[i];
            local_size++;
        }
    }

    local_depths.resize(local_size);
    batch_sizes.resize(local_size, 0);
    j = 0;
    if(rnodes.size()  > 0){
        last_seen = local_depths[j] = rnodes.depth()[0];
        batch_sizes[j]++;
    }
    for(SizeType i = 1; i < rnodes.size(); i++){
        if(rnodes.depth()[i] != last_seen){
            j++;
            last_seen = local_depths[j] = rnodes.depth()[i];
            batch_sizes[j]++;
        }
        else
          batch_sizes[j]++;
    }

    // all gather count
    std::vector<int> rcv_cts(nproc);
    MPI_Allgather(&local_size, 1 , get_mpi_dt<int>(),
                  &rcv_cts[0], 1 , get_mpi_dt<int>(),
                  _comm);
    std::vector<int> rcv_dsp = get_displacements(rcv_cts);
    SizeType dist_size = std::accumulate(rcv_cts.begin(), rcv_cts.end(), 0);

    // gather string depths from all processors
    std::vector<dcount_t> dist_depth(dist_size);
    MPI_Allgatherv(&local_depths[0], local_size,
                   get_mpi_dt<dcount_t>(),
                   &dist_depth[0], &rcv_cts[0], &rcv_dsp[0],
                   get_mpi_dt<dcount_t>(),
                   _comm);
    // remove duplicates
    std::sort(dist_depth.begin(), dist_depth.end());
    auto dit = std::unique(dist_depth.begin(), dist_depth.end());
    dist_depth.resize(std::distance(dist_depth.begin(), dit));

    // count the number of batches
    SizeType nbatches = dist_depth.size() + 1;
    batch_ptr.resize(nbatches);
    // for(SizeType i = 1; i < batch_sizes.size();i++)
    //   batch_sizes[i] = batch_sizes[i - 1] + batch_sizes[i];
    // batch_sizes = get_displacements(batch_sizes);

    batch_ptr[0] = 0;
    for(SizeType i = 1; i <= dist_depth.size();i++){
        auto er = equal_range(local_depths.begin(),
                              local_depths.end(),
                              dist_depth[i - 1]);
        if(er.first == local_depths.end()){
            batch_ptr[i] = batch_ptr[i - 1];
        } else {
            auto dt = std::distance(local_depths.begin(), er.first);
            batch_ptr[i] = batch_ptr[i - 1] + *(batch_sizes.begin() + dt);
        }
    }
    // if(rank == 0)
    //   std::cout << local_depths.size() << " "
    //             << batch_sizes.size() << " "
    //             << ((int)dist_depth.front()) << " "
    //             << ((int)local_depths.front()) << " "
    //             << batch_sizes.front() << " "
    //             << ((int)dist_depth[1]) << " "
    //             << ((int)local_depths[1]) << " "
    //             << batch_sizes[1] << " "
    //             << ((int)dist_depth.back()) << " "
    //             << batch_ptr.back() << std::endl;
}


template<typename RDSType, typename SizeType>
void root_schedule2(RDSType& rnodes,
                    const double& batch_weight,
                    std::vector<SizeType>& batch_ptr){
  // count the number of batches
  SizeType run_size = 0, run_width = 0, nbatches = 0;
  for(SizeType i = 0; i < rnodes.size();i++){
    run_size += 1;
    run_width += rnodes.width()[i];
    double run_weight = (1.0 * run_width)/ ((double) run_size);
    if(run_weight > batch_weight) {
      nbatches += 1;
      run_size = run_width = 0;
    }
  }
  if(run_width > 0)
    nbatches += 1;
  run_size = run_width = 0;

  SizeType j = 0;
  batch_ptr.resize(nbatches + 1);
  double run_weight = 0;
  for(SizeType i = 0; i < rnodes.size();i++){
    if(run_width == 0){
      batch_ptr[j++] = i;
    }
    run_width += rnodes.width()[i];
    run_size += 1;
    run_weight = (1.0 * run_width)/ ((double) run_size);
    if(run_weight > batch_weight){
      run_size = run_width = 0;
    }
  }
  batch_ptr[j] = rnodes.size();
}

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t,
         typename CountType=typename SDSType::dcount_t>
void sa_stats(const SDSType& sadt, const CountType& rbegin,
              const CountType& rend, std::ostream& ofs,
              std::string prefix, MPI_Comm _comm){
    int rank, nproc;
    MPI_Comm_rank(_comm, &rank);
    MPI_Comm_size(_comm, &nproc);
    for(CountType x = rbegin; x <= rend; x++){
        SizeType nsize = sadt.size();
        SizeType twidth = 0, nblocks = 0, rwidth = 0, max_width = 0, max2 = 0;
        double wgtw = 0.0;
        int fail = 0, gfail = 0;
        for(SizeType i = 0; i < sadt.size(); i++){
            if(sadt.lcp()[i] < x){
                if(rwidth > max_width){
                    max2  = max_width;
                    max_width = rwidth;
                } else if(rwidth > max2){
                    max2 = rwidth;
                }
                if(rwidth > 1){
                    wgtw += ((double) rwidth) * std::log2((double)rwidth);
                }
                rwidth = 0;
                continue;
            }
            twidth += 1;
            rwidth += 1;
            if(i == 0){
                nblocks += 1;
            }
            if(i > 0 && sadt.lcp()[i - 1] < x){
                nblocks += 1;
            }
        }
        if(rwidth > max_width){
            max2  = max_width;
            max_width = rwidth;
        } else if(rwidth > max2){
            max2 = rwidth;
        }
        if(rwidth > 1){
            wgtw += ((double) rwidth) * std::log2((double)rwidth);
        }
        fail = (twidth == 0) ? 0 : 1;
        // mpi reduce fail -> gfail
        MPI_Allreduce(&fail, &gfail, 1, MPI_INT, MPI_MIN, _comm);
        //   each processor prints width, width/ratio
        double bwgt;
        SizeType bnsize = 0, bnsmax  = 0,
            bmax = 0, bmax2 = 0,
            btwidth = 0, bnblocks = 0;
        for(int j = 0; j < nproc; j++){
            bwgt = 0.0;
            bmax = 0, bmax = 0, btwidth = 0, bnblocks = 0;
            if(j == rank){
                bnsize = nsize;
                bwgt = wgtw;
                bmax = max_width; bmax2 = max2;
                btwidth = twidth; bnblocks = nblocks;
            }
            MPI_Bcast(&bnsize, 1, get_mpi_dt<SizeType>(), j, _comm);
            MPI_Bcast(&bwgt, 1, MPI_DOUBLE, j, _comm);
            MPI_Bcast(&bmax, 1, get_mpi_dt<SizeType>(), j, _comm);
            MPI_Bcast(&bmax2, 1, get_mpi_dt<SizeType>(), j, _comm);
            MPI_Bcast(&btwidth, 1, get_mpi_dt<SizeType>(), j, _comm);
            MPI_Bcast(&bnblocks, 1, get_mpi_dt<SizeType>(), j, _comm);
            if(0 == rank){
                int xv = (int)x;
                std::stringstream oss;
                oss << "\"" <<  prefix << "STATX_" << j
                    << "_" << xv << "\": ["
                    << std::setw(15) << j
                    << ","
                    << std::setw(15) << xv
                    << ","
                    << std::setw(15) << bmax
                    << ","
                    << std::setw(15) << bmax2
                    << ","
                    << std::setw(15) << bwgt
                    << ","
                    << std::setw(15) << btwidth
                    << ","
                    << std::setw(15) << bnblocks
                    << ","
                    << std::setw(15)
                    << (bnsize > 0 ? ((1.0 * btwidth) / bnsize) : 0.0)
                    << ","
                    << std::setw(15)
                    << (bnblocks > 0 ? ((1.0 * btwidth) / bnblocks) : 0.0)
                    << "],"
                    << std::endl;
                ofs << oss.str();
            }
            MPI_Barrier(_comm);
        }

        MPI_Reduce(&wgtw, &bwgt, 1, MPI_DOUBLE, MPI_MAX, 0, _comm);
        MPI_Reduce(&nsize, &bnsmax, 1, get_mpi_dt<SizeType>(),
                   MPI_MAX, 0, _comm);
        MPI_Reduce(&max_width, &bmax, 1, get_mpi_dt<SizeType>(),
                   MPI_MAX, 0, _comm);
        MPI_Reduce(&max2, &bmax2, 1, get_mpi_dt<SizeType>(),
                  MPI_MAX, 0, _comm);
        MPI_Reduce(&twidth, &btwidth, 1, get_mpi_dt<SizeType>(),
                   MPI_MAX, 0, _comm);
        MPI_Reduce(&nblocks, &bnblocks, 1, get_mpi_dt<SizeType>(),
                   MPI_MAX, 0, _comm);
        if(0 == rank){
            int xv = (int)x;
            std::stringstream oss;
            oss << "\"" <<  prefix << "STATX_MAX"
                << "_" << xv << "\": ["
                << std::setw(15) << 0
                << ","
                << std::setw(15) << xv
                << ","
                << std::setw(15) << bmax
                << ","
                << std::setw(15) << bmax2
                << ","
                << std::setw(15) << bwgt
                << ","
                << std::setw(15) << btwidth
                << ","
                << std::setw(15) << bnblocks
                << ","
                << std::setw(15)
                << (bnsmax > 0 ? ((1.0 * btwidth) / bnsmax) : 0.0)
                << ","
                << std::setw(15)
                << (bnblocks > 0 ? ((1.0 * btwidth) / bnblocks) : 0.0)
                << "],"
                << std::endl;
            ofs << oss.str();
        }

    }
}

template<typename SDSType, typename size_type>
void print_schedule(const SDSType& sadt,
                    std::vector<size_type> batch_ptr,
                    unsigned nbatch, unsigned max_batch){
    for(int j = 0; j < sadt.comm_size(); j++){
        std::vector<size_type> rcv_batch;
        MPI_Bcast(&nbatch, 1, get_mpi_dt<unsigned>(),
                  j, sadt.comm());
        if(j == sadt.comm_rank()){
            rcv_batch = batch_ptr;
        } else{
            rcv_batch.resize(nbatch);
        }
        MPI_Bcast(&rcv_batch[0], nbatch, get_mpi_dt<size_type>(),
                  j, sadt.comm());
        if(0 == sadt.comm_rank()){
            std::cout <<  "\"BTH_"  << j << "\" : ["
                      << std::setw(15) << j
                      << ","
                      << std::setw(15) << nbatch;
            for(unsigned i = 0; i < max_batch; i++)
                std::cout << "," << std::setw(15)
                          << ((nbatch >= i) ? rcv_batch[i] : 0);
            std::cout << "]" << std::endl;
        }
        MPI_Barrier(sadt.comm());
    }

}


#endif /* SCHEDULE_H */
