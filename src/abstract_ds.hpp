#ifndef ABSTRACT_DS_H
#define ABSTRACT_DS_H

#include <vector>
#include <mpi.h>
#include "util.hpp"

template< typename TxtItr, typename SizeItr, typename CountItr>
class AbstractSuffixDS {
public:
    typedef typename std::iterator_traits<CountItr>::value_type dcount_t;
    typedef typename std::iterator_traits<TxtItr>::value_type dchar_t;
    typedef typename std::iterator_traits<SizeItr>::value_type dsize_t;

    typedef TxtItr txt_iterator_t;
    typedef SizeItr sa_iterator_t;
    typedef CountItr lcp_iterator_t;

    virtual inline dsize_t size() const = 0;
    virtual inline SizeItr sa() const = 0;
    virtual inline CountItr lcp() const = 0;
    virtual inline SizeItr isa() const = 0;
    virtual inline TxtItr sa_precede() const = 0;

    virtual inline dsize_t global_size() const = 0;
    virtual inline int comm_rank() const = 0;
    virtual inline int comm_size() const = 0;
    virtual inline MPI_Comm comm() const = 0;
    virtual std::vector<dchar_t> src_bucket_str(dsize_t) const {
        return std::vector<dchar_t>();
    }
};


template<typename CharItr, typename SizeItr, typename CountItr>
class AbstractInternalNodeDS {
public:
    typedef typename std::iterator_traits<CharItr>::value_type dchar_t;
    typedef typename std::iterator_traits<SizeItr>::value_type dsize_t;
    typedef typename std::iterator_traits<CountItr>::value_type dcount_t;

    typedef SizeItr size_iterator_t;
    typedef CountItr count_iterator_t;

    static inline dsize_t flip_sa(dsize_t sfx, dsize_t gsz){
        if(sfx < (gsz - 1)){
            return (gsz - 2  - sfx); // correct ?
        }
        return gsz;
    }

    inline dsize_t reverse_sa(dsize_t i, dsize_t leaf) const {
        dsize_t rev_sfx = sa(i, leaf);
        // if(rev_sfx >= (sa_global_size() - 1)){
        //     rev_sfx = sa_global_size();
        // } else if(rev_sfx < (sa_global_size() - 1)){
        //     rev_sfx = sa_global_size() - 2  - rev_sfx; // correct ?
        // }
        return flip_sa(rev_sfx, sa_global_size());
    }

    void reverse_suffixes(std::vector<dsize_t>& rsa) const{
        // TODO:: need to check if this is correct
        dsize_t ldx = rsa.size();
        dsize_t total_leaves = std::accumulate(width(),
                                                width() + size(), 0);
        rsa.resize(ldx + total_leaves);
        for(dsize_t i = 0; i < size(); i++){
            for(dsize_t leaf = 0; leaf < width()[i]; leaf++){
                rsa[ldx] = reverse_sa(i, leaf);
                ldx += 1;
            }
        }
    }

    void suffixes(std::vector<dsize_t>& rsa) const{
        // TODO:: need to check if this is correct
        dsize_t ldx = rsa.size();
        dsize_t total_leaves = std::accumulate(width(),
                                                width() + size(), 0);
        rsa.resize(ldx + total_leaves);
        for(dsize_t i = 0; i < size(); i++){
            for(dsize_t leaf = 0; leaf < width()[i]; leaf++){
                rsa[ldx] = sa(i, leaf);
                ldx += 1;
            }
        }
    }

    virtual void add_delta(dsize_t, dcount_t){
        // does nothing
    }

    virtual dsize_t sa(dsize_t i, dsize_t j) const = 0;
    virtual dsize_t sa_global_size() const = 0;
    virtual dsize_t size() const = 0;
    virtual dchar_t sa_precede(dsize_t i, dsize_t j) const = 0;
    virtual std::vector<dchar_t> bucket_str(dsize_t, dsize_t) const {
        return std::vector<dchar_t>();
    }

    virtual SizeItr width() const = 0;
    virtual SizeItr left() const = 0;
    virtual CountItr depth() const = 0;
    virtual CountItr delta() const = 0;
    virtual ~AbstractInternalNodeDS(){}
};


template< typename SDSType,
          typename CharType=typename SDSType::dchar_t,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t>
class InternalNodeDS :
    public AbstractInternalNodeDS<typename std::vector<CharType>::const_iterator,
                                  typename std::vector<SizeType>::const_iterator,
                                  typename std::vector<CountType>::const_iterator>
{
protected:
    const SDSType& root_sds;
    std::vector<SizeType>  node_width;
    std::vector<SizeType>  node_left;
    std::vector<CountType> node_depth;
    std::vector<CountType> node_delta; // ?
public:
    typedef InternalNodeDS<SDSType, CharType, SizeType, CountType> sinternal_t;
    typedef typename std::vector<SizeType>::const_iterator size_iterator_t;
    typedef typename std::vector<CountType>::const_iterator count_iterator_t;

    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;

    virtual SizeType sa(SizeType i, SizeType j) const = 0;
    virtual dchar_t sa_precede(dsize_t i, dsize_t j) const = 0;

    virtual inline SizeType sa_global_size() const{
        return root_sds.global_size();
    }

    virtual inline size_iterator_t width() const{
        return node_width.cbegin();
    }

    virtual inline size_iterator_t left() const{
        return node_left.cbegin();
    }

    virtual inline count_iterator_t depth() const{
        return node_depth.cbegin();
    }

    virtual inline count_iterator_t delta() const{
        return node_delta.cbegin();
    }

    inline const std::vector<SizeType>& width_vec() const{
        return node_width;
    }

    inline const std::vector<SizeType>& left_vec() const{
        return node_left;
    }

    inline const std::vector<CountType>& depth_vec() const{
        return node_depth;
    }

    inline const std::vector<CountType>& delta_vec() const{
        return node_delta;
    }

    virtual unsigned get_flag() const{
        unsigned flag = (size() == node_left.size()) ? 1 : 0;
        flag |=  (size() == node_depth.size()) ? 2 : 0;
        flag |=  (size() == node_delta.size()) ? 4 : 0;
        return flag;
    }

    virtual inline SizeType size() const{
        return (SizeType)node_width.size();
    }

    virtual inline SizeType total_width() const{
         return std::accumulate(this->node_width.begin(),
                                this->node_width.end(),
                                (SizeType) 0);
    }

    virtual inline void add_delta(dsize_t i, dcount_t dx){
        if(i < size())
            node_delta[i] += dx;
    }

    inline void write(std::ostream& ots, const SizeType& i,
               const char *sepStr = "\t") const{
        ots << left()[i] << sepStr << width()[i] << sepStr
            << depth()[i] << sepStr << delta()[i];
    };

    inline void writeln(std::ostream& ots, const SizeType& i) const{
        write(ots, i);
        ots << std::endl;
    }

    inline void print(std::ostream& ots) const{
        for(SizeType i = 0; i < size(); i++){
            ots << " [";
            write(ots, i, ",\t");
            ots << "]," << std::endl;
        }
    }

    virtual inline void print_summary(std::ostream& ots) const{
        std::stringstream oss;
        oss << "\"INTERNAL_NODE_X" << "\" : ["
            << std::setw(15) << size()
            << ","
            << std::setw(15) << get_flag()
            << "],"
            << std::endl;
        ots << oss.str();
    }

    inline void assign_to(SizeType x, SizeType y){
        if(x >= size() || y >= size())
            return;
        node_width[x] = node_width[y];
        node_depth[x] = node_depth[y];
        node_left[x] =  node_left[y];
        node_delta[x] = node_delta[y];
    }

    inline bool equal_to(SizeType x, SizeType y) const{
        if(x >= size() || y >= size())
            return false;

        return (node_width[x] == node_width[y]) &&
               (node_left[x] == node_left[y]);
    }

    inline void swap(SizeType x, SizeType y){
        if(x >= size() || y >= size())
            return;
        std::swap(node_width[x], node_width[y]);
        std::swap(node_depth[x], node_depth[y]);
        std::swap(node_left[x], node_left[y]);
        std::swap(node_delta[x], node_delta[y]);
    }

    virtual void sort_by_depth(SizeType begin, SizeType end){
        // compartor
        struct srtby_idx {
            const sinternal_t& mds;
            srtby_idx(const sinternal_t& dx) : mds(dx){} // constr.
            //
            bool compare_depth(const SizeType& a, const SizeType& b) const {
                return (mds.node_depth[a] < mds.node_depth[b]) ? true
                    : ((mds.node_depth[a] == mds.node_depth[b]) ?
                       ((mds.node_left[a] < mds.node_left[b]) ? true
                        : ((mds.node_left[a] == mds.node_left[b]) ?
                           (mds.node_width[a] < mds.node_width[b])
                           : false))
                       : false);
            }

            bool operator()(const SizeType& a, const SizeType& b) const {
                return compare_depth(a, b);
            }
        };
        // swapper
        struct swapper_idx {
            sinternal_t& mds;
            swapper_idx(sinternal_t& dx,
                        sinternal_t&) : mds(dx) {} // constr.
            //
            void operator()(const SizeType& i, const SizeType& j){
                mds.swap(i, j);
            }
        };

        sort_record_by<sinternal_t&, sinternal_t&, SizeType,
                       swapper_idx, srtby_idx>(*this, *this, begin, end);
    }

    virtual void sort(SizeType begin, SizeType end){
        // compartor
        struct srtby_idx {
            const sinternal_t& mds;
            srtby_idx(const sinternal_t& dx) : mds(dx){} // constr.
            //
            inline bool compare_width(const SizeType& a, const SizeType& b) const {
                return (mds.node_width[a] > mds.node_width[b]) ? true
                    : ((mds.node_width[a] == mds.node_width[b]) ?
                       ((mds.node_left[a] < mds.node_left[b]) ? true
                        : ((mds.node_left[a] == mds.node_left[b]) ?
                           (mds.node_delta[a] > mds.node_delta[b])
                           : false))
                       : false);
            }

            inline bool compare_left(const SizeType& a, const SizeType& b) const {
                return (mds.node_left[a] < mds.node_left[b]) ? true
                    : ((mds.node_left[a] == mds.node_left[b]) ?
                       ((mds.node_width[a] > mds.node_width[b]) ? true
                        : ((mds.node_width[a] == mds.node_width[b]) ?
                           (mds.node_delta[a] > mds.node_delta[b])
                           : false))
                       : false);
            }

            inline bool operator()(const SizeType& a, const SizeType& b) const {
                // return compare_width(a, b);
                return compare_left(a, b);
            }
        };
        // swapper
        struct swapper_idx {
            sinternal_t& mds;
            swapper_idx(sinternal_t& dx,
                        sinternal_t&) : mds(dx) {} // constr.
            //
            inline void operator()(const SizeType& i, const SizeType& j){
                mds.swap(i, j);
            }
        };

        sort_record_by<sinternal_t&, sinternal_t&, SizeType,
                       swapper_idx, srtby_idx>(*this, *this, begin, end);
    }

    inline void resize(SizeType nsize){
        node_width.resize(nsize);
        node_left.resize(nsize);
        node_depth.resize(nsize);
        node_delta.resize(nsize);
    }

    inline void shrink(SizeType nsize){
       SizeType csize = size();
       resize(nsize);
       if(csize < nsize)
           std::vector<SizeType>(node_width).swap(node_width);
       if(csize < nsize)
           std::vector<SizeType>(node_left).swap(node_left);
       if(csize < nsize)
           std::vector<CountType>(node_depth).swap(node_depth);
       if(csize < nsize)
           std::vector<CountType>(node_delta).swap(node_delta);
    }

    void eliminate_dupes(const SizeType& begin, const SizeType& end,
                         bool do_shrink){
        assert(end <= size());
        sort(begin, end);
        // memory_report(0, MPI_COMM_WORLD);
        SizeType j = begin;
        for(SizeType i = begin + 1; i < end; i++){
            if(equal_to(i, j))
                continue;
            j += 1;
            assign_to(j, i);
        }
        if(do_shrink) shrink(j + 1);
        else resize(j + 1);
    }

    InternalNodeDS(const SDSType& sadt):root_sds(sadt){}

    virtual ~InternalNodeDS(){}

    //
    //  In order to maintain consistency,
    //    - we start with curLeaf and its next as the tree 'x'
    //    - we grow this tree 'x' until we reach its left end
    // Left end of the internal node corresponding to curLeaf
    template<typename DSType>
    static SizeType leftBound0(const DSType& sadt, SizeType curLeaf){
        if(curLeaf + 1 >= sadt.size()) // reached the end
            curLeaf = (SizeType)sadt.size() - 1;
        if(curLeaf <= 0)
            return 0;
        SizeType lpos;
        CountType idxLCP = sadt.lcp()[curLeaf + 1], srcLCP = idxLCP;
        while(idxLCP >= srcLCP){
            lpos = curLeaf;
            curLeaf -= 1;
            if(curLeaf <= 0)
                break;
            idxLCP = sadt.lcp()[curLeaf + 1];
        }
        if(curLeaf == 0 && sadt.lcp()[curLeaf + 1] >=srcLCP)
            lpos = 0;
        return lpos;
    }

    //  In order to maintain consistency,
    //    - we start with curLeaf and its next as the tree 'x'
    //    - we grow this tree 'x' until we reach its right end
    // Right end of the internal node corresponding to curLeaf
    template<typename DSType>
    static SizeType rightBound0(const DSType& sadt, SizeType curLeaf){
        if(curLeaf <= 0)
            curLeaf = 0;
        if(curLeaf + 1 >= sadt.size()) // reached the end
            return (sadt.size() - 1);
        // compute rpos
        curLeaf += 1;
        SizeType rpos;
        CountType idxLCP = sadt.lcp()[curLeaf], srcLCP = idxLCP;
        while(idxLCP >= srcLCP){
            rpos = curLeaf;
            curLeaf += 1;
            if(curLeaf >= sadt.size())
                break;
            idxLCP = sadt.lcp()[curLeaf];
        }
        return rpos;
    }

    template<typename WidthItr, typename BoundsItr>
    static void max_ansv(WidthItr vbegin, WidthItr vend,
                         BoundsItr max_bounds){
        std::stack<SizeType> s;
        SizeType nsize = std::distance(vbegin, vend);
        for(SizeType ix = 0; ix < nsize; ix++){
            while(!s.empty() &&
                  vbegin[s.top()] <= vbegin[ix])
                s.pop();
            if(s.empty())
                max_bounds[ix] = 0;
            else
                max_bounds[ix] = s.top();
            s.push(ix);
        }
    }

    template<typename DSType>
    static void ansv_bounds(const DSType& sadt,
                            std::vector<SizeType>& left_bounds,
                            std::vector<SizeType>& right_bounds,
                            std::vector<SizeType>& left_sa,
                            std::vector<SizeType>& right_sa){
        std::stack<SizeType> s, t;
        // todo: find right and left snv
        left_bounds.resize(sadt.size());
        left_sa.resize(sadt.size());
        for(SizeType ix = 0; ix < sadt.size(); ix++){
            while(!s.empty() &&
                  sadt.lcp()[s.top()] >= sadt.lcp()[ix])
               {s.pop(); t.pop();}
            if(s.empty())
               {left_bounds[ix] = 0; left_sa[ix] = sadt.sa()[0];}
            else
               {left_bounds[ix] = s.top(); left_sa[ix] = t.top();}
            s.push(ix);
            t.push(sadt.sa()[ix]);
        }

        while(!s.empty()) s.pop();
        while(!t.empty()) t.pop();

        right_bounds.resize(sadt.size());
        right_sa.resize(sadt.size());
        for (SizeType ix = sadt.size(); ix > 0; --ix) {
            while (!s.empty() &&
                   sadt.lcp()[s.top()] >= sadt.lcp()[ix - 1])
              {s.pop(); t.pop();}
            if(s.empty())
               {right_bounds[ix - 1] = sadt.size() - 1;
                right_sa[ix - 1] = sadt.sa()[sadt.size() - 1];}
            else
              {right_bounds[ix - 1] = (s.top() > 0) ? s.top() - 1 : 0;
                right_sa[ix -1] = t.top();}
            s.push(ix - 1);
            t.push(sadt.sa()[ix - 1]);
        }
    }

    template<typename DSType>
    static void ansv_bounds(const DSType& sadt,
                            std::vector<SizeType>& left_bounds,
                            std::vector<SizeType>& right_bounds){
        std::stack<SizeType> s;
        // todo: find right and left snv
        left_bounds.resize(sadt.size());
        for(SizeType ix = 0; ix < sadt.size(); ix++){
            while(!s.empty() &&
                  sadt.lcp()[s.top()] >= sadt.lcp()[ix])
                s.pop();
            if(s.empty())
                left_bounds[ix] = 0;
            else
                left_bounds[ix] = s.top();
            s.push(ix);
        }

        while(!s.empty()) s.pop();

        right_bounds.resize(sadt.size());
        for (SizeType ix = sadt.size(); ix > 0; --ix) {
            while (!s.empty() &&
                   sadt.lcp()[s.top()] >= sadt.lcp()[ix - 1])
                s.pop();
            if(s.empty())
                right_bounds[ix - 1] = sadt.size() - 1;
            else
                right_bounds[ix - 1] = (s.top() > 0) ? s.top() - 1 : 0;
            s.push(ix - 1);
        }
    }

    static void memory_report(int rank, MPI_Comm _comm){
        static unsigned mem_report_count = 0;
        unsigned long int max_usage, my_usage;
        my_usage = mem_usage();
        MPI_Allreduce(&my_usage, &max_usage, 1,
                      MPI_UNSIGNED_LONG, MPI_MAX, _comm);
        mem_report_count += 1;
        if(rank == 0)
          std::cout
            << "\"" << "mem_usage_tds_"
            << mem_report_count << "\" : ["
            << mem_report_count << ","
            << ((1.0 * max_usage) / (1024 * 1024)) << "],"
            << std::endl;
    }

    template<typename DSType>
    static void add_internal_nodes_from(const DSType& sadt,
                                        sinternal_t& inode,
                                        CountType min_depth,
                                        CountType cdelta,
                                        bool do_shrink) {
        // TODO:: need to handle cdelta properly
        if(sadt.size() < 2)
            return;
        // asnv bounds
        std::vector<SizeType> left_bounds, right_bounds;
        ansv_bounds(sadt, left_bounds, right_bounds);

        SizeType nsize = inode.size(), j = nsize;
        inode.resize(nsize + sadt.size());
        // memory_report(0, MPI_COMM_WORLD);
        SizeType gsize = sadt.global_size();

        // Get all the internal nodes
        for(SizeType i = 0; i < sadt.size(); i++){
            if(sadt.lcp()[i] < min_depth)
                continue;
            // SizeType lb = leftBound0(sadt, i);
            // SizeType rb =  rightBound0(sadt, i);
            SizeType lb = left_bounds[i];
            SizeType rb = right_bounds[i];
            SizeType wd = 1 + rb - lb;
            if(wd < 2)
                continue;
            if(sadt.sa()[lb] >= gsize &&
               sadt.sa()[rb] >= gsize)
                continue;
            if(wd > 2 &&
               sadt.sa()[lb] >= sadt.global_size() &&
               sadt.sa()[rb - 1] >= sadt.global_size())
                continue;
            if(wd > 2 &&
               sadt.sa()[lb + 1] >= sadt.global_size() &&
               sadt.sa()[rb] >= sadt.global_size())
                continue;
            inode.node_left[j] = lb;
            inode.node_width[j] = wd;
            inode.node_depth[j] = sadt.lcp()[i];
            inode.node_delta[j] = cdelta;
            j += 1;
        }

        if(do_shrink) inode.shrink(j);
        else inode.resize(j);

        // memory_report(0, MPI_COMM_WORLD);
        if(left_bounds.size() > 0)
          std::vector<SizeType>().swap(left_bounds);
        if(right_bounds.size() > 0)
          std::vector<SizeType>().swap(right_bounds);
        inode.eliminate_dupes(nsize, inode.size(), do_shrink);

    }


    template<typename DSType>
    static void add_primary_nodes_from(const DSType& sadt,
                                       sinternal_t& inode,
                                       const SizeType& min_depth,
                                       const CountType& cdelta = 0,
                                       bool do_shrink = true) {
        // TODO:: need to handle cdelta properly
        if(sadt.size() < 2)
            return;
        SizeType nsize = inode.size(), kx = nsize;
        inode.resize(inode.size() + sadt.size() - 1);
        // Get all the internal nodes
        SizeType i = 0;
        while(i < sadt.size() - 1){
            SizeType j = i;
            CountType flcp = sadt.lcp()[j + 1];
            while(j < sadt.size() - 1 &&
                  sadt.lcp()[j + 1] >= min_depth){
                j += 1;
                flcp = std::min(flcp, sadt.lcp()[j + 1]);
            }
            if(i == j){
                i = j + 1;
                continue;
            }
            SizeType lb = i;
            SizeType rb = j;
            SizeType wd = 1 + rb - lb;
            inode.node_left[kx] = lb;
            inode.node_width[kx] = wd;
            inode.node_depth[kx] = flcp;
            inode.node_delta[kx] = cdelta;
            kx += 1;
            i = j + 1;
        }
        inode.resize(kx);
        inode.eliminate_dupes(nsize, inode.size(), do_shrink);
    }

};

template< typename RDSType,
          typename CharType=typename RDSType::dchar_t,
          typename SizeType=typename RDSType::dsize_t,
          typename CountType=typename RDSType::dcount_t>
class BatchInternalNodeDS :
    public AbstractInternalNodeDS<typename std::vector<CharType>::const_iterator,
                                  typename std::vector<SizeType>::const_iterator,
                                  typename std::vector<CountType>::const_iterator>
{
protected:
    RDSType& m_rds;
    SizeType m_offset;
    SizeType m_size;
public:
    typedef CharType dchar_t;
    typedef SizeType dsize_t;
    typedef CountType dcount_t;

    typedef typename std::vector<SizeType>::const_iterator size_iterator_t;
    typedef typename std::vector<CountType>::const_iterator count_iterator_t;

    BatchInternalNodeDS(RDSType& inrd, SizeType off, SizeType gsz)
        : m_rds(inrd), m_offset(off), m_size(gsz){
        if(m_offset > m_rds.size()){
            m_offset = 0;
        }
        if(m_offset + m_size > m_rds.size()){
            m_size = m_rds.size() - m_offset;
        }
    }

    virtual SizeType sa(SizeType i, SizeType j) const{
        return m_rds.sa(m_offset + i, j);
    }

    virtual CharType sa_precede(SizeType i, SizeType j) const{
        return m_rds.sa_precede(m_offset + i, j);
    }

    virtual SizeType sa_global_size() const {
        return m_rds.sa_global_size();
    }

    virtual size_iterator_t width() const{
        return m_rds.width() + m_offset;
    }

    virtual size_iterator_t left() const{
        return m_rds.left() + m_offset;
    }

    virtual count_iterator_t depth() const{
        return m_rds.depth() + m_offset;
    }

    virtual count_iterator_t delta() const{
        return m_rds.delta() + m_offset;
    }

    virtual SizeType size() const {
        return m_size;
    }

    virtual void add_delta(SizeType i, CountType dx){
        if(i < size())
            m_rds.add_delta(m_offset + i, dx);
    }

    virtual void print_summary(SizeType bid, std::ostream& ots) const{
        SizeType total_width = 0;
        for(SizeType ix = 0; ix < size(); ix++)
            total_width += width()[ix];

        for(int j = 0; j < m_rds.comm_size(); j++){
            if(j == m_rds.comm_rank()){
                std::stringstream oss;
                oss << "\"BATCH_NODES_" << bid << "_" << j << "\" : ["
                    << std::setw(15) << size()
                    << ","
                    << std::setw(15) << total_width
                    << "],"
                    << std::endl;
                ots << oss.str();
            }
            MPI_Barrier(m_rds.comm());
        }
    }

    virtual ~BatchInternalNodeDS(){}
};


template< typename SDSType,
          typename CharType=typename SDSType::dchar_t,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t>
class AbstractSuffixTrieDS
    : public AbstractSuffixDS<typename std::vector<CharType>::const_iterator,
                              typename std::vector<SizeType>::const_iterator,
                              typename std::vector<CountType>::const_iterator>{
public:
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;

    typedef typename std::vector<CharType>::const_iterator txt_iterator_t;
    typedef typename std::vector<SizeType>::const_iterator sa_iterator_t;
    typedef typename std::vector<CountType>::const_iterator lcp_iterator_t;

    typedef AbstractInternalNodeDS<txt_iterator_t, sa_iterator_t,
                                   lcp_iterator_t> IDSType;

    virtual void print_summary(std::string pfx, std::ostream& ots) const = 0;

    virtual void construct_trie_isa(const SDSType& sds,
                                    const std::vector<SizeType>& vsa,
                                    std::vector<SizeType>& visa) = 0;

    virtual void construct_trie_lcp(const SDSType& sds,
                                    typename IDSType::size_iterator_t nds_width,
                                    SizeType nds_count,
                                    std::vector<SizeType>& visa,
                                    std::vector<CountType>& vlcp) = 0;

    void resize(const SizeType& nsize){
        m_sa.resize(nsize);
        m_isa.resize(nsize);
        m_lcp.resize(nsize);
        m_src_node.resize(nsize);
        m_src_leaf.resize(nsize);
        m_sa_precede.resize(nsize);
    }

    void reserve(const SizeType& nsize){
        if(nsize > 0){
            m_sa.reserve(nsize);
            m_isa.reserve(nsize);
            m_lcp.reserve(nsize);
            m_src_node.reserve(nsize);
            m_src_leaf.reserve(nsize);
            m_sa_precede.reserve(nsize);
        }
    }

    inline SizeType size() const{
        return m_sa.size();
    }

    inline sa_iterator_t sa() const{
        return m_sa.cbegin();
    }

    inline sa_iterator_t isa() const{
        return m_isa.cbegin();
    }

    inline lcp_iterator_t lcp() const{
        return m_lcp.cbegin();
    }

    inline txt_iterator_t sa_precede() const{
        return m_sa_precede.cbegin();
    }

    inline const std::vector<SizeType>& sa_vec() const{
        return m_sa;
    }

    inline const std::vector<SizeType>& isa_vec() const{
        return m_isa;
    }

    inline const std::vector<CountType>& lcp_vec() const{
        return m_lcp;
    }

    inline const std::vector<CharType>& sa_precede_vec() const{
        return m_sa_precede;
    }

    virtual std::vector<CharType> src_bucket_str(SizeType dx) const{
        if(dx < m_src_node.size() && dx < m_src_leaf.size()){
            return m_inds.bucket_str(m_src_node[dx],
                                     m_src_leaf[dx]);
        }
        return std::vector<CharType>();
    }

    inline SizeType global_size() const{
        return m_sds.global_size();
    }

    virtual inline int comm_rank() const{
        return m_sds.comm_rank();
    }

    virtual inline int comm_size() const{
        return m_sds.comm_size();
    }

    virtual inline MPI_Comm comm() const{
        return m_sds.comm();
    }

    virtual unsigned get_flag() const{
        unsigned flag = (m_isa.size() == m_sa.size()) ? 1 : 0;
        flag |=  (m_lcp.size() == m_sa.size()) ? 2 : 0;
        return flag;
    }

    // Note that we don't care about parent's left
    typename IDSType::size_iterator_t parent_width() const{
        return m_inds.width();
    }

    typename IDSType::count_iterator_t parent_depth() const{
        return m_inds.depth();
    }

    typename IDSType::count_iterator_t parent_delta() const{
        return m_inds.delta();
    }

    const SizeType parent_size() const{
        return m_inds.size();
    }

    template <typename ReadBoundChecker>
    void check_read_bounds(std::vector<dsize_t>& vsa,
                           const ReadBoundChecker& read_bounds){
        // TODO: need to skip over these!
        for(SizeType k = 0; k < vsa.size(); k++){
            if(vsa[k] >= global_size())
                continue;
            if(vsa[k] > 0 && read_bounds(vsa[k]- 1))
                vsa[k] = global_size();
            if(read_bounds(vsa[k]))
                vsa[k] = global_size();
        }
    }

    void init_sa(){
        SizeType ldx = this->size();
        SizeType total_leaves = std::accumulate(m_inds.width(),
                                                m_inds.width() + m_inds.size(),
                                                0);
        resize(ldx + total_leaves);
        for(SizeType i = 0; i < m_inds.size(); i++){
            for(SizeType leaf = 0; leaf < m_inds.width()[i]; leaf++){
                SizeType ext_sfx = m_inds.sa(i, leaf);
                ext_sfx += m_inds.depth()[i] + 1;
                ext_sfx = ext_sfx > this->global_size() ?
                    this->global_size() : ext_sfx;
                m_sa[ldx] = ext_sfx;
                m_sa_precede[ldx] = ext_sfx > this->global_size() ?
                    '$' : m_inds.sa_precede(i, leaf);
                m_src_node[ldx] = i;
                m_src_leaf[ldx] = leaf;
                ldx += 1;
            }
        }
    }


    void init_lcp(){
        // 1. Query ISA for extended suffixes.
        construct_trie_isa(m_sds, m_sa, m_isa);

        // 2 : Sort the exenteded suffixes by ISA (NOT REQD ?)
        SizeType node_ptr = 0;
        for(SizeType i = 0; i < m_inds.size(); i++){
            sort_record_by(m_sa.begin() + node_ptr,
                           m_isa.begin() + node_ptr,
                           (SizeType)0, m_inds.width()[i]);
            node_ptr += m_inds.width()[i];
        }

        // 3. Get the LCP array for suffix trie built from extened suffixes
        construct_trie_lcp(m_sds, m_inds.width(), m_inds.size(),
                           m_isa, m_lcp);
        // memory_report();
    }

    void init_rev_width(SizeType ldx){
        for(SizeType i = 0, j = ldx; i < m_inds.size(); i++){
            for(SizeType leaf = 0; leaf < m_inds.width()[i]; leaf++){
                m_sa[j] = m_inds.reverse_sa(i, leaf);
                if(m_sa[j] < global_size()){
                    m_sa[j] += m_inds.delta()[i];
                    m_sa_precede[ldx] = m_inds.sa_precede(i, leaf);
                }
                else {
                    m_sa[j] = global_size();
                    m_sa_precede[ldx] = '$';
                }
                j++;
            }
        }
        // 1. Query ISA
        construct_trie_isa(m_sds, m_sa, m_isa);
        // query_isa(m_sds, m_sa, m_isa);
        // 2 : Sort the exenteded suffixes by ISA (NOT REQD ?)
        std::vector<dsize_t> rsize(m_inds.size(), 2),
            risa(m_inds.size() * 2, global_size());
        SizeType node_ptr = ldx;
        for(SizeType i = 0; i < m_inds.size(); i++){
            if(m_inds.width()[i] < 2)
                continue;
            sort_record_by(m_sa.begin() + node_ptr,
                           m_isa.begin() + node_ptr,
                           (SizeType)0, m_inds.width()[i]);
            risa[i * 2] = m_isa[node_ptr];
            for(SizeType leaf = 1; leaf < m_inds.width()[i]; leaf++){
                if(m_sa[node_ptr + leaf] >= global_size())
                    continue;
                risa[i * 2 + 1] = m_isa[node_ptr + m_inds.width()[i] - 1];
            }
            node_ptr += m_inds.width()[i];
        }
        // 3. Get the RMQ values
        std::vector<dcount_t> rdepth(m_inds.size() * 2, 0);
        construct_trie_lcp(m_sds, rsize.cbegin(), m_inds.size(), risa, rdepth);
        // suffix_trie_lcp(m_sds, rsize.begin(), m_inds.size(), risa, rdepth);
        // 4. update depth
        for(SizeType i = 0, j = ldx; i < m_inds.size(); i++){
            auto dx = rdepth[1 + i * 2];
            dx -= (dx > 0) ? 1 : 0; // should be greater than 0, anyways
            m_inds.add_delta(i, dx);
            for(SizeType leaf = 0; leaf < m_inds.width()[i]; leaf++){
                m_sa[j] += dx;
                j++;
            }
        }
    }


    void init_rev_sa(){
        SizeType ldx = size();
        SizeType total_leaves = std::accumulate(m_inds.width(),
                                                m_inds.width() + m_inds.size(),
                                                0);
        resize(ldx + total_leaves);
        init_rev_width(ldx);

        for(SizeType i = 0, j = ldx; i < m_inds.size(); i++){
            for(SizeType leaf = 0; leaf < m_inds.width()[i]; leaf++){
                SizeType ext_sfx = m_sa[j] + 2; // why 2 ?
                ext_sfx = ext_sfx > global_size() ? global_size() : ext_sfx;
                m_sa[j] = ext_sfx;
                m_src_node[ldx] = i;
                m_src_leaf[ldx] = leaf;
                j++;
            }
        }
    }

    AbstractSuffixTrieDS(const SDSType& sdx, IDSType& ndx)
        :m_sds(sdx), m_inds(ndx){
    }

protected:
    std::vector<uint32_t> m_src_node;
    std::vector<uint32_t> m_src_leaf;

    std::vector<SizeType> m_sa;
    std::vector<SizeType> m_isa;
    std::vector<CountType> m_lcp;
    std::vector<CharType> m_sa_precede;
    const SDSType& m_sds;
    IDSType& m_inds;
};

#endif // ABSTRACT_DS_H
