/**
 * @file    ldss.cpp
 * @author  Patrick Flick <patrick.flick@gmail.com>
 * @brief   Executes and times the suffix array construction using
 *          libdivsufsort.
 *
 * Copyright (c) 2014 Georgia Institute of Technology. All Rights Reserved.
 *
 * TODO add Licence
 */

// include MPI
#include <mpi.h>

// C++ includes
#include <fstream>
#include <iostream>
#include <string>

// using TCLAP for command line parsing
#include <tclap/CmdLine.h>

// distributed suffix array construction
#include <suffix_array.hpp>
#include <check_suffix_array.hpp>
//#include <alphabet.hpp>

// parallel file block decompose
#include <mxx/file.hpp>
// Timer
#include <mxx/timer.hpp>

// TODO differentiate between index types (input param or automatic based on
// size!)
typedef uint64_t index_t;
typedef uint32_t count_t;

count_t read_lcp(const index_t& psa, const index_t& csa,
                 const count_t& rlen, count_t lcpv){
    if(rlen > 0){
        count_t read_pos = std::max(csa % (rlen + 1), psa % (rlen + 1));
        lcpv = (lcpv > (rlen - read_pos)) ? (rlen - read_pos) : lcpv;
    }
    return lcpv;
}
void write_sa_lcp(std::vector<index_t>& vsa,
                  std::vector<index_t>& vlcp,
                  const std::string& oprefix,
                  count_t rlen,
                  count_t max_value = 0){
   int rank, nproc;
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    if(max_value == 0)
        max_value = std::numeric_limits<count_t>::max();
    std::string
        safn =  oprefix + ".sa",
        lcpfn =  oprefix + ".lcp";
    std::ofstream
        safs(safn, std::ios::out | std::ios::binary),
        lcpfs(lcpfn, std::ios::out | std::ios::binary);
    safs.write((const char *)&vsa[0], vsa.size() * sizeof(index_t));
    safs.close();

    std::vector<count_t> out_lcp(vlcp.size());
     index_t next_vsa = vsa.back(), pr_vsa;
     MPI_Status mpstat;
     int src = (rank - 1 + nproc) % nproc;
     int dst = (rank + 1) % nproc;
     MPI_Sendrecv(&next_vsa, 1, MPI_UNSIGNED_LONG_LONG, dst, 0,
                  &pr_vsa, 1, MPI_UNSIGNED_LONG_LONG, src, 0,
                  MPI_COMM_WORLD, &mpstat);
     if(rank == 0)
       out_lcp[0] = 0;
     else{
       count_t lcpv = read_lcp(pr_vsa, vsa[0], rlen, vlcp[0]);
       if(lcpv > (index_t)max_value)
         out_lcp[0] = max_value;
       else
         out_lcp[0] = lcpv;
     }

    for(std::size_t ix = 1; ix < vlcp.size(); ix++){
        count_t lcpv = read_lcp(vsa[ix - 1], vsa[ix], rlen, vlcp[ix]);
        if(lcpv > (index_t)max_value)
            out_lcp[ix] = max_value;
        else
            out_lcp[ix] = lcpv;
    }
    lcpfs.write((const char *)&out_lcp[0], vlcp.size() * sizeof(count_t));
    lcpfs.close();
}


int main(int argc, char *argv[])
{
    // set up MPI
    MPI_Init(&argc, &argv);
    int rank, p;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &p);

    try {
    // define commandline usage
    TCLAP::CmdLine cmd("Parallel distirbuted suffix array and LCP construction.");
    TCLAP::ValueArg<std::string> fileArg("f", "file", "Input filename.", true, "", "filename");
    TCLAP::ValueArg<std::size_t> randArg("r", "random", "Random input size", true, 0, "size");
    cmd.xorAdd(fileArg, randArg);
    TCLAP::ValueArg<int> seedArg("s", "seed", "Sets the seed for the ranom input generation", false, 0, "int");
    cmd.add(seedArg);
    TCLAP::SwitchArg  lcpArg("l", "lcp", "Construct the LCP alongside the SA.", false);
    cmd.add(lcpArg);
    TCLAP::SwitchArg  slowArg("w", "slow", "Use the faster method", false);
    cmd.add(slowArg);
    // TCLAP::SwitchArg  checkArg("c", "check", "Check correctness of SA (and LCP).", false);
    // cmd.add(checkArg);
    TCLAP::ValueArg<std::string>  outputArg("o", "output", "Write output to file.", false, "", "outfile");
    cmd.add(outputArg);
    TCLAP::ValueArg<count_t> rlenArg("g", "length", "Read length", false, 0, "rlen");
    cmd.add(rlenArg);
    cmd.parse(argc, argv);

    // read input file or generate input on master processor
    // block decompose input file
    std::string local_str;
    if (fileArg.getValue() != "") {
        local_str = mxx::file_block_decompose(fileArg.getValue().c_str(), MPI_COMM_WORLD);
    } else {
        // TODO proper distributed random!
        local_str = rand_dna(randArg.getValue()/p, seedArg.getValue() * rank);
    }

    // TODO differentiate between index types

    // run our distributed suffix array construction
    mxx::timer t;
    mxx::comm comm;
    double start = t.elapsed(), end;
    if (lcpArg.getValue()) {
      std::vector<index_t> local_SA,local_LCP;
        // construct with LCP
      if(slowArg.getValue()){
        suffix_array<char, index_t, false> sa(comm);
        // TODO choose construction method
        sa.construct(local_str.begin(), local_str.end(),false);

        end = t.elapsed() - start;
        if (rank == 0)
            std::cerr << "PSAC time: " << end << " ms" << std::endl;
        // if (checkArg.getValue()) {
        //    gl_check_correct(sa, local_str.begin(), local_str.end());
        // }
        local_SA.swap(sa.local_SA);
        local_LCP.swap(sa.local_LCP);
      } else {
		suffix_array<char, index_t, true> sa(comm);
		// TODO choose construction method
		sa.construct(local_str.begin(), local_str.end(),false);
        end = t.elapsed() - start;
        if (rank == 0)
            std::cerr << "PSAC time: " << end << " ms" << std::endl;
        // if (checkArg.getValue()) {
        //    gl_check_correct(sa, local_str.begin(), local_str.end());
        // }
        local_SA.swap(sa.local_SA);
        local_LCP.swap(sa.local_LCP);
      }
#ifdef DEBUG
        for(int j = 0; j < p; j++){
            if(j == rank)
                for(std::size_t ix = 0; ix < local_SA.size(); ix++){
                    std::cout << std::setw(15) << ix
                              << std::setw(15) << local_SA[ix]
                              << std::setw(15) << local_LCP[ix]
                              << std::setw(15)
                              << ((ix == 0) ? 0 : read_lcp(local_SA[ix - 1], local_SA[ix],
                                                          rlenArg.getValue(), local_LCP[ix]))
                              << std::endl;
                }
            MPI_Barrier(MPI_COMM_WORLD);
        }
#endif
        if(outputArg.getValue() != ""){
            std::stringstream oss;
            oss << outputArg.getValue() << "."
                << std::setw(6) << std::setfill('0') << rank
                << std::setfill(' ')
                << ".out";
            write_sa_lcp(local_SA, local_LCP, oss.str(), rlenArg.getValue());
        }
    } else {
        // construct without LCP

    	suffix_array<char, index_t, false> sa(comm);
		// TODO choose construction method
		sa.construct_arr<2>(local_str.begin(), local_str.end(), true);

        double end = t.elapsed() - start;
        if (rank == 0)
            std::cerr << "PSAC time: " << end << " ms" << std::endl;
        // if (checkArg.getValue()) {
        //    gl_check_correct(sa, local_str.begin(), local_str.end());
        // }
    }

    // catch any TCLAP exception
    } catch (TCLAP::ArgException& e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        exit(EXIT_FAILURE);
    }

    // finalize MPI
    MPI_Finalize();

    return 0;
}
