#ifndef APPCONFIG_H
#define APPCONFIG_H

#include <vector>
#include <string>
#include <fstream>
#include <mpi.h>

// PARAMETERS   ------------------------------------------------------
struct AppConfig{
    std::string sfxf;
    //std::vector<std::string> ifiles;
    std::string inpf;
    std::string outf;
    std::string errf;
    std::string logf;
    std::string app;
    unsigned    K;
    unsigned    tau;
    unsigned    chunk_size;
    uint64_t    batch_factor;
    uint64_t    batch_width;
    unsigned    data_factor;
    unsigned    read_length;
    bool        help;
    bool        stats_only;
    bool        run32;
    bool        gen_pairs;
    bool        gen_maximal;
    bool        heuristic;
    bool        load_gen;
    bool        no_mismatch;
    bool        dynamic;
    bool        warmup;
    unsigned    nthreads;
    std::ofstream ofs;
    std::ofstream efs;
    std::ofstream lfs;

    std::string txtfn, safn, lcpfn;
    std::string rtxtfn, rsafn, rlcpfn;

    bool validateIO(int rank, MPI_Comm comm, std::ostream& ots);
    bool validateParams(int rank, std::ostream& ots);
    void printHelp(std::ostream& ots);
    bool validate(int rank, MPI_Comm comm, std::ostream& ots);
    bool test_comm(bool value, MPI_Comm comm);
    void write(std::ostream& ots, int nproc = 1);
    AppConfig(int argc, char* argv[]);
    ~AppConfig();
};


#endif /* APPCONFIG_H */
