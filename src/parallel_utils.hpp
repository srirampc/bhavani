///
// @file parallel_utils.hpp
// @ingroup group
// @author Patrick Flick <patrick.flick@gmail.com>
// @author Nagakishore Jammula <njammula3@mail.gatech.edu>
// @author Sriram PC <sriram.pc@gmail.com>
// @brief Implements utility functions needed when implementing parallel
// algorithms.
//
//
///
#ifndef _PARALLEL_UTILS_H_
#define _PARALLEL_UTILS_H_
#include <mpi.h>
#include <vector>
#include <cstdlib>
#include <iterator>

/// macros for block decomposition
#define BLOCK_LOW(i,p,n) ( (i*n) / p)
#define BLOCK_HIGH(i,p,n) ( (((i+1)*n)/p) - 1)
#define BLOCK_SIZE(i,p,n) (BLOCK_LOW((i+1),p,n) - BLOCK_LOW(i,p,n))
#define BLOCK_OWNER(j,p,n) (((p) * ((j)+1)-1)/(n))


template<typename SizeType, typename T>
static inline SizeType block_low(const T& rank, const T& nproc,
                                 const SizeType& n){
    return (rank * n) / nproc;
}

template<typename SizeType, typename T>
static inline SizeType block_high(const T& rank, const T& nproc,
                                  const SizeType& n){
    return (((rank + 1) * n) / nproc) - 1;
}

template<typename SizeType, typename T>
static inline SizeType block_size(const T& rank, const T& nproc,
                                  const SizeType& n){
    return block_low<SizeType, T>(rank + 1, nproc, n)
        - block_low<SizeType, T>(rank, nproc, n);
}

template<typename SizeType, typename T>
static inline T block_owner(const SizeType& j, const SizeType& n,
                            const T& nproc){
    return (((nproc) * ((j) + 1) - 1) / (n));
}

///
// @brief Calculates the inclusive prefix sum of the given input range.
//
// @param begin An iterator to the beginning of the sequence.
// @param end An iterator to the end of the sequence.
///
template <typename Iterator>
void prefix_sum2(Iterator begin, const Iterator end)
{
    // set the total sum to zero
    typename std::iterator_traits<Iterator>::value_type sum = 0;
    // calculate the inclusive prefix sum
    while (begin != end)
    {
        sum += *begin;
        *begin = sum;
        ++begin;
    }
}

///
// @brief Calculates the exclusive prefix sum of the given input range.
//
// @param begin An iterator to the beginning of the sequence.
// @param end An iterator to the end of the sequence.
///
template <typename Iterator>
void excl_prefix_sum2(Iterator begin, const Iterator end)
{
    // set the total sum to zero
    typename std::iterator_traits<Iterator>::value_type sum = 0;
    typename std::iterator_traits<Iterator>::value_type tmp;
    // calculate the inclusive prefix sum
    while (begin != end) {
        tmp = sum;
        sum += *begin;
        *begin = tmp;
        ++begin;
    }
}


///
// @brief Outputs the number of elements that need to be sent to each processor.
//
// Calculates the send count of number of elements to send to each processor,
// given that this processor has control of the elements given by the range:
// [offset, offset + number_to_send)
//
// @param out       The output sequence for the counts.
// @param target_partition  The new allocation of processors (num els per
//                           processor).
// @param offset            The number of elements that lie before this processor.
// @param number_to_send    The number of elements to send.
///
template <typename OutputIterator, typename SizeType>
void get_send_counts(OutputIterator out,
                     const std::vector<SizeType>& target_partition,
                     SizeType offset,
                     SizeType number_to_send)
{
    for (int i = 0; i < (int) target_partition.size(); ++i)
    {
        int send_count;
        if (target_partition[i] <= offset)
        {
            offset -= target_partition[i];
            send_count = 0;
        }
        else
        {
            // get number of elements to send
            send_count = std::min((int)number_to_send,
                                  (int)(target_partition[i] - offset));

            // if there are more receiving processors left, the offset is now zero
            offset = 0;
        }

        // set send count
        (*out++) = send_count;
        // subtract the number of send elements from the number of elements
        // to be send
        number_to_send -= send_count;
    }
}

template<class InputIt, class T>
T xaccumulate(InputIt first, InputIt last, T init)
{
    for (; first != last; ++first) {
        init = init + *first;
    }
    return init;
}

///
// @brief   Gets the `recv_counts` argument for the all2allv communication.
//
// This takes the prefix sum of the sizes of the partioned sequences and
// the processor partioning and retuns the `recv_counts` argument for all2all.
//
// @param out               An output iterator to be filled with the recv counts.
// @param prefix_counts     The prefix sum of length of partitions.
// @param target_partition  The new processor allocation (num els per processor).
// @param proc_offset       The offset of this processor in its new communicator.
///
template <typename OutputIterator, typename SizeType>
void get_recv_counts(OutputIterator out,
                     const std::vector<SizeType>& prefix_counts,
                     const std::vector<SizeType>& target_partition,
                     const int& proc_offset)
{
    // FIXME: maybe do this more efficiently by calculating it directly
    SizeType element_offset = xaccumulate(target_partition.begin(),
                                          target_partition.begin() + proc_offset,
                                          (SizeType)0);

    // get number of elements that need to be received
    SizeType recv_total = target_partition[proc_offset];
    // walk through the prefix sum until the number of elements passed
    // are bigger than the elements that have to lie before this processor
    for (int i = 0; i < (int) prefix_counts.size(); ++i)
    {
        int recv_count;
        if (recv_total == 0 || prefix_counts[i] <= element_offset)
        {
            recv_count = 0;
        }
        else
        {
            // get number of elements to send
            recv_count = std::min((int)recv_total,
                                  (int)(prefix_counts[i] - element_offset));
            element_offset += recv_count;
        }

        // set send count
        (*out++) = recv_count;
        // subtract the number of send elements from the number of elements
        // to be send
        recv_total -= recv_count;
    }
}


///
// @brief   Returns the displacements vector needed by MPI_Alltoallv.
//
// @param counts    The `counts` array needed by MPI_Alltoallv
//
// @return The displacements vector needed by MPI_Alltoallv.
///
template<typename OffsetType>
static inline
std::vector<OffsetType> get_displacements(const std::vector<OffsetType>& counts)
{
    // copy and do an exclusive prefix sum
    std::vector<OffsetType> result = counts;
    excl_prefix_sum2(result.begin(), result.end());
    return result;
}

///
// @brief   Returns the displacements vector needed by MPI_Alltoallv.
//
// @param counts    The `counts` array needed by MPI_Alltoallv
//
// @return The displacements vector needed by MPI_Alltoallv.
///
template<typename OffsetItr,
         typename OffsetType=typename std::iterator_traits<OffsetItr>::value_type>
static inline
std::vector<OffsetType> get_displacements(OffsetItr count_start,
                                          OffsetItr count_end)
{
    // copy and do an exclusive prefix sum
    std::vector<OffsetType> result(count_start, count_end);
    excl_prefix_sum2(result.begin(), result.end());
    return result;
}

////
// @brief Returns a block partitioning of an input of size `n` among `p` processors.
//
// @param n The number of elements.
// @param p The number of processors.
//
// @return A vector of the number of elements for each processor.
////
template<typename SizeType>
static inline
std::vector<SizeType> block_partition(SizeType n,int  p)
{
    // init result
    std::vector<SizeType> partition(p);
    for (int i = 0; i < p; ++i) {
        partition[i] = block_size(i, p, n);
    }
    return partition;
}


///
// @brief Returns a block partitioning of an input of size `n` among `p` processors.
//
// @param n The number of elements.
// @param p The number of processors.
//
// @return A vector of the number of elements for each processor.
///
template<typename SizeType>
static inline
std::vector<SizeType> modulo_block_partition(SizeType n,int  p)
{
    // init result
    std::vector<SizeType> partition(p);
    // get the number of elements per processor
    SizeType local_size = n / (SizeType)p;
   // and the elements that are not evenly distributable
    SizeType remaining = n % p;
    for (int i = 0; i < p; ++i) {
        if (i < remaining) {
            partition[i] = local_size + 1;
        } else {
            partition[i] = local_size;
        }
    }
    return partition;
}

#endif // _PARALLEL_UTILS_H_
