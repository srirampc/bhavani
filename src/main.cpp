#include <vector>
#include <string>
#include <iostream>
#include <cassert>
#include <mpi.h>
#include "AppConfig.hpp"
#include "suffix_ds.hpp"
#include "serial_ds.hpp"
#include "run_stats.hpp"
// #include "sdsl/rmq_support_sparse_table.hpp"
#include "psac_rmq.hpp"
#include "run_kmcs.hpp"
#include <mxx/collective.hpp>

// typedef rmq_support_sparse_table<
//     std::vector<unsigned char>, true, std::vector<uint64_t> > sdsl_rmq_table_t;

typedef psac_rmq<unsigned char, uint64_t> psac_rmq_table_t;
typedef SuffixDS<char, uint64_t, unsigned char, psac_rmq_table_t> suffix_t;
typedef SerialSuffixDS<char, uint64_t,
                       unsigned char, psac_rmq_table_t> serial_suffix_t;

typedef psac_rmq<unsigned char, uint64_t> psac_rmq_table32_t;
typedef SerialSuffixDS<char, unsigned,
                       unsigned char, psac_rmq_table32_t> serial_suffix32_t;

double warmup(const mxx::comm& comm) {
    auto mpi_curr_start = MPI_Wtime();
    std::vector<uint8_t> send(comm.size());
    std::vector<uint8_t> recv(comm.size());
    // First, warmup Alltoall of size 1
    mxx::all2all(&send[0], 1, &recv[0], comm);
    // Then, warmup Alltoallv of size 1
    std::vector<size_t> sendSizes(comm.size(), 1);
    std::vector<size_t> sendDispls(comm.size());
    std::iota(sendDispls.begin(), sendDispls.end(), 0);
    std::vector<size_t> recvSizes(comm.size(), 1);
    std::vector<size_t> recvDispls(sendDispls);
    mxx::all2allv(&send[0], sendSizes, sendDispls, &recv[0], recvSizes, recvDispls, comm);
    MPI_Barrier(comm);
    auto mpi_curr_stop = MPI_Wtime();
    return mpi_curr_stop - mpi_curr_start;
}


int main(int argc,char** argv){
    MPI_Init(&argc, &argv);
    //int provided;
    //MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    //assert(provided ==  MPI_THREAD_FUNNELED);

    AppConfig param(argc, argv);
    double warmup_time = 0.0;
    if(param.warmup){
        warmup_time = warmup(mxx::comm());
    }
    
    RunStats rstats(MPI_COMM_WORLD);
    
    if((param.help == true) ||
       !param.validate(rstats.comm_rank(), rstats.comm(), std::cout)){
        if(rstats.comm_rank() == 0){
            param.printHelp(std::cout);
        }
        return 0;
    }

    if(rstats.comm_rank() == 0)
        std::cout << "{" << std::endl;

    uint64_t fsize = 0;

    fsize = file_size<char>(param.txtfn);

    if(param.batch_factor == 0)
        param.batch_factor = rstats.comm_size();

    if(param.heuristic)
      param.batch_width = (fsize / (param.batch_factor));
    else
      param.batch_width = (fsize / (param.K * param.K * param.batch_factor));

    if(rstats.comm_rank() == 0){
        param.write(std::cout, rstats.comm_size());

        if(param.warmup)
            std::cout << "\"warmup_time\"    : " << warmup_time << "," << std::endl;
    }

    if(param.no_mismatch){
        if(param.run32){
            run_heuristic_0<suffix_t, unsigned, unsigned>(param, rstats);
        } else {
            run_heuristic_0<suffix_t, uint64_t, uint32_t>(param, rstats);
        }
    } else if(param.heuristic) {
        if(param.run32){
            run_heuristic<suffix_t, unsigned, unsigned>(param, rstats);
        } else {
            run_heuristic<suffix_t, uint64_t, uint32_t>(param, rstats);
        }
    } else if(param.dynamic) {
        if(param.run32){
            run_dynamic<serial_suffix32_t, uint64_t, uint32_t>(param, rstats);
        } else {
            run_dynamic<serial_suffix_t, uint64_t, uint32_t>(param, rstats);
        }
    }else {
        if(param.run32){
            run<suffix_t, unsigned, unsigned>(param, rstats);
        } else {
            run<suffix_t, uint64_t, uint32_t>(param, rstats);
        }
     }

    rstats.finalize(std::cout);
    if(rstats.comm_rank() == 0)
        std::cout << "\"empty\" : 0 }" << std::endl;

    return MPI_Finalize();
}
