#ifndef TRIE_DS_H
#define TRIE_DS_H

#include <vector>
#include <iostream>
#include "suffix_ds.hpp"
#include "dist_query.hpp"

template <typename T, typename S>
class DefaultBoundsChecker{
    S read_length;
public:
    DefaultBoundsChecker(const T& rl):read_length(rl){};

    bool operator()(S ext_sfx) const{
        return (ext_sfx % read_length == 0);
    }


    inline bool same_read(S i,S j) const{
        return (i / read_length) == (j / read_length);
    }

    inline S read_id(S i) const{
        return i/read_length;
    }

    inline S read_pos(S i) const{
        return i % read_length;
    }

    inline bool read_begin(S sfx) const{
        return (sfx % read_length == 0);
    }

};


//
// @brief LCP array for leaves of the suffix trie.
//
// @param  sadt          Distributed SA-LCP-RMQ data structure.
// @param  node_sizes    Vector of sizes of internal nodes.
// @param  node_isa      Vector of ISA of suffixes/leaves corresponding to
//    each internal node, the leaves of the internal nodes should be
//    consistent with node_sizes order are given in the sorted order.
// @return node_lcp      Updated load balanced array
//
template< typename SDSType,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t,
          typename SizeItr=typename std::vector<SizeType>::const_iterator >
void suffix_trie_lcp(const SDSType& sadt, // IN
                     SizeItr node_sizes, // IN
                     SizeType node_count, // IN
                     const std::vector<SizeType>& node_isa, // IN
                     std::vector<CountType>& node_lcp){ // OUT
    // 1. Query RMQ
    query_rmq_pair(sadt, node_isa, node_lcp);

    // 2. Query min between the ends
    std::vector<SizeType>
        node_ptrs = get_displacements(node_sizes,
                                      node_sizes + node_count),
        left_qry, right_qry, lr_ptr;
    left_qry.reserve(node_isa.size());
    right_qry.reserve(node_isa.size());
    lr_ptr.reserve(node_isa.size());

    //  - 2.1. Idenify query for between ends
    for(SizeType i = 0; i < (SizeType)node_ptrs.size(); i++){
        if(node_sizes[i] < 2)
            continue;
        bool pvalid = false;
        for(SizeType j = 0; j < (SizeType)node_sizes[i]; j++){
            SizeType k = node_ptrs[i] + j;
            if(pvalid && node_isa[k] < sadt.global_size()){
                int px = sadt.owner(node_isa[k - 1] + 1),
                    py = sadt.owner(node_isa[k]);
                if(py >= sadt.comm_size())
                    continue;
                int pz = sadt.owner(node_isa[k - 1]);
                if(px >= 0 && px < py){
                    lr_ptr.push_back(k);
                    right_qry.push_back(node_isa[k - 1] + 1);
                    left_qry.push_back(node_isa[k]);
                } else if(px == py && pz >= 0 && pz == (py - 1)){
                    lr_ptr.push_back(k);
                    right_qry.push_back(sadt.global_size());
                    left_qry.push_back(node_isa[k]);
                }
            }
            pvalid = (node_isa[k] + 1) < sadt.global_size();
        }
    }

    //  - 2.2. Query for RMQ with left and right most ends of the processors
    std::vector<CountType> left_rslt, right_rslt;
    query_rmq_left(sadt, left_qry, left_rslt); // (a) left end
    query_rmq_right(sadt, right_qry, right_rslt); // (b) right end

    //  - 2.3 update results for min across many processors
    for(size_t i = 0; i < lr_ptr.size();i++){
        auto& x = lr_ptr[i];
        int px = sadt.owner(node_isa[x - 1] + 1),
            py = sadt.owner(node_isa[x]),
            pz = sadt.owner(node_isa[x - 1]);
        if(px == py && pz >= 0 && pz == (py - 1)){
            node_lcp[x] = left_rslt[i];
        } else {
            node_lcp[x] = rmq_span_min(sadt,
                                       node_isa[x - 1] + 1, node_isa[x],
                                       right_rslt[i], left_rslt[i]);
        }
    }

    // 3. Reset rmq output values for intersecting internal nodes.
    for(SizeType i = 0; i < (SizeType)node_ptrs.size(); i++)
        for(SizeType j = 0; j < (SizeType)node_sizes[i]; j++){
            SizeType k = node_ptrs[i] + j;
            if(j == 0 || node_isa[k] < node_isa[k - 1])
                node_lcp[k] = 0;
        }
}


//
// @brief LCP array for leaves of the suffix trie.
//
// @param  sadt          Distributed SA-LCP-RMQ data structure.
// @param  node_sizes    Vector of sizes of internal nodes.
// @param  node_isa      Vector of ISA of suffixes/leaves corresponding to
//    each internal node, the leaves of the internal nodes should be
//    consistent with node_sizes order are given in the sorted order.
// @return node_lcp      Updated load balanced array
//
template< typename SDSType,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t>
void suffix_trie_lcp(const SDSType& sadt, // IN
                     const std::vector<SizeType>& node_sizes, // IN
                     const std::vector<SizeType>& node_isa, // IN
                     std::vector<CountType>& node_lcp){ // OUT

    suffix_trie_lcp(sadt, node_sizes.begin(), node_sizes.size(), node_lcp);
}


template< typename SDSType,
          typename CharType=typename SDSType::dchar_t,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t>
class SuffixTrieDS
    : public AbstractSuffixTrieDS<SDSType, CharType, SizeType, CountType>{
public:
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;

    typedef AbstractSuffixTrieDS<SDSType, CharType,
                                 SizeType, CountType> abstract_trie_t;

    typedef typename abstract_trie_t::txt_iterator_t txt_iterator_t;
    typedef typename abstract_trie_t::sa_iterator_t sa_iterator_t;
    typedef typename abstract_trie_t::lcp_iterator_t lcp_iterator_t;

    typedef AbstractInternalNodeDS<txt_iterator_t, sa_iterator_t,
                                   lcp_iterator_t> IDSType;

    virtual void print_summary(std::string pfx, std::ostream& ots) const{
        for(int j = 0; j < this->m_sds.comm_size(); j++){
            SizeType nsize = this->size();
            unsigned flag = (this->m_isa.size() == this->m_sa.size()) ? 1 : 0;
            flag |=  (this->m_lcp.size() == this->m_sa.size()) ? 2 : 0;
            MPI_Bcast(&nsize, 1, get_mpi_dt<SizeType>(),
                      j, this->m_sds.comm());
            MPI_Bcast(&flag, 1, get_mpi_dt<unsigned>(),
                      j, this->m_sds.comm());
            if(0 == this->m_sds.comm_rank()) {
                std::stringstream oss;
                oss << "\"SFX_TRIE_" << pfx << "_" << j << "\" : ["
                    << std::setw(15) << nsize
                    << ","
                    << std::setw(15) << flag
                    << "],"
                    << std::endl;
                ots << oss.str();
            }
            MPI_Barrier(this->m_sds.comm());
        }
    }

    void print(std::ostream& ots) const{
        for(SizeType k = 0, i = 0; k < this->m_inds.size(); k++){
            ots << "---    " << std::setw(4) << std::setfill('0') << k
                << "    ---" << std::endl;
            for(SizeType j = 0; j < this->m_inds.width()[k]; j++){
                ots << std::setfill(' ')
                    << std::setw(6) << ((i < this->m_sa.size()) ?
                                        this->m_sa[i] : 0)
                    << std::setw(6) << ((i < this->m_isa.size()) ?
                                        this->m_isa[i] : 0)
                    << std::setw(6) << ((i < this->m_lcp.size()) ?
                                        this->m_lcp[i] : 0)
                    << std::endl;
                i++;
            }
        }
        ots << "------" << std::endl;
    }

    inline CharType skip(SizeType i) const{
        if(i < m_skip.size())
            return m_skip[i];
        return '$';
    }

    virtual void construct_trie_isa(const SDSType& sds,
                                    const std::vector<SizeType>& vsa,
                                    std::vector<SizeType>& visa){
        query_isa(sds, vsa, visa);

    }

    virtual void construct_trie_lcp(const SDSType& sds,
                                    typename IDSType::size_iterator_t nds_width,
                                    SizeType nds_count,
                                    std::vector<SizeType>& visa,
                                    std::vector<CountType>& vlcp){
        suffix_trie_lcp(sds, nds_width, nds_count, visa, vlcp);
    }

    SuffixTrieDS(const SDSType& sdx, IDSType& ndx)
        : AbstractSuffixTrieDS<SDSType, CharType,
                               SizeType, CountType>(sdx, ndx){
        this->init_sa();
    }

    void memory_report(){
        static unsigned mem_report_count = 0;
        unsigned long int max_usage, my_usage;
        my_usage = mem_usage();
        MPI_Allreduce(&my_usage, &max_usage, 1,
                      MPI_UNSIGNED_LONG, MPI_MAX, this->comm());
        mem_report_count += 1;
        if(this->comm_rank() == 0)
          std::cout
            << "\"" << "mem_usage_tds_"
            << mem_report_count << "\" : ["
            << mem_report_count << ","
            << ((1.0 * max_usage) / (1024 * 1024)) << "],"
            << std::endl;
    }

    template <typename ReadBoundChecker>
    SuffixTrieDS(const SDSType& sdx, IDSType& ndx,
                 ReadBoundChecker& read_bounds, bool forward)
        : AbstractSuffixTrieDS<SDSType, CharType,
                               SizeType, CountType>(sdx, ndx){
        if(forward) {
            this->init_sa();
        } else {
            this->init_rev_sa();
        }
        // memory_report();
        this->check_read_bounds(this->m_sa, read_bounds);
        this->init_lcp();
    }

    void load_skip(){
        query_prev(this->m_sds, this->m_sa, m_skip);
    }

    CharType skip(SizeType i){
        if(i < m_skip.size())
            return m_skip[i];
        else
            return '$';
    }

    virtual ~SuffixTrieDS(){}
private:
    std::vector<CharType> m_skip;
};


template< typename SDSType,
          typename TDSType=SuffixTrieDS<SDSType>,
          typename CharType=typename SDSType::dchar_t,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t>
class TrieInternalNodeDS
    : public InternalNodeDS<SDSType, CharType, SizeType, CountType> {

    std::vector<SizeType> t_parents;

protected:
    const TDSType& node_tds;
    typedef IteratorSuffixDS<SizeType, CountType> IteratorDSType;
    typedef TrieInternalNodeDS<SDSType, TDSType> trie_internal_t;
    typedef InternalNodeDS<SDSType, CharType, SizeType, CountType> sinternal_t;

    void add_nodes(SizeType min_depth = 0){
        SizeType node_ptr = 0;
        CountType shift = 1;
        for(SizeType i = 0; i < node_tds.parent_size(); i++){
            IteratorDSType lxds(node_tds.sa() + node_ptr,
                                node_tds.lcp() + node_ptr,
                                node_tds.parent_width()[i],
                                this->sa_global_size());
            SizeType nsize = this->size();
            this->add_internal_nodes_from(lxds, *this, min_depth,
                                          node_tds.parent_delta()[i] +
                                          node_tds.parent_depth()[i] + shift,
                                          false);
            for(SizeType j = nsize; j < this->size(); j++)
                this->node_left[j] += node_ptr;

            t_parents.resize(this->size());
            this->max_ansv(this->node_width.begin() + nsize,
                           this->node_width.begin() + this->size(),
                           t_parents.begin() + nsize);

            for(SizeType jx = nsize; jx < this->size(); jx++) {
                SizeType my_left = this->node_left[jx];
                SizeType my_width = this->node_width[jx];
                SizeType p_left = this->node_left[t_parents[jx]];
                SizeType p_width = this->node_width[t_parents[jx]];
                if(my_left < p_left ||
                   (my_left + my_width) > (p_left + p_width))
                    t_parents[jx] = t_parents[t_parents[jx]];
            }

            node_ptr += node_tds.parent_width()[i];
        }
        this->shrink(this->size());
    }

public:
    virtual SizeType sa(SizeType i, SizeType j) const{
        if(i < this->size()) {
            assert((this->node_left[i] + j) < node_tds.size());
            return node_tds.sa()[this->node_left[i] + j];
        }
        return this->root_sds.global_size();
    }

    CharType skip(SizeType i, SizeType j) const{
        // need to keep a prefix sum of
        // search to find
        if(i < this->size()) {
            assert((this->node_left[i] + j) < node_tds.size());
            return node_tds.skip(this->node_left[i] + j);
        }
        return '$';
    }

    virtual std::vector<CharType> bucket_str(SizeType i, SizeType j) const{
        if(i < this->size()) {
            // -- src_bucket_str(i, j) + skip(i, j)
            CharType sc = skip(i, j);
            assert((this->node_left[i] + j) < node_tds.size());
            auto dx = this->node_left[i] + j;
            std::vector<CharType> bstx = node_tds.src_bucket_str(dx);
            bstx.resize(bstx.size() + 1);
            bstx[bstx.size() - 1] = sc;
            return bstx;
        }
        return std::vector<CharType>();
    }


    virtual CharType sa_precede(SizeType i, SizeType j) const{
        if(i < this->size()) {
            assert((this->node_left[i] + j) < node_tds.size());
            return node_tds.sa_precede()[this->node_left[i] + j];
        } else {
            return '$';
        }
    }

    virtual void print_lsum(std::ostream& ots, int j) const{
        std::stringstream oss;
        oss << "\"TRIE_NODES_" << j << "\" : ["
            << std::setw(15) << this->size()
            << ","
            << std::setw(15) << this->node_left.size()
            << ","
            << std::setw(15) << this->node_width.size()
            << ","
            << std::setw(15) << this->node_depth.size()
            << ","
            << std::setw(15) << node_tds.size()
            << "],"
            << std::endl;
        ots << oss.str();
    }

    virtual void print_summary(std::ostream& ots) const{
        if(this->root_sds.comm_size() == 1) {
            print_lsum(ots, 0);
        } else {
            for(int j = 0; j < this->root_sds.comm_size(); j++){
                if(j == this->root_sds.comm_rank())
                    print_lsum(ots, j);
                MPI_Barrier(this->root_sds.comm());
            }
        }
    }

    virtual void trie_parents(std::vector<SizeType>& parents) const {
        parents.resize(t_parents.size());
        for(SizeType i = 0; i < t_parents.size(); i++)
            parents[i] = t_parents[i];
    }


    TrieInternalNodeDS(SDSType& sadt, TDSType& tadt,
                       SizeType min_depth = 0)
        : InternalNodeDS<SDSType>(sadt), node_tds(tadt){
        add_nodes(min_depth);
    }

    virtual ~TrieInternalNodeDS(){}
};


#endif // TRIE_DS_H
