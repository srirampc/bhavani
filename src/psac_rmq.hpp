#include "rmq.hpp"

template <typename CountType, typename SizeType>
class psac_rmq {
public:
    typedef typename std::vector<CountType>::const_iterator lcp_iterator_t;
    typedef psac_rmq<CountType, SizeType> psac_rmq_t;

    psac_rmq(std::vector<CountType>* lcpvals)
        : rmq_table(lcpvals->cbegin(), lcpvals->cend()),
          m_pv(lcpvals), m_lcp_begin(lcpvals->cbegin()){
        assert(m_pv != nullptr);
        assert(m_pv->size() > 0);
    }

    SizeType operator()(SizeType i, SizeType j) const{
        assert(m_pv != nullptr);
        assert(m_pv->size() > 0);
        assert(i < m_pv->size());
        assert(j < m_pv->size());
        assert(i <= j);
        lcp_iterator_t lcp_min =
            const_cast<psac_rmq_t*>(this)->rmq_table.query(m_lcp_begin + i,
                                                           m_lcp_begin + j + 1);
        assert(lcp_min != m_pv->cend());
        return (SizeType) std::distance(m_lcp_begin, lcp_min);
    }

    void swap(psac_rmq<CountType, SizeType>& other){
        std::swap(rmq_table, other.rmq_table);
    }

    void set_vector(std::vector<CountType>* px){
        m_pv = px;
        m_lcp_begin = m_pv->cbegin();
    }

private:
    psac_rmq(){}
    psac_rmq(psac_rmq<CountType, SizeType>&){}
    rmq<lcp_iterator_t, SizeType> rmq_table;
    const std::vector<CountType>* m_pv;
    lcp_iterator_t m_lcp_begin;

};
