#ifndef GEN_PAIRS_H
#define GEN_PAIRS_H
#include <prettyprint.hpp>
#include <vector>
#include <algorithm>
#include "util.hpp"
#include "txt_db.hpp"

#define PRINT_PAIR() {                                                  \
    do{                                                                 \
      if(m_rbounds.read_id(msfx[src + x]) == 0 ||                       \
         m_rbounds.read_id(msfx[dst + y]) == 0) {                       \
        if(m_rbounds.read_id(msfx[src + x]) <                           \
           m_rbounds.read_id(msfx[dst + y]))                            \
          ots << ix << " " << jx << " "                                 \
              << src << " " << dst << " "                               \
              << x << " " << y << " "                                   \
              << msfx[src + x] << " "                                   \
              << msfx[dst + y] << " "                                   \
              << m_rbounds.read_id(msfx[src + x]) << " "                \
              << m_rbounds.read_id(msfx[dst + y]) << " "                \
              << m_rbounds.read_pos(msfx[src + x]) << " "               \
              << m_rbounds.read_pos(msfx[dst + y]) << " "               \
              << width << " "                                           \
              << ((int)depth) << " " << ((int)delta) << " "             \
              << msprt[src + x] << " "                                  \
              << msprt[dst + y] << " "                                  \
              << msnext[src + x] << " "                                  \
              << msnext[dst + y] << " "                                  \
              << msskip[src + x] << " "                                 \
              << msskip[dst + y] << " "                                 \
              << std::endl;                                             \
        else                                                            \
          ots << ix << " " << jx << " "                                 \
              << src << " " << dst << " "                               \
              << x << " " << y << " "                                   \
              << msfx[dst + y] << " "                                   \
              << msfx[src + x] << " "                                   \
              << m_rbounds.read_id(msfx[dst + y]) << " "                \
              << m_rbounds.read_id(msfx[src + x]) << " "                \
              << m_rbounds.read_pos(msfx[dst + y]) << " "               \
              << m_rbounds.read_pos(msfx[src + x]) << " "               \
              << width << " "                                           \
              << ((int)depth) << " " << ((int)delta) << " "             \
              << msprt[dst + y] << " "                                  \
              << msprt[src + x] << " "                                  \
              << msnext[dst + y] << " "                                  \
              << msnext[src + x] << " "                                  \
              << msskip[dst + y] << " "                                 \
              << msskip[src + x] << " "                                 \
              << std::endl;                                             \
      }                                                                 \
    } while(0);                                                         \
}

template<typename IDSType>
void valid_nodes(const IDSType& inds,
                 std::vector<typename IDSType::dsize_t>& lvalid,
                 unsigned threshold){
    typedef typename IDSType::dsize_t dsize_t;
    typedef typename IDSType::dcount_t dcount_t;

    std::vector<dsize_t> parents;
    inds.trie_parents(parents);

    dsize_t vdx = 0;
    for(dsize_t i = 0; i < inds.size(); i++){
        dsize_t pi = parents[i];
        dcount_t depth = inds.depth()[i];
        dcount_t delta = inds.delta()[i];
        dcount_t pdepth = inds.depth()[pi];
        dcount_t pdelta = inds.delta()[pi];
        unsigned tdepth = (unsigned) (depth + delta);
        unsigned ptdepth = (unsigned) (pdepth + pdelta);
        if(tdepth >= threshold && ptdepth < threshold) vdx++;
    }
    if(vdx == 0)
        return;

    lvalid.resize(vdx);
    vdx = 0;
    for(dsize_t i = 0; i < inds.size(); i++){
        dsize_t pi = parents[i];
        dcount_t depth = inds.depth()[i];
        dcount_t delta = inds.delta()[i];
        dcount_t pdepth = inds.depth()[pi];
        dcount_t pdelta = inds.delta()[pi];
        unsigned tdepth = (unsigned)(depth + delta);
        unsigned ptdepth = (unsigned) (pdepth + pdelta);
        if(tdepth >= threshold && ptdepth < threshold) {
            lvalid[vdx] = i;
            vdx++;
        }
    }

}

template<typename SDSType, typename IDSType, typename ReadBoundsChecker>
void load_left_extensible_char(const SDSType& sds, const IDSType& inds,
                               ReadBoundsChecker& rbounds,
                               std::vector<typename SDSType::dsize_t>& lvalid,
                               std::vector<typename SDSType::dchar_t>& vprt) {

    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dcount_t dcount_t;

    dsize_t tleaves = 0;
    for(dsize_t i = 0; i < lvalid.size(); i++)
        tleaves += inds.width()[lvalid[i]];

    std::vector<dsize_t> vsa(tleaves);
    for(dsize_t k = 0, j = 0; k < lvalid.size(); k++){
        size_t i = lvalid[k];
        dsize_t width = inds.width()[i];
        dcount_t delta = inds.delta()[i];
        for(dsize_t leaf = 0; leaf < width; leaf++) {
            dsize_t sx = inds.sa(i, leaf);
            vsa[j++] = sx < inds.sa_global_size() ? (sx - (dsize_t)delta) :
                inds.sa_global_size();
        }
    }

    vprt.resize(vsa.size());
    for(dsize_t k = 0, j = 0; k < lvalid.size(); k++)
        for(dsize_t leaf = 0; leaf < inds.width()[k]; leaf++)
            vprt[j++] = inds.sa_precede(k, leaf);
    // sds.precede(vsa, vprt);

    for(dsize_t i = 0; i < vsa.size(); i++)
        if(rbounds(vsa[i]))
            vprt[i] = '$';
}

template<typename SDSType, typename IDSType, typename ReadBoundsChecker>
void load_right_extensible_char(const SDSType& sds, const IDSType& inds,
                                ReadBoundsChecker& rbounds,
                                std::vector<typename SDSType::dsize_t>& lvalid,
                                std::vector<typename SDSType::dchar_t>& vnext) {

    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dcount_t dcount_t;

    dsize_t tleaves = 0;
    for(dsize_t i = 0; i < lvalid.size(); i++)
        tleaves += inds.width()[lvalid[i]];

    std::vector<dsize_t> vsa(tleaves);
    for(dsize_t k = 0, j = 0; k < lvalid.size(); k++){
        size_t i = lvalid[k];
        dsize_t width = inds.width()[i];
        dcount_t depth = inds.depth()[i];
        for(dsize_t leaf = 0; leaf < width; leaf++) {
            dsize_t sx = inds.sa(i, leaf);
            sx += depth;
            vsa[j++] = sx < inds.sa_global_size() ? sx :
                inds.sa_global_size();
        }
    }

    query_text(sds, vsa, vnext);

    for(dsize_t i = 0; i < vsa.size(); i++)
        if(vsa[i] > 0 && rbounds(vsa[i]))
            vnext[i] = '$';
}

template< typename SDSType,
          typename IDSType=TrieInternalNodeDS<SDSType> >
class DefaultPairGenerator{
public:

    DefaultPairGenerator(const SDSType&, unsigned,
                         bool, unsigned){

    }
    void generate(const IDSType&, std::ostream&){
        // does nothing
    }

    uint64_t get_pair_count(){
        return 0;
    }

    uint64_t get_candidates(){
        return 0;
    }

    void set_forward(bool){
    }

    bool report_counts(){
        return false;
    }

    void load_txt(std::string){}
};

template< typename SDSType,
          typename IDSType=TrieInternalNodeDS<SDSType> >
class CSPairGenerator{

public:
    typedef typename SDSType::dchar_t dchar_t;
    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dcount_t dcount_t;

private:

    const SDSType& m_sds;
    unsigned m_rlength;
    bool m_forward;
    uint64_t m_npairs, m_candidates;
    unsigned m_threshold;
    DefaultBoundsChecker<dcount_t, dsize_t> m_rbounds;

public:

    CSPairGenerator(const SDSType& sds,
                    unsigned rlen,
                    bool fwd = true,
                    unsigned threshold = 0)
        : m_sds(sds), m_rlength(rlen), m_forward(fwd),
          m_npairs(0), m_candidates(0), m_threshold(threshold),
          m_rbounds(rlen){
    }

    uint64_t get_pair_count(){
        return m_npairs;
    }

    uint64_t get_candidates(){
        return m_candidates;
    }

    void set_forward(bool fval){
        m_forward = fval;
    }

    bool report_counts(){
        return true;
    }

    unsigned get_length(){
        return m_rlength;
    }

    bool get_forward(){
        return m_forward;
    }

    bool same_read(dsize_t i, dsize_t j){
        return m_rbounds.same_read(i, j);
    }

    dsize_t read_id(dsize_t i){
        return m_rbounds.read_id(i);
    }

    dsize_t read_pos(dsize_t i){
        return m_rbounds.read_pos(i);
    }

    dsize_t get_pos(dsize_t sfx, dcount_t depth, dcount_t delta){
        if(!m_forward){
            return sfx - depth + 1;
        } else {
            return sfx - delta;
        }
    }

    void load_txt(std::string){}

    void generate(const IDSType& inds, std::ostream& ){
        // for each internal node
        for(dsize_t i = 0; i < inds.size(); i++){
            // 0. check threshold
            dcount_t depth = inds.depth()[i];
            dcount_t delta = inds.delta()[i];
            unsigned tdepth = (unsigned)(depth + delta);
            if(tdepth < m_threshold) continue;

            dsize_t width = inds.width()[i];
            //   - get the sufixes
            std::vector<dsize_t> msfx(width), mbuckets(width+1);
            dsize_t pdx = 0;
            // No buckets here!
            for(dsize_t xleaf = 0; xleaf < width; xleaf++) {
                dsize_t isx = inds.sa(i, xleaf);
                if(isx >= inds.sa_global_size())
                    continue;
                for(dsize_t yleaf = xleaf+1; yleaf < width; yleaf++) {
                    dsize_t isy = inds.sa(i, yleaf);
                    if(isy >= inds.sa_global_size())
                        continue;
                    if(same_read(isx, isy))
                        continue;
                    if(inds.sa_precede(i, xleaf) == inds.sa_precede(i, yleaf) &&
                       inds.sa_precede(i, xleaf) != '$')
                        continue;
                    auto bsx = inds.bucket_str(i, xleaf);
                    auto bsy = inds.bucket_str(i, yleaf);
                    if(bsx.size() == bsy.size() && bsx != bsy){
                        m_npairs++;
                    }
                }
            }
            continue;
            for(dsize_t leaf = 0; leaf < width; leaf++) {
                dsize_t sx = inds.sa(i, leaf);
                if(sx >= inds.sa_global_size())
                    continue;
                msfx[pdx] = (m_forward) ? inds.sa(i, leaf)
                    : inds.reverse_sa(i, leaf);
                pdx++;
            }
            if(pdx == 0)
                continue;
            msfx.resize(pdx);
            //   - sort the suffixes by index
            std::sort(msfx.begin(), msfx.end());
            // - initialize suffix pointers for suffixes from different strings
            pdx = 0;
            mbuckets[pdx++] = 0;
            for(dsize_t rdx = 1; rdx <  msfx.size(); rdx++){
                if(!same_read(msfx[mbuckets[pdx - 1]], msfx[rdx]))
                    mbuckets[pdx++] = rdx;
            }
            mbuckets[pdx++] = msfx.size();
            mbuckets.resize(pdx);
            m_candidates += pdx * pdx;
            // count valid pairs
            // TODO:: use depth and delta
            // dcount_t depth = inds.depth()[i];
            // dcount_t delta = inds.delta()[i];
            // continue;
            for(dsize_t ix = 0; ix < (pdx - 1); ix++){
                dsize_t src = mbuckets[ix];
                dsize_t src_sz = mbuckets[ix + 1] - src;
                for(dsize_t jx = ix + 1; jx < (pdx - 1); jx++){
                    dsize_t dst = mbuckets[jx];
                    dsize_t dst_sz = mbuckets[jx + 1] - dst;
                    m_npairs += src_sz * dst_sz;
                }
            }
        }
    }

};


template< typename SDSType,
          typename IDSType=TrieInternalNodeDS<SDSType> >
class TxtDBCSPairGenerator{

public:
    typedef typename SDSType::dchar_t dchar_t;
    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dcount_t dcount_t;

private:

    const SDSType& m_sds;
    unsigned m_rlength;
    bool m_forward;
    uint64_t m_npairs, m_candidates;
    unsigned m_threshold;
    DefaultBoundsChecker<dcount_t, dsize_t> m_rbounds;
    TextDB<dchar_t, dsize_t, unsigned> m_tdb;

public:

    TxtDBCSPairGenerator(const SDSType& sds,
                         unsigned rlen,
                         bool fwd = true,
                         unsigned threshold = 0)
        : m_sds(sds), m_rlength(rlen), m_forward(fwd),
          m_npairs(0), m_candidates(0), m_threshold(threshold),
          m_rbounds(rlen){
    }

    uint64_t get_pair_count(){
        return m_npairs;
    }

    uint64_t get_candidates(){
        return m_candidates;
    }

    void set_forward(bool fval){
        m_forward = fval;
    }

    bool report_counts(){
        return true;
    }

    unsigned get_length(){
        return m_rlength;
    }

    bool get_forward(){
        return m_forward;
    }

    bool same_read(dsize_t i, dsize_t j){
        return m_rbounds.same_read(i, j);
    }

    dsize_t read_id(dsize_t i){
        return m_rbounds.read_id(i);
    }

    dsize_t read_pos(dsize_t i){
        return m_rbounds.read_pos(i);
    }

    void load_txt(std::string txtfn){
        TextDB<dchar_t, dsize_t, unsigned> tdb(txtfn, m_rlength);
        m_tdb.swap(tdb);
    }

    dsize_t get_pos(dsize_t sfx, dcount_t depth, dcount_t delta){
        if(!m_forward){
            return sfx - depth + 1;
        } else {
            return sfx - delta;
        }
    }

    void generate(const IDSType& inds, std::ostream& ){
        // TODO::
    }

};


template< typename SDSType,
          typename IDSType=TrieInternalNodeDS<SDSType> >
class MaximalCSPairGenerator{
    const SDSType& m_sds;
    uint64_t m_npairs, m_candidates, m_threshold;
    CSPairGenerator<SDSType, IDSType> m_csp;
public:
    typedef typename SDSType::dchar_t dchar_t;
    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dcount_t dcount_t;
    typedef DefaultBoundsChecker<dcount_t, dsize_t> rbounds_t;

    rbounds_t m_rbounds;

    MaximalCSPairGenerator(const SDSType& sds, unsigned rlen,
                           unsigned threshold,
                           bool fwd = true)
        : m_sds(sds), m_npairs(0), m_candidates(0),
          m_threshold(threshold),
          m_csp(sds, rlen, fwd, threshold), m_rbounds(rlen)
    {
        // TODO ?
    }

    uint64_t get_pair_count(){
        return m_npairs + m_csp.get_pair_count();
    }

    uint64_t get_candidates(){
        return m_candidates + m_csp.get_candidates();
    }

    void set_forward(bool fval){
        m_csp.set_forward(fval);
    }

    bool report_counts(){
        return true;
    }

    unsigned get_length(){
        return m_csp.get_length();
    }

    bool get_forward(){
        return m_csp.get_forward();
    }

    void load_txt(std::string){}

    void generate(const IDSType& inds, std::ostream& ots){
        std::vector<dsize_t> lvalid;
        valid_nodes(inds, lvalid, m_threshold);

        if(!get_forward()){
            if(lvalid.size() > 0) m_csp.generate(inds, ots);
            return;
        }

        std::vector<dchar_t> vprt;
        load_left_extensible_char<SDSType, IDSType, rbounds_t>(m_sds, inds,
                                                               m_rbounds,
                                                               lvalid,
                                                               vprt);

        // for each internal node
        for(dsize_t j = 0, ldx = 0; j < lvalid.size(); j++){
            dsize_t i = lvalid[j];
            // 1. get root suffix indices
            dsize_t width = inds.width()[i];
            //   - get the sufixes
            std::vector<dsize_t> msfx(width), mbuckets(width+1);
            std::vector<dchar_t> msprt(width);
            dsize_t pdx = 0;
            for(dsize_t leaf = 0; leaf < width; leaf++, ldx++) {
                dsize_t sx = inds.sa(i, leaf);
                if(sx >= inds.sa_global_size())
                    continue;
                msfx[pdx] =inds.sa(i, leaf);
                msprt[pdx] = vprt[ldx];
                //msprt[pdx] = (m_rbounds(msfx[pdx])) ? '$' :
                //    inds.sa_precede(i, leaf);
                pdx++;
            }
            if(pdx == 0)
                continue;
            msfx.resize(pdx);
            msprt.resize(pdx);
            //   - sort the suffixes by index
            sort_record_by(msfx.begin(), msprt.begin(),
                           (dsize_t)0, pdx);
            // - initialize suffix pointers for suffixes from different strings
            pdx = 0;
            mbuckets[pdx++] = 0;
            for(dsize_t rdx = 1; rdx <  msfx.size(); rdx++){
                if(msprt[mbuckets[pdx - 1]] != msprt[rdx])
                    mbuckets[pdx++] = rdx;
            }
            mbuckets[pdx++] = msfx.size();
            mbuckets.resize(pdx);
            std::vector<dsize_t> nbuckets = mbuckets;
            //pdx = mbuckets.size();
            for(dsize_t ix = 0; ix < (pdx - 1); ix++){
              dsize_t bsrc = mbuckets[ix];
              dsize_t esrc = mbuckets[ix + 1];
              dsize_t src_sz = mbuckets[ix + 1] - mbuckets[ix];
              std::sort(msfx.begin() + bsrc,  msfx.begin() + esrc);
              dsize_t dx = 0, px = nbuckets[ix];
              dsize_t jx = bsrc;
              if(src_sz > 0){
                msfx[px] = msfx[jx];
                msprt[px] = msfx[jx];
                dx += 1;
                px += 1;
                jx += 1;
              }
              for(; jx < esrc; jx++){
                if(m_rbounds.same_read(msfx[jx - 1], msfx[jx]))
                  continue;
                msfx[px] = msfx[jx];
                msprt[px] = msfx[jx];
                dx += 1;
                px += 1;
              }
              nbuckets[ix + 1] = (dx < src_sz) ? px : esrc;
            }


            //continue;
            dcount_t depth = inds.depth()[i];
            dcount_t delta = inds.delta()[i];
            depth = (depth == 0) ? 1 : depth;
            generate(nbuckets, msfx, msprt, width, delta, depth);
        }
    }

    void generate(std::vector<dsize_t>& mbuckets,
                  std::vector<dsize_t>& msfx,
                  std::vector<dchar_t>& msprt,
                  dsize_t , dcount_t , dcount_t ){
        dsize_t pdx = mbuckets.size();
        for(dsize_t ix = 0; ix < (pdx - 1); ix++){
            dsize_t src = mbuckets[ix];
            dsize_t src_sz = mbuckets[ix + 1] - mbuckets[ix];
            for(dsize_t jx = ix; jx < (pdx - 1); jx++){
                dsize_t dst = mbuckets[jx];
                if(src == dst && msprt[src] != '$')
                     continue;
                dsize_t dst_sz = mbuckets[jx + 1] - mbuckets[jx];
                m_candidates += (src == dst) ? (src_sz * (src_sz - 1)) : (src_sz * dst_sz);
                /// continue;
                for(dsize_t x = 0; x < src_sz; x++){
                    for(dsize_t y = (src == dst) ? (x + 1) : 0; y < dst_sz;
                        y++){
                        if(m_rbounds.same_read(msfx[dst + y], msfx[src + x]))
                            continue;
                        m_npairs += 1;
                    }
                }
            }
        }
    }
};


template<typename SDSType,
         typename IDSType=TrieInternalNodeDS<SDSType> >
class HeuristicCSPairGenerator{

    const SDSType& m_sds;
    uint64_t m_npairs, m_candidates;
    unsigned m_kval;
    unsigned m_rlength;
    unsigned m_tau;
    bool m_forward;

    //
    void clear_sfx_data(){

    }

public:

    typedef typename SDSType::dchar_t dchar_t;
    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dcount_t dcount_t;

    typedef TextDB<dchar_t, dsize_t, dcount_t> text_db_t;
    typedef DefaultBoundsChecker<dcount_t, dsize_t> rbounds_t;

    const text_db_t& m_tdb;
    rbounds_t m_rbounds;

    HeuristicCSPairGenerator(const SDSType& sds,
                             unsigned rlen, unsigned kval,
                             unsigned tau, bool fwd,
                             text_db_t& td)
        : m_sds(sds),
          m_npairs(0), m_candidates(0), m_kval(kval),
          m_rlength(rlen), m_tau(tau), m_forward(fwd),
          m_tdb(td), m_rbounds(rlen){
        // TODO ?
    }

    uint64_t get_pair_count(){
        return m_npairs;
    }

    uint64_t get_candidates(){
        return m_candidates;
    }

    void set_forward(bool){
        m_forward = true;
    }

    bool report_counts(){
        return true;
    }

    unsigned get_length(){
        return m_rlength;
    }

    bool get_forward(){
        return m_forward;
    }

    void reset(){
        m_candidates = 0;
        m_npairs = 0;
    }


    void load_txt(std::string){}

    void generate(const IDSType& inds, std::ostream& ots){
        if(m_kval <= 0)
            return;

        std::vector<dsize_t> lvalid;
        valid_nodes(inds, lvalid, (2 * m_tau));
        if(lvalid.size() == 0) return;

        std::vector<dchar_t> vprt, vnext;
        load_left_extensible_char<SDSType, IDSType, rbounds_t>(m_sds, inds,
                                                               m_rbounds,
                                                               lvalid,
                                                               vprt);

        load_right_extensible_char<SDSType, IDSType, rbounds_t>(m_sds, inds,
                                                                m_rbounds,
                                                                lvalid,
                                                                vnext);

        // for each internal node
        for(dsize_t j = 0, ldx = 0; j < lvalid.size(); j++){
            dsize_t i = lvalid[j];
            // 1. get root suffix indices
            dsize_t width = inds.width()[i];
            dcount_t depth = inds.depth()[i];
            dcount_t delta = inds.delta()[i];
            unsigned tval = depth;
            tval += delta;
            //   - get the sufixes
            std::vector<dsize_t> msfx(width), mbuckets(width + 1);
            std::vector<dchar_t> msprt(width), msnext(width), msskip(width);
            dsize_t pdx = 0;
            for(dsize_t leaf = 0; leaf < width; leaf++, ldx++) {
                dsize_t sx = inds.sa(i, leaf);
                if(sx >= inds.sa_global_size())
                    continue;
                msfx[pdx] = sx;
                msskip[pdx] = inds.skip(i, leaf);
                msprt[pdx] = vprt[ldx];
                msnext[pdx] = vnext[ldx];
                pdx++;
            }
            if(pdx == 0)
                continue;
            msfx.resize(pdx);
            msprt.resize(pdx);
            msnext.resize(pdx);
            msskip.resize(pdx);
            //   - sort the suffixes by index
            sort_record_by3(msfx.begin(), msnext.begin(),
                            msskip.begin(), msprt.begin(),
                            (dsize_t)0, pdx);
            // - initialize suffix pointers for suffixes
            //    from different preceding char
            pdx = 0;
            mbuckets[pdx++] = 0;
            for(dsize_t rdx = 1; rdx <  msfx.size(); rdx++){
                if(msprt[mbuckets[pdx - 1]] != msprt[rdx])
                    mbuckets[pdx++] = rdx;
            }
            mbuckets[pdx++] = msfx.size();
            mbuckets.resize(pdx);
            depth = (depth == 0) ? 1 : depth;
            continue;
            generate(mbuckets, msfx, msprt, msnext, msskip,
                     width, delta, depth, ots);
        }
    }


    void generate(std::vector<dsize_t>& mbuckets,
                  std::vector<dsize_t>& msfx,
                  std::vector<dchar_t>& msprt,
                  std::vector<dchar_t>& msnext,
                  std::vector<dchar_t>& msskip,
                  dsize_t , dcount_t delta, dcount_t depth,
                  std::ostream& ){
        dsize_t pdx = mbuckets.size();
        for(dsize_t ix = 0; ix < (pdx - 1); ix++){
            dsize_t src = mbuckets[ix];
            dsize_t src_sz = mbuckets[ix + 1] - mbuckets[ix];
            for(dsize_t jx = ix; jx < (pdx - 1); jx++){
                dsize_t dst = mbuckets[jx];
                if(src == dst && msprt[src] != '$')
                     continue;

                dsize_t dst_sz = mbuckets[jx+1] - mbuckets[jx];
                m_candidates += src_sz * dst_sz;
                for(dsize_t x = 0; x < src_sz; x++){
                    for(dsize_t y = (src == dst) ? (x + 1) : 0; y < dst_sz;
                        y++){
                        if(!m_forward && depth >= m_tau)
                            continue;
                        if(msskip[src + x] == msskip[dst + y])
                            continue;
                        if(msnext[src + x] == msnext[dst + y] && msnext[src + x] != '$')
                            continue;
                        if(m_rbounds.same_read(msfx[dst + y], msfx[src + x]))
                            continue;
                        m_npairs += 1;
                        // PRINT_PAIR();
                        if(m_kval <= 1)
                            continue;
                        // m_tdb.align(msfx[src + x], msfx[dst + y], m_rbounds);
                        // 1. match right
                        m_tdb.match_txt_right(msfx[src + x] + depth - 1,
                                              msfx[dst + y] + depth - 1,
                                              m_kval - 1, m_rbounds);
                        // 2. match left
                        m_tdb.match_txt_left(msfx[src + x] - delta,
                                             msfx[dst + y] - delta,
                                             m_kval - 1, m_rbounds);
                    }
                }
            }
        }
    }

    void generate0(const IDSType& inds, std::ostream& ){

        // all valid here
        std::vector<dsize_t> lvalid(inds.size());
        for(dsize_t ix = 0; ix < inds.size(); ix++) lvalid[ix] = ix;

        // std::vector<dchar_t> vprt;
        // load_left_extensible_char<SDSType, IDSType, rbounds_t>(m_sds, inds,
        //                                                        m_rbounds,
        //                                                        lvalid,
        //                                                        vprt);

        // for each internal node
        for(dsize_t j = 0, ldx = 0; j < lvalid.size(); j++){
            dsize_t i = lvalid[j];
            // 1. get root suffix indices
            dsize_t width = inds.width()[i];
            //   - get the sufixes
            std::vector<dsize_t> msfx(width), mbuckets(width + 1);
            std::vector<dchar_t> msprt(width);
            dsize_t pdx = 0;
            for(dsize_t leaf = 0; leaf < width; leaf++, ldx++) {
                dsize_t sx = inds.sa(i, leaf);
                if(sx >= inds.sa_global_size())
                    continue;
                msfx[pdx] = sx;
                //msprt[pdx] = vprt[ldx];
                msprt[pdx] = (m_rbounds(msfx[pdx])) ? '$' :
                    inds.sa_precede(i, leaf);
                pdx++;
            }
            if(pdx == 0)
                continue;
            msfx.resize(pdx);
            msprt.resize(pdx);
            //   - sort the suffixes by index
            sort_record_by(msfx.begin(), msprt.begin(),
                           (dsize_t)0, pdx);
            // - initialize suffix pointers for suffixes
            //    from different preceding char
            pdx = 0;
            mbuckets[pdx++] = 0;
            for(dsize_t rdx = 1; rdx <  msfx.size(); rdx++){
                if(msprt[mbuckets[pdx - 1]] != msprt[rdx])
                    mbuckets[pdx++] = rdx;
            }
            mbuckets[pdx++] = msfx.size();
            mbuckets.resize(pdx);
            continue;
            dcount_t depth = inds.depth()[i];
            dcount_t delta = inds.delta()[i];
            depth = (depth == 0) ? 1 : depth;
            if(m_kval > 0)
                generate0(mbuckets, msfx, msprt,
                          width, delta, depth);
        }
    }

    void generate0(std::vector<dsize_t>& mbuckets,
                   std::vector<dsize_t>& msfx,
                   std::vector<dchar_t>& msprt,
                   dsize_t , dcount_t , dcount_t ){
        dsize_t pdx = mbuckets.size();
        for(dsize_t ix = 0; ix < (pdx - 1); ix++){
            dsize_t src = mbuckets[ix];
            dsize_t src_sz = mbuckets[ix + 1] - mbuckets[ix];
            for(dsize_t jx = ix; jx < (pdx - 1); jx++){
                dsize_t dst = mbuckets[jx];
                if(src == dst && msprt[src] != '$')
                     continue;
                dsize_t dst_sz = mbuckets[jx+1] - mbuckets[jx];
                m_candidates += src_sz * dst_sz;
                for(dsize_t x = 0; x < src_sz; x++){
                    for(dsize_t y = (src == dst) ? (x + 1) : 0; y < dst_sz; y++){
                        if(m_rbounds.same_read(msfx[dst + y], msfx[src + x]))
                            continue;
                        m_npairs += 1;

                        // m_tdb.align(msfx[src + x], msfx[dst + y], m_rbounds);
                        // 1. match right
                        // m_tdb.match_txt_right(msfx[src + x] + depth - 1,
                        //                       msfx[dst + y] + depth - 1,
                        //                       m_kval, m_rbounds);
                        // 2. match left
                        // m_tdb.match_txt_left(msfx[src + x] - delta,
                        //                      msfx[dst + y] - delta,
                        //                      m_kval, m_rbounds);
                    }
                }
            }
        }
    }

};

#endif // GEN_PAIRS_H
