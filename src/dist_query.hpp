#ifndef DIST_QUERY_H
#define DIST_QUERY_H

#include <vector>
#include "query_oracle.hpp"
#include "util.hpp"
#include "parallel_utils.hpp"
#include "mpi_util.hpp"

#define _QRY_PROF 1

#ifdef _QRY_PROF
#define QRY_PROFILE2(ORCL, STR, QCALL, COMM) {       \
        static int ctx = 0; \
        ORCL.set_name(STR); \
        ctx++; \
        ORCL.set_counter(ctx); \
        std::chrono::steady_clock::time_point tstart, tend; \
        double mpi_start, mpi_end; \
        typedef std::chrono::duration<double, std::milli> duration_t; \
        tstart = std::chrono::steady_clock::now(); \
        mpi_start = MPI_Wtime(); \
        MPI_Barrier((COMM)); \
        (QCALL); \
        tend = std::chrono::steady_clock::now(); \
        MPI_Barrier((COMM)); \
        mpi_end = MPI_Wtime(); \
        report_time(ORCL.prefix_str().c_str(), (COMM), \
                    elapsed_local<duration_t>(tstart, tend), \
                    mpi_end - mpi_start, \
                    std::cout); \
    }

extern double query_timings;
//        MPI_Barrier((COMM));

#define QRY_PROFILE(ORCL, STR, QCALL, COMM) {       \
        std::chrono::steady_clock::time_point tstart, tend; \
        typedef std::chrono::duration<double, std::milli> duration_t; \
        tstart = std::chrono::steady_clock::now(); \
        (QCALL); \
        tend = std::chrono::steady_clock::now(); \
        query_timings += elapsed_local<duration_t>(tstart, tend);  \
    }
#else
#define QRY_PROFILE(ORCL, STR, QCALL, COMM) {    \
        static int ctx = 0; \
        ctx++; \
        ORCL.set_counter(ctx); \
        ORCL.set_name(STR); \
        (QCALL); \
    }
#endif

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t>
void query_isa(const SDSType& sadt,
               const std::vector<SizeType>& queries,
               std::vector<SizeType>& results){
    DataVectorOracle<SizeType, SizeType> isa_access(sadt.isa_vec(),
                                                    sadt.global_size(),
                                                    sadt.global_size());
    QRY_PROFILE(isa_access, "qisa_",
                get_query_results_uniq(isa_access, queries, results),
                sadt.comm());
}

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t,
         typename CharType=typename SDSType::dchar_t>
void query_text(const SDSType& sadt,
               const std::vector<SizeType>& queries,
               std::vector<CharType>& results){
    DataVectorOracle<CharType, SizeType> txt_access(sadt.txt_vec(),
                                                    sadt.global_size(),
                                                    '$');
    QRY_PROFILE(txt_access, "qtxt_",
                get_query_results(txt_access, queries, results),
                sadt.comm());
}

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t,
         typename CountType=typename SDSType::dcount_t,
         typename RMQTable=typename SDSType::drmq_t>
void query_rmq_pair(const SDSType& sadt,
                    const std::vector<SizeType>& queries,
                    std::vector<CountType>& results){
    RMQPairOracle<CountType, SizeType, RMQTable>
        rmq_access(sadt.lcp_vec(), sadt.rmq_table(), sadt.global_size());

    QRY_PROFILE(rmq_access, "qrmq_pair_",
                get_query_results(rmq_access, queries, results),
                sadt.comm());
}

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t,
         typename CountType=typename SDSType::dcount_t,
         typename RMQTable=typename SDSType::drmq_t>
void query_rmq_left(const SDSType& sadt,
                    const std::vector<SizeType>& queries,
                    std::vector<CountType>& results){
    RMQLeftOracle<CountType, SizeType, RMQTable>
        rmq_access(sadt.lcp_vec(), sadt.rmq_table(), sadt.global_size());

    QRY_PROFILE(rmq_access, "qrmq_left_",
                get_query_results_uniq(rmq_access, queries, results),
                sadt.comm());
}

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t,
         typename CountType=typename SDSType::dcount_t,
         typename RMQTable=typename SDSType::drmq_t>
void query_rmq_right(const SDSType& sadt,
                     const std::vector<SizeType>& queries,
                     std::vector<CountType>& results){
    RMQRightOracle<CountType, SizeType, RMQTable>
        rmq_access(sadt.lcp_vec(), sadt.rmq_table(), sadt.global_size());
    QRY_PROFILE(rmq_access, "qrmq_right_",
                get_query_results_uniq(rmq_access, queries, results),
                sadt.comm());
}

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t,
         typename CountType=typename SDSType::dcount_t>
CountType rmq_span_min(const SDSType& sadt,
                       const SizeType& x, const SizeType& y,
                       const CountType& rminx, const CountType& lminy){
    assert(x <= y);
    int px = sadt.owner(x),
        py = sadt.owner(y);
    assert(px <= py);
    if(py == (px + 1) || px == py)
        return std::min(rminx, lminy);
    else
        return std::min(std::min(rminx,
                                 sadt.proc_rmq(px + 1, py - 1)), lminy);
}

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t,
         typename CharType=typename SDSType::dchar_t>
void query_prev(const SDSType& sadt,
               const std::vector<SizeType>& queries,
               std::vector<CharType>& results){

    PrevVectorOracle<CharType, SizeType> prt_access(sadt.txt_vec(),
                                                    sadt.global_size(),
                                                    sadt.prev_char(),
                                                    '$');
    QRY_PROFILE(prt_access, "qry_prev_",
                get_query_results_uniq(prt_access, queries, results),
                sadt.comm());
}

template<typename SDSType,
         typename SizeType=typename SDSType::dsize_t,
         typename CharType=typename SDSType::dchar_t>
void query_next(const SDSType& sadt,
               const std::vector<SizeType>& queries,
               std::vector<CharType>& results){

    NextVectorOracle<CharType, SizeType> nxt_access(sadt.txt_vec(),
                                                    sadt.global_size(),
                                                    sadt.next_char(),
                                                    '$');
    QRY_PROFILE(nxt_access, "qry_next_",
                get_query_results(nxt_access, queries, results),
                sadt.comm());
}


#endif // DIST_QUERY_H
