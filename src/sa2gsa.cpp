#include <iostream>
#include <vector>
#include <fstream>
#include <mpi.h>
#include "parallel_utils.hpp"
#include "psac_rmq.hpp"
#include "suffix_ds.hpp"
typedef psac_rmq<uint32_t, uint64_t> psac_rmq_table_t;

typedef SuffixDS<char, uint64_t, uint32_t, psac_rmq_table_t> suffix_t;

template<typename T, typename S1, typename L1,
         typename S2, typename L2>
void load_files(const std::string& txtf,
                const std::string& saf,
                const std::string& lcpf,
                std::vector<T>& f_txt,
                std::vector<S2>& f_sa,
                std::vector<L2>& f_lcp,
                MPI_Comm comm){
    f_txt.clear(); f_sa.clear(); f_lcp.clear();
    int rank, nproc;
    uint64_t total_size = file_size<S1>(saf);
    std::ifstream safs(saf, std::ios::in | std::ios::binary),
      lcpfs(lcpf, std::ios::in | std::ios::binary),
      txtfs(txtf, std::ios::in | std::ios::binary);

    MPI_Comm_size(comm, &nproc);
    MPI_Comm_rank(comm, &rank);
    uint64_t blk_begin = block_low(rank, nproc, total_size);
    uint64_t local_size = block_size(rank, nproc, total_size);

    safs.seekg(blk_begin * sizeof(S1));
    std::vector<S1> tmp_sa(local_size);
    safs.read((char *) &tmp_sa[0], sizeof(S1) * local_size);
    f_sa.resize(local_size);
    for(uint64_t i = 0; i < local_size; i++)
      f_sa[i] = (S2) tmp_sa[i];
    tmp_sa.clear();

    lcpfs.seekg(blk_begin * sizeof(L1));
    std::vector<L1> tmp_lcp(local_size);
    lcpfs.read((char *) &tmp_lcp[0], sizeof(L1) * local_size);
    f_lcp.resize(local_size);
    for(uint64_t i = 0; i < local_size;i++)
      f_lcp[i] = (L2) tmp_lcp[i];
    tmp_lcp.clear();

    f_txt.resize(local_size);
    txtfs.seekg(blk_begin * sizeof(T));
    txtfs.read((char *) &f_txt[0], sizeof(T) * local_size);
}

int main(int argc, char *argv[]){
    MPI_Init(&argc, &argv);
    std::string pfx = argv[1];
    MPI_Comm comm = MPI_COMM_WORLD;
    int rank, nproc;
    MPI_Comm_size(comm, &nproc);
    MPI_Comm_rank(comm, &rank);
    if(rank == 0)
        std::cout << pfx << std::endl;

    std::string txtf = pfx + ".txt";
    std::string saf = pfx + ".sa";
    std::string lcpf = pfx + ".lcp";
    std::vector<char> vtxt;
    std::vector<uint64_t> vsa, visa;
    std::vector<uint32_t> vlcp;

    load_files<char, uint64_t, uint32_t,
               uint64_t, uint32_t>(txtf, saf, lcpf, vtxt, vsa, vlcp, comm);

    suffix_t sadt(vtxt, vsa, vlcp, file_size<uint64_t>(saf), comm);

    if(rank == 0)
        std::cout << "done" << std::endl;
    return MPI_Finalize();
}
