#ifndef BGZF_FILE_H
#define BGZF_FILE_H

#include <string>
#include <cassert>
#include "htslib/bgzf.h"

class BGZFFile{
    BGZF* _bgzf;
    BGZFFile(BGZF* in):_bgzf(in){ }
public:

    BGZFFile(const char* fpath, const char* idx_path, std::size_t offset = 0){
        _bgzf = bgzf_open(fpath, "r");
        bgzf_index_load(_bgzf, idx_path, NULL);
        assert(_bgzf);
        assert(_bgzf->idx);
        useek(offset);
    }

    ~BGZFFile(){
        bgzf_close(_bgzf);
    }

    inline bool is_not_null(){
        return (_bgzf != NULL);
    }

    inline bool is_valid_idx(){
        return is_not_null() && (_bgzf->idx != NULL);
    }

    inline bool is_valid(){
        return (_bgzf != NULL) && (_bgzf->idx != NULL);
    }

    inline void get_line(std::string& outs){
        kstring_t str = { 0, 0, NULL };

        bgzf_getline(_bgzf, '\n', &str);
        std::string tmp(str.s);
        outs = tmp;
        free(str.s);
    }

    inline bool good(){
        bool is_good = false;
        int64_t cpos = bgzf_tell(_bgzf);

        is_good = (bgzf_getc(_bgzf) >= 0);
        if(is_good)
            bgzf_seek(_bgzf, cpos, SEEK_SET);
        return is_good;
    }

    inline char getc(){
        return (char) bgzf_getc(_bgzf);
    }

    inline bool is_end(std::size_t offset){
        return bgzf_utell(_bgzf) >= (long) offset;
    }

    static long offset_range(BGZF* bgf, long cpos, long step){
        if(!bgf || !bgf->idx)
            return 0;
        cpos = (cpos == 0) ? step : cpos;
        // std::cout << cpos << "::" << std::endl;
        if(bgzf_useek(bgf, cpos, SEEK_SET) < 0)
            return 0;
        while(bgzf_useek(bgf, cpos + step, SEEK_SET) >= 0)
            cpos += step;
        return cpos;
    }

    static long size_mbs(BGZF* bgf){
        if(!bgf || !bgf->idx)
            return 0;
        long gbs = offset_range(bgf,   0,  1000000000);
        long hmb = offset_range(bgf, gbs,   100000000);
        long tmb = offset_range(bgf, hmb,    10000000);
        long mb  = offset_range(bgf, tmb,     1000000);

        long hkb = offset_range(bgf,  mb,      100000);
        long tkb = offset_range(bgf, hkb,       10000);
        long  kb = offset_range(bgf, tkb,        1000);

        long  hb = offset_range(bgf,  kb,         100);
        long  tb = offset_range(bgf,  hb,          10);
        long   b = offset_range(bgf,  tb,           1);
        return b;
    }

    static long size_mbs(const char* fpath, const char* idxfp){
        BGZFFile lbg(fpath, idxfp);
        return lbg.file_size();
    }

    long file_size(){
        return size_mbs(_bgzf);
    }

    int64_t seek(int64_t pos){
        return bgzf_seek(_bgzf, pos, SEEK_SET);
    }

    int64_t useek(int64_t pos){
        return bgzf_useek(_bgzf, pos, SEEK_SET);
    }

};



#endif /* BGZF_FILE_H */
