#include "util.hpp"
#include "AppConfig.hpp"
// using TCLAP for command line parsing
#include <tclap/CmdLine.h>
#include <iomanip>
#include <mpi.h>
#include "bgzf_file.hpp"
const unsigned DEFAULT_PARAMETER_TAU = 15;
const unsigned DEFAULT_PARAMETER_K   = 2;
const unsigned DEFAULT_PARAMETER_BATCH = 128;
const unsigned DEFAULT_PARAMETER_CHUNK = 512;

// PARAMETERS -------------------------------------------------------
void AppConfig::write(std::ostream& ots, int nproc){
    ots << "\"params\"      : {" << std::endl;
    ots << " \t\"in_sa\"    : \"" << sfxf << "\"," << std::endl;
    ots << " \t\"in_files\" : \"" << inpf << "\"," << std::endl;
    ots << " \t\"out_file\" : \"" << outf << "\"," << std::endl;
    ots << " \t\"err_file\" : \"" << errf << "\"," << std::endl;
    ots << " \t\"log_file\" : \"" << logf << "\"," << std::endl;
    ots << " \t\"tau\"      : " << tau << "," << std::endl;
    ots << " \t\"k\"        : " << K << "," << std::endl;
    ots << " \t\"batch factor\"    : " << batch_factor << "," << std::endl;
    ots << " \t\"batch width\"    : " << batch_width << "," << std::endl;
    ots << " \t\"sfx_32bit\"    : " << run32 << "," << std::endl;
    ots << " \t\"stats_only\"    : " << stats_only << "," << std::endl;
    ots << " \t\"gen_pairs\"    : " << gen_pairs << "," << std::endl;
    ots << " \t\"heuristic\"    : " << heuristic << "," << std::endl;
    ots << " \t\"load_gen\"    : " << load_gen << "," << std::endl;
    ots << " \t\"no_mismatch\"    : " << no_mismatch << "," << std::endl;
    ots << " \t\"data_factor\"    : " << data_factor << "," << std::endl;
    ots << " \t\"warmup_cfg\"      : " << warmup << "," << std::endl;
    ots << " \t\"nproc\"    : " << nproc << "," << std::endl;
    ots << " \t\"nthreads\"    : " << nthreads << "," << std::endl;
    ots << " \t\"final\"    : 0" << std::endl;
    ots << " }," << std::endl;
}

void AppConfig::printHelp(std::ostream& ots){
    ots << "Usage : " << app
        << "    -i <input sa/lcp> -o <output file> -g <rlen> [OPTIONS]"
        << std::endl
        << "                       (OR)" << std::endl
        << "        " << app
        << "    -f <input file> -o <output file>  -g <rlen> [OPTIONS]"
        << std::endl;
    ots << "Available options:" << std::endl
        << "\t -i <prefix>   prefix to SA/LCP data files with file extensions :"
        << std::endl
        << "\t                - forward str: .fwd.txt, .fwd.sa, .fwd.lcp "
        << std::endl
        << "\t                - reverse str: .rev.txt, .rev.sa, .rev.lcp "
        << std::endl
        << "\t -f <file>     path to fastq file (NOT SUPPORTED YET)"
        << std::endl
        << "\t -g <length>   read length " << std::endl
        << "\t -o <file>     path to output file (should be writable)" << std::endl
        << "\t -l <file>     path to log file (should be writable) ["
        << "/path/to/output/file.log]" << std::endl
        << "\t -e <file>     path to err file (should be writable) ["
        << "/path/to/output/file.err]" << std::endl
        << "\t -t <tau>      minimum match length ["
        << DEFAULT_PARAMETER_TAU << "]" << std::endl
        << "\t -k <K>        minimum distance between matches ["
        << DEFAULT_PARAMETER_K << "]" << std::endl
        << "\t -h            print this help " << std::endl;
}

AppConfig::AppConfig(int argc, char* argv[]) {
    help = false;
    app = argv[0];

    TCLAP::CmdLine cmd_line("Pair-wise k-mismatch maximal common sub-string");

    // Input prefix/file
    TCLAP::ValueArg<std::string> prefix_arg("i", "prefix",
                                            "Prefix of TXT/SA/LCP files", true,
                                            "", "/path/to/file_prefix.txt|sa|lcp");
    TCLAP::ValueArg<std::string> input_arg("f", "input",
                                           "Input fastq file (NOT USED currently)",
                                           true, "", "/path/to/input_fastq");
    cmd_line.xorAdd(prefix_arg, input_arg);

    // Ouput prefix/file
    TCLAP::ValueArg<std::string>
        output_arg("o", "output", "Output fastq file (NOT USED currently)",
                   true, "", "/path/to/output_fastq");
    cmd_line.add(output_arg);

    // Error file
    TCLAP::ValueArg<std::string>
        errf_arg("e", "error", "Error output file (NOT USES currently)",
                 false, "", "/path/to/out_error");
    cmd_line.add(errf_arg);

    // Log file
    TCLAP::ValueArg<std::string>
        logf_arg("l", "log", "Log output file (NOT USES currently)",
                 false, "", "/path/to/out_log");
    cmd_line.add(logf_arg);

    // Tau value
    TCLAP::ValueArg<unsigned> tau_arg("t", "tau",
                                      "Tau input. Commonly, ciel(L/k+1)",
                                      false, DEFAULT_PARAMETER_TAU, "tau");
    cmd_line.add(tau_arg);

    // K value
    TCLAP::ValueArg<unsigned> k_arg("k", "mismatch",
                                    "No. of mismatches allowed",
                                    false, DEFAULT_PARAMETER_K, "no_mismatch");
    cmd_line.add(k_arg);

    // factor
    TCLAP::ValueArg<size_t>
        batch_arg("b", "batch",
                  "Use to compute batch_width = total_wt/(p * batch_factor).",
                  false, DEFAULT_PARAMETER_BATCH,
                  "batch_factor");
    cmd_line.add(batch_arg);

    // chunk size
    TCLAP::ValueArg<size_t>
        chunk_arg("c", "chunk",
                  "Chunk size for dynamic allocation of internal nodes.",
                  false, DEFAULT_PARAMETER_CHUNK,
                  "batch_factor");
    cmd_line.add(chunk_arg);

    // read length
    TCLAP::ValueArg<unsigned>
        rlen_arg("r", "read",
                 "Length of the reads (assuming const length reads)",
                 true, 0, "read_length");
    cmd_line.add(rlen_arg);

    // run 32
    TCLAP::SwitchArg
        run32_arg("2", "32bit",
                  "Use \"unsigned\" integer for SA/LCP", false);
    cmd_line.add(run32_arg);

    // Generate SA/LCP
    TCLAP::SwitchArg load_arg("d", "gen_sa",
                              "Don't load SA/LCP. Construct SA/LCP from input",
                              false);
    cmd_line.add(load_arg);

    // Generate maximal
    TCLAP::SwitchArg gmax_arg("x", "maximal",
                              "Generate the maximal pairs", false);
    cmd_line.add(gmax_arg);

    // Generate pairs
    TCLAP::SwitchArg gpairs_arg("p", "pairs",
                                "Generate all occurence pairs", false);
    cmd_line.add(gpairs_arg);

    // stats only
    TCLAP::SwitchArg stats_arg("s", "stats_only",
                               "Generate only statistics of the GST", false);
    cmd_line.add(stats_arg);

    // add heuristics
    TCLAP::SwitchArg heuristic_arg("u", "heuristic",
                                   "Use the heuristic pipline", false);
    cmd_line.add(heuristic_arg);

    // No mismatch Path
    TCLAP::SwitchArg nomismatch_arg("0", "zero_mismatch",
                                    "Use the zero mismatch pipeline", false);
    cmd_line.add(nomismatch_arg);

    // dynamic only
    TCLAP::SwitchArg dynamic_arg("y", "dynamic",
                                 "Dynamic work distribution", false);
    cmd_line.add(dynamic_arg);

    // num of threads
    TCLAP::ValueArg<unsigned>
        workers_arg("w", "workers",
                    "number of threads to use for dynamic",
                    false, 16, "read_length");
    cmd_line.add(workers_arg);

    // read length
    TCLAP::ValueArg<unsigned>
        dataf_arg("g", "data_factor",
                 "Use only the top g % of input data. Min 0.1.",
                  false, 100, "data_factor");
    cmd_line.add(dataf_arg);

    // Warmup flag
    TCLAP::SwitchArg warmup_arg("m", "warmup",
                                "Do MPI Warmup", false);
    cmd_line.add(warmup_arg);

    // parse and get values
    cmd_line.parse(argc, argv);
    sfxf = prefix_arg.getValue();
    inpf = input_arg.getValue();
    outf = output_arg.getValue();
    errf = errf_arg.getValue();
    logf = logf_arg.getValue();
    tau = tau_arg.getValue();
    K = k_arg.getValue();
    batch_factor = batch_arg.getValue();
    chunk_size = chunk_arg.getValue();
    // if (chunk_size <= DEFAULT_PARAMETER_CHUNK)
    if (chunk_size <= 0)
        chunk_size = DEFAULT_PARAMETER_CHUNK;
    read_length = rlen_arg.getValue();
    run32 = run32_arg.getValue();
    load_gen = load_arg.getValue();
    gen_maximal = gmax_arg.getValue();
    gen_pairs = gpairs_arg.getValue();
    stats_only = stats_arg.getValue();
    heuristic = heuristic_arg.getValue();
    no_mismatch = nomismatch_arg.getValue();
    dynamic  = dynamic_arg.getValue();
    nthreads = workers_arg.getValue();
    data_factor = dataf_arg.getValue();
    if(data_factor > 100) data_factor = 100;
    if(data_factor <= 10) data_factor = 10;
    warmup = warmup_arg.getValue();

    if(K == 0)
        no_mismatch = true;
}

bool AppConfig::test_comm(bool value, MPI_Comm comm){
    int test_val = (value) ? 1 : 0, gfail;
    MPI_Allreduce(&test_val, &gfail, 1, MPI_INT, MPI_MIN, comm);
    return (gfail == 0) ? false : true;
}

bool AppConfig::validateIO(int rank, MPI_Comm comm, std::ostream& ots){
    bool validCfg = true;
    if(outf.length() > 0){
        std::stringstream ossr;
        ossr << outf << "."
            << std::setw(6) << std::setfill('0') << rank
            << std::setfill(' ') << ".out";
        outf = ossr.str();
        ofs.open(outf, std::ofstream::out);
        if(!test_comm(ofs.is_open(), comm)){
            if(rank == 0)
                ots << "Error opening output file : " << outf << std::endl;
            validCfg = false;
        }
    }

    /*
    if(errf.length() == 0)
        errf = outf;
    std::stringstream ossx;
    ossx << outf << "."
         << std::setw(6) << std::setfill('0') << rank
         << std::setfill(' ') << ".err";
    errf = ossx.str();
    efs.open(errf, std::ofstream::out);
    if(!test_comm(efs.is_open(), comm)){
        if(rank == 0)
            ots << "Error opening output file : " << errf << std::endl;
        validCfg = false;
    }

    if(logf.length() == 0)
        logf = outf;
    std::stringstream ossy;
    ossy << outf << "."
         << std::setw(6) << std::setfill('0') << rank
         << std::setfill(' ') << ".log";
    logf = ossy.str();
    lfs.open(logf, std::ofstream::out);
    if(!test_comm(lfs.is_open(), comm)){
        if(rank == 0)
            ots << "Error opening output file : " << logf << std::endl;
        validCfg = false;
    }
    */
    if(sfxf.length() > 0){
        txtfn = sfxf + ".fwd.txt";
        safn = sfxf + ".fwd.sa";
        lcpfn = sfxf + ".fwd.lcp";
        rtxtfn = sfxf + ".rev.txt";
        rsafn = sfxf + ".rev.sa";
        rlcpfn = sfxf + ".rev.lcp";
        std::ifstream safs(safn, std::ios::in | std::ios::binary),
            lcpfs(lcpfn, std::ios::in | std::ios::binary),
            txtfs(txtfn, std::ios::in | std::ios::binary);
        std::ifstream rsafs(rsafn, std::ios::in | std::ios::binary),
            rlcpfs(lcpfn, std::ios::in | std::ios::binary),
            rtxtfs(txtfn, std::ios::in | std::ios::binary);
        if(!test_comm(txtfs.is_open(), comm)){
            if(rank == 0)
                ots << "Cant open : " << txtfn << std::endl;
            validCfg = false;
        }
        if(!load_gen && !test_comm(safs.is_open(), comm)){
            if(rank == 0)
                ots << "Cant open : " << safn << std::endl;
            validCfg = false;
        }
        if(!load_gen && !test_comm(lcpfs.is_open(), comm)){
            if(rank == 0)
                ots << "Cant open : " << lcpfn << std::endl;
            validCfg = false;
        }
        if(!test_comm(rtxtfs.is_open(), comm)){
            if(rank == 0)
                ots << "Cant open : " << rtxtfn << std::endl;
            validCfg = false;
        }
        if(!load_gen && !test_comm(rsafs.is_open(), comm)){
            if(rank == 0)
                ots << "Cant open : " << rsafn << std::endl;
            validCfg = false;
        }
        if(!load_gen && !test_comm(rlcpfs.is_open(), comm)){
            if(rank == 0)
                ots << "Cant open : " << rlcpfn << std::endl;
            validCfg = false;
        }
    }

    // validate input fastq or fasta file
    if(inpf.length() > 0){
        load_gen = true;
        std::string idxf = inpf + ".gzi";
        std::ifstream infs(inpf, std::ios::in),
            idxfs(idxf, std::ios::in);
        if( !ends_with(inpf, ".fasta") && !ends_with(inpf, ".fa") &&
            !ends_with(inpf, ".fastq") && !ends_with(inpf, ".fq") &&
            !ends_with(inpf, ".fasta.gz") && !ends_with(inpf, ".fa.gz") &&
            !ends_with(inpf, ".fastq.gz") && !ends_with(inpf, ".fq.gz") ){
            if(rank == 0)
                ots << "Invalid extension : [" << inpf << "]. "
                    << "Extensions should be one of "
                    << " .fa/.fasta.gz/.fq/.fastq.gz "
                    << std::endl;
        }
        if( !test_comm(infs.is_open(), comm) ){
            if(rank == 0)
                ots << "Cant open input file : " << inpf << std::endl;
            validCfg = false;
        }
        if(ends_with(inpf, ".gz") &&  !test_comm(idxfs.is_open(), comm)){
            if(rank == 0)
                ots << "Cant open index file : " << inpf << std::endl;
            validCfg = false;
        }
        if(ends_with(inpf, ".gz") && !bgzf_is_bgzf(inpf.c_str())) {
            if(rank == 0)
                ots << "Invalid bgzf file : " << inpf << std::endl;
            validCfg = false;
        }
        if(ends_with(inpf, ".gz") && test_comm(infs.is_open(), comm) &&
           test_comm(idxfs.is_open(), comm)) {
            BGZFFile bgf(inpf.c_str(), idxf.c_str());
            if(!bgf.is_valid() && !bgf.is_valid_idx()){
                if(rank == 0)
                    ots << "Invalid bgzf index  : " << idxf << std::endl;
                validCfg = false;
            }
        }
    }
    return validCfg;
}

bool AppConfig::validateParams(int rank, std::ostream& ots){
    bool validCfg = true;
    if(sfxf.length() == 0 && inpf.length() == 0){
        if(rank == 0){
            ots << "Invalid input files " << sfxf << " (OR) "
                << inpf << std::endl;
            // ots << "Input file not yet supported." << std::endl;
        }
        validCfg = false;
    }

    if(outf.length() == 0){
        if(rank == 0)
            ots << "Invalid output file " << outf << std::endl;
        validCfg = false;
    }

    if(tau == 0){
        if(rank == 0)
            ots << "Invalid parameter for tau " << tau << std::endl;
        validCfg = false;
    }

    // if(K == 0){
    //     if(rank == 0)
    //         ots << "Invalid parameter for K " << K << std::endl;
    //     validCfg = false;
    // }

    if(batch_factor == 0){
        if(rank == 0)
            ots << "Invalid parameter for batch size "
                << batch_factor << std::endl;
        validCfg = false;
    }

    if(read_length == 0){
        if(rank == 0)
            ots << "Invalid parameter for read length "
                << read_length << std::endl;
        validCfg = false;
    }

    return validCfg;
}

bool AppConfig::validate(int rank, MPI_Comm comm, std::ostream& ots){
    bool validCfg = true;
    if(help){
        if(rank == 0)
            printHelp(ots);
        return false;
    }
    // validate parmeters
    bool validPrm = validateParams(rank, ots);
    // valiate ouput files
    validCfg = validateIO(rank, comm, ots) && validPrm;

    if(!validCfg){
        if(rank == 0)
            printHelp(ots);
        ots << "...exiting" << std::endl;
    }
    return validCfg;
}

AppConfig::~AppConfig(){
    if(ofs.good() && ofs.is_open())
        ofs.close();

  /*
    if(efs.good() && efs.is_open())
        efs.close();
    if(lfs.good() && lfs.is_open())
        lfs.close();

    std::remove(outf.c_str());
    std::remove(errf.c_str());
    std::remove(logf.c_str());
  */
}
