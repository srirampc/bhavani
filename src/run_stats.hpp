#ifndef RUN_STATS_H
#define RUN_STATS_H

#include <fstream>
#include <chrono>
#include <mpi.h>
#include <iostream>

class RunStats{
    std::chrono::steady_clock::time_point
        total_start, curr_start, curr_stop,
        batch_loop;
    std::chrono::steady_clock::time_point trie_start, trie_stop;
    double forward_runs, reverse_runs;
    double constr_fwd, constr_rev;
    double fwd_runs_batch, rev_runs_batch;
    double region_build, root_nodes;
    double root_schedule, batch_loop_run;
    double  mpi_trie_start, trie_build, mpi_trie_build;

    double phase_time, min_time,  max_time, avg_time;
    MPI_Comm _comm;
    int rank, nproc;
    int nfwd, nrev;

    static int mem_report_count;

    double mpi_total_start, mpi_curr_start, mpi_curr_stop;
    double mpi_forward_runs, mpi_reverse_runs;
    double mpi_constr_fwd, mpi_constr_rev;
    double mpi_fwd_runs_batch, mpi_rev_runs_batch;
    double mpi_region_build, mpi_root_nodes;
    double mpi_root_schedule, mpi_batch_loop_run;
    double mpi_batch_loop;

    double mpi_phase_time;

    double trie_internal_size, trie_internal_width;

public:

    inline MPI_Comm comm() { return _comm; }
    inline int comm_rank() { return rank; }
    inline int comm_size() { return nproc; }

    RunStats(MPI_Comm cx);
    void record_start();
    void record_stop();
    void record_trie_start();
    void record_trie_stop(double sz = 0.0,
                          double tw = 0.0);
    void reset_trie_build();
    void report_time(const char* phase_name,
                     std::ostream& ofs);
    void construct_fwd(std::ostream& ofs);
    void construct_rev(std::ostream& ofs);
    void root_nodes_time(std::ostream& ofs);
    void root_schedule_time(std::ostream& ofs);
    void select_region_time(std::ostream& ofs);
    void batch_loop_start();
    void batch_loop_stop(std::ostream& ofs);
    void forward_run(std::ostream& ofs, bool in_batch);
    void reverse_run(std::ostream& ofs, bool in_batch);
    void get_minmax_timings();
    void reduce_timings();
    void finalize(std::ostream& ofs);
    void mem_report(std::ostream& ofs);
    void report_all(const char* phase_name,
                    double rtime, double btime,
                    std::ostream& ofs);
};

#endif /* RUN_STATS_H */
