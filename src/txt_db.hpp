#ifndef TXT_DB_H
#define TXT_DB_H
#include "align.hpp"
#include "util.hpp"
#include "fastq_reader.hpp"

template<typename CharType,
         typename SizeType,
         typename CountType>
class TextDB{
private:
    std::vector<CharType> m_txt;
    CountType m_rlen;
public:
    typedef StreamWrapper<std::ifstream> text_stream_t;
    typedef FastaReader<text_stream_t>  fasta_txt_t;
    typedef FastqReader<text_stream_t>  fastq_txt_t;

    TextDB(std::vector<CharType>& txt,
           CharType sep,
           CountType rlen){
        m_txt.swap(txt);
        for(SizeType i = 0, j = 0; i < m_txt.size(); i++){
            if(m_txt[i] == sep)
                continue;
            m_txt[j++] = m_txt[i];
        }
        m_rlen = rlen;
    }

    TextDB(std::string txtfn, CountType rlen){
        ReadStore<CharType, SizeType> rstore('$');
        SizeType txfz = file_size<CharType>(txtfn);
        //
        if(ends_with(txtfn, std::string(".fa")) ||
           ends_with(txtfn, std::string(".fasta"))){
            text_stream_t fa_strm(txtfn.c_str(), 0);
            // load the reads
            read_batch<CharType, SizeType,
                       text_stream_t, fasta_txt_t>(fa_strm, txfz,
                                                   txfz, rstore);
        } else if(ends_with(txtfn, std::string(".fq")) ||
                  ends_with(txtfn, std::string(".fastq"))){
            text_stream_t fq_strm(txtfn.c_str(), 0);
            // load the reads
            read_batch<CharType, SizeType,
                       text_stream_t, fastq_txt_t>(fq_strm, txfz,
                                                   txfz, rstore);
        }
        m_txt.swap(rstore.readsString);
        m_rlen = rlen;
    }

    TextDB(){

    }

    void swap(TextDB& other){
        m_txt.swap(other.m_txt);
        m_rlen = other.m_rlen;
    }

    template<typename ReadBoundsChecker>
    CountType match_txt_right(SizeType xstart, SizeType ystart,
                              CountType kval,
                              ReadBoundsChecker& read_bounds) const{
        CountType howfar = 0, kdx = 0;
        if(xstart >= m_txt.size() || ystart >= m_txt.size())
            return howfar;
        while(!read_bounds(ystart + howfar) &&
              !read_bounds(xstart + howfar)){
            if(m_txt[xstart + howfar] != m_txt[ystart + howfar]){
                kdx += 1;
            }

            if(kval < (int)kdx)
                break;
            howfar++;
            if(xstart+howfar >= m_txt.size() ||
               ystart+howfar >= m_txt.size())
              break;
        }
        return howfar;
    }

    template<typename ReadBoundsChecker>
    CountType match_txt_left(SizeType xstart, SizeType ystart,
                             CountType kval,
                             ReadBoundsChecker& read_bounds,
                             CountType cdepth = 0) const{
        CountType kdx = 0, howfar = cdepth;
        if(xstart >= (m_txt.size() + (SizeType)howfar) ||
           ystart >= (m_txt.size() + (SizeType)howfar))
            return howfar;
        while(xstart >= howfar &&
              ystart >= howfar &&
              !read_bounds(ystart - howfar) &&
              !read_bounds(xstart - howfar)){
            if(m_txt[xstart - howfar] != m_txt[ystart - howfar]){
                kdx += 1;
            }

            if(kval < (int)kdx)
                break;
            howfar++;
            if(xstart >= (m_txt.size() + (SizeType)howfar) ||
               ystart >= (m_txt.size() + (SizeType)howfar))
              break;
        }
        return howfar - cdepth;
    }

    template<typename ReadBoundsChecker>
    CountType align(SizeType xsuf, SizeType ysuf,
               ReadBoundsChecker& read_bounds) const{
      CountType howfar = 0;
      SizeType rx = read_bounds.read_id(xsuf);
      SizeType ry = read_bounds.read_id(ysuf);
      SizeType xpos = rx * read_bounds.read_length;
      SizeType ypos = ry * read_bounds.read_length;

      if(xpos + read_bounds.read_length >= m_txt.size() ||
         ypos + read_bounds.read_length >= m_txt.size())
          return howfar;
      std::vector< std::vector<int> > score(read_bounds.read_length);
      for(int i = 0; i < score.size(); i++)
          score[i].resize(score.size());
      for(int i = 0; i < score.size(); i++)
          score[0][i] = -3 * i;
      for(int i = 0; i < score.size(); i++)
          score[i][0] = -3 * i;
      for(int i = 1; i < score.size(); i++)
          for(int j = 1; j < score.size(); j++)
              score[i][j] = std::max(score[i - 1][j - 1] +
                                     (m_txt[xpos + i] == m_txt[ypos + j] ? 1 : 0),
                                     std::max(score[i - 1][j]  - 3,
                                              score[i][j - 1] - 3));

      // int xhowfar = 0;
      // bool isLow = false;
      // align_band_with_traceback(((char*)&m_txt[0]) + xpos, read_bounds.read_length,
      //                           ((char*)&m_txt[0]) + ypos, read_bounds.read_length,
      //                           xhowfar, isLow, std::cout, 10);
      howfar = (CountType) score.back().back();
      return howfar;
    }
};

#endif // TXT_DB_H */
