#ifndef QUERY_ORACLE_H
#define QUERY_ORACLE_H

#include <vector>
#include <mpi.h>
#include "mpi_util.hpp"


template<typename ValueType, typename SizeType>
class DataVectorOracle {
 protected :
    const std::vector<ValueType>& local_data;
    SizeType dist_size;
    int rank, nproc;
    MPI_Comm _comm;
    SizeType m_begin, m_end;
    ValueType default_value;
    std::string m_query_name;
    volatile int m_ctx;
 public:
    typedef SizeType size_type;
    typedef ValueType value_type;

    DataVectorOracle(const std::vector<ValueType>& ldat, const SizeType& gsz,
                     ValueType dvalue = 0,
                     MPI_Comm cx = MPI_COMM_WORLD):
        local_data(ldat), dist_size(gsz), _comm(cx){
        MPI_Comm_rank(_comm, &rank);
        MPI_Comm_size(_comm, &nproc);
        default_value = dvalue;
        m_begin = block_low(rank, nproc, dist_size);
        m_end = block_high(rank, nproc, dist_size);
        set_name("data_vector_");
    }

    virtual inline ValueType at(const SizeType& query) const{
        return in_range(query) ? local_data[query - m_begin] : default_value;
    }

    virtual void get(std::vector<SizeType>& queries,
                     std::vector<ValueType>& results) const{
        results.resize(queries.size());
        for(SizeType i = 0; i < (SizeType)queries.size(); i++){
            results[i] = at(queries[i]);
        }
    }

    virtual inline SizeType size() const{
        return (SizeType)local_data.size();
    }

    virtual inline SizeType global_size() const{
        return dist_size;
    }

    // the owning process
    virtual inline int owner(const SizeType& x) const{
        return (x >= global_size()) ? rank
            : block_owner(x, global_size(), nproc);
    }

    virtual inline int comm_size() const{
        return nproc;
    }

    virtual inline int comm_rank() const{
        return rank;
    }

    virtual inline MPI_Comm comm() const{
        return _comm;
    }

    inline bool in_range(const SizeType& sdx) const{
        return (m_begin <= sdx) && (sdx <= m_end);
    }

    inline SizeType relative_index(const SizeType& sdx) const{
        return in_range(sdx) ? (sdx - m_begin) : size();
    }

    inline virtual std::string prefix_str() const{
      std::stringstream oss;
      oss << name() << counter();
      return oss.str();
    }

    inline virtual std::string name() const{
      return m_query_name;
    }

    inline virtual void set_name(std::string qname){
      m_query_name = qname;
    }

    inline virtual void set_counter(int ctx){
      m_ctx = ctx;
    }

    inline virtual int counter() const{
      return m_ctx;
    }
};

template<typename ValueType, typename SizeType>
class PrevVectorOracle : public DataVectorOracle<ValueType, SizeType>{
protected:
    ValueType prev_value;
public:
    PrevVectorOracle(const std::vector<ValueType>& ldat, const SizeType& gsz,
                     ValueType pval = 0, ValueType dvalue = 0,
                     MPI_Comm cx = MPI_COMM_WORLD)
        : DataVectorOracle<ValueType, SizeType>(ldat, gsz, dvalue, cx){
        prev_value = pval;
        this->set_name("prev_vector_");
    }

    virtual void get(std::vector<SizeType>& queries,
                     std::vector<ValueType>& results) const{
        results.resize(queries.size());
        for(SizeType i = 0; i < (SizeType)queries.size(); i++){
            results[i] = queries[i] > this->m_begin ?
                this->at(queries[i] - 1) : prev_value;
        }
    }
};

template<typename ValueType, typename SizeType>
class NextVectorOracle : public DataVectorOracle<ValueType, SizeType>{
protected:
    ValueType next_value;
public:
    NextVectorOracle(const std::vector<ValueType>& ldat, const SizeType& gsz,
                     ValueType nval = 0, ValueType dvalue = 0,
                     MPI_Comm cx = MPI_COMM_WORLD)
        : DataVectorOracle<ValueType, SizeType>(ldat, gsz, dvalue, cx){
        next_value = nval;
        this->set_name("next_vector_");
    }

    virtual void get(std::vector<SizeType>& queries,
                     std::vector<ValueType>& results) const{
        results.resize(queries.size());
        for(SizeType i = 0; i < (SizeType)queries.size(); i++){
            results[i] = queries[i] < this->m_end ?
                this->at(queries[i] + 1) : next_value;
        }
    }

};

template< typename ValueType,
          typename SizeType,
          typename RMQTable>
class RMQOracle {
 public:
    typedef SizeType size_type;
    typedef ValueType value_type;

    RMQOracle(const std::vector<ValueType>& ldat, const RMQTable& lrmqt,
                const SizeType& gsz, MPI_Comm cx = MPI_COMM_WORLD):
        local_data(ldat), local_rmqt(lrmqt), dist_size(gsz), _comm(cx){
        MPI_Comm_rank(_comm, &rank);
        MPI_Comm_size(_comm, &nproc);
        m_begin = block_low(this->rank, this->nproc, this->dist_size);
        m_end = block_high(this->rank, this->nproc, this->dist_size);
    }

    inline SizeType size() const{
        return (SizeType)local_data.size();
    }

    inline SizeType global_size() const{
        return dist_size;
    }

    // the owning process
    inline int owner(const SizeType& x) const{
        return (x >= dist_size) ? rank
            : block_owner(x, dist_size, nproc);
    }

    inline int comm_size() const{
        return nproc;
    }

    inline int comm_rank() const{
        return rank;
    }

    MPI_Comm comm() const{
        return _comm;
    }

    inline bool in_range(const SizeType& sdx) const{
        return (m_begin <= sdx) && (sdx <= m_end);
    }

    inline SizeType relative_index(const SizeType& sdx) const{
        return in_range(sdx) ? (sdx - m_begin) : size();
    }

    virtual std::string prefix_str() const{
      std::stringstream oss;
      oss << name() << counter();
      return oss.str();
    }

    virtual std::string name() const{
      return m_query_name;
    }

    virtual void set_name(std::string qname){
      m_query_name = qname;
    }

    inline virtual void set_counter(int ctx){
      m_ctx = ctx;
    }

    inline virtual int counter() const{
      return m_ctx;
    }

protected:
    const std::vector<ValueType>& local_data;
    const RMQTable& local_rmqt;
    const SizeType dist_size;
    int rank, nproc;
    MPI_Comm _comm;
    SizeType m_begin, m_end;
    std::string m_query_name;
    int m_ctx;
};


template<typename ValueType,
         typename SizeType,
         typename RMQTable>
class RMQPairOracle: public RMQOracle<ValueType, SizeType, RMQTable>{

 public:
    // constructor
    RMQPairOracle(const std::vector<ValueType>& ldat, const RMQTable& lrmqt,
                    const SizeType& gsz):
        RMQOracle<ValueType, SizeType, RMQTable>(ldat, lrmqt, gsz){
        this->set_name("rmq_pair");
    }


    void get(std::vector<SizeType>& queries,
             std::vector<ValueType>& results) const{
        if(queries.size() == 0)
            return;
        results.resize(queries.size());
        results[0] = 0;
        SizeType x = this->relative_index(queries[0]);
        for(SizeType i = 1; i < (SizeType)queries.size(); i++){
            SizeType y = this->relative_index(queries[i]);
            if(y < this->local_data.size() && x <= y && x >= 0) {
                results[i] =
                    this->local_data[this->local_rmqt(x == y ? x : (x + 1), y)];
            }
            else {
                results[i] = 0;
            }
            x = y;
        }
    }

};


template<typename ValueType,
         typename SizeType,
         typename RMQTable>
class RMQLeftOracle: public RMQOracle<ValueType, SizeType, RMQTable>{
 public:
    // constructor
    RMQLeftOracle(const std::vector<ValueType>& ldat, const RMQTable& lrmqt,
                    const SizeType& gsz):
        RMQOracle<ValueType, SizeType, RMQTable>(ldat, lrmqt, gsz){
        this->set_name("rmq_left");
    }

    void get(std::vector<SizeType>& queries,
             std::vector<ValueType>& results) const{
        results.resize(queries.size());

        for(SizeType i = 0; i < (SizeType)queries.size(); i++){
            if(this->in_range(queries[i]) == false)
                continue;

            SizeType x = queries[i] - this->m_begin;
            auto rq = this->local_rmqt(0,x);
            results[i] = this->local_data[rq];
        }
    }
};


template<typename ValueType,
         typename SizeType,
         typename RMQTable>
class RMQRightOracle: public RMQOracle<ValueType, SizeType, RMQTable>{
 public:
    // constructor
    RMQRightOracle(const std::vector<ValueType>& ldat, const RMQTable& lrmqt,
                     const SizeType& gsz):
        RMQOracle<ValueType, SizeType, RMQTable>(ldat, lrmqt, gsz){
        this->set_name("rmq_left");
    }

    void get(std::vector<SizeType>& queries,
             std::vector<ValueType>& results) const{
        results.resize(queries.size());
        for(SizeType i = 0; i < (SizeType)queries.size(); i++){
            if(this->in_range(queries[i]) == false)
                continue;

            SizeType x = queries[i] - this->m_begin;
            auto rq = this->local_rmqt(x, this->local_data.size() - 1);
            results[i] = this->local_data[rq];
        }
    }
};


#endif // QUERY_ORACLE_H
