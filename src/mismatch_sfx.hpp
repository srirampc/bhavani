#ifndef MISMATCH_SFX_H
#define MISMATCH_SFX_H

#include <vector>
#include "AppConfig.hpp"
#include "run_stats.hpp"
#include "suffix_ds.hpp"
#include "trie_ds.hpp"
#include "gen_pairs.hpp"
#include "dist_query.hpp"
#include "schedule.hpp"
#include "serial_ds.hpp"
#include "work_distribution.hpp"

template< bool stats_report, bool mem_report,
          typename ConfigType, typename StatsMonitor,
          typename SDSType,
          typename NodesType,
          typename SfxTrieType=SuffixTrieDS<SDSType>,
          typename SfxTrieNodesType=TrieInternalNodeDS<SDSType> >
class MismatchSuffixTries{
    SDSType& sadt;
    SDSType& rev_sadt;
    NodesType& batch_nodes;
    ConfigType& param;
    StatsMonitor& rstats;

public:
    std::vector<SfxTrieType*> trie_sa;
    std::vector<SfxTrieNodesType*> trie_nodes;
    std::vector<SfxTrieType*> rev_trie_sa;
    std::vector<SfxTrieNodesType*> rev_trie_nodes;
    typedef typename SDSType::dcount_t  dcount_t;
    typedef typename SDSType::dsize_t  dsize_t;


public:

    MismatchSuffixTries(SDSType& sa, SDSType& rsa, NodesType& bn,
                        ConfigType& cfg, StatsMonitor& rs)
        : sadt(sa), rev_sadt(rsa), batch_nodes(bn), param(cfg), rstats(rs) {

        trie_sa.resize(param.K, nullptr);
        trie_nodes.resize(param.K, nullptr);
        rev_trie_sa.resize(param.K, nullptr);
        rev_trie_nodes.resize(param.K, nullptr);
    }

    template< typename PairGenerator=DefaultPairGenerator<SDSType, SfxTrieNodesType> >
    void extend_right(PairGenerator& pgen){
        if(stats_report) rstats.record_start();

        DefaultBoundsChecker<unsigned, dsize_t> rcheck(param.read_length);
        for(int idx = 0; idx < (int)param.K; idx++) {
            // 1. Suffix trie of extended mis-matches
            if(idx == 0){
                trie_sa[idx] = new SfxTrieType(sadt, batch_nodes,
                                               rcheck, true);
            } else {
                trie_sa[idx] = new SfxTrieType(sadt, *(trie_nodes[idx - 1]),
                                               rcheck, true);
            }
            trie_sa[idx]->load_skip();

            if(stats_report && mem_report) rstats.mem_report(std::cout);

            // 2. Internal nodes of the suffix tries
            if(stats_report) rstats.record_trie_start();

            trie_nodes[idx] = new SfxTrieNodesType(sadt, *trie_sa[idx]);
            if(stats_report)
                rstats.record_trie_stop(trie_nodes[idx]->size(),
                                        trie_nodes[idx]->total_width());

        }

        pgen.generate(*(trie_nodes.back()), param.ofs);

        if(stats_report) rstats.forward_run(std::cout, batch_nodes.size() > 0);
        if(stats_report && mem_report) rstats.mem_report(std::cout);
    }

    template< typename PairGenerator=DefaultPairGenerator<SDSType, SfxTrieNodesType> >
    void extend_right_left(PairGenerator& pgen){
        // assume that all left extensions are done
        delete trie_nodes[param.K - 1];
        delete trie_sa[param.K - 1];
        trie_nodes[param.K - 1] = nullptr;
        trie_sa[param.K - 1] = nullptr;

        if(stats_report) rstats.record_start();
        DefaultBoundsChecker<unsigned, dsize_t> rcheck(param.read_length);
        for(int idx = 1; idx < (int)param.K; idx++){
            SfxTrieNodesType& fwd_nodes = *trie_nodes[param.K - idx - 1];
            for(int rdx = 0; rdx < idx; rdx++){
                // 1. Suffix trie of extended mis-matches
                if(rdx == 0){
                    rev_trie_sa[rdx] = new SfxTrieType(rev_sadt, fwd_nodes,
                                                       rcheck, false);
                } else {
                    rev_trie_sa[rdx] = new SfxTrieType(rev_sadt,
                                                       *(rev_trie_nodes[rdx - 1]),
                                                       rcheck, true);
                }
                rev_trie_sa[rdx]->load_skip();

                // 2. Internal nodes of the suffix tries
                if(stats_report) rstats.record_trie_start();
                rev_trie_nodes[rdx] = new SfxTrieNodesType(rev_sadt,
                                                           *rev_trie_sa[rdx]);
                if(stats_report)
                    rstats.record_trie_stop(rev_trie_nodes[rdx]->size(),
                                            rev_trie_nodes[rdx]->total_width());
            }
            pgen.generate(*rev_trie_nodes[idx - 1], param.ofs);

            if(stats_report && mem_report)
                rstats.mem_report(std::cout);
            for(int rdx = 0; rdx < idx; rdx++){
                delete rev_trie_sa[rdx];
                delete rev_trie_nodes[rdx];
                rev_trie_sa[rdx] = nullptr;
                rev_trie_nodes[rdx] = nullptr;
            }
            delete trie_nodes[param.K - idx - 1];
            delete trie_sa[param.K - idx - 1];
            trie_nodes[param.K - idx - 1] = nullptr;
            trie_sa[param.K - idx - 1] = nullptr;
        }
        if(stats_report) rstats.reverse_run(std::cout, batch_nodes.size() > 0);
    }

    template< typename PairGenerator=DefaultPairGenerator<SDSType, SfxTrieNodesType> >
    void extend_left(PairGenerator& pgen){
        // final reverse run
        if(stats_report) rstats.record_start();
        DefaultBoundsChecker<unsigned, dsize_t> rcheck(param.read_length);

        for(int idx = 0; idx < (int)param.K; idx++) {
            // 1. Suffix trie of extended mis-matches
            if(idx == 0){
                rev_trie_sa[idx] = new SfxTrieType(rev_sadt, batch_nodes,
                                                   rcheck, false);
            } else {
                rev_trie_sa[idx] = new SfxTrieType(rev_sadt,
                                                   *(rev_trie_nodes[idx - 1]),
                                                   rcheck, true);
            }
            rev_trie_sa[idx]->load_skip();

            // 2. Internal nodes of the suffix tries
            if(stats_report) rstats.record_trie_start();
            rev_trie_nodes[idx] = new SfxTrieNodesType(rev_sadt,
                                                       *rev_trie_sa[idx]);
            if(stats_report)
                rstats.record_trie_stop(rev_trie_nodes[idx]->size(),
                                        rev_trie_nodes[idx]->total_width());
        }
        // process the trie nodes
        pgen.generate(*(rev_trie_nodes.back()), param.ofs);

        if(stats_report) rstats.reverse_run(std::cout, batch_nodes.size() > 0);
        if(stats_report && mem_report)
            rstats.mem_report(std::cout);
    }

    template< typename PairGenerator=DefaultPairGenerator<SDSType, SfxTrieNodesType> >
    void lr_extend(PairGenerator& pgen){
        extend_right<PairGenerator>(pgen);
        pgen.set_forward(false);
        extend_right_left<PairGenerator>(pgen);
        extend_left<PairGenerator>(pgen);
    }

    template< typename PairGenerator=DefaultPairGenerator<SDSType, SfxTrieNodesType> >
    void extend(PairGenerator& pgen, uint64_t& total_pairs,
                uint64_t& candidate_pairs) {
        lr_extend<PairGenerator>(pgen);
        if(pgen.report_counts()){
            static int batch = 0;
            uint64_t lpairs = 0, cpairs = 0;
            batch++;
            lpairs = pgen.get_pair_count();
            cpairs = pgen.get_candidates();
            total_pairs += lpairs;
            candidate_pairs += cpairs;
            if(stats_report){
              uint64_t spairs = 0, dpairs = 0;
              MPI_Allreduce(&lpairs, &dpairs, 1,
                            MPI_UNSIGNED_LONG_LONG, MPI_SUM, sadt.comm());
              MPI_Allreduce(&cpairs, &spairs, 1,
                            MPI_UNSIGNED_LONG_LONG, MPI_SUM, sadt.comm());

              if(sadt.comm_rank() == 0)
                std::cout << "\"batch_pairs_" << batch << "\" : ["
                          << batch << ", "
                          << spairs << ", "
                          << dpairs << "], "
                          << std::endl;
            }
        }
    }


    ~MismatchSuffixTries(){
        for(int idx = 0; idx < (int)param.K; idx++){
            if(trie_sa[idx] != nullptr)
                delete trie_sa[idx];
            if(trie_nodes[idx] != nullptr)
                delete trie_nodes[idx];
            if(rev_trie_nodes[idx] != nullptr)
                delete rev_trie_nodes[idx];
            if(rev_trie_nodes[idx] != nullptr)
                delete rev_trie_sa[idx];
        }
    }

};

template< typename SDSType,
          typename NodesType,
          typename SfxTrieType=SuffixTrieDS<SDSType>,
          typename SfxTrieNodesType=TrieInternalNodeDS<SDSType>,
          typename PairGenerator=DefaultPairGenerator<SDSType, SfxTrieNodesType> >
void extend_batch(SDSType& sadt, SDSType& rev_sadt,
                  NodesType& batch_nodes, PairGenerator& pgen,
                  AppConfig& param, RunStats& rstats,
                  uint64_t& total_pairs, uint64_t& candidate_pairs) {

    // typedef MismatchSuffixTries< false, false,
    //                              AppConfig, RunStats,
    //                              SDSType, NodesType > fmst_t;

    typedef MismatchSuffixTries< false, false,
                                 AppConfig, RunStats,
                                 SDSType, NodesType > tmst_t;

    tmst_t mst(sadt, rev_sadt, batch_nodes, param, rstats);
    mst.extend(pgen, total_pairs, candidate_pairs);

}


template< typename SDSType, typename PairGenerator,
          typename BaseRegionType=BaseRegionSuffixDS<SDSType>,
          typename RootNodesType=RootInternalNodeDS<SDSType, BaseRegionType>,
          typename BatchNodesType=BatchInternalNodeDS<RootNodesType>,
          typename SfxTrieType=SuffixTrieDS<SDSType>,
          typename SfxTrieNodesType=TrieInternalNodeDS<SDSType>,
          typename CfgType=AppConfig>
void mismatch_suffixes(SDSType& sadt, SDSType& rev_sadt,
                       CfgType& param, RunStats& rstats){

    typedef typename SDSType::dcount_t dcount_t;
    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dchar_t dchar_t;
    typedef BaseRegionType base_region_t;
    typedef RegionSuffixDS<dchar_t, dsize_t, dcount_t> region_t;

    dcount_t min_depth = (dcount_t) param.tau;

    // 1. Select regions : local and straddling regions
    rstats.record_start();
    base_region_t base_ds(sadt, min_depth);
    region_t rmt_ds(sadt.global_size(), sadt.comm_rank(),
                    sadt.comm_size(), sadt.comm());
    region_t::left_shift_boundary(base_ds, min_depth, rmt_ds);
    base_ds.eliminate_boundaries(min_depth);

    rstats.select_region_time(std::cout);
    // print it
    // sa_stats(sadt, param.tau, param.tau, std::cout);
    // sa_stats(base_ds, param.tau, param.tau, std::cout, "BASE_");
    // base_ds.print_summary(std::cout);
    // sa_stats(rmt_ds, param.tau, param.tau, std::cout, "RMT_");
    rstats.mem_report(std::cout);
    base_ds.append_region(rmt_ds);
    // sa_stats(base_ds, param.tau, param.tau, std::cout, "NBASE_");
    rmt_ds.clear();

    // 2. Get internal nodes of selected regions
    rstats.record_start();
    sadt.clear_sa();
    RootNodesType root_nodes(sadt, base_ds, param.tau, false);
    rstats.root_nodes_time(std::cout);
    // rstats.mem_report(std::cout);
    // root_nodes.print_summary(std::cout);

    // 3. Process a batch of internal nodes
    rstats.record_start();
    std::vector<dsize_t> batch_ptr;
    dsize_t nbatch = 0, max_batch = 0, bwidth = param.batch_width;
    root_schedule(root_nodes, bwidth, batch_ptr);
    //root_schedule_sd(root_nodes, bwidth, batch_ptr);
    nbatch = batch_ptr.size() > 0 ? (batch_ptr.size() - 1) : 0;
    MPI_Allreduce(&nbatch, &max_batch, 1,
                  get_mpi_dt<dsize_t>(), MPI_MAX, sadt.comm());
    rstats.root_schedule_time(std::cout);
    rstats.mem_report(std::cout);
    if(sadt.comm_rank() == 0) {
        std::cout << "\"bwidth\" : [" << param.batch_width << "]," << std::endl;
        std::cout << "\"batches\" : [" << max_batch << "]," << std::endl;
    }
    // print_schedule(sadt, batch_ptr, nbatch, max_batch);
    // max_batch = max_batch/2;
    uint64_t tpairs = 0, cpairs = 0;
    reset_query_timings();
    reset_a2a_timings();
    rstats.batch_loop_start();
    unsigned threshold = (unsigned) param.tau;
    threshold *= (unsigned)(param.K + 1);
    PairGenerator pgen(sadt, param.read_length, true, threshold);
    pgen.load_txt(param.txtfn);
    for(dsize_t i = 0; i < max_batch; i++){
    //for(dsize_t i = 59; i < max_batch; i++){
        // rstats.mem_report(std::cout);
        dsize_t bptr = (i < nbatch) ? batch_ptr[i] : 0;
        dsize_t bsize = (i < nbatch) ? batch_ptr[i + 1] - bptr : 0;
        BatchNodesType batch_nodes(root_nodes, bptr, bsize);
        // batch_nodes.print_summary(i, std::cout);
        extend_batch<SDSType, BatchNodesType,
                     SfxTrieType, SfxTrieNodesType>(sadt, rev_sadt,
                                                    batch_nodes, pgen,
                                                    param, rstats,
                                                    tpairs, cpairs);
        // rstats.mem_report(std::cout);
    }
    rstats.batch_loop_stop(std::cout);
    report_query_timings(sadt.comm(), std::cout);
    report_a2a_timings(sadt.comm(), std::cout);
}


template< typename SDSType,
          typename BaseRegionType=BaseRegionSuffixDS<SDSType>,
          typename RootNodesType=RootInternalNodeDS<SDSType, BaseRegionType>,
          typename BatchNodesType=BatchInternalNodeDS<RootNodesType>,
          typename SfxTrieType=SuffixTrieDS<SDSType>,
          typename SfxTrieNodesType=TrieInternalNodeDS<SDSType>,
          typename CfgType=AppConfig
          >
void heuristic_mismatch_suffixes(SDSType& sadt,
                                 CfgType& param,
                                 RunStats& rstats,
                                 std::vector<char>& vtxt,
                                 bool fwd = true){

    typedef typename SDSType::dcount_t dcount_t;
    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dchar_t dchar_t;
    typedef BaseRegionType base_region_t;
    typedef RegionSuffixDS<dchar_t, dsize_t, dcount_t> region_t;

    dcount_t min_depth = (dcount_t) param.tau;
    DefaultBoundsChecker<unsigned, dsize_t> rcheck(param.read_length);

    // 1. Select regions : local and straddling regions
    rstats.record_start();
    base_region_t base_ds(sadt, min_depth);
    region_t rmt_ds(sadt.global_size(), sadt.comm_rank(),
                    sadt.comm_size(), sadt.comm());
    region_t::left_shift_boundary(base_ds, min_depth, rmt_ds);
    base_ds.eliminate_boundaries(min_depth);

    rstats.select_region_time(std::cout);
    // print it
    // sa_stats(sadt, param.tau, param.tau, std::cout, "SADT");
    // sa_stats(base_ds, param.tau, param.tau, std::cout, "BASE");
    // sa_stats(rmt_ds, param.tau, param.tau, std::cout, "RMT");
    // base_ds.print_summary(std::cout);
    // rmt_ds.print_summary(std::cout);
    base_ds.append_region(rmt_ds);
    rstats.mem_report(std::cout);
    rmt_ds.clear();

    // 2. Get internal nodes of selected regions
    rstats.record_start();
    sadt.clear_sa();
    RootNodesType root_nodes(sadt, base_ds, param.tau, false);
    rstats.root_nodes_time(std::cout);

    rstats.mem_report(std::cout);
    // root_nodes.print_summary(std::cout);

    // 3. Process a batch of internal nodes
    std::vector<dsize_t> batch_ptr;
    dsize_t nbatch = 0, max_batch = 0, bwidth = param.batch_width;
    root_schedule(root_nodes, bwidth, batch_ptr);
    nbatch = batch_ptr.size() - 1;
    MPI_Allreduce(&nbatch, &max_batch, 1,
                  get_mpi_dt<dsize_t>(), MPI_MAX, sadt.comm());
    // print_schedule(sadt, batch_ptr, nbatch, max_batch);
    rstats.mem_report(std::cout);
    if(sadt.comm_rank() == 0)
        std::cout << "\"batches\" : [" << max_batch << "]," << std::endl;

     // load whole txt
     TextDB<dchar_t, dsize_t, dcount_t> tdb(vtxt, '$', param.read_length);
     static uint64_t total_pairs = 0, candidate_pairs = 0;

    for(dsize_t i = 0; i < max_batch; i++){
    //for(dsize_t i = 16; i < max_batch; i++){
        dsize_t bptr = (i < nbatch) ? batch_ptr[i] : 0;
        dsize_t bsize = (i < nbatch) ? batch_ptr[i + 1] - bptr : 0;
        BatchNodesType batch_nodes(root_nodes, bptr, bsize);

        rstats.record_start();
        SfxTrieType trie_sa(sadt, batch_nodes, rcheck, true);
        SfxTrieNodesType trie_nodes(sadt, trie_sa);

        trie_sa.load_skip(); // load preceding character
        // TODO: process these nodes
        HeuristicCSPairGenerator<SDSType,
                                 SfxTrieNodesType> pgen(sadt,
                                                        param.read_length,
                                                        param.K,
                                                        param.tau, fwd,
                                                        tdb);
        pgen.generate(trie_nodes, param.ofs);

        if(fwd)
            rstats.forward_run(std::cout, i < nbatch);
        else
            rstats.reverse_run(std::cout, i < nbatch);
        uint64_t lpairs = pgen.get_pair_count(), spairs;

        MPI_Allreduce(&lpairs, &spairs, 1,
                      MPI_UNSIGNED_LONG_LONG, MPI_SUM, sadt.comm());
        total_pairs += spairs;
        lpairs = pgen.get_candidates(), spairs = 0;

        MPI_Allreduce(&lpairs, &spairs, 1,
                      MPI_UNSIGNED_LONG_LONG, MPI_SUM, sadt.comm());
        candidate_pairs += spairs;
        if(sadt.comm_rank() == 0)
            std::cout << "\"batch_" << i << "\" : ["
                      << i << ", "
                      << candidate_pairs << ", "
                      << total_pairs << "], "
                      << std::endl;
    }
}


template< typename SDSType,
          typename BaseRegionType=BaseRegionSuffixDS<SDSType>,
          typename RootNodesType=RootInternalNodeDS<SDSType, BaseRegionType>,
          typename BatchNodesType=BatchInternalNodeDS<RootNodesType>,
          typename SfxTrieType=SuffixTrieDS<SDSType>,
          typename SfxTrieNodesType=TrieInternalNodeDS<SDSType>,
          typename CfgType=AppConfig>
void heuristic_mismatch_suffixes_0(SDSType& sadt,
                                   CfgType& param,
                                   RunStats& rstats){

    typedef typename SDSType::dcount_t dcount_t;
    typedef typename SDSType::dsize_t dsize_t;
    typedef typename SDSType::dchar_t dchar_t;
    typedef BaseRegionType base_region_t;
    typedef RegionSuffixDS<dchar_t, dsize_t, dcount_t> region_t;

    dcount_t min_depth = (dcount_t) param.tau;
    DefaultBoundsChecker<unsigned, dsize_t> rcheck(param.read_length);

    // 1. Select regions : local and straddling regions
    rstats.record_start();
    base_region_t base_ds(sadt, min_depth);
    region_t rmt_ds(sadt.global_size(), sadt.comm_rank(),
                    sadt.comm_size(), sadt.comm());
    region_t::left_shift_boundary(base_ds, min_depth, rmt_ds);
    base_ds.eliminate_boundaries(min_depth);

    // print it
    // sa_stats(sadt, param.tau, param.tau, std::cout);
    // sa_stats(base_ds, param.tau, param.tau, std::cout);
    // base_ds.print_summary(std::cout);
    // rmt_ds.print_summary(std::cout);
    base_ds.append_region(rmt_ds);
    rstats.select_region_time(std::cout);
    rstats.mem_report(std::cout);
    rmt_ds.clear();

    // 2. Get internal nodes of selected regions
    rstats.record_start();
    sadt.clear_sa();
    RootNodesType root_nodes(sadt, base_ds, param.tau, false, true);
    rstats.root_nodes_time(std::cout);

    rstats.mem_report(std::cout);
    // root_nodes.print_summary(std::cout);

    // 3. Process a batch of internal nodes
    std::vector<dsize_t> batch_ptr;
    dsize_t nbatch = 0, max_batch = 0, bwidth = param.batch_width;
    root_schedule(root_nodes, bwidth, batch_ptr);
    nbatch = batch_ptr.size() - 1;
    MPI_Allreduce(&nbatch, &max_batch, 1,
                  get_mpi_dt<dsize_t>(), MPI_MAX, sadt.comm());
    // print_schedule(sadt, batch_ptr, nbatch, max_batch);
    rstats.mem_report(std::cout);
    if(sadt.comm_rank() == 0) {
        std::cout << "\"bwidth\" : [" << param.batch_width << "]," << std::endl;
        std::cout << "\"batches\" : [" << max_batch << "]," << std::endl;
    }

    // TODO: properly load whole txt
    std::vector<dchar_t> vtxt;
    TextDB<dchar_t, dsize_t, dcount_t> tdb(vtxt, '$', param.read_length);
    static uint64_t total_pairs = 0, candidate_pairs = 0;

    HeuristicCSPairGenerator<SDSType,
                             BatchNodesType> pgen(sadt,
                                                  param.read_length,
                                                  param.K,
                                                  param.tau, true,
                                                  tdb);
    for(dsize_t i = 0; i < max_batch; i++){
    //for(dsize_t i = 16; i < max_batch; i++){
        rstats.record_start();
        pgen.reset();
        dsize_t bptr = (i < nbatch) ? batch_ptr[i] : 0;
        dsize_t bsize = (i < nbatch) ? batch_ptr[i + 1] - bptr : 0;
        BatchNodesType batch_nodes(root_nodes, bptr, bsize);

        // TODO: process these nodes
        pgen.generate0(batch_nodes, param.ofs);

        rstats.forward_run(std::cout, i < nbatch);

        total_pairs += pgen.get_pair_count();
        candidate_pairs +=  pgen.get_candidates();
        continue;
        uint64_t lpairs = pgen.get_pair_count(), spairs, dpairs;

        MPI_Allreduce(&lpairs, &spairs, 1,
                      MPI_UNSIGNED_LONG_LONG, MPI_SUM, sadt.comm());
        lpairs = pgen.get_candidates();

        MPI_Allreduce(&lpairs, &dpairs, 1,
                      MPI_UNSIGNED_LONG_LONG, MPI_SUM, sadt.comm());
        if(sadt.comm_rank() == 0)
            std::cout << "\"batch_" << i << "\" : ["
                      << i << ", "
                      << spairs << ", "
                      << dpairs << "], "
                      << std::endl;
    }
}

// Work items are ids
template<typename SizeType>
struct RNWorkItem{

    SizeType m_offset;
    SizeType m_size;


    SizeType offset() const{
        return m_offset;
    }

    SizeType size() const{
        return m_size;
    }

    void reset(){
        // reset to size = 0;
        m_size = m_offset = 0;
    }

    void swap(RNWorkItem& other){
        std::swap(m_offset, other.m_offset);
        std::swap(m_size, other.m_size);
    }

    RNWorkItem(){ reset(); }

    RNWorkItem(SizeType off, SizeType sz){
        m_offset = off;
        m_size = sz;
    }
};


struct TestCfg{
  unsigned chunk_size;
};

template<typename SizeType>
struct TestTDS{
  uint64_t total_sum;

  SizeType _size;

  TestCfg _tcfg;


  inline TestCfg& get_config(){
      return _tcfg;
  }

  inline SizeType size(){
    return _size;
  }

  void extend(const RNWorkItem<SizeType>& wbatch){
    // do nothing
    for(SizeType i = 0; i < wbatch.size();i++)
      total_sum += i % size();
  }

  TestTDS(SizeType twork){
    _size = twork;
    _tcfg.chunk_size = 1;
  }
};

template<typename SizeType, typename TDSType>
    struct TestMSTLoader{
    bool operator()(TDSType& sfx_ds, SizeType start_offset,
                    SizeType end_offset,
                    RNWorkItem<SizeType>& wbatch){
      if(start_offset < end_offset && end_offset <= sfx_ds.size()){
        wbatch.m_offset = start_offset;
        wbatch.m_size = 90000000;
      }
      return true;
    }
};


template< typename ConfigType, typename StatsMonitor,
          typename SDSType,
          typename RootNodesType=SerialRootInternalNodeDS<SDSType>,
          typename BatchNodesType=BatchInternalNodeDS<RootNodesType>,
          typename SfxTrieType=SerialSuffixTrieDS<SDSType>,
          typename SfxTrieNodesType=TrieInternalNodeDS<SDSType, SfxTrieType> >
class ThreadSuffixDS{
    // shared data structures

    SDSType& sadt;
    SDSType& rev_sadt;
    RootNodesType& root_nodes;
    ConfigType& param;
    StatsMonitor& rstats;
    uint64_t npairs;
    uint64_t tpairs;
    uint64_t cpairs;

    ThreadSuffixDS(){}

public:

    inline ConfigType& get_config(){
        return param;
    }

    inline uint64_t get_pairs(){
        return npairs;
    }

    inline typename SDSType::dsize_t size(){
        return root_nodes.size();
    }

    virtual void print_summary(std::ostream& ots) const {
        sadt.print_summary(ots);
        rev_sadt.print_summary(ots);
    }

    // thread specific data structures
    void extend(const RNWorkItem<typename SDSType::dsize_t>& wbatch){
        if(wbatch.size() == 0)
            return;
        // process this batch
        BatchNodesType batch_nodes(root_nodes, wbatch.offset(),
                                   wbatch.size());

        MismatchSuffixTries< false, false,
                             ConfigType, StatsMonitor,
                             SDSType, BatchNodesType,
                             SfxTrieType, SfxTrieNodesType>
            mst(sadt, rev_sadt, batch_nodes, param, rstats);
        unsigned threshold = (unsigned) param.tau;
        threshold *= (unsigned)(param.K + 1);
        if(param.gen_pairs){
            CSPairGenerator<SDSType, SfxTrieNodesType> pgen(sadt,
                                                            param.read_length,
                                                            true,
                                                            threshold);
            mst.lr_extend(pgen);
            npairs += pgen.get_pair_count();
        } else if(param.gen_maximal){
            MaximalCSPairGenerator<SDSType, SfxTrieNodesType> pgen(sadt,
                                                                   param.read_length,
                                                                   true,
                                                                   threshold);
            mst.lr_extend(pgen);
            npairs += pgen.get_pair_count();
        } else {
            DefaultPairGenerator<SDSType, SfxTrieNodesType> pgen(sadt,
                                                                 param.read_length,
                                                                 true,
                                                                 threshold);
            mst.lr_extend(pgen);
        }
    }

    ThreadSuffixDS(SDSType& sa, SDSType& rsa, RootNodesType& rn,
                   ConfigType& cfg, StatsMonitor& rs)
        : sadt(sa), rev_sadt(rsa), root_nodes(rn), param(cfg), rstats(rs) {
        npairs = 0;
        tpairs = 0; cpairs = 0;
    }

    // ThreadSuffixDS(const ThreadSuffixDS& other)
    //     : sadt(other.sadt), rev_sadt(other.rev_sadt),
    //     root_nodes(other.root_nodes), param(other.param),
    //     rstats(other.rstats) {
    //     npairs = other.npairs;
    // }

    ~ThreadSuffixDS(){ }
};

template<typename SizeType, typename TDSType>
struct BatchMST{
    void operator()(const RNWorkItem<SizeType>& wbatch,
                    TDSType& sfx_ds, int tid, int){
        // generate pairs for this nodes
      // std::cout << std::hex << std::uppercase << tid
      //           << std::nouppercase << std::dec;
        // if(tid == 0)
        sfx_ds.extend(wbatch);
    }
};

template<typename SizeType, typename TDSType>
struct UnitMST{
    void operator()(const RNWorkItem<SizeType>& wbatch, TDSType& sfx_ds,
                    SizeType woff){
      RNWorkItem<SizeType> ubatch(wbatch.offset() + woff, 1);
      sfx_ds.extend(ubatch);
    }
};

template<typename SizeType, typename TDSType>
struct BatchMSTLoader{
    bool operator()(TDSType& sfx_ds, SizeType start_offset,
                    SizeType end_offset,
                    RNWorkItem<SizeType>& wbatch){
        if(start_offset < end_offset){
            wbatch.m_offset = start_offset;
            wbatch.m_size = end_offset - start_offset;
        }
        return true;
    }
};

template<typename SizeType, typename TDSType>
struct ChunkSizePolicy{
    // SizeType nWorkers;
    SizeType tChunk;
    ChunkSizePolicy(SizeType, TDSType& tds){
        // nWorkers = sfx_ds.workerCount();
        tChunk = tds.get_config().chunk_size;
    }

    unsigned long operator()(SizeType){
        return tChunk;
    }
};


void test_dynamic(AppConfig& param, RunStats& rstats){
    typedef unsigned dsize_t;
    typedef TestTDS<dsize_t> test_tds_t;
    typedef RNWorkItem<dsize_t> work_item_t;
    typedef BatchMST<dsize_t, test_tds_t> tbatch_worker_t;
    typedef UnitMST<dsize_t, test_tds_t> tunit_worker_t;
    typedef TestMSTLoader<dsize_t, test_tds_t> tbatch_loader_t;
    typedef ChunkSizePolicy<dsize_t, test_tds_t> tchunk_policy_t;

    dsize_t totalWork = 1024;
    std::vector<test_tds_t> threads_tds
        = std::vector<test_tds_t>(param.nthreads,
                                  test_tds_t(totalWork));

    rstats.record_start();
    WorkDistribution<work_item_t, dsize_t, test_tds_t,
                     tbatch_loader_t, tbatch_worker_t,
                     tunit_worker_t, tchunk_policy_t>
        wtt(totalWork, threads_tds, param.nthreads, false);
    wtt.main();
    rstats.root_nodes_time(std::cout);
    return;
}

template< typename SDSType,
          typename RootNodesType=SerialRootInternalNodeDS<SDSType>,
          typename BatchNodesType=BatchInternalNodeDS<RootNodesType>,
          typename SfxTrieType=SerialSuffixTrieDS<SDSType>,
          typename SfxTrieNodesType=TrieInternalNodeDS<SDSType, SfxTrieType>,
          typename CfgType=AppConfig>
void dynamic_mismatch_suffixes(SDSType& sadt, SDSType& rev_sadt,
                               CfgType& param, RunStats& rstats,
                               MPI_Comm _comm){

    typedef typename SDSType::dcount_t dcount_t;
    typedef typename SDSType::dsize_t dsize_t;

    typedef ThreadSuffixDS<CfgType, RunStats, SDSType,
                           RootNodesType, BatchNodesType,
                           SfxTrieType, SfxTrieNodesType> thread_sfx_t;
    typedef RNWorkItem<dsize_t> work_item_t;
    typedef BatchMST<dsize_t, thread_sfx_t> batch_worker_t;
    typedef UnitMST<dsize_t, thread_sfx_t> unit_worker_t;
    typedef BatchMSTLoader<dsize_t, thread_sfx_t> batch_loader_t;
    typedef ChunkSizePolicy<dsize_t, thread_sfx_t> chunk_policy_t;

    dcount_t min_depth = (dcount_t) param.tau;
    int rank;
    MPI_Comm_rank(_comm, &rank);

    // 1. Get internal nodes of selected regions
    rstats.record_start();
    RootNodesType root_nodes(sadt, min_depth);
    rstats.root_nodes_time(std::cout);
    rstats.mem_report(std::cout);
    if(rank == 0)
      root_nodes.print_summary(std::cout);


    // 2. Construct thread local data structures
    std::vector<thread_sfx_t> threads_pds
        = std::vector<thread_sfx_t>(param.nthreads,
                                    thread_sfx_t(sadt, rev_sadt,
                                                 root_nodes,
                                                 param, rstats));

    // 3. dynamic distribution of work
    dsize_t totalWork = 1024 * 1024 * 32;
    // rstats.record_start();
    // thread_sfx_t tsx(sadt, rev_sadt,
    //                  root_nodes, param, rstats);
    // for(dsize_t i = 0; i < totalWork; i++){
    //     work_item_t wbatch(i, 1);
    //     tsx.extend(wbatch);
    // }
    // rstats.forward_run(std::cout, true);

    // rstats.record_start();
    // // totalWork = root_nodes.size();
    // WorkDistribution<work_item_t, dsize_t, thread_sfx_t,
    //                  batch_loader_t, batch_worker_t,
    //                  unit_worker_t, chunk_policy_t>
    //     qwdt(totalWork, threads_pds,  (param.nthreads/4), false);
    // qwdt.main();
    // rstats.forward_run(std::cout, true);

    rstats.record_start();
    // totalWork = root_nodes.size();
    WorkDistribution<work_item_t, dsize_t, thread_sfx_t,
                     batch_loader_t, batch_worker_t,
                     unit_worker_t, chunk_policy_t>
        hwdt(totalWork, threads_pds, param.nthreads/2, true);
    hwdt.main();
    rstats.forward_run(std::cout, true);

    // rstats.record_start();
    // // totalWork = root_nodes.size();
    // WorkDistribution<work_item_t, dsize_t, thread_sfx_t,
    //                  batch_loader_t, batch_worker_t,
    //                  unit_worker_t, chunk_policy_t>
    //     hqwdt(totalWork, threads_pds, (param.nthreads/2) +  (param.nthreads/4), false);
    // hqwdt.main();
    // rstats.forward_run(std::cout, true);

    rstats.batch_loop_start();
    // totalWork = root_nodes.size();
    WorkDistribution<work_item_t, dsize_t, thread_sfx_t,
                     batch_loader_t, batch_worker_t,
                     unit_worker_t, chunk_policy_t>
        wdt(totalWork, threads_pds, param.nthreads, true);
    wdt.main();
    rstats.forward_run(std::cout, true);
}
#endif // MISMATCH_SFX_H
