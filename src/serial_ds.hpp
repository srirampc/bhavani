#ifndef SERIAL_DS_H
#define SERIAL_DS_H

#include <vector>
#include "abstract_ds.hpp"

template< typename CharType,
          typename SizeType,
          typename CountType,
          typename RMQTable>
class SerialSuffixDS
    : public AbstractSuffixDS<typename std::vector<CharType>::const_iterator,
                              typename std::vector<SizeType>::const_iterator,
                              typename std::vector<CountType>::const_iterator>{
public:
    typedef RMQTable drmq_t;
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;
    typedef SuffixPair<SizeType> spair_t;
    typedef uint32_t snds_t;

    typedef typename std::vector<CharType>::const_iterator txt_iterator_t;
    typedef typename std::vector<SizeType>::const_iterator sa_iterator_t;
    typedef typename std::vector<CountType>::const_iterator lcp_iterator_t;

protected:
    SizeType m_local_size;

    // vector of data structures : SA, ISA, LCP, NDS
    std::vector<SizeType> m_sa, m_isa;
    std::vector<CountType> m_lcp;
    std::vector<CharType> m_txt;
    std::vector<snds_t> m_nds;
    std::vector<CharType> m_sa_precede;

    // rmq table
    drmq_t* m_rmqt;

    // nds max
    snds_t m_max_nds;
public:
    virtual inline SizeType global_size() const{
        return size();
    }

    virtual inline SizeType size() const{
        return m_local_size;
    }

    virtual inline CountType rmq(SizeType i, SizeType j) const{
        assert(j < m_local_size);
        assert(i >= 0);
        assert(i <= j);
        assert(m_rmqt != nullptr);
        return m_lcp[(*m_rmqt)(i, j)];
    }

    virtual inline MPI_Comm comm() const{
        throw std::runtime_error("No comm for Local DS");
        return MPI_COMM_WORLD;
    }

    virtual inline int comm_size() const{
        return 1;
    }

    virtual inline int comm_rank() const{
        return 0;
    }

    inline sa_iterator_t sa() const {
        return m_sa.cbegin();
    }

    inline lcp_iterator_t lcp() const {
        return m_lcp.cbegin();
    }

    inline sa_iterator_t isa() const {
        return m_isa.cbegin();
    }

    inline txt_iterator_t sa_precede() const{
        return m_sa_precede.cbegin();
    }

    inline const std::vector<SizeType>& sa_vec() const {
        return m_sa;
    }

    inline const std::vector<CountType>& lcp_vec() const {
        return m_lcp;
    }

    inline const std::vector<SizeType>& isa_vec() const {
        return m_isa;
    }

    inline const std::vector<CharType>& txt_vec() const {
        return m_txt;
    }

    inline const drmq_t& rmq_table() const {
        return (*m_rmqt);
    }

   unsigned get_flag() const{
       unsigned flag = (this->m_isa.size() == this->m_sa.size()) ? 1 : 0;
       flag |=  (this->m_lcp.size() == this->m_sa.size()) ? 2 : 0;
       flag |=  (this->m_txt.size() == this->m_sa.size()) ? 4 : 0;
       flag |=  (this->size() == this->m_sa.size()) ? 8 : 0;
     return flag;
   }

    virtual void print_summary(std::ostream& ots) const {
        std::stringstream oss;
        oss << "\"SA_DS_X" << "\" : ["
            << std::setw(15) << this->size()
            << ","
            << std::setw(15) << this->m_sa.size()
            << ","
            << std::setw(15) << get_flag()
            << "],"
            << std::endl;
        ots << oss.str();
    }

    inline void precede(std::vector<SizeType>& vsa,
                        std::vector<CharType>& vprt) const{
        // load preceding character
        if(vprt.size() < vsa.size())
            vprt.resize(vsa.size());
        for(SizeType ix = 0; ix < vsa.size(); ix++){
            if(vsa[ix] == 0) vprt[ix] = '$';
            else if(vsa[ix] >= global_size()) vprt[ix] = '$';
            else vprt[ix] = m_txt[vsa[ix] - 1];
        }
    }

     void clear_txt() {
       std::vector<CharType>().swap(m_txt);
    }

    void clear_sa(){
        std::vector<SizeType>().swap(m_sa);
    }

    void swap(SerialSuffixDS< CharType, SizeType, CountType, RMQTable>& other){
        std::swap(m_local_size, other.m_local_size);

        // vector of data structures : SA, ISA, LCP, NDS, PRT
        m_sa.swap(other.m_sa);
        m_isa.swap(other.m_isa);
        m_lcp.swap(other.m_lcp);
        m_nds.swap(other.m_nds);
        m_txt.swap(other.m_txt);

        // rmq table
        std::swap(m_rmqt, other.m_rmqt);
        // m_rmqt->swap(*other.m_rmqt);
        if(m_rmqt != nullptr)
            m_rmqt->set_vector(&m_lcp);
        if(other.m_rmqt != nullptr)
            other.m_rmqt->set_vector(&other.m_lcp);

        // nds max
        std::swap(m_max_nds, other.m_max_nds);
    }

    SerialSuffixDS(std::vector<CharType>& f_txt,
             std::vector<SizeType>& f_sa,
             std::vector<CountType>& f_lcp, SizeType gsize){
        m_rmqt = nullptr;
        // get total size
        m_local_size =  gsize;

        // load data
        m_sa.swap(f_sa);
        m_txt.swap(f_txt);
        m_lcp.swap(f_lcp);
        assert(m_sa.size() == m_local_size);
        assert(m_lcp.size() == m_local_size);
        assert(m_txt.size() == m_local_size);
        // build data structures
        build();
    }

    SerialSuffixDS(txt_iterator_t txt, sa_iterator_t sa,
             lcp_iterator_t lcp, SizeType gsize){
        m_rmqt = nullptr;
        m_local_size = gsize;

        // load data
        m_sa.resize(m_local_size);
        m_txt.resize(m_local_size);
        m_lcp.resize(m_local_size);
        std::copy(sa, sa + m_local_size, m_sa.begin());
        std::copy(lcp, lcp + m_local_size, m_lcp.begin());
        std::copy(txt, txt + m_local_size, m_txt.begin());
        // build data structures
        build();
    }

    SerialSuffixDS(){
        m_rmqt = nullptr;
    }

    ~SerialSuffixDS(){
        if(m_rmqt != nullptr)
            delete m_rmqt;

    }
private:
    void init_nds(){
        // count the $
        m_nds.resize(m_local_size);
        m_nds[0] = (m_txt[0] == '$') ? 1 : 0;
        for(SizeType i = 1; i < m_nds.size(); i++)
            m_nds[i] = m_nds[i - 1] + ((m_txt[i] == '$') ? 1 : 0);

        m_max_nds = m_nds.back();
    }

    void update_sa_nds(){
        //  update SA subtract with
        for(SizeType i = 0; i < m_sa.size(); i++)
            m_sa[i] -= m_nds[m_sa[i]];
    }

    void compact(){
        SizeType csize = 0;
        //std::cout << m_sa[m_max_nds - 1 - m_blk_begin] << std::endl;
        for(SizeType i = m_max_nds - 1;
            i < m_local_size; i++, csize++){
            m_sa[csize] = m_sa[i];
            m_lcp[csize] = m_lcp[i];
        }
        // std::cout << csize << " " << m_local_size << std::endl;
        m_sa[0] = m_local_size - m_max_nds; // a $ at the beginning
        m_sa.resize(csize);
        m_lcp.resize(csize);
        if(csize > 0){
          std::vector<SizeType>(m_sa).swap(m_sa);
          std::vector<CountType>(m_lcp).swap(m_lcp);
        }
        // eliminate entries in txt where txt has '$'s
        csize = 0;
        for(SizeType i = 0; i < (m_local_size - 1); i++){
            if(m_txt[i] == '$')
                continue;
            m_txt[csize] = m_txt[i];
            csize++;
        }
        m_txt[csize] = m_txt[m_local_size - 1]; csize++;
        m_txt.resize(csize);
        if(csize > 0){
          std::vector<CharType>(m_txt).swap(m_txt);
        }
        m_local_size = m_sa.size();
    }

    void build_isa(){
        m_isa.resize(m_local_size);
        for(SizeType ix=0; ix < m_isa.size(); ++ix) {
            m_isa[m_sa[ix]] = ix;
        }
    }

    RMQTable* get_rmq_table(std::vector<CountType>& src_vec){
        RMQTable* tmp_rq = new RMQTable(&src_vec);
        return tmp_rq;
    }

    void build_rmq_tables(){
        // build rmq
        m_rmqt = get_rmq_table(m_lcp);
    }

    void init_prt(){
        m_sa_precede.resize(m_sa.size());
        for(SizeType ix=0; ix < m_sa.size(); ++ix) {
            if(m_sa[ix] > 0 && m_sa[ix] < m_txt.size())
                m_sa_precede[ix] = m_txt[m_sa[ix] - 1];
            else
                m_sa_precede[ix] = '$';
        }
    }

    void build(){
        // init nds (num $ signs) and prev. char
        init_nds();
        // update SA indices
        update_sa_nds();
        // compact the SA, LCP and TXT
        std::vector<snds_t>().swap(m_nds);
        compact();
        // construct isa and rmq tables
        build_isa();
        build_rmq_tables();
        init_prt();
    }
};

template< typename SDSType,
          typename CharType=typename SDSType::dchar_t,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t>
class SerialRootInternalNodeDS : public InternalNodeDS<SDSType> {
public:
    typedef
    SerialRootInternalNodeDS<SDSType,
                             CharType, SizeType, CountType> serial_root_nodes_t;
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;
    inline MPI_Comm comm() const{
        return this->root_sds.comm();
    }

    inline int comm_rank() const{
        return this->root_sds.comm_rank();
    }

    inline int comm_size() const{
        return this->root_sds.comm_size();
    }

    virtual SizeType sa(SizeType i, SizeType j) const{
        if(i < this->size()) {
            return this->root_sds.sa()[this->node_left[i] + j];
        }
        return this->sa_global_size();
    }

    virtual CharType sa_precede(SizeType i, SizeType j) const{
        if(i < this->size()) {
            return this->root_sds.sa_precede()[this->node_left[i] + j];
        } else {
            return '$';
        }
    }

    virtual void print_summary(std::ostream& ots) const {
        std::stringstream oss;
        oss << "\"SA_DS_X" << "\" : ["
            << std::setw(15) << this->size()
            << ","
            << std::setw(15) << this->get_flag()
            << "],"
            << std::endl;
        ots << oss.str();
    }

    SerialRootInternalNodeDS(const SDSType& sadt, CountType min_depth)
        : InternalNodeDS<SDSType>(sadt) {
        this->add_internal_nodes_from(sadt, *this, min_depth, 0, true);
    }

};

template< typename SDSType,
          typename CharType=typename SDSType::dchar_t,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t>
class SerialSuffixTrieDS
    : public AbstractSuffixTrieDS<SDSType, CharType, SizeType, CountType>{
public:
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;

    typedef typename std::vector<CharType>::const_iterator txt_iterator_t;
    typedef typename std::vector<SizeType>::const_iterator sa_iterator_t;
    typedef typename std::vector<CountType>::const_iterator lcp_iterator_t;

    typedef AbstractInternalNodeDS<txt_iterator_t, sa_iterator_t,
                                   lcp_iterator_t> IDSType;

    virtual void print_summary(std::string pfx, std::ostream& ots) const{
        std::stringstream oss;
        oss << "\"SERIRAL_SFX_TRIE_" << pfx << "_X"
            << "\" : ["
            << std::setw(15) << this->size()
            << ","
            << std::setw(15) << this->get_flag()
            << "],"
            << std::endl;
        ots << oss.str();
    }

    virtual inline int comm_rank() const{
        return this->m_sds.comm_rank();
    }

    virtual inline int comm_size() const{
        return this->m_sds.comm_size();
    }

    virtual inline MPI_Comm comm() const{
        return this->m_sds.comm();
    }

    virtual void construct_trie_isa(const SDSType& sds,
                                    const std::vector<SizeType>& vsa,
                                    std::vector<SizeType>& visa){
        // load visa from sds
        for(SizeType k = 0; k < vsa.size(); k++){
            if(vsa[k] >= this->global_size())
                visa[k] = 0;
            else
                visa[k] = sds.isa()[vsa[k]];
        }
    }

    virtual void construct_trie_lcp(const SDSType& sds,
                                    typename IDSType::size_iterator_t nds_width,
                                    SizeType nds_count,
                                    std::vector<SizeType>& visa,
                                    std::vector<CountType>& vlcp){
        //  compute lcp by rmq
        if(visa.size() != vlcp.size())
            vlcp.resize(visa.size());

        SizeType node_ptr = 0;
        for(SizeType i = 0; i < nds_count; i += 1) {
            bool pvalid = false;
            vlcp[node_ptr] = (CountType)0;
            for(SizeType j = 0; j < (SizeType)nds_width[i]; j++) {
                SizeType k = node_ptr + j;
                if(pvalid && visa[k] < sds.global_size())
                    vlcp[node_ptr + j] = sds.rmq(visa[k - 1] + 1, visa[k]);
                pvalid = (visa[k] + 1) < sds.global_size();
            }
            node_ptr += nds_width[i];
        }

    }

    void load_skip(){
        // TODO: load skip
    }

    CharType skip(SizeType) const{
        // TODO:: do skip
        return '$';
    }

   template <typename ReadBoundChecker>
    SerialSuffixTrieDS(const SDSType& sdx, IDSType& ndx,
                       ReadBoundChecker& read_bounds, bool forward)
        : AbstractSuffixTrieDS<SDSType, CharType,
                               SizeType, CountType>(sdx, ndx) {
        if(forward) {
            this->init_sa();
        } else {
            this->init_rev_sa();
        }
        this->check_read_bounds(this->m_sa, read_bounds);
        this->init_lcp();
    }

    virtual ~SerialSuffixTrieDS(){}

};

#endif /* SERIAL_DS_H */
