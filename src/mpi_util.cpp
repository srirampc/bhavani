#include <mpi.h>
#include <stdexcept>
#include <iomanip>
#include "util.hpp"
#include "mpi_util.hpp"

template <typename T>
MPI_Datatype get_mpi_dt()
{
    throw std::runtime_error("Unsupported MPI datatype");
    // default to int
    return MPI_INT;
}

template <>
MPI_Datatype get_mpi_dt<char>(){
    return MPI_CHAR;
}

template <>
MPI_Datatype get_mpi_dt<unsigned char>(){
    return MPI_UNSIGNED_CHAR;
}

template <>
MPI_Datatype get_mpi_dt<double>(){
    return MPI_DOUBLE;
}

template <>
MPI_Datatype get_mpi_dt<int>(){
    return MPI_INT;
}

template <>
MPI_Datatype get_mpi_dt<unsigned>(){
    return MPI_UNSIGNED;
}

template <>
MPI_Datatype get_mpi_dt<long>(){
    return MPI_LONG;
}

template <>
MPI_Datatype get_mpi_dt<unsigned long>(){
    return MPI_UNSIGNED_LONG;
}

template <>
MPI_Datatype get_mpi_dt<unsigned long long>(){
    return MPI_UNSIGNED_LONG_LONG;
}

void summary_mem_report(int rank, MPI_Comm _comm, std::ostream& ofs){
    static int ctx = 0;
    unsigned long int max_usage, my_usage;
    my_usage = mem_usage();
    MPI_Allreduce(&my_usage, &max_usage, 1,
                  MPI_UNSIGNED_LONG, MPI_MAX, _comm);
    ctx += 1;
    if(rank == 0)
        ofs << "\"max_memory_usage" << "_" << ctx << "\" : ["
            << ((1.0 * max_usage) / (1024 * 1024))
            << "],"
            << std::endl;
}

double query_timings = 0.0;
double a2a_timings = 0.0;

void reset_query_timings(){
    query_timings = 0.0;
}

double get_query_timings(){
    return query_timings;
}

void report_query_timings(MPI_Comm _comm, std::ostream& ofs){
    avg_min_max_report("query timings",
                       query_timings/1000.0,
                       _comm, ofs);
}

void reset_a2a_timings(){
    a2a_timings = 0.0;
}

double get_a2a_timings(){
    return a2a_timings;
}

void report_a2a_timings(MPI_Comm _comm, std::ostream& ofs){
    avg_min_max_report<double>("a2a timings",
                               a2a_timings/1000.0,
                               _comm, ofs);
}
