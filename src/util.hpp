#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <algorithm>
#include <functional>
#include <vector>
#include <sstream>
#include <chrono>
#include <fstream>

// CONSTANTS ---------------------------------------------------------
const unsigned DNA_ALPHABET_SIZE           = 4;
const char DNA_ALPHABET[DNA_ALPHABET_SIZE] = {'A', 'C', 'G', 'T'};
const unsigned ST_ALPHABET_SIZE            = DNA_ALPHABET_SIZE + 1;
const char ST_ALPHABET[ST_ALPHABET_SIZE]   = {'A', 'C', 'G', 'T', '$'};

const unsigned PROT_ALPHABET_SIZE              = 23;
const char PROT_ALPHABET[PROT_ALPHABET_SIZE]   = {'A', 'C', 'D', 'E', 'F', 'G',
                                                  'H', 'I', 'K', 'L', 'M', 'N',
                                                  'P', 'Q', 'R', 'S', 'T', 'V',
                                                  'W', 'Y', 'B', 'Z', 'X'};

// UTILITY FUNCTIONS -------------------------------------------------
// trim taken from stack overflow
// http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(),
            std::find_if(s.begin(), s.end(),
                         std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
            s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}


static inline std::vector<std::string> &split(const std::string &s, char delim,
                                              std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

static inline bool ends_with(std::string const &value,
                             std::string const &ending) {
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

static inline char bits_to_char(unsigned value){
    switch(value) {
    case 0:
        return 'A';
    case 1:
        return 'C';
    case 2:
        return 'G';
    case 3:
        return 'T';
    default:
        return 'N';
    }
}

static inline int char_to_bits(char c) {
    int cvalue = -1;
    switch (c) {
        case 'A':
        case 'a':
            cvalue = 0;
            break;
        case 'C':
        case 'c':
            cvalue = 1;
            break;
        case 'G':
        case 'g':
            cvalue = 2;
            break;
        case 'T':
        case 't':
            cvalue = 3;
            break;
    }
    return cvalue;
}

static inline std::string reverse_complement(std::string read){
    // add reverse complementary
    std::reverse(read.begin(), read.end());
    for(auto rb = read.begin();rb != read.end(); rb++){
        switch(*rb){
        case 'A':
            *rb = 'T';
            break;
        case 'C':
            *rb = 'G';
            break;
        case 'G':
            *rb = 'C';
            break;
        case 'T':
            *rb = 'A';
            break;
        default:
            *rb = 'N';
            break;
        }
    }
    return read;
}

static inline bool is_valid_dna(char c){
    for(auto i = 0u; i < DNA_ALPHABET_SIZE; i++)
        if(c == DNA_ALPHABET[i])
            return true;
    return false;
}

static inline bool is_valid_prot(char c){
    for(auto i = 0u; i < PROT_ALPHABET_SIZE; i++)
        if(c == PROT_ALPHABET[i])
            return true;
    return false;
}

static inline bool is_valid_alpha(char c){
    return is_valid_dna(c) || is_valid_prot(c);
}

template<typename X,  typename Y, typename T>
struct DefaultSwapper{
    X& m_xitr;
    Y& m_yitr;
    DefaultSwapper(X& xin, Y& yin): m_xitr(xin), m_yitr(yin){}

    void operator()(const T& a, const T& b){
        std::swap(m_xitr[a], m_xitr[b]);
        std::swap(m_yitr[a], m_yitr[b]);
    }
};

template<typename X,  typename Z, typename Y, typename T>
struct DefaultSwapper2{
    X& m_xitr;
    Z& m_zitr;
    Y& m_yitr;

    DefaultSwapper2(X& xin, Z& zin, Y& yin):
        m_xitr(xin), m_zitr(zin), m_yitr(yin){}

    void operator()(const T& a, const T& b){
        std::swap(m_xitr[a], m_xitr[b]);
        std::swap(m_zitr[a], m_zitr[b]);
        std::swap(m_yitr[a], m_yitr[b]);
    }
};

template<typename X,  typename W, typename Z, typename Y, typename T>
struct DefaultSwapper3{
    X& m_xitr;
    W& m_witr;
    Z& m_zitr;
    Y& m_yitr;

    DefaultSwapper3(X& xin, W& win, Z& zin, Y& yin):
        m_xitr(xin), m_witr(win), m_zitr(zin), m_yitr(yin) {}

    void operator()(const T& a, const T& b){
        std::swap(m_xitr[a], m_xitr[b]);
        std::swap(m_witr[a], m_witr[b]);
        std::swap(m_zitr[a], m_zitr[b]);
        std::swap(m_yitr[a], m_yitr[b]);
    }
};

template<typename X, typename T>
struct DefaultCompartor{
    const X& m_bitr;
    DefaultCompartor(const X& in): m_bitr(in){ }

    bool operator()(const T& a, const T& b) const{
        return m_bitr[a] < m_bitr[b];
    }
};


template< typename RecordIterator,
          typename DataIterator,
          typename SizeType=std::size_t,
          typename Swapper=DefaultSwapper<RecordIterator, DataIterator, SizeType>,
          typename Compartor=DefaultCompartor<DataIterator, SizeType> >
void sort_record_by(RecordIterator rcd_begin,
                    DataIterator srtby_begin,
                    const SizeType& begin, const SizeType& end){
    SizeType nrecords = end - begin;
    std::vector<SizeType> srt_idx(nrecords), cur_idx(nrecords),
        inv_cur_idx(nrecords);
    Swapper swap_record(rcd_begin, srtby_begin);

    // Initialize sorted index
    for(SizeType i = 0; i < nrecords; i++){
        srt_idx[i] = i + begin;
        cur_idx[i] = inv_cur_idx[i] = i;
    }

    // sort index by data compartor
    Compartor compare_data(srtby_begin);
    std::sort(srt_idx.begin(), srt_idx.end(), compare_data);
    // shit leftwards
    for(SizeType i = 0; i < nrecords; i++)
        srt_idx[i] -=  begin;
    // swap
    for(SizeType i = 0u; i < nrecords; i++){
        SizeType& x = srt_idx[i];
        SizeType& y = inv_cur_idx[x];
        SizeType& p = cur_idx[i];
        SizeType& q = cur_idx[y];

        //std::swap(rcd_begin[i], rcd_begin[y]); // swap data
        swap_record(begin + i, begin + y);  // swap data
        std::swap(cur_idx[i], cur_idx[y]); // Swap cur_idx x and y
        std::swap(inv_cur_idx[p], inv_cur_idx[q]); // Swap for inv_cur_idx
    }
}

template< typename RecordIterator1,
          typename RecordIterator2,
          typename DataIterator,
          typename SizeType=std::size_t,
          typename Swapper=DefaultSwapper2<RecordIterator1, RecordIterator2,
                                           DataIterator, SizeType>,
          typename Compartor=DefaultCompartor<DataIterator, SizeType> >
void sort_record_by2(RecordIterator1 rcd_begin1,
                     RecordIterator2 rcd_begin2,
                     DataIterator srtby_begin,
                     const SizeType& begin, const SizeType& end){
    SizeType nrecords = end - begin;
    std::vector<SizeType> srt_idx(nrecords), cur_idx(nrecords),
        inv_cur_idx(nrecords);
    Swapper swap_record(rcd_begin1, rcd_begin2, srtby_begin);

    // Initialize sorted index
    for(SizeType i = 0; i < nrecords; i++){
        srt_idx[i] = i + begin;
        cur_idx[i] = inv_cur_idx[i] = i;
    }

    // sort index by data compartor
    Compartor compare_data(srtby_begin);
    std::sort(srt_idx.begin(), srt_idx.end(), compare_data);
    // shit leftwards
    for(SizeType i = 0; i < nrecords; i++)
        srt_idx[i] -=  begin;
    // swap
    for(SizeType i = 0u; i < nrecords; i++){
        SizeType& x = srt_idx[i];
        SizeType& y = inv_cur_idx[x];
        SizeType& p = cur_idx[i];
        SizeType& q = cur_idx[y];

        //std::swap(rcd_begin[i], rcd_begin[y]); // swap data
        swap_record(begin + i, begin + y);  // swap data
        std::swap(cur_idx[i], cur_idx[y]); // Swap cur_idx x and y
        std::swap(inv_cur_idx[p], inv_cur_idx[q]); // Swap for inv_cur_idx
    }
}

template< typename RecordIterator1,
          typename RecordIterator2,
          typename RecordIterator3,
          typename DataIterator,
          typename SizeType=std::size_t,
          typename Swapper=DefaultSwapper3<RecordIterator1,
                                           RecordIterator2,
                                           RecordIterator3,
                                           DataIterator, SizeType>,
          typename Compartor=DefaultCompartor<DataIterator, SizeType> >
void sort_record_by3(RecordIterator1 rcd_begin1,
                     RecordIterator2 rcd_begin2,
                     RecordIterator3 rcd_begin3,
                     DataIterator srtby_begin,
                     const SizeType& begin, const SizeType& end){
    SizeType nrecords = end - begin;
    std::vector<SizeType> srt_idx(nrecords), cur_idx(nrecords),
        inv_cur_idx(nrecords);
    Swapper swap_record(rcd_begin1, rcd_begin2, rcd_begin3, srtby_begin);

    // Initialize sorted index
    for(SizeType i = 0; i < nrecords; i++){
        srt_idx[i] = i + begin;
        cur_idx[i] = inv_cur_idx[i] = i;
    }

    // sort index by data compartor
    Compartor compare_data(srtby_begin);
    std::sort(srt_idx.begin(), srt_idx.end(), compare_data);
    // shit leftwards
    for(SizeType i = 0; i < nrecords; i++)
        srt_idx[i] -=  begin;
    // swap
    for(SizeType i = 0u; i < nrecords; i++){
        SizeType& x = srt_idx[i];
        SizeType& y = inv_cur_idx[x];
        SizeType& p = cur_idx[i];
        SizeType& q = cur_idx[y];

        //std::swap(rcd_begin[i], rcd_begin[y]); // swap data
        swap_record(begin + i, begin + y);  // swap data
        std::swap(cur_idx[i], cur_idx[y]); // Swap cur_idx x and y
        std::swap(inv_cur_idx[p], inv_cur_idx[q]); // Swap for inv_cur_idx
    }
}

#ifdef __linux__
#include <unistd.h>
#include <sys/resource.h>
#endif

#ifdef __APPLE__
# include <mach/task.h>
# include <mach/mach_init.h>
#endif


static inline unsigned long int mem_usage() {
#if defined(__linux__)
	// Linux ----------------------------------------------------
	long rss = 0L;
	FILE* fp = NULL;
	if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL )
		return (size_t)0L;		/* Can't open? */
	if ( fscanf( fp, "%*s%ld", &rss ) != 1 )
	{
		fclose( fp );
		return (size_t)0L;		/* Can't read? */
	}
	fclose( fp );
	return (size_t)rss * (size_t)sysconf( _SC_PAGESIZE);
#elif defined(__APPLE__)
    // Inspired by:
    // http://miknight.blogspot.com/2005/11/resident-set-size-in-mac-os-x.html
    struct task_basic_info t_info;
    mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
    task_info(current_task(), TASK_BASIC_INFO, (task_info_t)&t_info, &t_info_count);
    // size_t size = (resident ? t_info.resident_size : t_info.virtual_size);
    return t_info.resident_size;
#else
    return 0;
#endif
} // mem_usage

template <typename duration>
typename duration::rep
elapsed_local(std::chrono::steady_clock::time_point& tp_start,
              std::chrono::steady_clock::time_point& tp_stop){
    typename duration::rep elapsed_time = duration(tp_stop-tp_start).count();
    return elapsed_time;
}

template<typename T>
uint64_t file_size(const std::string& infn){
  // get total size
  std::ifstream safs(infn, std::ios::in | std::ios::binary);
  safs.seekg(0, std::ios::end);
  uint64_t fsize = safs.tellg();
  uint64_t dist_size = fsize/sizeof(T);
  safs.close();
  return dist_size;
}


#endif /* UTIL_H */
