#ifndef WORKDISTRIBUTION_H
#define WORKDISTRIBUTION_H

#include <iostream>
#include <thread>
#include <vector>
#include <queue>
#include <cassert>
#include <mutex>
#include <atomic>
#include <sstream>
#include <chrono>
#include <mpi.h>

#include "mpi_util.hpp"
//
// A share queue using a lock
//  - T should have constructor with empty arguments
//  - swap(T&, T&) should be implemented to avoid unnecessary copying
template <typename T>
class SharedQueue{

public:
    SharedQueue() = default;
    SharedQueue(const SharedQueue&) = delete;
    SharedQueue& operator=(const SharedQueue&) = delete;
    bool pop(T& item){
        using std::swap;
        std::lock_guard<std::mutex> lock(m_qmut);
        if(m_queue.empty())
            return false;
        swap(m_queue.front(), item);
        m_queue.pop();
        return true;
    }

    void push(std::vector<T>& items){
        using std::swap;
        std::lock_guard<std::mutex> lock(m_qmut);
        for(auto ait = items.begin(); ait != items.end();++ait){
            T tmp;
            m_queue.push(tmp);
            swap(m_queue.back(), *ait);
        }
    }

    void push(T* items, int size){
        using std::swap;
        std::lock_guard<std::mutex> lock(m_qmut);
        for(int i = 0; i< size;i++){
            T tmp;
            m_queue.push(tmp);
            swap(m_queue.back(), items[i]);
        }
    }

    void push(T& item){
        using std::swap;
        std::lock_guard<std::mutex> lock(m_qmut);
        T tmp;
        m_queue.push(tmp);
        swap(m_queue.back(), item);
    }

    size_t size() const{
        return m_queue.size();
    }

    bool empty(){
        return m_queue.empty();
    }

private:
    std::queue<T> m_queue;
    std::mutex m_qmut;
};


enum WDState{
    ASSIGN_WORK = 0, // ready for work to be assigned
    PENDING_WORK,  // no more work left, but work queue not empty
    FINISHED_WORK, // work queue empty, go and wait for thread for
    NR_WORK_STATES // place holder for counting states
};

enum WorkRequest{
    ASK_WORK_TAG = 0x1,
    SND_WORK_TAG = 0x2
};

// WorkDistribution
//
// - Distributes a given quantity of work into workchunks across the
//   all distributed processes and their shared memory threads.
// - Architected as a single master procecss and a bunch of slave
//   processes
// - Each process has a co-ordination thread and  'numThreads' no. of
///  worker threads.
// - Initial work chunk is assigned without explicit allocation by the
//   master process.
// - Co-ordination threads in slave processes have the responsibility
//   to allocate work for worker threads by asking work from the
//   master process.
// - Co-ordination threads in master process have the responsibility
//   to allocate work for its worker threads and also allocate work for
//   all slave processes.
// - Co-ordination threads in all the processes also load the work item
//   (incase it needs to be loaded from a file or network) and do some
//   part of the work in between other responsibilites.
//
// WorkItemType holds the information regarding work that needs to be
// done and is expected to have the following interface
//   0. Constructor with no arguments
//   1. Has a size() function that returns
//   2. Has a reset() function that clears the state of the work item
//   3. implement a swap function to avoid copying in the queue
// size() should correspond to the number of units of work.
//
// SizeType is used to measure the quantity of work, size of the work
// chunk, and offsets work. This type is expected to an integral type.
//
// A reference to PayLoadType object passsed to the worker
// functions. This object is expected to have all the information
// necessary to be made use of during the work. Each thread is supposed
// to get its own copy of a PayLoadType object so that the thread can
// write any results to this object with out any conflict.
//
// BatchLoaderType, BatchWorkerType, UnitWorkerType, and ChunkSizeType
// are function objects.
//
// BatchLoaderType loads a WorkItemType object from starting and ending
// offset. It also takes the payload object as an argument to retrieve
// the meta data necessary. The arguments are const PayLoadType& pl,
// SizeType startOffset, SizeType endOffset, WorkItemType& item
//
// BatchWokerType does the work corresponding to a given batch. Its
// arguments are WorkItemType& item, PayLoadType& pl, int threadid, int rank
//
// UnitWokerType does the work corresponding to a single unit with in a
// batch. Its arguments are WorkItemType& item, PayLoadType& pl,
// unsigned unit_id
//
// ChunkPolicyType is a function objects that estimates the chunk size for
// the current work load
template <typename WorkItemType, typename SizeType, typename PayLoadType,
          typename BatchLoaderType, typename BatchWorkerType,
          typename UnitWorkerType, typename ChunkPolicyType>
class WorkDistribution{
public:
    typedef std::chrono::steady_clock::time_point time_point_t;
    typedef std::chrono::duration<double, std::milli> duration_t;
private:
    SharedQueue<WorkItemType> wrkQueue;
    std::atomic_bool wrkFinished;
    SizeType numWorkers;
    bool isCoordWorker; // If co-ordination thread a worker too ?

    // work distribution
    SizeType totalWork;
    std::vector<PayLoadType>& payLoads;
    unsigned numThreads;
    SizeType initChunk; // initial allocation chunk size

    // woker functions
    BatchLoaderType load_work;
    UnitWorkerType unit_work;
    ChunkPolicyType chunk_policy;

    // work item and progress indicator for co-ordination thread
    WorkItemType crdWork;
    SizeType workProgress;

    // mpi information
    int mpiSize;
    int mpiRank;

    // co-ordination variables
    int numWorkZero; // master co-ord for counting no. 'zero work'
    SizeType workAssigned; // master co-ord tracks how much work is assinged
    WDState coordState; // current state
    bool coordWork; // should co-ord thread work too ?

    std::vector<time_point_t> stateTimings;
    std::stringstream msgOut;

    MPI_Comm _comm; // communicator
    //This function will be called from a thread,
    //   therfore any function that is called by this function should be thread safe
    // It is assumed that the PayLoadType object is exclusive to this thread
    static void worker_thread(int tid, int rank, PayLoadType& pload,
                              SharedQueue<WorkItemType>& wrkQueueRef,
                              std::atomic_bool& wrkFinishedRef) {
        BatchWorkerType batch_work;
        WorkItemType work;

        while(true) {
            if(wrkQueueRef.pop(work)){
                // do a batch of work
                batch_work(work, pload, tid, rank);
                work.reset();
            }
            if(wrkFinishedRef.load(std::memory_order_relaxed))
                break;
        }
    }

    inline unsigned worker_id(unsigned thread_id){
        return (numWorkers == numThreads) ?
            thread_id : (1 + thread_id);
    }

    inline SizeType getInitOffset(unsigned thread_id){
        //   (work_chunk) ((rank * (numWorkers)) + worker_id);
        SizeType threadOffset = mpiRank * (numWorkers) * initChunk;
        threadOffset +=  worker_id(thread_id) * initChunk;
        return threadOffset;
    }

    // Launch a group of worker threads
    void startThreads(std::vector<std::thread>& threads) {
        for (unsigned tid = 0; tid < numThreads; ++tid) {
            // initialize the first chunk
            BatchLoaderType load_work;
            WorkItemType work;
            // My first work chunk
            SizeType threadOffset = getInitOffset(tid);
            unsigned wid = worker_id(tid);

            bool initLoad = load_work(payLoads[wid], threadOffset,
                                      threadOffset + initChunk, work);
            if(!initLoad)
                msgOut << "E";
            wrkQueue.push(work);
            // start thread
            threads.push_back(std::thread(worker_thread, tid, mpiRank,
                                          std::ref(payLoads[wid]),
                                          std::ref(wrkQueue),
                                          std::ref(wrkFinished)));
        }
    }

    // Wait for worker threads to finish
    void joinThreads(std::vector<std::thread>& threads) {
        for(auto wit = threads.begin(); wit != threads.end(); wit++){
            wit->join();
        }
    }

    // Work done by coord thread
    bool doWork(){
        // get the next work item from queue
        if(workProgress == 0 &&
           crdWork.size() == 0 && !wrkQueue.pop(crdWork)){
            return false;
        }
        // TODO: do work of some predefined granularity
        if(workProgress < crdWork.size()){
            unit_work(crdWork, payLoads[0], workProgress);
            workProgress += 1;
        } else {
            workProgress = 0;
            crdWork.reset();
        }
        return true;
    }

    // Compute the work to be assigned as vector of offets
    void assignWork(std::vector<SizeType>& wrkOffsets){
        for(auto sit = wrkOffsets.begin(); sit != wrkOffsets.end(); sit++){
            if(workAssigned < totalWork){
                *sit = workAssigned;
                workAssigned += chunk_policy(workAssigned);
            } else {
                *sit = 0;
            }
        }
    }

    void updateWorkQueue(typename std::vector<SizeType>::iterator first,
                         typename std::vector<SizeType>::iterator last){
        for(auto ait = first; ait != last; ait++){
            WorkItemType tmp;
            if(load_work(payLoads[0], *ait,
                         (*ait) + chunk_policy(*ait), tmp))
                wrkQueue.push(tmp);
        }
        //std::cout << wrkOffsets.back();

    }

    // Update work queue with new set of items
    //   - returns true when the whole of wrkOffsets is actual work!
    //     i.e. returns true when there is no 'zero work' assigned
    bool updateWorkQueue(std::vector<SizeType>& wrkOffsets){
        auto last = std::find(wrkOffsets.begin(), wrkOffsets.end(), 0);

        updateWorkQueue(wrkOffsets.begin(), last);

        return (wrkOffsets.back() != 0);
    }

    // Check if any one is asking for work assignment
    int probeQuery(MPI_Status& status){
        int flag;
        MPI_Iprobe(MPI_ANY_SOURCE, ASK_WORK_TAG, _comm, &flag, &status);
        return flag;
    }

    // Assigns work to remote processes, if they are asking for it
    //  - returns true when the whole of threadWork is actual work!
    //    or when nothing is assigned
    bool assignWorkRemote(){
        int count, recvMsg, nSent = 0;
        MPI_Status status;
        MPI_Request request;
        std::vector<SizeType> threadWork(numThreads);
        bool fullAssigned = true;
        // if anybody asking work,
        //   Either assign work to them or tell them there is no more work to do
        while(probeQuery(status)){
            // who is asking for work ?
            MPI_Get_count(&status, MPI_INT, &count);
            assert(count == 1);
            MPI_Recv(&recvMsg, 1, MPI_INT, status.MPI_SOURCE,
                     ASK_WORK_TAG, _comm, &status);
            // always assign work of size 'numThreads'
            assignWork(threadWork);
            // send the allocated work
            MPI_Isend(&(threadWork[0]), numThreads, get_mpi_dt<SizeType>(),
                      status.MPI_SOURCE, SND_WORK_TAG, _comm, &request);
            MPI_Wait(&request, &status); // should I need to wait ?
            nSent += 1;
            if(threadWork.back() == 0){
                fullAssigned = false;
                numWorkZero += 1;
            }
        }

        return fullAssigned;
    }

    // Ask and recieve work from the master co-ord process
    void recvWorkRemote(std::vector<SizeType>& recvMessage){
        MPI_Status status;
        MPI_Request request;
        int sendMsg = (int) numThreads;
        recvMessage.resize(numThreads);
        MPI_Isend(&sendMsg, 1, MPI_INT, 0, ASK_WORK_TAG, _comm, &request);
        MPI_Wait(&request, &status);
        MPI_Recv(&(recvMessage[0]), numThreads, get_mpi_dt<SizeType>(), 0,
                 SND_WORK_TAG, _comm, &status);
    }

    void updateCoordState(WDState newState){
        coordState = newState;
        if(coordState >= stateTimings.size())
            stateTimings.resize(coordState + 1);
        stateTimings[coordState] = std::chrono::steady_clock::now();;
    }

    // Co-ordination thread in master process.
    //  - Fullfills the responsibilites based on the coordState
    //  - This function is not thread safe, only one thread in a process
    //     is allowed to run this function.
    void masterCoord(){

        switch(coordState){
        case ASSIGN_WORK:
            // Assign to work local threads, if they don't have enough
            if(wrkQueue.size() < WORK_QUEUE_FACTOR * numThreads){
                std::vector<SizeType> threadWork(numThreads);
                assignWork(threadWork);
                if(!updateWorkQueue(threadWork)) // if assigned 'zero work'
                    updateCoordState(PENDING_WORK); // update state
            }
            // Assign work to remote processes, if they are asking for it
            if(!assignWorkRemote()){
                // Update state, if I have assigned 'zero work'
                updateCoordState(PENDING_WORK);
            }
            break;
        case PENDING_WORK:
            // I, root, haven't recieved message from every one:
            //   Assign 'zero work', to any remote process asking for work
            if(numWorkZero < mpiSize){
                assignWorkRemote();
            }
            // I, root, have assigned 'zero work' to every process;
            //   Also, my queue is empty. I can finish work now.
            if(numWorkZero == mpiSize && wrkQueue.empty()){
                updateCoordState(FINISHED_WORK);
            }
            break;
        case FINISHED_WORK: // nothing to done, signal threads
            if(!wrkFinished.load(std::memory_order_relaxed)){
                wrkFinished.store(true, std::memory_order_relaxed);
            }
            break;
        default:
            break;
        }
    }

    // Co-ordination thread in slave process.
    //  - Fullfills the responsibilites based on the coordState
    //  - This function is not thread safe, only one thread in a process
    //     is allowed to run this function.
    int slaveCoord(){

        switch(coordState){
        case ASSIGN_WORK:
            // If my local threads don't have enough work, ask from the root
            if(wrkQueue.size() < WORK_QUEUE_FACTOR * numThreads){
                std::vector<SizeType> recvMessage(numThreads);
                recvWorkRemote(recvMessage);
                // If I have been assigned 'zero work', update state to pending
                if(!updateWorkQueue(recvMessage))
                    updateCoordState(PENDING_WORK);
            }
            break;
        case PENDING_WORK:
            // No more work available, but work queue might not be empty
            if(wrkQueue.empty()){
                updateCoordState(FINISHED_WORK);
            }
            break;
        case FINISHED_WORK:
            // No more work available and work queue is empty too
            if(!wrkFinished.load(std::memory_order_relaxed)){
                wrkFinished.store(true, std::memory_order_relaxed);
            }
            break;
        default:
            break;
        }
        return 0;
    }

    // Load a batch as the work item for coord thread
    void loadCoordWork(){
        SizeType startOffset = mpiRank * (numWorkers) * initChunk;
        // std::cout << mpiRank << " " << startOffset << std::endl;
        bool cwork = load_work(payLoads[0], startOffset,
                               startOffset + initChunk, crdWork);
        if(!cwork)
            msgOut << "E";
    }

    // Inital work allocation
    void initQueue(){
        std::vector<SizeType> iOffsets(numWorkers);
        SizeType currOffset;
        for(int j = 1; j <= INIT_QUEUE_FACTOR; j++){
            currOffset = mpiRank + (j * mpiSize);
            currOffset *= (numWorkers) * initChunk;
            for(auto oit = iOffsets.begin(); oit != iOffsets.end(); ++oit){
                *oit = currOffset;
                currOffset += initChunk;
                // std::cout << mpiRank << " " << currOffset << std::endl;
            }
            updateWorkQueue(iOffsets.begin(), iOffsets.end());
        }
    }

public:

    const int WORK_QUEUE_FACTOR = 2;
    const int INIT_QUEUE_FACTOR = 2;
    const std::vector<time_point_t>& getStateTimings() const{
        return stateTimings;
    }

    bool validate(){
        if(numThreads == 0 || payLoads.size() == 0){
            if(mpiRank == 0)
                std::cout << "Can't have zero threads or empty payloads" << std::endl;
            return false;
        }
        if(payLoads.size() < numWorkers){
            if(mpiRank == 0)
                std::cout << "Payload size(" << payLoads.size()
                          << ") doesn't match with no. workers ("
                          << numWorkers << ")" << std::endl;
            return false;
        }
        return true;
    }

    // Main loop of the co-rodnation thread
    //  - assumes the root process
    //  - this function is not thread safe, only the main thread should be
    //     allowed to run this function.
    void main(int root = 0){
        if(!validate()){
            if(mpiRank == 0)
                std::cout << "Can't run with given parameters" << std::endl;
            return;
        }

        stateTimings.resize(NR_WORK_STATES + 1);
        wrkFinished.store(false, std::memory_order_relaxed);
        updateCoordState(ASSIGN_WORK);
        std::vector<std::thread> threads;
        startThreads(threads); // start threads

        if(isCoordWorker) loadCoordWork(); // Load the local work first

        initQueue(); // Initializes the queue with 2 * numWorkers

        workAssigned = (1 + INIT_QUEUE_FACTOR) * mpiSize *
            (numWorkers) * initChunk;
        // std::cout << mpiRank << " " << workAssigned << std::endl;
        // Start co-ordination loop
        do {
            if(isCoordWorker) doWork();

            if(mpiRank == root)
                masterCoord();
            else
                slaveCoord();

            if(wrkFinished.load(std::memory_order_relaxed))
                break;
        } while(true);

        if(isCoordWorker) while(doWork());  // finish if any local work left

        joinThreads(threads);
        updateCoordState(NR_WORK_STATES);
        if(msgOut.str().size() > 0){
            msgOut << std::endl;
            std::cout << msgOut.str();
        }
    }

    WorkDistribution(SizeType tWork, std::vector<PayLoadType>& refPayload,
                     unsigned nThreads, bool cWorkerFlag = true,
                     MPI_Comm cx = MPI_COMM_WORLD):
        totalWork(tWork), payLoads(refPayload),
        numThreads(nThreads), chunk_policy(tWork, refPayload[0]),
        _comm(cx) {

        MPI_Comm_size(_comm, &mpiSize);
        MPI_Comm_rank(_comm, &mpiRank);

        isCoordWorker = cWorkerFlag;
        numWorkers =  (unsigned) refPayload.size(); // 1 + numThreads;
        workProgress = 0;
        numWorkZero = 1;
        initChunk = chunk_policy(0);

        assert(numThreads > 0);
        assert(payLoads.size() >= numWorkers);
        assert(initChunk > 0);
    }
};

#endif // WORKDISTRIBUTION_H
