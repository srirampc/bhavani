/**
 * @file    align.c
 * @author  Chirag Jain <cjain@gatech.edu>
 * @date    Jan 1, 2015
 * @brief   All to all pairwise sequence alignment. The number of gaps are restricted,
 *          therefore we compute a single diagonal band of the matrix.
 */

#include "align.hpp"
#include <stdio.h>
#include <assert.h>
#include <algorithm>
#include <iostream>
#include <cstring>

//Scoring scheme (Need to rethink)
//#define MATCH_SCORE 1
//#define GAP_PENALTY 3
//#define MISMATCH_PENALTY 0
//Alignment score will equal the number of matches with the above configuration

//To count the number of gaps in the optimal alignment , we
//need to save the matrix as well and do a traceback in the end
bool align_band_with_traceback(const char *seq1, const int& len1,
                               const char *seq2, const int& len2,
                               int& lastScore, bool& isLow, std::ostream& ots,
                               const int gapLimit,
                               const float PCT_THRESHOLD,
                               const int MATCH_SCORE,
                               const int GAP_PENALTY,
                               const int MISMATCH_PENALTY
                               )
{
  int bufferNeeded = 2*gapLimit + 1;
  int *prevRow = (int *)malloc(bufferNeeded * sizeof(int));
  int *curRow = (int *)malloc(bufferNeeded * sizeof(int));

  //For storing the log for final traceback
  //Protocol : {0,1,2,3} imply {Match, Mismatch, left gap, right gap} respectively
  int *pointers = (int *)malloc(bufferNeeded * std::min(len1, len2) * sizeof(int));
  lastScore = 0; isLow = false;
  int i_Last, j_Last;

  //Initialise to zero
  memset(prevRow, 0, bufferNeeded * sizeof(int));
  memset(curRow, 0, bufferNeeded * sizeof(int));

  assert(len1 > bufferNeeded);
  assert(len2 > bufferNeeded);

  // if(std::max(len1, len2) - std::min(len1, len2) > gapLimit)
  //   ots << "WARN: Difference between sequence lengths > " << gapLimit << std::endl;


  /* For example, if gapLimit is 2, then first iteration (row 1) involves computing 3 cells.
   * Subsequent rows require computation of 4,5,5,...,4 & finally 3 cells if DP matrix is square.
   * Might be useful to draw this picture before understanding the code below */


  //Begin iteration
  int endIndex = bufferNeeded;
  for(int i=0 ; i < len1; i++)
  {
    //For the initial few iterations
    int startIndex = std::max(0, gapLimit - i);

    int sequence2Offset = (startIndex > 0 ? 0 : i - gapLimit);
    for(int j = startIndex; j < endIndex; j++)
    {
      bool match = (seq1[i] == seq2[sequence2Offset++]);
      int upperDiagonalCell = prevRow[j];
      int leftCell = (j == 0) ? 0 : curRow[j-1];
      int upperCell =  (j == bufferNeeded-1) ? 0 : prevRow[j+1];

      int tempScore1 = match ? upperDiagonalCell + MATCH_SCORE : upperDiagonalCell - MISMATCH_PENALTY;
      int tempScore2 = (j == 0) ? std::numeric_limits<int>::min() : leftCell - GAP_PENALTY;   // -INF for not allowing gaps outside the band
      int tempScore3 = (j == bufferNeeded-1) ? std::numeric_limits<int>::min() : upperCell - GAP_PENALTY;

      if(tempScore2 >= tempScore1 && tempScore2 >= tempScore3)
      {
        curRow[j] = tempScore2;
        pointers[i*bufferNeeded + j] = 2;
      }
      else if(tempScore3 >= tempScore1 && tempScore3 >= tempScore2)
      {
        curRow[j] = tempScore3;
        pointers[i*bufferNeeded + j] = 3;
      }
      else
      {
        curRow[j] = tempScore1;
        pointers[i*bufferNeeded + j] = match ? 0 : 1;
      }

      lastScore = curRow[j];
      i_Last = i;
      j_Last = j;
    }

    memcpy(prevRow, curRow,  bufferNeeded * sizeof(int));

    //Towards the last few iterations
    if (gapLimit + 1 + i >= len2)
      endIndex--;

  }

  //Final Traceback
  int totalMatches = 0;
  int totalMismatches = 0;
  int totalGaps = 0;
  int totalAlignmentLength = 0;

  int initialJ = j_Last;
  for(int i=i_Last ; i >= 0 ; i--)
  {
    if(pointers[i*bufferNeeded + initialJ] == 0)
      totalMatches += 1;
    else if(pointers[i*bufferNeeded + initialJ] == 1)
      totalMismatches += 1;
    else if(pointers[i*bufferNeeded + initialJ] == 2)
    {
      totalGaps += 1;
      initialJ -= 1;
      i+=1;
    }
    else if(pointers[i*bufferNeeded + initialJ] == 3)
    {
      totalGaps += 1;
      initialJ += 1;
    }
    totalAlignmentLength += 1;
  }

  int denominator = totalAlignmentLength - totalGaps;
  float errorPercent = ((float)totalMismatches*100)/denominator;
  isLow = (errorPercent <= PCT_THRESHOLD);

  errorPercent = (int)(errorPercent*100);
  errorPercent /= 100;
#ifdef DEBUG
  if (isLow) {
      ots << totalMatches << "\t" << totalMismatches << "\t"
          << totalGaps << "\t" << lastScore << "\t"
          << denominator << "\t" << errorPercent << "\t" << isLow;
  }
  else{
      ots << totalMatches << "\t" << totalMismatches << "\t"
          << totalGaps << "\t" << lastScore << "\t"
          << denominator << "\t" << errorPercent << "\t" << isLow;
  }
#endif


  //Free memory
  free(curRow);
  free(prevRow);
  free(pointers);
#ifdef DEBUG
  return true;
#endif
  return isLow;
}

bool align_band_with_traceback(const std::string& seq1,
                               const std::string& seq2,
                               int& score, bool& isLow, std::ostream& ots,
                               const int gapLimit,
                               const float PCT_THRESHOLD,
                               const int MATCH_SCORE,
                               const int GAP_PENALTY,
                               const int MISMATCH_PENALTY
                               ){
    return align_band_with_traceback(seq1.c_str(), seq1.length(),
                                     seq2.c_str(), seq2.length(),
                                     score, isLow, ots,
                                     gapLimit,
                                     PCT_THRESHOLD,
                                     MATCH_SCORE,
                                     GAP_PENALTY,
                                     MISMATCH_PENALTY
                                     );
}
