#ifndef ALIGN_H
#define ALIGN_H
#include <ostream>
#include <string>
bool align_band_with_traceback(const char *seq1, const int& len1,
                               const char *seq2, const int& len2,
                               int& score, bool& isLow, std::ostream& ots,
                               const int gapLimit,
                               const float PCT_THRESHOLD = 3.0,
                               const int MATCH_SCORE = 1,
                               const int GAP_PENALTY = 3,
                               const int MISMATCH_PENALTY = 0
                               );

bool align_band_with_traceback(const std::string& seq1,
                               const std::string& seq2,
                               int& score, bool& isLow, std::ostream& ots,
                               const int gapLimit,
                               const float PCT_THRESHOLD = 3.0,
                               const int MATCH_SCORE = 1,
                               const int GAP_PENALTY = 3,
                               const int MISMATCH_PENALTY = 0
                               );


#endif /* ALIGN_H */
