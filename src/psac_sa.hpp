#ifndef PSAC_SA_H
#define PSAC_SA_H

// include MPI
#include <mpi.h>

// C++ includes
#include <fstream>
#include <iostream>
#include <string>


#include <suffix_array.hpp>
#include <mxx/file.hpp>
#include "run_stats.hpp"
#include "fastq_reader.hpp"
#include "bgzf_file.hpp"

template<typename T>
void make_sa(std::string txtfn,
             std::string& local_str,
             std::vector<T>& gsa,
             std::vector<T>& glcp,
             MPI_Comm _comm){
  mxx::comm comm(_comm);
  local_str = mxx::file_block_decompose(txtfn.c_str(), _comm);
  suffix_array<char, T, true> sa(comm);
  sa.construct(local_str.begin(), local_str.end(),true);

  gsa.swap(sa.local_SA);
  glcp.swap(sa.local_LCP);
}

template<typename T>
T read_lcp(const T& psa, const T& csa,
                  T rlen, T lcpv){
    if(rlen > 0){
        T read_pos = std::max(csa % (rlen + 1), psa % (rlen + 1));
        lcpv = (lcpv > (rlen - read_pos)) ? (rlen - read_pos) : lcpv;
    }
    return lcpv;
}

template<typename T, typename S, typename L>
void dist_sa_lcp(uint64_t fsize, std::string& local_str,
                 std::vector<uint64_t>& vsa,
                 std::vector<uint64_t>& vlcp,
                 std::vector<T>& out_txt,
                 std::vector<S>& out_sa,
                 std::vector<L>& out_lcp,
                 L rlen,
                 RunStats& rstats, L max_value,
                 MPI_Comm _comm){
    int rank, nproc;
    MPI_Comm_rank(_comm, &rank);
    MPI_Comm_size(_comm, &nproc);
    if(max_value == 0)
        max_value = std::numeric_limits<L>::max();
    if(rank == 0)
        std::cout << "\"fsz\" : " << fsize << "," << std::endl;

    out_lcp.resize(vlcp.size());
    uint64_t next_vsa = vsa.back(), pr_vsa;
    MPI_Status mpstat;
    int src = (rank - 1 + nproc) % nproc;
    int dst = (rank + 1) % nproc;
    MPI_Sendrecv(&next_vsa, 1, MPI_UNSIGNED_LONG_LONG, dst, 0,
                 &pr_vsa, 1, MPI_UNSIGNED_LONG_LONG, src, 0,
                 _comm, &mpstat);
    if(rank == 0)
        out_lcp[0] = 0;
    else{
        uint64_t lcpv = read_lcp<uint64_t>(pr_vsa, vsa[0], rlen, vlcp[0]);
        if(lcpv > (uint64_t) max_value)
            out_lcp[0] = max_value;
        else
            out_lcp[0] = (L) lcpv;
    }
    for(std::size_t ix = 1; ix < vlcp.size(); ix++){
        uint64_t lcpv = read_lcp<uint64_t>(vsa[ix - 1], vsa[ix], rlen, vlcp[ix]);
        if(lcpv > (uint64_t) max_value)
            out_lcp[ix] = max_value;
        else
            out_lcp[ix] = (L) lcpv;
    }
    vlcp.clear();
    rstats.mem_report(std::cout);
    out_sa.resize(vsa.size());
    for(uint64_t ix = 0; ix < vsa.size(); ix++)
        out_sa[ix] = vsa[ix];
    vsa.clear();
    rstats.mem_report(std::cout);

    out_txt.resize(local_str.length());
    for(std::size_t ix = 0; ix < out_txt.size(); ix++)
        out_txt[ix] = local_str[ix];
    local_str = "";

    load_balance_stable(out_txt, get_mpi_dt<T>(), _comm);
    assert(out_txt.size() == block_size(rank, nproc, fsize));
    rstats.mem_report(std::cout);
    load_balance_stable(out_sa, get_mpi_dt<S>(), _comm);
    assert(out_sa.size() == block_size(rank, nproc, fsize));
    rstats.mem_report(std::cout);
    load_balance_stable(out_lcp, get_mpi_dt<L>(), _comm);
    assert(out_lcp.size() == block_size(rank, nproc, fsize));
    rstats.mem_report(std::cout);
}

template< typename CharType, typename SizeType,
          typename InputStream, typename FileReader>
uint64_t reads_sa(const std::string& inpfn,
                  InputStream& istrm,
                  std::string& local_str,
                  std::vector<uint64_t>& vsa,
                  std::vector<uint64_t>& vlcp,
                  bool fwd,
                  SizeType offsetEnd,
                  SizeType nreads,
                  MPI_Comm _comm) {
    int rank, nproc;
    MPI_Comm_rank(_comm, &rank);
    MPI_Comm_size(_comm, &nproc);
     // offsetStart = (fileSize/m_size) * m_rank;
    uint64_t fsize = 0;
    ReadStore<CharType, SizeType> rstore('$');
    // load the reads
    read_batch<CharType, SizeType,
               InputStream, FileReader>(istrm, nreads,
                                        offsetEnd, rstore);
    if(fwd == false) rstore.reverse();

    // redistribute based on block parition
    struct mxx_partitioner{
      inline std::vector<SizeType> operator()(SizeType n,int  p, int rank){
        mxx::blk_dist bprt(n, p, rank);
        std::vector<SizeType> parts(p);
        for(int i = 0; i < p;i++)
          parts[i] = bprt.local_size(i);
        return  parts;
      }
    };


    // construct the distributed string
    load_balance_stable<char, SizeType, mxx_partitioner>(rstore.readsString,
                                                         get_mpi_dt<char>(),
                                                         _comm);
    local_str.resize(rstore.readsString.size(), 0);
    for(SizeType ix = 0; ix < local_str.length(); ix++)
        local_str[ix] = rstore.readsString[ix];

    SizeType local_size = local_str.size();
    MPI_Allreduce(&local_size, &fsize, 1, get_mpi_dt<SizeType>(), MPI_SUM, _comm);
    // if(rank == 0)
    //  std::cout << fsize << std::endl;
    if(rank == 0)
        std::cout << "\"fsz\" : " << fsize << "," << std::endl;

    // make sa
    mxx::comm comm(_comm);
    suffix_array<char, uint64_t, true> sa(comm);
    sa.construct(local_str.begin(), local_str.end(),true);

    vsa.swap(sa.local_SA);
    vlcp.swap(sa.local_LCP);

    return fsize;
}

template <typename SizeType>
void getOffsets(bool fwd, SizeType gsize,
                SizeType& offsetStart, SizeType& offsetEnd,
                MPI_Comm _comm){
    int rank, nproc;
    MPI_Comm_rank(_comm, &rank);
    MPI_Comm_size(_comm, &nproc);
    if(fwd){
        offsetStart = (gsize/nproc) * (rank);
        offsetEnd = (gsize/nproc) * (rank + 1);
    } else {
        int drnk = nproc - 1 - rank;
        offsetStart = (gsize/nproc) * (drnk);
        offsetEnd = (gsize/nproc) * (drnk + 1);
    }
}

template<typename T, typename S, typename L>
uint64_t load_gen(AppConfig& param, RunStats& rstats, bool fwd,
                  std::vector<T>& out_txt,
                  std::vector<S>& out_sa,
                  std::vector<L>& out_lcp,
                  MPI_Comm _comm){

    typedef StreamWrapper<std::ifstream> text_stream_t;
    typedef FastaReader<text_stream_t>  fasta_txt_t;
    typedef FastqReader<text_stream_t>  fastq_txt_t;
    typedef FastaReader<BGZFFile>  fasta_bgzf_t;
    typedef FastqReader<BGZFFile>  fastq_bgzf_t;

    std::string txtfn = (fwd) ? param.txtfn : param.rtxtfn;
    std::string inpfn = param.inpf;
    L rlen = (L) param.read_length;
    L max_value = 0;

    uint64_t offsetStart, offsetEnd;
    uint64_t fsize, gsize;
    std::string local_str;
    std::vector<uint64_t> vsa;
    std::vector<uint64_t> vlcp;
    S max_reads = 1000000;

    int rank, nproc;
    MPI_Comm_rank(_comm, &rank);
    MPI_Comm_size(_comm, &nproc);

    if(ends_with(txtfn, std::string(".txt"))){
        fsize = file_size<char>(txtfn);
        gsize = fsize;
        make_sa<uint64_t>(txtfn, local_str, vsa, vlcp, _comm);
        rstats.mem_report(std::cout);
    } else if(ends_with(inpfn, std::string(".fa")) ||
              ends_with(inpfn, std::string(".fasta"))){
        // TODO: load reads
        gsize = file_size<char>(inpfn);
        gsize *= param.data_factor;
        gsize /= 100;

        getOffsets(fwd, gsize, offsetStart, offsetEnd, _comm);
        text_stream_t fa_stream(inpfn.c_str(), offsetStart);
        fsize = reads_sa<T, S, text_stream_t,
                         fasta_txt_t>(inpfn, fa_stream,
                                      local_str,
                                      vsa, vlcp, fwd,
                                      offsetEnd,
                                      max_reads, _comm);

    } else if(ends_with(inpfn, std::string(".fq")) ||
              ends_with(inpfn, std::string(".fastq"))){
        gsize = file_size<char>(inpfn);
        gsize *= param.data_factor;
        gsize /= 100;

        getOffsets(fwd, gsize, offsetStart, offsetEnd, _comm);
        text_stream_t fq_stream(inpfn.c_str(), offsetStart);
        fsize = reads_sa<T, S, text_stream_t,
                         fastq_txt_t>(inpfn, fq_stream,
                                      local_str,
                                      vsa, vlcp, fwd,
                                      offsetEnd,
                                      max_reads, _comm);
    } else if(ends_with(inpfn, std::string(".fa.gz")) ||
              ends_with(inpfn, std::string(".fasta.gz"))){
        std::string idx_fn = inpfn + ".gzi";
        gsize = BGZFFile::size_mbs(inpfn.c_str(), idx_fn.c_str());
        gsize *= param.data_factor;
        gsize /= 100;

        getOffsets(fwd, gsize, offsetStart, offsetEnd, _comm);
        BGZFFile bgzf_stream(inpfn.c_str(), idx_fn.c_str(), offsetStart);

        fsize = reads_sa<T, S, BGZFFile,
                         fasta_bgzf_t>(inpfn, bgzf_stream,
                                       local_str,
                                       vsa, vlcp, fwd,
                                       offsetEnd,
                                       max_reads, _comm);

    } else if(ends_with(inpfn, std::string(".fq.gz")) ||
              ends_with(inpfn, std::string(".fastq.gz"))){
        std::string idx_fn = inpfn + ".gzi";
        gsize = BGZFFile::size_mbs(inpfn.c_str(), idx_fn.c_str());
        gsize *= param.data_factor;
        gsize /= 100;

        getOffsets(fwd, gsize, offsetStart, offsetEnd, _comm);
        BGZFFile bgzf_stream(inpfn.c_str(), idx_fn.c_str(), offsetStart);

        fsize = reads_sa<T, S, BGZFFile,
                         fastq_bgzf_t>(inpfn, bgzf_stream,
                                       local_str,
                                       vsa, vlcp, fwd,
                                       offsetEnd,
                                       max_reads, _comm);
    } else {
        return 0;
    }

    if(rank == 0)
        std::cout << "\"gsz\" : " << gsize << "," << std::endl;
    param.batch_width = (fsize / (param.K * param.K * param.batch_factor));
    dist_sa_lcp<T,S,L>(fsize, local_str, vsa, vlcp,
                       out_txt, out_sa, out_lcp,
                       rlen, rstats, max_value, _comm);
    return fsize;
}
#endif /* PSAC_SA_H */
