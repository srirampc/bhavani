#ifndef RUN_HEURISTIC_H
#define RUN_HEURISTIC_H

#include <vector>
#include <string>
#include <iostream>
#include <cassert>
#include <mpi.h>
#include "AppConfig.hpp"
#include "trie_ds.hpp"
#include "suffix_ds.hpp"
#include "run_stats.hpp"
#include "mismatch_sfx.hpp"
#include "load_sa.hpp"
#include "psac_sa.hpp"

template<typename T, typename S, typename L>
void run(AppConfig& param, RunStats& rstats){
    typedef typename T::dchar_t dchar_t;
    typedef typename T::dcount_t dcount_t;
    typedef typename T::dsize_t dsize_t;

    std::vector<typename T::dchar_t> f_txt;
    std::vector<typename T::dsize_t> f_sa;
    std::vector<typename T::dcount_t> f_lcp;

    // Build SA, ISA and LCP
    dsize_t dist_size  = 0;
    rstats.record_start();
    if(param.load_gen)
        dist_size = load_gen<dchar_t, dsize_t, dcount_t>(param, rstats, true,
                                                         f_txt, f_sa, f_lcp,
                                                         rstats.comm());
    else
        dist_size = load_files<dchar_t, S, L,
                               dsize_t, dcount_t>(param.txtfn, param.safn,
                                                  param.lcpfn,
                                                  f_txt, f_sa, f_lcp,
                                                  rstats.comm());

    T sadt(f_txt, f_sa, f_lcp, dist_size, rstats.comm());
    rstats.mem_report(std::cout);
    // sadt.clear_txt();
    rstats.construct_fwd(std::cout);

    if(param.stats_only) {
        sadt.print_summary(std::cout);
        dcount_t rb = ((dcount_t)param.tau) - 5;
        dcount_t re = rb + 10;
        sa_stats(sadt, rb, re, std::cout, "", rstats.comm());
        return;
    }

    rstats.record_start();
    dist_size = 0;
    if(param.load_gen)
        dist_size = load_gen<dchar_t, dsize_t, dcount_t>(param, rstats, false,
                                                         f_txt, f_sa, f_lcp,
                                                         rstats.comm());
    else
        dist_size = load_files<dchar_t, S, L,
                               dsize_t, dcount_t>(param.rtxtfn, param.rsafn,
                                                  param.rlcpfn,
                                                  f_txt, f_sa, f_lcp,
                                                  rstats.comm());
    T rev_sadt(f_txt, f_sa, f_lcp, dist_size, rstats.comm());
    rev_sadt.clear_sa();
    // rev_sadt.clear_txt();
    rstats.mem_report(std::cout);
    rstats.construct_rev(std::cout);

    // rev_sadt.print_summary(std::cout);
    // k-mismatch suffixes
    if(param.gen_pairs){
        //mismatch_suffixes<T, TxtDBCSPairGenerator<T> >
        mismatch_suffixes<T, CSPairGenerator<T> >
            (sadt, rev_sadt, param, rstats);
    } else if(param.gen_maximal){
        mismatch_suffixes<T, MaximalCSPairGenerator<T> >
            (sadt, rev_sadt, param, rstats);
    } else {
        mismatch_suffixes<T, DefaultPairGenerator<T> >
            (sadt, rev_sadt, param, rstats);
    }
}

template<typename T, typename S, typename L>
void run_heuristic_sfx(std::string& txtfn, std::string& safn, std::string& lcpfn,
                       std::vector<typename  T::dchar_t>& vtxt,
                       AppConfig& param, RunStats& rstats,
                       bool fwd = true){
    typedef typename T::dchar_t dchar_t;
    typedef typename T::dcount_t dcount_t;
    typedef typename T::dsize_t dsize_t;

    std::vector<dchar_t> f_txt;
    std::vector<dsize_t> f_sa;
    std::vector<dcount_t> f_lcp;

    // Build SA, ISA and LCP
    dsize_t dist_size = 0;
    if(param.load_gen)
        dist_size = load_gen<dchar_t, dsize_t, dcount_t>(param, rstats, fwd,
                                                         f_txt, f_sa, f_lcp,
                                                         rstats.comm());
    else
        dist_size = load_files<dchar_t, S, L,
                               dsize_t, dcount_t>(txtfn, safn, lcpfn,
                                                  f_txt, f_sa, f_lcp,
                                                  rstats.comm());


    rstats.mem_report(std::cout);
    MPI_Barrier(rstats.comm());
    rstats.record_start();
    T sadt(f_txt, f_sa, f_lcp, dist_size, rstats.comm());
    rstats.mem_report(std::cout);
    if(fwd)
      rstats.construct_fwd(std::cout);
    else
      rstats.construct_rev(std::cout);

    if(param.stats_only) {
        dcount_t rb = ((dcount_t)param.tau) - 5;
        dcount_t re = rb + 10;
        sa_stats(sadt, rb, re, std::cout, "", rstats.comm());
        return;
    }

    heuristic_mismatch_suffixes(sadt, param, rstats, vtxt, fwd);
}

template<typename T, typename S, typename L>
void run_heuristic(AppConfig& param, RunStats& rstats){
    typedef typename T::dchar_t dchar_t;

    std::vector<dchar_t> vtxt;
    if(param.K > 1){
      uint64_t fsize = file_size<dchar_t>(param.txtfn);
      vtxt.resize(fsize);
      std::ifstream txtfs(param.txtfn, std::ios::in | std::ios::binary);
      txtfs.read((char *) &vtxt[0], sizeof(dchar_t) * fsize);
      txtfs.close();
    }

    rstats.mem_report(std::cout);
    // fowrad run
    run_heuristic_sfx<T, S, L>(param.txtfn, param.safn, param.lcpfn,
                               vtxt, param, rstats);
    if(param.stats_only)
        return;
    rstats.mem_report(std::cout);
    // reverse run
    run_heuristic_sfx<T, S, L>(param.rtxtfn, param.rsafn, param.rlcpfn,
                               vtxt, param, rstats, false);
}


template<typename T, typename S, typename L>
void run_heuristic_0(AppConfig& param, RunStats& rstats){
    typedef typename T::dchar_t dchar_t;
    typedef typename T::dcount_t dcount_t;
    typedef typename T::dsize_t dsize_t;

    std::vector<dchar_t> f_txt;
    std::vector<dsize_t> f_sa;
    std::vector<dcount_t> f_lcp;

    // Build SA, ISA and LCP
    dsize_t dist_size = 0;
    if(param.load_gen)
        dist_size = load_gen<dchar_t, dsize_t, dcount_t>(param, rstats, true,
                                                         f_txt, f_sa, f_lcp,
                                                         rstats.comm());
    else
        dist_size = load_files<dchar_t, S, L,
                               dsize_t, dcount_t>(param.txtfn, param.safn,
                                                  param.lcpfn,
                                                  f_txt, f_sa, f_lcp,
                                                  rstats.comm());

    rstats.mem_report(std::cout);

    rstats.record_start();
    T sadt(f_txt, f_sa, f_lcp, dist_size, rstats.comm());
    rstats.mem_report(std::cout);
    rstats.construct_fwd(std::cout);

    if(param.stats_only) {
        dcount_t rb = ((dcount_t)param.tau) - 5;
        dcount_t re = rb + 10;
        sa_stats(sadt, rb, re, std::cout, "", rstats.comm());
        return;
    }

    heuristic_mismatch_suffixes_0(sadt, param, rstats);
}


template<typename T, typename S, typename L>
void run_dynamic(AppConfig& param, RunStats& rstats){
    typedef typename T::dchar_t dchar_t;
    typedef typename T::dcount_t dcount_t;
    typedef typename T::dsize_t dsize_t;

    std::vector<dchar_t> f_txt;
    std::vector<dsize_t> f_sa;
    std::vector<dcount_t> f_lcp;

    int rank;
    MPI_Comm _comm = rstats.comm();
    MPI_Comm_rank(_comm, &rank);
    // test_dynamic(param, rstats);
    // return;

    // Build SA, ISA and LCP
    rstats.record_start();
    load_full_files<dchar_t,
                    S, L,
                    dsize_t, dcount_t>(param.txtfn, param.safn, param.lcpfn,
                                       f_txt, f_sa, f_lcp);

    rstats.mem_report(std::cout);
    T sadt(f_txt, f_sa, f_lcp, file_size<dchar_t>(param.txtfn));
    //sadt.clear_txt();
    rstats.mem_report(std::cout);
    rstats.construct_fwd(std::cout);

    rstats.record_start();
    load_full_files<dchar_t, S, L,
                    dsize_t, dcount_t>(param.rtxtfn, param.rsafn, param.rlcpfn,
                                       f_txt, f_sa, f_lcp);
    T rev_sadt(f_txt, f_sa, f_lcp, file_size<dchar_t>(param.rtxtfn));
    rev_sadt.clear_sa();
    rev_sadt.clear_txt();
    rstats.mem_report(std::cout);
    rstats.construct_rev(std::cout);

    if(rank == 0){
        sadt.print_summary(std::cout);
        rev_sadt.print_summary(std::cout);
    }

    dynamic_mismatch_suffixes(sadt, rev_sadt, param, rstats, _comm);
}

#endif /* RUN_HEURISTIC_H */
