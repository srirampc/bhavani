#include <iomanip>
#include <sstream>
#include "run_stats.hpp"
#include "util.hpp"
#include "mpi_util.hpp"

// double elapsed_local(const timespec& finish, const timespec& start){
//   double tdiff;
//   tdiff = (finish.tv_sec - start.tv_sec);
//   tdiff += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
//   return tdiff;
// }

// returns time elapsed since creation

typedef std::chrono::duration<double, std::milli> duration_t;

int RunStats::mem_report_count = 0;

RunStats::RunStats(MPI_Comm cx){
    _comm = cx;
    MPI_Comm_rank(_comm, &rank);
    MPI_Comm_size(_comm, &nproc);
    MPI_Barrier(_comm);
    mpi_curr_start = MPI_Wtime();
    curr_start = std::chrono::steady_clock::now();
    total_start = curr_start;
    mpi_total_start = mpi_curr_start;
    constr_fwd = constr_rev = region_build = root_nodes = 0.0;
    fwd_runs_batch = rev_runs_batch = 0.0;
    reverse_runs = forward_runs = 0;
    mpi_forward_runs = mpi_reverse_runs = 0;
    mpi_constr_fwd = mpi_constr_rev = 0;
    mpi_fwd_runs_batch = mpi_rev_runs_batch = 0;
    mpi_region_build = mpi_root_nodes = 0;
    nfwd = nrev = 0;
    reset_trie_build();
}

void RunStats::report_time(const char* phase_name,
                           std::ostream& ofs){
    if(rank == 0){
        std::stringstream oss;
        oss
            << "\"" << phase_name << "\" : ["
            << std::setw(15) << (min_time/1000.0)
            << ","
            << std::setw(15) << (max_time/1000.0)
            << ","
            << std::setw(15) << (avg_time/1000.0)
            << ","
            << std::setw(15) << mpi_phase_time
            << "], "
            << std::endl;
        ofs << oss.str();
    }
}

#ifdef DISABLE_RUNSTATS
void RunStats::record_start(){};
void RunStats::record_stop(){};
void RunStats::reduce_timings(){};
void RunStats::get_minmax_timings(){};
void RunStats::record_trie_start(){};
void RunStats::record_trie_stop(double sz,
                          double tw){};
void RunStats::reset_trie_build(){};
void RunStats::construct_fwd(std::ostream& ofs){};
void RunStats::construct_rev(std::ostream& ofs){};
void RunStats::root_nodes_time(std::ostream& ofs){};
void RunStats::root_schedule_time(std::ostream& ofs){};
void RunStats::select_region_time(std::ostream& ofs){};
void RunStats::batch_loop_start(){};
void RunStats::batch_loop_stop(std::ostream& ofs){};
void RunStats::forward_run(std::ostream& ofs, bool in_batch){};
void RunStats::reverse_run(std::ostream& ofs, bool in_batch){};
void RunStats::mem_report(std::ostream& ofs){};
void RunStats::report_all(const char* phase_name,
                    double rtime, double btime,
                    std::ostream& ofs){};

void RunStats::finalize(std::ostream& ofs){
    curr_stop = std::chrono::steady_clock::now();
    MPI_Barrier(_comm);
    mpi_curr_stop = MPI_Wtime();
    phase_time = elapsed_local<duration_t>(total_start, curr_stop);
    mpi_phase_time = mpi_curr_stop - mpi_total_start;
    double sum_time;
    MPI_Allreduce(&phase_time, &min_time, 1, MPI_DOUBLE, MPI_MIN, _comm);
    MPI_Allreduce(&phase_time, &max_time, 1, MPI_DOUBLE, MPI_MAX, _comm);
    MPI_Allreduce(&phase_time, &sum_time, 1, MPI_DOUBLE, MPI_SUM, _comm);
    avg_time = sum_time / (1.0 * nproc);
    report_time("complete run", ofs);
};

#else

void RunStats::reduce_timings(){
    double sum_time;
    MPI_Allreduce(&phase_time, &min_time, 1, MPI_DOUBLE, MPI_MIN, _comm);
    MPI_Allreduce(&phase_time, &max_time, 1, MPI_DOUBLE, MPI_MAX, _comm);
    MPI_Allreduce(&phase_time, &sum_time, 1, MPI_DOUBLE, MPI_SUM, _comm);
    avg_time = sum_time / (1.0 * nproc);
}

void RunStats::get_minmax_timings(){
    mpi_phase_time = mpi_curr_stop - mpi_curr_start;
    phase_time = elapsed_local<duration_t>(curr_start, curr_stop);
    reduce_timings();
}

void RunStats::record_start(){
    MPI_Barrier(_comm);
    mpi_curr_start = MPI_Wtime();
    curr_start = std::chrono::steady_clock::now();
}

void RunStats::record_stop(){
    curr_stop = std::chrono::steady_clock::now();
    MPI_Barrier(_comm);
    mpi_curr_stop = MPI_Wtime();
    get_minmax_timings();
}

void RunStats::report_all(const char* phase_name, double rtime, double btime,
                          std::ostream& ofs){
    std::vector<double> vrtime(nproc), vbtime(nproc);
    MPI_Gather(&rtime, 1, MPI_DOUBLE,
               &vrtime[0], 1, MPI_DOUBLE, 0, _comm);
    MPI_Gather(&btime, 1, MPI_DOUBLE,
               &vbtime[0], 1, MPI_DOUBLE, 0, _comm);
  if(rank == 0) {
      for(int j = 0; j < nproc; j++) {
          std::stringstream oss;
          oss
              << "\"" << phase_name << "_" << j << "\" : ["
              << std::setw(15) <<  vrtime[j]
              << ","
              << std::setw(15) <<  vbtime[j]
              << "], "
            << std::endl;
          ofs << oss.str();
      }
  }
}

void RunStats::record_trie_start(){
    mpi_trie_start = MPI_Wtime();
    trie_start = std::chrono::steady_clock::now();
}

void RunStats::record_trie_stop(double sz, double tw){
    trie_stop = std::chrono::steady_clock::now();
    trie_build += elapsed_local<duration_t>(trie_start, trie_stop);
    mpi_trie_build += MPI_Wtime() - mpi_trie_start;
    trie_internal_size += sz;
    trie_internal_width += tw;
}

void RunStats::reset_trie_build(){
    trie_build = 0.0;
    mpi_trie_build = 0.0;
    trie_internal_size = 0.0;
    trie_internal_width = 0.0;
}

void RunStats::construct_fwd(std::ostream& ofs){
    record_stop();
    report_time("construct fwd", ofs);
    constr_fwd = phase_time;
    mpi_constr_fwd = mpi_phase_time;
    // record_start();
}

void RunStats::construct_rev(std::ostream& ofs){
    record_stop();
    report_time("construct rev", ofs);
    constr_rev = phase_time;
    mpi_constr_rev = mpi_phase_time;
    // record_start();
}

void RunStats::select_region_time(std::ostream& ofs){
    record_stop();
    report_time("select region", ofs);
    region_build = phase_time;
    mpi_region_build = mpi_phase_time;
    // record_start();
}

void RunStats::batch_loop_start(){
    record_start();
    batch_loop = curr_start;
    mpi_batch_loop = mpi_curr_start;
}

void RunStats::batch_loop_stop(std::ostream& ofs){
    curr_stop = std::chrono::steady_clock::now();
    MPI_Barrier(_comm);
    mpi_phase_time = MPI_Wtime() - mpi_batch_loop;
    phase_time = elapsed_local<duration_t>(batch_loop, curr_stop);
    reduce_timings();
    report_time("batch loop", ofs);
    batch_loop_run = phase_time;
    mpi_batch_loop_run = mpi_phase_time;
}

void RunStats::root_nodes_time(std::ostream& ofs){
    record_stop();
    report_time("root_nodes", ofs);
    root_nodes = phase_time;
    mpi_root_nodes = mpi_phase_time;
    // report_all("root_nodes", phase_time, ofs);
    // record_start();
}

void RunStats::root_schedule_time(std::ostream& ofs){
    record_stop();
    report_time("root_schedule", ofs);
    root_schedule = phase_time;
    mpi_root_schedule = mpi_phase_time;
    // report_all("root_nodes", phase_time, ofs);
    // record_start();
}

void RunStats::forward_run(std::ostream& ofs, bool in_batch){
    record_stop();
    std::stringstream oss;
    nfwd++; nrev = 0;
    oss << "forward_run_" << nfwd;
    report_time(oss.str().c_str(), ofs);
    // report_all(oss.str().c_str(), phase_time, mpi_phase_time, ofs);

    forward_runs += phase_time;
    mpi_forward_runs += mpi_phase_time;
    if(in_batch){
        fwd_runs_batch += phase_time;
        mpi_fwd_runs_batch += mpi_phase_time;
    }
    std::stringstream oss2;
    oss2 << "trie_forward_run_" << nfwd;
    phase_time = trie_build;
    mpi_phase_time = mpi_trie_build;
    report_time(oss2.str().c_str(), ofs);
    // report_all(oss2.str().c_str(), phase_time, mpi_phase_time, ofs);

    std::stringstream oss3;
    oss3 << "trie_forward_run_size_" << nfwd;
    sum_max_report(oss3.str().c_str(), trie_internal_width,
                   rank, _comm, ofs);
    // report_all(oss3.str().c_str(),
    //           trie_internal_size, trie_internal_width, ofs);
    reset_trie_build();
    // record_start();
}

void RunStats::reverse_run(std::ostream& ofs, bool in_batch){
    record_stop();
    std::stringstream oss;
    nrev++;
    oss << "reverse runs (" << nfwd << "," << nrev << ")";
    report_time(oss.str().c_str(), ofs);
    // report_all(oss.str().c_str(), phase_time, mpi_phase_time, ofs);
    reverse_runs += phase_time;
    mpi_reverse_runs += mpi_phase_time;
    if(in_batch){
        rev_runs_batch += phase_time;
        mpi_rev_runs_batch += mpi_phase_time;
    }
    std::stringstream oss2;
    oss2 << "trie_reverse runs (" << nfwd << "," << nrev << ")";
    phase_time = trie_build;
    mpi_phase_time = mpi_trie_build;
    report_time(oss2.str().c_str(), ofs);
    // report_all(oss2.str().c_str(), phase_time, mpi_phase_time, ofs);

    std::stringstream oss3;
    oss3 << "trie_reverse runs size ("
         << nfwd << "," << nrev << ")";
    sum_max_report(oss3.str().c_str(), trie_internal_width,
                   rank, _comm, ofs);
    // report_all(oss3.str().c_str(),
    //           trie_internal_size, trie_internal_width, ofs);
    reset_trie_build();
    // record_start();
}

void RunStats::finalize(std::ostream& ofs){
    record_stop();
    phase_time = elapsed_local<duration_t>(total_start, curr_stop);
    mpi_phase_time = mpi_curr_stop - mpi_total_start;
    reduce_timings();
    report_time("total run", ofs);
    phase_time = forward_runs;
    mpi_phase_time = mpi_forward_runs;
    reduce_timings();
    report_time("forward runs", ofs);
    phase_time = reverse_runs;
    mpi_phase_time = mpi_reverse_runs;
    reduce_timings();
    report_time("reverse runs", ofs);
    phase_time = fwd_runs_batch;
    mpi_phase_time = mpi_fwd_runs_batch;
    reduce_timings();
    report_time("forward runs batch", ofs);
    phase_time = rev_runs_batch;
    mpi_phase_time = mpi_rev_runs_batch;
    reduce_timings();
    report_time("reverse runs batch", ofs);
    phase_time = reverse_runs + forward_runs + root_nodes + root_schedule +
        region_build;
    mpi_phase_time = mpi_reverse_runs + mpi_forward_runs + mpi_root_nodes +
        mpi_root_schedule + mpi_region_build;
    reduce_timings();
    report_time("total computation", ofs);
    double qtm = get_query_timings(),
      a2atm = get_a2a_timings(), qmiscl = qtm - a2atm;
    phase_time -= qtm;
    mpi_phase_time  -= (qtm/1000.0);
    reduce_timings();
    report_time("total computation - query.", ofs);
    phase_time = reverse_runs + forward_runs + root_nodes + root_schedule +
        region_build;
    mpi_phase_time = mpi_reverse_runs + mpi_forward_runs + mpi_root_nodes +
        mpi_root_schedule + mpi_region_build;
    phase_time -= a2atm;
    mpi_phase_time  -= (a2atm/1000.0);
    reduce_timings();
    report_time("total computation - a2a.", ofs);
    phase_time = qmiscl;
    mpi_phase_time  = qmiscl;
    reduce_timings();
    report_time("qmiscl.", ofs);
    phase_time = batch_loop_run + root_nodes +
        root_schedule + region_build;
    mpi_phase_time = mpi_batch_loop_run +  mpi_root_nodes +
        mpi_root_schedule + mpi_region_build;
    reduce_timings();
    report_time("total comp. w loop", ofs);
    if(rank == 0)
        ofs
            << "\"mem_report_count\" : "
            <<  mem_report_count << "," << std::endl;
}


void RunStats::mem_report(std::ostream& ofs){
    unsigned long int max_usage, my_usage;
    my_usage = mem_usage();
    MPI_Allreduce(&my_usage, &max_usage, 1,
                  MPI_UNSIGNED_LONG, MPI_MAX, _comm);
    mem_report_count += 1;
    if(rank == 0)
      ofs
            << "\"" << "mem_usage_"
            << mem_report_count << "\" : ["
            << mem_report_count << ","
            << ((1.0 * max_usage) / (1024 * 1024)) << "],"
            << std::endl;
}
#endif
