#ifndef SUFFIX_DS_H
#define SUFFIX_DS_H

#include <iomanip>
#include <vector>
#include <stack>
#include <mpi.h>
#include "util.hpp"
#include "parallel_utils.hpp"
#include "mpi_util.hpp"
#include "query_oracle.hpp"
#include "abstract_ds.hpp"

template<typename ValueType>
struct SuffixPair{
    ValueType first;
    ValueType second;
};

template< typename CharType,
          typename SizeType,
          typename CountType,
          typename RMQTable>
class SuffixDS
    : public AbstractSuffixDS<typename std::vector<CharType>::const_iterator,
                              typename std::vector<SizeType>::const_iterator,
                              typename std::vector<CountType>::const_iterator>{
public:
    typedef RMQTable drmq_t;
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;
    typedef SuffixPair<SizeType> spair_t;
    typedef uint32_t snds_t;

    typedef typename std::vector<CharType>::const_iterator txt_iterator_t;
    typedef typename std::vector<SizeType>::const_iterator sa_iterator_t;
    typedef typename std::vector<CountType>::const_iterator lcp_iterator_t;

protected:
    SizeType m_dist_size;
    SizeType m_blk_begin;
    SizeType m_local_size;

    // vector of data structures : SA, ISA, LCP, NDS
    std::vector<SizeType> m_sa, m_isa;
    std::vector<CountType> m_lcp;
    std::vector<CharType> m_txt;
    std::vector<CharType> m_sa_precede;
    std::vector<snds_t> m_nds;
    CharType m_pr_txt, m_nxt_txt;

    // rmq table
    drmq_t* m_rmqt;

    // procesor minimums
    std::vector<CountType> m_pmin;
    drmq_t* m_pmin_rmqt;
    // nds max
    snds_t m_max_nds;

    int m_rank, m_nproc;
    MPI_Datatype m_mpi_pair_t, m_mpi_idx_t, m_mpi_cnt_t,
      m_mpi_chr_t, m_mpi_nds_t;
    MPI_Comm _comm;

public:
    inline SizeType global_size() const{
        return m_dist_size;
    }

    inline SizeType size() const{
        return m_local_size;
    }

    inline CountType rmq(SizeType i, SizeType j) const{
        assert(j < m_local_size);
        assert(i >= 0);
        assert(i <= j);
        assert(m_rmqt != nullptr);
        return m_lcp[(*m_rmqt)(i, j)];
    }

    inline CountType proc_rmq(int px, int py) const{
        assert(px >= 0);
        assert(px <= py);
        assert(py < m_nproc);
        assert(m_pmin_rmqt != nullptr);
        return m_pmin[(*m_pmin_rmqt)(px, py)];
    }

    inline SizeType block_begin() const{
        return m_blk_begin;
    }

    inline SizeType block_end() const{
        return block_high(m_rank, m_nproc, m_dist_size);
    }

    inline MPI_Comm comm() const{
        return _comm;
    }

    inline int comm_size() const{
        return m_nproc;
    }

    inline int comm_rank() const{
        return m_rank;
    }

    inline sa_iterator_t sa() const{
        return m_sa.cbegin();
    }

    inline lcp_iterator_t lcp() const{
        return m_lcp.cbegin();
    }

    inline sa_iterator_t isa() const{
        return m_isa.cbegin();
    }

    inline txt_iterator_t sa_precede() const{
        return m_sa_precede.cbegin();
    }

    inline const std::vector<SizeType>& sa_vec() const{
        return m_sa;
    }

    inline const std::vector<CountType>& lcp_vec() const{
        return m_lcp;
    }

    inline const std::vector<SizeType>& isa_vec() const{
        return m_isa;
    }

    inline const std::vector<CharType>& sa_precede_vec() const{
        return m_sa_precede;
    }

    inline const std::vector<CharType>& txt_vec() const{
        return m_txt;
    }

    inline const drmq_t& rmq_table() const{
        return (*m_rmqt);
    }

    inline void precede(std::vector<SizeType>& vsa,
                        std::vector<CharType>& vprt) const{
        query_prev(*this, vsa, vprt);
    }

    inline CharType prev_char() const{
        return m_pr_txt;
    }

    inline CharType next_char() const{
        return m_nxt_txt;
    }

    inline CharType precede(SizeType idx) const{
        if(idx < size())
            return (idx > 0) ? m_txt[idx - 1] : m_pr_txt;

        return (CharType) '$';
    }

    void clear_txt() {
        std::vector<CharType>().swap(m_txt);
    }

    void clear_sa(){
        std::vector<SizeType>().swap(m_sa);
    }

    // the owning process
    inline int owner(const SizeType& x) const{
        return block_owner(x, global_size(), comm_size());
    }

    virtual void print_summary(std::ostream& ots) const{
        for(int j = 0; j < m_nproc; j++){
            if(j == m_rank){
                std::stringstream oss;
                oss << "\"SA_DS_" << j << "\" : ["
                    << std::setw(15) << m_blk_begin
                    << ","
                    << std::setw(15) << block_end()
                    << ","
                    << std::setw(15) << size()
                    << "],"
                    << std::endl;
                ots << oss.str();
            }
            MPI_Barrier(comm());
        }
    }

    virtual void print(std::ostream& ots) const{
        assert(m_local_size == m_sa.size());
        assert(m_local_size == m_lcp.size());
        assert(m_local_size == m_txt.size());
        for(int j = 0; j < m_nproc; j++){
            if(j == m_rank)
                for(SizeType i = 0; i < m_local_size; i++){
                    std::stringstream oss;
                    oss << std::setw(6) << m_blk_begin + i
                        << std::setw(6) << m_txt[i]
                        << std::setw(6) << ((i < m_isa.size()) ? m_isa[i] : 0)
                        << std::setw(6) << ((i < m_nds.size()) ? m_nds[i] : 0)
                        << std::setw(6) << m_sa[i]
                        << std::setw(6) << m_lcp[i] << std::endl;
                    ots << oss.str();
                }
            MPI_Barrier(comm());
        }
    }

    void swap(SuffixDS< CharType, SizeType, CountType, RMQTable>& other){
        std::swap(m_dist_size, other.m_dist_size);
        std::swap(m_blk_begin, other.m_blk_begin);
        std::swap(m_local_size, other.m_local_size);

        // vector of data structures : SA, ISA, LCP, NDS, PRT
        m_sa.swap(other.m_sa);
        m_isa.swap(other.m_isa);
        m_lcp.swap(other.m_lcp);
        m_nds.swap(other.m_nds);
        m_txt.swap(other.m_txt);

        // rmq table
        std::swap(m_rmqt, other.m_rmqt);
        // m_rmqt->swap(*other.m_rmqt);
        if(m_rmqt != nullptr)
            m_rmqt->set_vector(&m_lcp);
        if(other.m_rmqt != nullptr)
            other.m_rmqt->set_vector(&other.m_lcp);
        // procesor minimums
        m_pmin.swap(other.m_pmin);
        std::swap(m_pmin_rmqt, other.m_pmin_rmqt);
        // m_pmin_rmqt->swap(other.m_pmin_rmqt);
        if(m_pmin_rmqt != nullptr)
            m_pmin_rmqt->set_vector(&m_pmin);
        if(other.m_pmin_rmqt != nullptr)
            other.m_pmin_rmqt->set_vector(&other.m_pmin);
        // nds max
        std::swap(m_max_nds, other.m_max_nds);
        // mpi vars
        std::swap(m_rank, other.m_rank);
        std::swap(m_nproc, other.m_nproc);
        std::swap(_comm, other._comm);
        // mpi types to swap
        std::swap(m_mpi_chr_t, other.m_mpi_chr_t);
        std::swap(m_mpi_idx_t, other.m_mpi_idx_t);
        std::swap(m_mpi_cnt_t, other.m_mpi_cnt_t);
        std::swap(m_mpi_pair_t, other.m_mpi_pair_t);
        std::swap(m_mpi_nds_t, other.m_mpi_nds_t);
    }

    SuffixDS(std::vector<CharType>& f_txt,
             std::vector<SizeType>& f_sa,
             std::vector<CountType>& f_lcp, SizeType gsize,
             MPI_Comm cx){
        m_rmqt = nullptr; m_pmin_rmqt = nullptr;
        // get total size
        m_dist_size =  gsize;
        // register mpi types
        register_mpi_types(cx);
        // load data
        m_sa.swap(f_sa);
        m_txt.swap(f_txt);
        m_lcp.swap(f_lcp);
        assert(m_sa.size() == m_local_size);
        assert(m_lcp.size() == m_local_size);
        assert(m_txt.size() == m_local_size);
        // build data structures
        build();
    }

    SuffixDS(txt_iterator_t txt, sa_iterator_t sa,
             lcp_iterator_t lcp, SizeType gsize,
             MPI_Comm cx){
        m_rmqt = nullptr; m_pmin_rmqt = nullptr;
        m_dist_size = gsize;
        // register mpi types
        register_mpi_types(cx);
        // load data
        m_sa.resize(m_local_size);
        m_txt.resize(m_local_size);
        m_lcp.resize(m_local_size);
        std::copy(sa, sa + m_local_size, m_sa.begin());
        std::copy(lcp, lcp + m_local_size, m_lcp.begin());
        std::copy(txt, txt + m_local_size, m_txt.begin());
        // build data structures
        build();
    }

    SuffixDS(){
        m_rmqt = nullptr; m_pmin_rmqt = nullptr;
    }

    ~SuffixDS(){
        if(m_rmqt != nullptr)
            delete m_rmqt;

        if(m_pmin_rmqt != nullptr)
            delete m_pmin_rmqt;
    }

protected:

    void register_mpi_types(MPI_Comm cx){
        assert(m_dist_size > 0);
        _comm = cx;

        MPI_Comm_rank(comm(), &m_rank);
        MPI_Comm_size(comm(), &m_nproc);

        m_local_size = block_size(m_rank, m_nproc, m_dist_size);
        m_blk_begin = block_low(m_rank, m_nproc, m_dist_size);

        m_mpi_idx_t = get_mpi_dt<SizeType>();
        m_mpi_cnt_t = get_mpi_dt<CountType>();
        m_mpi_chr_t = get_mpi_dt<CharType>();
        m_mpi_nds_t = get_mpi_dt<snds_t>();

        MPI_Datatype type[2] = { m_mpi_idx_t, m_mpi_idx_t };
        int blocklen[2] = {1, 1};
        MPI_Aint disp[2] = {0, sizeof(SizeType)};

        MPI_Type_create_struct(2, blocklen, disp, type, &m_mpi_pair_t);
        MPI_Type_commit(&m_mpi_pair_t);
    }

    void init_nds(){
        m_nds.resize(m_local_size);
        m_nds[0] = (m_txt[0] == '$') ? 1 : 0;
        for(SizeType i = 1; i < m_nds.size(); i++)
            m_nds[i] = m_nds[i - 1] + ((m_txt[i] == '$') ? 1 : 0);
        snds_t local_max = m_nds.back(), scan_max;
        // scan prev
        MPI_Scan(&local_max, &scan_max, 1, m_mpi_nds_t,
                 MPI_SUM, comm());
        scan_max -= local_max;
        for(SizeType i = 0; i < m_nds.size(); i++)
            m_nds[i] += scan_max;

        // Get the shift value :  max of nds array
        //    the processor (nproc - 1) broadcasts max nds to array
        if(m_rank == m_nproc - 1)
            m_max_nds = m_nds.back();
        MPI_Bcast(&m_max_nds, 1, m_mpi_nds_t, m_nproc - 1, comm());
    }

    void init_prt(){
        // Initialize pr_txt
        CharType prev_chr = m_txt.back();
        MPI_Status mpstat;
        int src = (m_rank - 1 + m_nproc) % m_nproc;
        int dst = (m_rank + 1) % m_nproc;
        MPI_Sendrecv(&prev_chr, 1, m_mpi_chr_t, dst, 0,
                     &m_pr_txt, 1, m_mpi_chr_t, src, 0, comm(), &mpstat);
        if(m_rank == 0)
            m_pr_txt = '$';

        CharType next_chr = m_txt.front();
        std::swap(src, dst);

        MPI_Sendrecv(&next_chr, 1, m_mpi_chr_t, dst, 0,
                     &m_nxt_txt, 1, m_mpi_chr_t, src, 0, comm(), &mpstat);

        if(m_rank == (m_nproc - 1))
            m_nxt_txt = '$';
        // precede(m_sa, m_sa_precede);
        PrevVectorOracle<CharType, SizeType> prt_access(txt_vec(),
                                                        global_size(),
                                                        prev_char(),
                                                        '$');
        get_query_results(prt_access, m_sa, m_sa_precede);
    }

    void build(){
        // init nds (num $ signs) and prev. char
        init_nds();
        // update SA indices
        update_sa_nds();
        std::vector<snds_t>().swap(m_nds);
        // compact the SA, LCP and TXT
        compact();
        // construct isa and rmq tables
        build_isa();
        build_rmq_tables();
        // prev character
        init_prt();
    }

    void compact(){
        // eliminate in txt sa and lcp entries prior to the location m_max_nds
        int px = owner(m_max_nds - 1);
        if(m_rank == px){
            SizeType csize = 0;
            //std::cout << m_sa[m_max_nds - 1 - m_blk_begin] << std::endl;
            for(SizeType i = m_max_nds - 1 - m_blk_begin, j = 0;
                i < m_local_size; i++, j++, csize++){
                m_sa[j] = m_sa[i];
                m_lcp[j] = m_lcp[i];
            }
            m_sa[0] = m_dist_size - m_max_nds; // a $ at the beginning
            m_sa.resize(csize);
            m_lcp.resize(csize);
        } else if(m_rank < px){
            m_sa.clear();
            m_lcp.clear();
        }

        // eliminate entries in txt where txt has '$'s
        SizeType csize = 0;
        for(SizeType i = 0; i < (m_local_size - 1); i++){
            if(m_txt[i] == '$')
                continue;
            m_txt[csize] = m_txt[i];
            csize++;
        }
        if(m_rank == (m_nproc - 1) || m_txt[m_local_size - 1] != '$'){
            m_txt[csize] = m_txt[m_local_size - 1];
            csize++;
        }
        m_txt.resize(csize);

        // compact
        load_balance_stable(m_sa, m_mpi_idx_t, comm());
        load_balance_stable(m_lcp, m_mpi_cnt_t, comm());
        load_balance_stable(m_txt, m_mpi_chr_t, comm());

        // update total and local length
        m_local_size = m_sa.size();
        MPI_Allreduce(&m_local_size, &m_dist_size, 1,
                      get_mpi_dt<SizeType>(), MPI_SUM, comm());
        m_blk_begin = block_low(m_rank, m_nproc, m_dist_size);

        //shrink
        std::vector<SizeType>(m_sa).swap(m_sa);
        std::vector<CharType>(m_txt).swap(m_txt);
        std::vector<CountType>(m_lcp).swap(m_lcp);
    }

    void update_sa_nds(){
        static int cx = 0;
        // query nds information
        std::vector<snds_t> sa_nds(m_sa.size(), 0);
        DataVectorOracle<snds_t, SizeType> nds_accessor(m_nds, global_size());
        nds_accessor.set_name("nds_"); cx++;
        nds_accessor.set_counter(cx);
        get_query_results(nds_accessor, m_sa, sa_nds);

        assert(sa_nds.size() == m_sa.size());
        //  update SA subtract with recieved data - use rcvptr to track
        if(sa_nds.size() == m_sa.size())
              for(SizeType i = 0; i < size(); i++)
                  m_sa[i] -= sa_nds[i];
    }

    void build_isa(){
        std::vector<int> sndcts(m_nproc), rcvcts(m_nproc), sndptr, rcvptr;
        // 1. Prepare data to be sent : count no. sent the procesor
        for(auto& x: m_sa){
            int px = owner(x); // owning process
            sndcts[px] += 1;
        }
        sndptr = get_displacements(sndcts);

        // 2. build the data to send : the pairs (sa_entry, isa_entry)
        std::vector<spair_t> snddata(m_sa.size());
        SizeType mbegin = block_low(m_rank, m_nproc, global_size());
        for(SizeType i = 0; i < size(); i++){
            SizeType x = m_sa[i];
            int px = owner(x); // owning process
            snddata[sndptr[px]].first = x;
            snddata[sndptr[px]].second = mbegin + i;
            sndptr[px] += 1;
        }

        // 3. communication to send (sa, isa) pairs
        //   3.1 all-to-all to specify how many to send to each proc.
        MPI_Alltoall(&sndcts[0], 1, MPI_INT, &rcvcts[0], 1, MPI_INT,
                     comm());

        sndptr = get_displacements(sndcts);
        rcvptr = get_displacements(rcvcts);
        int rcvtotal = std::accumulate(rcvcts.begin(), rcvcts.end(), 0);
        //  3.2 all-to-all to send data
        std::vector<spair_t> rcvdata(rcvtotal);
        MPI_Alltoallv(&snddata[0], &sndcts[0], &sndptr[0], m_mpi_pair_t,
                      &rcvdata[0], &rcvcts[0], &rcvptr[0], m_mpi_pair_t,
                      comm());

        // 4. Load ISA
        m_isa.resize(size());
        for(auto& rp : rcvdata){
            m_isa[rp.first - mbegin] = rp.second;
        }
        //shrink
        std::vector<SizeType>(m_isa).swap(m_isa);
    }

    RMQTable* get_rmq_table(std::vector<CountType>& src_vec){
        RMQTable* tmp_rq = new RMQTable(&src_vec);
        return tmp_rq;
    }

    void build_rmq_tables(){
        // 1. build rmq
        m_rmqt = get_rmq_table(m_lcp);
        // 2. compute minimum in the processor
        CountType pmin_local = rmq(0, m_local_size - 1);
        m_pmin.resize(m_nproc);
        // 3. all gather from minimum from all processors; make proc. rmq
        MPI_Allgather(&pmin_local, 1 , m_mpi_cnt_t,
                      &m_pmin[0], 1 , m_mpi_cnt_t, comm());
        m_pmin_rmqt = get_rmq_table(m_pmin);
    }


};

template< typename SizeType,
          typename CountType>
class IteratorSuffixDS
    // : public AbstractSuffixDS<typename std::vector<char>::const_iterator,
    //                           typename std::vector<SizeType>::const_iterator,
    //                           typename std::vector<CountType>::const_iterator>
{
public:
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef typename std::vector<SizeType>::const_iterator leaves_t;
    typedef typename std::vector<CountType>::const_iterator lcp_t;

    IteratorSuffixDS(leaves_t leaves, lcp_t lcp,
                     SizeType sz,
                     SizeType dsz)
        : m_leaves(leaves), m_lcp(lcp), m_size(sz), m_dist_size(dsz) {}

    virtual inline SizeType size() const{
        return m_size;
    }

    virtual inline leaves_t sa() const{
        return m_leaves;
    }

    virtual inline lcp_t lcp() const{
        return m_lcp;
    }

    virtual inline leaves_t isa() const{
        throw std::runtime_error("ISA not supported for Iterator suffixes");
        leaves_t tmp;
        return tmp;
    }

    virtual inline SizeType global_size() const{
        return m_dist_size;
    }

    virtual inline MPI_Comm comm() const{
        throw std::runtime_error("No comm for Iterator suffixes");
        return MPI_COMM_WORLD;
    }

    virtual inline int comm_size() const{
        throw std::runtime_error("No comm for Iterator suffixes");
        return 1;
    }

    virtual inline int comm_rank() const{
        throw std::runtime_error("No comm for Iterator suffixes");
        return 0;
    }

private:
    leaves_t m_leaves;
    lcp_t m_lcp;
    SizeType m_size;
    SizeType m_dist_size;
};

template<typename CharType,
         typename SizeType,
         typename CountType>
class RegionSuffixDS
   : public AbstractSuffixDS<typename std::vector<CharType>::const_iterator,
                             typename std::vector<SizeType>::const_iterator,
                             typename std::vector<CountType>::const_iterator>{
public:
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;

    typedef typename std::vector<CharType>::const_iterator txt_iterator_t;
    typedef typename std::vector<SizeType>::const_iterator sa_iterator_t;
    typedef typename std::vector<CountType>::const_iterator lcp_iterator_t;

    typedef RegionSuffixDS<CharType, SizeType, CountType> region_sfx_t;
    inline SizeType size() const{
        return m_sa.size();
    }

    inline sa_iterator_t sa() const{
        return m_sa.cbegin();
    }

    inline lcp_iterator_t lcp() const{
        return m_lcp.cbegin();
    }

    inline sa_iterator_t isa() const{
        throw std::runtime_error("ISA not supported for Region suffixes");
        sa_iterator_t tmp;
        return tmp;
    }

    inline txt_iterator_t sa_precede() const{
        return m_sa_precede.cbegin();
    }

    inline const std::vector<SizeType>& sa_vec() const{
        return m_sa;
    }

    inline const std::vector<CountType>& lcp_vec() const{
        return m_lcp;
    }

    inline const std::vector<CharType>& sa_precede_vec() const{
        return m_sa_precede;
    }

    inline SizeType global_size() const{
        return m_dist_size;
    }

    inline int comm_rank() const{
        return m_rank;
    }

    inline int comm_size() const{
        return m_nproc;
    }

    inline MPI_Comm comm() const{
        return _comm;
    }

    RegionSuffixDS(SizeType gsz, int i, int p, MPI_Comm cx)
        : m_dist_size(gsz), m_rank(i), m_nproc(p), _comm(cx){
    }

    void print_summary(std::ostream& ots,
                       const char *pfx = "\"REGION_SFX_") const{
        for(int j = 0; j < m_nproc; j++){
            if(j == m_rank){
                std::stringstream oss;
                oss << pfx << j << "\" : ["
                    << std::setw(15) << size()
                    << "],"
                    << std::endl;
                ots << oss.str();
            }
            MPI_Barrier(comm());
        }
    }

    void print(std::ostream& ots) const{
        assert(m_lcp.size() == m_sa.size());
        for(int i = 0; i < comm_size(); i++){
            if(i == comm_rank())
                for(SizeType i = 0; i < size(); i++){
                    std::stringstream oss;
                    oss << std::setw(6) << i
                        << std::setw(6) << m_sa[i]
                        << std::setw(6) << m_lcp[i]
                        << std::endl;
                    ots << oss.str();
                }
            MPI_Barrier(comm());
        }
    }

    template< typename SDSType, typename RemoteDS>
    static void left_shift_boundary(const SDSType& sadt,
                                    const CountType& min_depth,
                                    RemoteDS& rmt_ds){

        SizeType left_idx = 0, right_idx = sadt.size() - 1,
            left_size = 0, right_size = 0, nbr_left;
        // Identify the bordering region
        while(left_idx < sadt.size() && sadt.lcp()[left_idx] >= min_depth)
            left_idx += 1;

        while(right_idx > 0 && sadt.lcp()[right_idx] >= min_depth)
            right_idx -= 1;

        left_size = left_idx;
        right_size = (right_idx < sadt.size() - 1) ?
            (sadt.size() - right_idx) : 0;

        // Get the data size I,rank, shall recieve from my right neighbor
        MPI_Status mpstat;
        int src = (sadt.comm_rank() + 1) % sadt.comm_size();
        int dst = (sadt.comm_rank() - 1 + sadt.comm_size()) % sadt.comm_size();

        MPI_Sendrecv(&left_size, 1, get_mpi_dt<SizeType>(), dst, 0,
                     &nbr_left, 1, get_mpi_dt<SizeType>(), src, 0,
                     sadt.comm(), &mpstat);
        // Communicate the straddling region to 'dst' processor
        std::vector<int> snd_cts(sadt.comm_size()), rcv_cts(sadt.comm_size()),
            snd_ptr(sadt.comm_size()), rcv_ptr(sadt.comm_size());

        if(right_size == 0 && nbr_left > 0){
            right_size = 1; right_idx = sadt.size() - 1;
        }

        if(sadt.comm_rank() != (sadt.comm_size() - 1)){
            snd_cts[sadt.comm_rank()] = right_size;
            snd_ptr[sadt.comm_rank()] = right_idx;
            rcv_cts[sadt.comm_rank()] = right_size;
            rcv_ptr[sadt.comm_rank()] = 0;
        }
        snd_cts[dst] = left_size; snd_ptr[dst] = 0;
        rcv_cts[src] = nbr_left; rcv_ptr[src] = right_size;
        if(sadt.comm_rank() != (sadt.comm_size() - 1))
            rmt_ds.resize(right_size + nbr_left);
#ifdef DEBUG
        print_disp(sadt.comm(), snd_cts, snd_ptr, rcv_cts, rcv_ptr);
#endif
        MPI_Alltoallv((void*)&sadt.sa_vec()[0], &snd_cts[0], &snd_ptr[0],
                      get_mpi_dt<SizeType>(),
                      &rmt_ds.m_sa[0], &rcv_cts[0], &rcv_ptr[0],
                      get_mpi_dt<SizeType>(),
                      sadt.comm());
        MPI_Alltoallv((void*)&sadt.lcp_vec()[0], &snd_cts[0], &snd_ptr[0],
                      get_mpi_dt<CountType>(),
                      &rmt_ds.m_lcp[0], &rcv_cts[0], &rcv_ptr[0],
                      get_mpi_dt<CountType>(),
                      sadt.comm());
        MPI_Alltoallv((void*)&sadt.sa_precede_vec()[0], &snd_cts[0], &snd_ptr[0],
                      get_mpi_dt<CharType>(),
                      &rmt_ds.m_sa_precede[0], &rcv_cts[0], &rcv_ptr[0],
                      get_mpi_dt<CharType>(),
                      sadt.comm());
    }

    void append_region(RegionSuffixDS<CharType,SizeType,CountType> other){
        if(other.size() <= 0)
            return;
        SizeType csize = size(),
            nsize = csize + other.size();
        resize(nsize);
        shrink();
        for(SizeType i = csize, j = 0; j < other.size(); j++, i++){
            m_sa[i] = other.sa()[j];
            m_lcp[i] = other.lcp()[j];
            m_sa_precede[i] = other.sa_precede()[j];
        }
    }

    void clear(){
        std::vector<SizeType>().swap(m_sa);
        std::vector<CountType>().swap(m_lcp);
        std::vector<CharType>().swap(m_sa_precede);
    }

protected:
    void resize(SizeType nsize){
        m_sa.resize(nsize);
        m_lcp.resize(nsize);
        m_sa_precede.resize(nsize);
    }

    void shrink(){
        std::vector<SizeType>(m_sa).swap(m_sa);
        std::vector<CountType>(m_lcp).swap(m_lcp);
        std::vector<CharType>(m_sa_precede).swap(m_sa_precede);
    }

    std::vector<SizeType> m_sa;
    std::vector<CountType> m_lcp;
    std::vector<CharType> m_sa_precede;
    SizeType m_dist_size;
    int m_rank, m_nproc;
    MPI_Comm _comm;
};


template<typename SDSType,
         typename CharType=typename SDSType::dchar_t,
         typename SizeType=typename SDSType::dsize_t,
         typename CountType=typename SDSType::dcount_t>
class BaseRegionSuffixDS
    : public RegionSuffixDS<CharType, SizeType, CountType> {
private:
    const SDSType& m_sadt;

    void init(CountType min_depth) {
        // is this correct ?
        this->resize(m_sadt.size());

        bool in_region = false;
        SizeType tidx = 0, lidx = 0;
        for(SizeType x = 0; x < m_sadt.size(); x++){
            if(m_sadt.lcp()[x] < min_depth){
                if(in_region == true){
                    this->m_sa[tidx] = m_sadt.sa()[x];
                    this->m_lcp[tidx] = m_sadt.lcp()[x];
                    this->m_sa_precede[tidx] = m_sadt.sa_precede()[x];
                    lidx = x; tidx++;
                    in_region = false;
                    // std::cout << x << "(" << tidx << ")" << std::endl;
                }
                continue;
            }
            if(in_region == false && x > 0 && lidx != (x - 1)){
                this->m_sa[tidx] = m_sadt.sa()[x - 1];
                this->m_lcp[tidx] = m_sadt.lcp()[x - 1];
                this->m_sa_precede[tidx] = m_sadt.sa_precede()[x - 1];
                // std::cout << x - 1 << " ";
                tidx++;
            }
            this->m_sa[tidx] = m_sadt.sa()[x];
            this->m_lcp[tidx] = m_sadt.lcp()[x];
            this->m_sa_precede[tidx] = m_sadt.sa_precede()[x];
            // std::cout << x << " ";
            lidx = x; tidx++;
            in_region = true;
        }
        this->resize(tidx);

        //
        load_balance_stable(this->m_sa, get_mpi_dt<SizeType>(), this->comm());
        load_balance_stable(this->m_lcp, get_mpi_dt<CountType>(), this->comm());
        load_balance_stable(this->m_sa_precede, get_mpi_dt<CharType>(), this->comm());
    }

public:
    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;

    typedef typename std::vector<CharType>::const_iterator txt_iterator_t;
    typedef typename std::vector<SizeType>::const_iterator sa_iterator_t;
    typedef typename std::vector<CountType>::const_iterator lcp_iterator_t;

    BaseRegionSuffixDS(const SDSType& sds,
                       CountType min_depth)
        : RegionSuffixDS<CharType, SizeType, CountType>(sds.global_size(),
                                                        sds.comm_rank(),
                                                        sds.comm_size(),
                                                        sds.comm()),
        m_sadt(sds) {
        init(min_depth);
    }

    void eliminate_boundaries(CountType min_depth) {
        if(this->size() == 0)
            return;
        SizeType left_idx = 0, right_idx = this->size() - 1;
        // Identify the bordering region
        if(this->comm_rank() > 0) {
            while(left_idx < this->size() &&
                  this->m_lcp[left_idx] >= min_depth)
                left_idx += 1;
        }

        if(this->comm_rank() < (this->comm_size() - 1)) {
            while(right_idx > 0 &&
                  this->m_lcp[right_idx] >= min_depth)
                right_idx -= 1;
        }

        SizeType tidx = 0;
        for(SizeType x = left_idx; x <= right_idx; x++) {
            this->m_lcp[tidx] = this->m_lcp[x];
            this->m_sa[tidx] = this->m_sa[x];
            this->m_sa_precede[tidx] = this->m_sa_precede[x];
            tidx++;
        }
        this->resize(tidx);
    }

    void print_summary(std::ostream& ots,
                       const char *pfx = "\"BASE_REGION_SFX_") const{
         RegionSuffixDS<CharType, SizeType, CountType>::print_summary(ots, pfx);
    }
};



template< typename SDSType,
          typename RegionType,
          typename CharType=typename SDSType::dchar_t,
          typename SizeType=typename SDSType::dsize_t,
          typename CountType=typename SDSType::dcount_t>
class RootInternalNodeDS : public InternalNodeDS<SDSType> {
public:
    typedef RootInternalNodeDS<SDSType, RegionType,
                               CharType, SizeType, CountType> root_nodes_t;

    typedef SizeType dsize_t;
    typedef CountType dcount_t;
    typedef CharType dchar_t;

    inline MPI_Comm comm() const{
        return this->root_sds.comm();
    }

    inline int comm_rank() const{
        return this->root_sds.comm_rank();
    }

    inline int comm_size() const{
        return this->root_sds.comm_size();
    }

    virtual SizeType sa(SizeType i, SizeType j) const{
        if(i < this->size()) {
            return bse_region.sa()[this->node_left[i] + j];
        } else {
            return this->sa_global_size();
        }
    }

    virtual CharType sa_precede(SizeType i, SizeType j) const{
        if(i < this->size()) {
            return bse_region.sa_precede()[this->node_left[i] + j];
        } else {
            return '$';
        }
    }

    void sort_d(){
        this->sort_by_depth(0, this->size());
    }

    virtual void print_summary(std::ostream& ots) const {
         uint64_t total_leaves = std::accumulate(this->node_width.begin(),
                                                 this->node_width.end(), 0);
         for(int j = 0; j < this->root_sds.comm_size(); j++){
              if(j == this->root_sds.comm_rank()){
                  std::stringstream oss;
                  oss << "\"ROOT_NODE_" << j << "\" : ["
                      << std::setw(15) << this->size()
                      << ","
                      << std::setw(15) << total_leaves
                      << ","
                      << std::setw(15) << this->get_flag()
                      << "],"
                      << std::endl;
                  ots << oss.str();
              }
         }
         MPI_Barrier(this->root_sds.comm());
         uint64_t phase_weight = total_leaves,
           min_weight, max_weight, sum_weight;
         MPI_Allreduce(&phase_weight, &min_weight, 1,
                       MPI_UNSIGNED_LONG_LONG, MPI_MIN,
                       this->root_sds.comm());
         MPI_Allreduce(&phase_weight, &max_weight, 1,
                       MPI_UNSIGNED_LONG_LONG, MPI_MAX,
                       this->root_sds.comm());
         MPI_Allreduce(&phase_weight, &sum_weight, 1,
                       MPI_UNSIGNED_LONG_LONG, MPI_SUM,
                       this->root_sds.comm());
         double ratio = ((double)max_weight) / (double)min_weight;
         double avg_weight  =
             ((double) sum_weight)/this->root_sds.comm_size();

         if(this->root_sds.comm_rank() == 0){
             std::stringstream oss;
             oss
                 << "\"" << "weights" << "\" : ["
                 << std::setw(15) << min_weight
                 << ","
                 << std::setw(15) << (max_weight)
                 << ","
                 << std::setw(15) << avg_weight
                 << ","
                 << std::setw(15) << ratio
                 << "], "
                 << std::endl;
             ots << oss.str();
         }
    }


    template<typename T>
    void eliminate_boundaries(const T& radt, root_nodes_t& rnodes) {
        // move up
        SizeType j = 0;
        int nproc = radt.comm_size(),
            rank = radt.comm_rank();
        for(SizeType i = 0; i < rnodes.size(); i++){
            auto nright = rnodes.node_left[i] + rnodes.node_width[i] - 1;
            if((rank < (nproc - 1) &&
                nright == (radt.size() - 1))
               ||
               (rank > 0 &&
                rnodes.node_left[i] == 0))
                continue;
            rnodes.assign_to(j, i);
            j += 1;
        }
        rnodes.resize(j);
    }

    virtual void trie_parents(std::vector<SizeType>& parents) const{
        parents.resize(this->size());
        // 1. max ANSV
        this->max_ansv(this->node_width.begin(),
                       this->node_width.end(),
                       parents.begin());
        // 2.
        for(SizeType ix = 0; ix < this->size(); ix++){
            SizeType my_left = this->node_left[ix];
            SizeType my_width = this->node_width[ix];
            SizeType p_left = this->node_left[parents[ix]];
            SizeType p_width = this->node_width[parents[ix]];
            if(my_left < p_left ||
               (my_left + my_width) > (p_left + p_width))
                parents[ix] = parents[parents[ix]];
        }

    }

    void add_primary_nodes(const RegionType& radt, CountType min_depth) {
        this->add_primary_nodes_from(radt, *this, min_depth);
    }

    void add_nodes(const RegionType& radt, CountType min_depth,
                   bool elim_boundaries){
        this->add_internal_nodes_from(radt, *this, min_depth, 0, true);
        if(elim_boundaries == true && (radt.comm_rank() != (radt.comm_size() - 1)))
            eliminate_boundaries(radt, *this);
    }

    RootInternalNodeDS(SDSType& sadt, RegionType& radt,
                       CountType min_depth, bool elim_boundaries,
                       bool only_primary = false)
        : InternalNodeDS<SDSType>(sadt), bse_region(radt) {
        if(only_primary) {
            init_primary(radt, min_depth);
        } else {
            add_nodes(radt, min_depth, elim_boundaries);
        }
        min_string_depth = min_depth;
    }

    void init_primary(RegionType& radt, CountType min_depth) {
        add_primary_nodes(radt, min_depth);
    }

    virtual ~RootInternalNodeDS(){};

protected:
    const RegionType& bse_region;
    CountType min_string_depth;
};

#endif // SUFFIX_DS_H
