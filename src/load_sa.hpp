#ifndef LOAD_SA_H
#define LOAD_SA_H

#include <string>
#include <fstream>
#include "mpi.h"


template<typename T, typename S1, typename L1,
         typename S2, typename L2>
uint64_t load_files(const std::string& txtfn,
                const std::string& safn,
                const std::string& lcpfn,
                std::vector<T>& f_txt,
                std::vector<S2>& f_sa,
                std::vector<L2>& f_lcp,
                MPI_Comm comm){
    f_txt.clear(); f_sa.clear(); f_lcp.clear();
    int rank, nproc;
    uint64_t total_size = file_size<S1>(safn);
    std::ifstream safs(safn, std::ios::in | std::ios::binary),
      lcpfs(lcpfn, std::ios::in | std::ios::binary),
      txtfs(txtfn, std::ios::in | std::ios::binary);

    MPI_Comm_size(comm, &nproc);
    MPI_Comm_rank(comm, &rank);
    uint64_t blk_begin = block_low(rank, nproc, total_size);
    uint64_t local_size = block_size(rank, nproc, total_size);

    safs.seekg(blk_begin * sizeof(S1));
    std::vector<S1> tmp_sa(local_size);
    safs.read((char *) &tmp_sa[0], sizeof(S1) * local_size);
    f_sa.resize(local_size);
    for(uint64_t i = 0; i < local_size; i++)
      f_sa[i] = (S2) tmp_sa[i];
    tmp_sa.clear();

    lcpfs.seekg(blk_begin * sizeof(L1));
    std::vector<L1> tmp_lcp(local_size);
    lcpfs.read((char *) &tmp_lcp[0], sizeof(L1) * local_size);
    f_lcp.resize(local_size);
    for(uint64_t i = 0; i < local_size;i++)
      f_lcp[i] = (L2) tmp_lcp[i];
    tmp_lcp.clear();

    f_txt.resize(local_size);
    txtfs.seekg(blk_begin * sizeof(T));
    txtfs.read((char *) &f_txt[0], sizeof(T) * local_size);
    return total_size;
}


template<typename T, typename S1, typename L1,
         typename S2, typename L2>
void load_full_files(const std::string& txtfn,
                     const std::string& safn,
                     const std::string& lcpfn,
                     std::vector<T>& f_txt,
                     std::vector<S2>& f_sa,
                     std::vector<L2>& f_lcp){
    f_txt.clear(); f_sa.clear(); f_lcp.clear();

    uint64_t total_size = file_size<T>(txtfn);
    std::ifstream safs(safn, std::ios::in | std::ios::binary),
      lcpfs(lcpfn, std::ios::in | std::ios::binary),
      txtfs(txtfn, std::ios::in | std::ios::binary);

    std::vector<S1> tmp_sa(total_size);
    safs.read((char *) &tmp_sa[0], sizeof(S1) * total_size);
    f_sa.resize(total_size);
    for(uint64_t i = 0; i < total_size; i++)
      f_sa[i] = (S2) tmp_sa[i];
    tmp_sa.clear();

    std::vector<L1> tmp_lcp(total_size);
    lcpfs.read((char *) &tmp_lcp[0], sizeof(L1) * total_size);
    f_lcp.resize(total_size);
    for(uint64_t i = 0; i < total_size;i++)
      f_lcp[i] = (L2) tmp_lcp[i];
    tmp_lcp.clear();

    f_txt.resize(total_size);
    txtfs.read((char *) &f_txt[0], sizeof(T) * total_size);
}



#endif /* LOAD_SA_H */
