//
// @file    mpi_utils.hpp
// @author  Nagakishore Jammula <njammula3@mail.gatech.edu>
// @author  Patrick Flick <patrick.flick@gmail.com>
// @author  Sriram P C <sriram.pc@gmail.com>
// @brief   Implements some helpful MPI utility function, mostly for
//          interacting with MPI using std::vectors
//
// Copyright (c) TODO
//
// TODO add Licence
///

#ifndef MPI_UTIL_H
#define MPI_UTIL_H

#include <vector>
#include <mpi.h>
#include <numeric>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "parallel_utils.hpp"
#ifdef WITH_MXX
#include <mxx/collective.hpp>
#endif

template <typename T>
MPI_Datatype get_mpi_dt();

template <>
MPI_Datatype get_mpi_dt<char>();

template <>
MPI_Datatype get_mpi_dt<double>();

template <>
MPI_Datatype get_mpi_dt<int>();

template <>
MPI_Datatype get_mpi_dt<unsigned>();

template <>
MPI_Datatype get_mpi_dt<unsigned long>();

template <>
MPI_Datatype get_mpi_dt<long>();

void summary_mem_report(int rank, MPI_Comm _comm, std::ostream& ofs);
void report_query_timings(MPI_Comm _comm, std::ostream& ofs);
void reset_query_timings();
double get_query_timings();

void report_a2a_timings(MPI_Comm _comm, std::ostream& ofs);
void reset_a2a_timings();
double get_a2a_timings();

#define _A2A_PROF 1

#ifdef _A2A_PROF
extern double a2a_timings;
// MPI_Barrier((COMM));
// MPI_Barrier((COMM));

#define A2A_PROFILE(QCALL, COMM) {                               \
        std::chrono::steady_clock::time_point tstart, tend; \
        typedef std::chrono::duration<double, std::milli> duration_t; \
        tstart = std::chrono::steady_clock::now(); \
        (QCALL); \
        tend = std::chrono::steady_clock::now(); \
        a2a_timings += elapsed_local<duration_t>(tstart, tend);  \
    }
#else
#define A2A_PROFILE(QCALL, COMM) {                   \
        (QCALL); \
    }
#endif


template<typename T>
void avg_min_max_report(const char *prefix, T value,
                        MPI_Comm _comm, std::ostream& ofs){
    int nproc, rank;
    MPI_Comm_size(_comm, &nproc);
    MPI_Comm_rank(_comm, &rank);

    T min_value, max_value, sum_value;
    MPI_Allreduce(&value, &min_value, 1,
                  get_mpi_dt<T>(), MPI_MIN, _comm);
    MPI_Allreduce(&value, &max_value, 1,
                  get_mpi_dt<T>(), MPI_MAX, _comm);
    MPI_Allreduce(&value, &sum_value, 1,
                  get_mpi_dt<T>(), MPI_SUM, _comm);
    if(rank == 0)
      ofs << "\"" << prefix << "\" : ["
          << min_value
          << ", "
          << max_value
          << ", "
          << (sum_value/((T)nproc))
          << "]," << std::endl;
}

template<typename T>
void sum_max_report(const char *prefix, T value, int rank,
                    MPI_Comm _comm, std::ostream& ofs){
    T max_value, sum_value;
    MPI_Allreduce(&value, &max_value, 1,
                  get_mpi_dt<T>(), MPI_MAX, _comm);
    MPI_Allreduce(&value, &sum_value, 1,
                  get_mpi_dt<T>(), MPI_SUM, _comm);
    if(rank == 0)
      ofs << "\"" << prefix
        // << "_" << ctx
          << "\" : ["
          << max_value << ", " << sum_value << "],"
          << std::endl;
}


//
// @brief   Broadcast local std::vector from the root processor to
//          all other process in the given communicator.
//
// @param bvec  The local vector to be broadcasted/recieved.
// @param comm  The communicator.
// @param root  root processor rank
//
// @return (On the master processor): No change
//         (On the slave processors): vector broadcasted from root proc.
//
template <typename T>
void broadcast_vector(std::vector<T>& bvec, MPI_Comm comm, int root = 0){
    int rank;
    int p;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &p);
    int vsize = 0;

    MPI_Datatype mpi_dt = get_mpi_dt<T>();
    if(rank == 0){
        // broadcast the size
        vsize = (int) bvec.size();
        MPI_Bcast(&vsize, 1, MPI_INT, root, comm);

        // broadcast the vector
        MPI_Bcast(&bvec[0], vsize, mpi_dt, root, comm);
    } else {
        // recieve the size
        MPI_Bcast(&vsize, 1, MPI_INT, root, comm);
        bvec.resize(vsize);

        // recieve the broadcasted vector
        MPI_Bcast(&bvec[0], vsize, mpi_dt, root, comm);
    }
}

//
// @brief   Gathers local std::vectors to the master processor inside the
//          given communicator.
//
// @param local_vec The local vectors to be gathered.
// @param comm      The communicator.
//
// @return (On the master processor): The vector containing the concatenation
//                                    of all distributed vectors.
//         (On the slave processors): An empty vector.
//
template <typename T>
std::vector<T> gather_vectors(std::vector<T>& local_vec, MPI_Comm comm,
                              int root = 0)
{
    // get MPI parameters
    int rank;
    int p;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &p);

    // get local size
    int local_size = (int) local_vec.size();

    // init result
    std::vector<T> result;

    MPI_Datatype mpi_dt = get_mpi_dt<T>();

    // master process: receive results
    if (rank == root)
    {
        // gather local array sizes, sizes are restricted to `int` by MPI anyway
        // therefore use int
        std::vector<int> local_sizes(p);
        MPI_Gather(&local_size, 1, MPI_INT,
                   &local_sizes[0], 1, MPI_INT,
                   root, comm);

        // gather-v to collect all the elements
        int total_size = std::accumulate(local_sizes.begin(), local_sizes.end(), 0);
        result.resize(total_size);
        std::vector<int> recv_displs = get_displacements(local_sizes);

        // gather v the vector data to the root
        MPI_Gatherv(&local_vec[0], local_size, mpi_dt,
                    &result[0], &local_sizes[0], &recv_displs[0], mpi_dt,
                    root, comm);
    }
    // else: send results
    else
    {
        // gather local array sizes
        MPI_Gather(&local_size, 1, MPI_INT, NULL, 1, MPI_INT, root, comm);

        // sent the actual data
        MPI_Gatherv(&local_vec[0], local_size, mpi_dt,
                    NULL, NULL, NULL, mpi_dt,
                    root, comm);
    }

    return result;
}


template <typename T>
std::vector<T> gather_rows(std::vector<T>& local_vec, int nrows, MPI_Comm comm,
                           int root = 0)
{
    // get MPI parameters
    int rank;
    int p;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &p);

    // get local size
    int local_size = local_vec.size() / nrows;

    // init result
    std::vector<T> result;

    MPI_Datatype mpi_dt = get_mpi_dt<T>();

    // master process: receive results
    if (rank == root)
    {
        // gather local array sizes, sizes are restricted to `int` by MPI anyway
        // therefore use int
        std::vector<int> local_sizes(p);
        MPI_Gather(&local_size, 1, MPI_INT,
                   &local_sizes[0], 1, MPI_INT,
                   root, comm);

        // gather-v to collect all the elements
        int total_size = std::accumulate(local_sizes.begin(), local_sizes.end(), 0);
        result.resize(total_size*nrows);
        std::vector<int> recv_displs = get_displacements(local_sizes);

        for (int i = 0; i < nrows; ++i)
        {
            // gather v the vector data to the root
            MPI_Gatherv(&local_vec[local_size*i], local_size, mpi_dt,
                    &result[total_size*i], &local_sizes[0], &recv_displs[0], mpi_dt,
                    root, comm);
        }
    }
    // else: send results
    else
    {
        // gather local array sizes
        MPI_Gather(&local_size, 1, MPI_INT, NULL, 1, MPI_INT, root, comm);

        for (int i = 0; i < nrows; ++i)
        {
            // sent the actual data
            MPI_Gatherv(&local_vec[local_size*i], local_size, mpi_dt,
                        NULL, NULL, NULL, mpi_dt,
                        root, comm);
        }
    }

    return result;
}

template <typename InputIterator, typename OutputIterator>
void copy_n(InputIterator& in, std::size_t n, OutputIterator out)
{
    for (std::size_t i = 0u; i < n; ++i)
        *(out++) = *(in++);
}


template <typename T>
std::vector<T> scatter_vector_block_decomp(std::vector<T>& global_vec, MPI_Comm comm,
                                           int root = 0)
{
    // get MPI Communicator properties
    int rank, p;
    MPI_Comm_size(comm, &p);
    MPI_Comm_rank(comm, &rank);


    // the local vector size (MPI restricts message sizes to `int`)
    int local_size;

    // get the MPI data type
    MPI_Datatype mpi_dt = get_mpi_dt<T>();

    // init result
    std::vector<T> local_elements;

    if (rank == root)
    {
        // I am the root process

        // get size of global array
        int n = (int) global_vec.size();

        //Read from the input filename
        std::vector<int> block_decomp = block_partition(n, p);

        //scatter the sizes to expect
        MPI_Scatter(&block_decomp[0], 1, MPI_INT,
                    &local_size, 1, MPI_INT, root, comm);

        // scatter-v the actual data
        // or stream
        local_elements.resize(local_size);
        std::vector<int> displs = get_displacements(block_decomp);
        MPI_Scatterv(&global_vec[0], &block_decomp[0], &displs[0],
                     mpi_dt, &local_elements[0], local_size, mpi_dt, root, comm);
    }
    else
    {
        // I am NOT the root process

        // receive the size of my local array
        MPI_Scatter(NULL, 1, MPI_INT,
                    &local_size, 1, MPI_INT,
                    root, comm);

        // resize result buffer
        local_elements.resize(local_size);
        // actually receive all the data
        MPI_Scatterv(NULL, NULL, NULL,
                     mpi_dt, &local_elements[0], local_size, mpi_dt,
                     root, comm);
    }

    // return local array
    return local_elements;
}

template <typename T>
std::vector<T> scatter_vrows_block_decomp(std::vector<T>& global_vec, unsigned rowLength,
                                          MPI_Comm comm, int root = 0)
{
    // get MPI Communicator properties
    int rank, p;
    MPI_Comm_size(comm, &p);
    MPI_Comm_rank(comm, &rank);


    // the local vector size (MPI restricts message sizes to `int`)
    int local_size;

    // get the MPI data type
    MPI_Datatype mpi_dt = get_mpi_dt<T>();

    // init result
    std::vector<T> local_elements;

    if (rank == root)
    {
        /* I am the root process */

        // get size of global array
        int n = (int) (global_vec.size() / rowLength);

        //Read from the input filename
        std::vector<int> block_decomp = block_partition(n, p);

        // scatter the sizes to expect
        MPI_Scatter(&block_decomp[0], 1, MPI_INT, &local_size, 1, MPI_INT, root, comm);

        // scatter-v the actual data
        // or stream
        //
        std::vector<int> displs = get_displacements(block_decomp);
        for(unsigned i = 0u; i < block_decomp.size();i++)
            block_decomp[i] = block_decomp[i] * rowLength;
        for(unsigned i = 0u; i < displs.size();i++)
            displs[i] = displs[i] * rowLength;
        local_size = local_size * rowLength;
        local_elements.resize(local_size);

        MPI_Scatterv(&global_vec[0], &block_decomp[0], &displs[0],
                     mpi_dt, &local_elements[0], local_size, mpi_dt, root, comm);
    }
    else
    {
        /* I am NOT the root process */

        // receive the size of my local array
        MPI_Scatter(NULL, 1, MPI_INT,
                    &local_size, 1, MPI_INT,
                    root, comm);

        local_size = local_size * rowLength;
        // resize result buffer
        local_elements.resize(local_size);
        // actually receive all the data
        MPI_Scatterv(NULL, NULL, NULL, mpi_dt,
                     &local_elements[0], local_size, mpi_dt,
                     root, comm);
    }

    // return local array
    return local_elements;
}


template <typename T>
std::vector<T> scatter_rows_block_decomp(std::vector<T>& global_vec, int row_length,
                                         MPI_Comm comm, int root = 0)
{
    // get MPI Communicator properties
    int rank, p;
    MPI_Comm_size(comm, &p);
    MPI_Comm_rank(comm, &rank);


    // the local vector size (MPI restricts message sizes to `int`)
    int local_size;

    // get the MPI data type
    MPI_Datatype mpi_dt = get_mpi_dt<T>();

    // init result
    std::vector<T> local_elements;

    if (rank == root)
    {
        /* I am the root process */

        // get size of global array
        int n = global_vec.size();

        int n_rows = n / row_length;

        //Read from the input filename
        std::vector<int> block_decomp = block_partition(row_length, p);

        // scatter the sizes to expect
        MPI_Scatter(&block_decomp[0], 1, MPI_INT, &local_size, 1, MPI_INT, root, comm);


        // broadcast the number of rows to expect
        MPI_Bcast(&n_rows, 1, MPI_INT , root, comm);

        // scatter-v the actual data
        // or stream
        //
        local_elements.resize(local_size*n_rows);

        std::vector<int> displs = get_displacements(block_decomp);
        for (int i = 0; i < n_rows; ++i)
        {
            MPI_Scatterv(&global_vec[row_length*i], &block_decomp[0],
                         &displs[0], mpi_dt, &local_elements[local_size*i],
                         local_size, mpi_dt, root, comm);
        }
    }
    else
    {
        /* I am NOT the root process */

        // receive the size of my local array
        MPI_Scatter(NULL, 1, MPI_INT,
                    &local_size, 1, MPI_INT,
                    root, comm);

        // receive the number of rows to expect
        int n_rows;
        MPI_Bcast(&n_rows, 1, MPI_INT , root, comm);

        // resize result buffer
        local_elements.resize(local_size*n_rows);
        for (int i = 0; i < n_rows; ++i)
        {
            // actually receive all the data
            MPI_Scatterv(NULL, NULL, NULL,
                         mpi_dt, &local_elements[local_size*i],
                         local_size, mpi_dt, root, comm);
        }
    }

    // return local array
    return local_elements;
}

//
// @brief   Print all-to-all's counts and displacement information.
//
// @param comm       communicator.
// @param sendcts    sendcts[i] : no. of elts to send to proc. i
// @param senddisp   senddisp[i] : offset for sendcts[i]
// @param recvcts    recvcts[i] : no. of elts to recieve from proc. i
// @param recvdisp   recvdisp[i] : offset for recvcts[i]
//
template<typename SizeType>
void print_disp(const MPI_Comm& comm,
                const std::vector<SizeType>& sendcts,
                const std::vector<SizeType>& senddisp,
                const std::vector<SizeType>& recvcts,
                const std::vector<SizeType>& recvdisp){
    int rank, nproc;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &nproc);

    if(rank == 0){
        std::stringstream sout2;
        sout2 << "PROCX\tPROCY\tSNDCTS\t"
              << "SNDDISP\tRCVCTS\tRCVDISP (BY COLX)" << std::endl;
        std::cout << sout2.str();
    }
    MPI_Barrier(comm);
    // just printing to test
    for(int i = 0 ; i < nproc; i++){
        std::stringstream sout;
        if(rank == i){
            for(int j =0; j < nproc; j++)
                sout << i << "\t" << j << "\t"
                     << sendcts[j] << "\t"
                     << senddisp[j] << "\t"
                     << recvcts[j] << "\t"
                     << recvdisp[j] << std::endl;
            sout << std::endl;
            std::cout << sout.str();
        }
        MPI_Barrier(comm);
    }
}


//
// @brief   Load balancing algorithm for a distributed vector
//  - Redistribution does NOT maintain the stable order.
//  - Partitions the processors of comm. into senders and
//    recievers; senders send src_vec elts. they own to recievers.
//
// @param  src_vec    A distributed array of structures
// @return src_vec load balanced src_vec
//
template <typename DataType, typename SizeType = std::size_t>
void load_balance(std::vector<DataType>& src_vec, //IN-OUT
                  const MPI_Datatype& mpi_data_type, // IN
                  MPI_Comm _comm){ // IN
    int rank, nproc;
    MPI_Comm_rank(_comm, &rank);
    MPI_Comm_size(_comm, &nproc);
    if(nproc == 1){
        return ;
    }

    SizeType local_size = src_vec.size();
    std::vector<SizeType> cur_size(nproc);

    MPI_Allgather(&local_size, 1 , get_mpi_dt<SizeType>(),
                  &cur_size[0], 1 , get_mpi_dt<SizeType>(),
                  _comm);

    // 1. Count the available number of elements and target number.
    SizeType dist_size = std::accumulate(cur_size.begin(), cur_size.end(), 0);

    // count the target size for good load balancing
    std::vector<SizeType> target_size = block_partition(dist_size, nproc);

    //---------------------------------------------------------
    std::vector<int> senders,  // ranks of proc that only sends
        recievers,  // ranks of proc that only recvs
        under(nproc), over(nproc);
    // 2. Partition the processors into senders, recievers and "nothing-to-do"
    //     Compute the over/under for senders/recievers repectively.
    for(int i = 0; i < nproc; i++){
        over[i] = under[i] = 0;
        if(cur_size[i] < target_size[i]) {
            under[i] = target_size[i] - cur_size[i];
            recievers.push_back(i);
        } else if (cur_size[i] > target_size[i]) {
            over[i] = cur_size[i] - target_size[i];
            senders.push_back(i);
        }
    }

    std::vector<int> rcv_cts(nproc), snd_cts(nproc),
        snd_disp(nproc), rcv_disp(nproc);
    int sent_offset = 0, // offset starting from which i should send next
        rcvd_offset = 0; // offset starting from which i should rcv next
    // 3. I am rank. Set up the no. of elts. I should send-to/recv-from myself.
    if(over[rank] > under[rank]){
        sent_offset = rcv_cts[rank] = snd_cts[rank] = target_size[rank];
        rcv_disp[rank] = snd_disp[rank] = 0;
    } else {
        rcvd_offset = rcv_cts[rank] = snd_cts[rank] = cur_size[rank];
        rcv_disp[rank] = snd_disp[rank] = 0;
    }

    // 4. Set up send/recv counts I should send to/recv frm every one else.
    int snd_idx = 0, // processes that only send i.e. are over
        rcv_idx = 0; //  processes that only recv i.e. are under
    int sent = 0, recvd = 0;
    while(snd_idx < senders.size() && rcv_idx < recievers.size()) {
        int snd_rank = senders[snd_idx],
            rcv_rank = recievers[rcv_idx];
        // I am rank : Setup no. of elts. send-to/recv-from rcv_rank/snd_rank
        // How much ? : xfer the min between what can be send-to/recv-from.
        int xfernow = std::min(over[snd_rank] - sent,
                               under[rcv_rank] - recvd);
        // If I should send, then set up send counts against 'rcv_rank'
        if(rank == snd_rank) {
            snd_cts[rcv_rank] = xfernow;
            snd_disp[rcv_rank] = sent_offset;
            sent_offset += xfernow;
        }
        // If I should recv, then set up recv counts against 'snd_rank'
        if(rank == rcv_rank) {
            rcv_cts[snd_rank] = xfernow;
            rcv_disp[snd_rank] = rcvd_offset;
            rcvd_offset += xfernow;
        }
        // Set-up done. Now, update snd_idx, rcv_idx, sent and recvd.
        sent += xfernow; recvd += xfernow;
        if(sent == over[snd_rank]) {
            snd_idx++; sent = 0;
        }
        if(recvd == under[rcv_rank]) {
            rcv_idx++; recvd = 0;
        }
    }

#ifdef DEBUG
    print_disp(_comm, snd_cts, snd_disp, rcv_cts, rcv_disp);
#endif
    //---------------------------------------------------------
    // 5. do an all-to-all!
    std::vector<DataType> tgt_vec(target_size[rank]);

    MPI_Alltoallv(&src_vec[0], &snd_cts[0], &snd_disp[0], mpi_data_type,
                  &tgt_vec[0], &rcv_cts[0], &rcv_disp[0], mpi_data_type,
                  _comm);
    src_vec.swap(tgt_vec);
}


template<typename SizeType>
struct DefaultBlockPartitioner{
    inline std::vector<SizeType> operator()(SizeType n,int  p, int ){
        return  block_partition(n, p);
    }
};

//
// @brief   Stable Load balancing algorithm for a distributed vector
//    Redistribution maintains the stable order
//
// @param  src_vec       A distributed array with DataType entries
// @param  mpi_data_type MPI data type about DataType
// @return src_vec       Updated load balanced array
//
template <typename DataType, typename SizeType=std::size_t,
          typename BlockPartioner=DefaultBlockPartitioner<SizeType> >
void load_balance_stable(std::vector<DataType>& src_vec, //IN-OUT
                         MPI_Datatype mpi_data_type, // IN
                         MPI_Comm comm){ // IN
    SizeType local_size = src_vec.size();
    int rank, nproc;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &nproc);

    if(nproc == 1){
        return ;
    }
    // Gather local counts from each processor
    std::vector<SizeType> cur_size(nproc), target_size(nproc);
    MPI_Allgather(&local_size, 1 , get_mpi_dt<SizeType>(),
                  &cur_size[0], 1 , get_mpi_dt<SizeType>(), comm);

    //--------Send/Recieve Computation----------------------------
    // Count the total number of elements
    SizeType dist_size = std::accumulate(cur_size.begin(), cur_size.end(),
                                         (SizeType)0);
    // Count the expected number of elements
    BlockPartioner partion_sizes;
    target_size =  partion_sizes(dist_size, nproc, rank);

    std::vector<SizeType> cur_disp = get_displacements(cur_size),
                          pfx_pcount(cur_size);
    // get send/recieve counts
    std::vector<int> snd_cts(nproc), rcv_cts(nproc);
    // send counts
    get_send_counts(snd_cts.begin(), target_size,
                    cur_disp[rank], cur_size[rank]);
    // recive counts
    prefix_sum2(pfx_pcount.begin(), pfx_pcount.end());
    get_recv_counts(rcv_cts.begin(), pfx_pcount, target_size, rank);

    // get displacements
    std::vector<int> snd_disp = get_displacements(snd_cts),
                     rcv_disp = get_displacements(rcv_cts);

#ifdef DEBUG
    print_disp(comm, snd_cts, snd_disp, rcv_cts, rcv_disp);
#endif
    //---------------------------------------------------------

    std::vector<DataType> tgt_vec(target_size[rank]);
#ifdef WITH_MXX
    std::vector<std::size_t> sndcts(nproc), rcvcts(nproc);
    for(int i = 0; i < nproc; i++){
      sndcts[i] = (std::size_t) snd_cts[i];
      rcvcts[i] = (std::size_t) rcv_cts[i];
    }
    snd_cts.clear();
    rcv_cts.clear();
    mxx::all2all(src_vec.begin(), tgt_vec.begin(), sndcts, rcvcts, comm);
#else
    MPI_Alltoallv(&src_vec[0], &snd_cts[0], &snd_disp[0], mpi_data_type,
                  &tgt_vec[0], &rcv_cts[0], &rcv_disp[0], mpi_data_type,
                  comm);
#endif
    src_vec.swap(tgt_vec);
}

// A template for local oracle
//  Local oracle objects are used in the functions
//   get_query_results and get_query_results_perm
//  Local oracle objects has to satisfy the following
// "contract"
//   Oracle is expected to define the following types
//     - size_type : type of indices
//     - value_type : type of data queried
//   Oracle is expected to define the following fns
//     - get(queries, results) : local query results
//     - owner(idx) : owining process of index idx
//     - size() : size of the local data
//     - global_size() : size of the global distributed data
//     - comm() : communicator name
//     - comm_rank() : rank of this processor within the comm
//     - comm_size() : size of the communicator
template<typename ValueType, typename SizeType>
class LocalOracle{
public:
    typedef SizeType size_type;
    typedef ValueType value_type;

    LocalOracle(){}
    virtual void get(std::vector<SizeType>& queries,
                     std::vector<ValueType>& results) const = 0;
    virtual SizeType size() const = 0;
    virtual SizeType global_size() const = 0;
    virtual int owner(const SizeType& x) const = 0;
    virtual int comm_size() const = 0;
    virtual int comm_rank() const = 0;
    virtual MPI_Comm comm() const = 0;
    virtual ~LocalOracle(){}
};

///
// @brief   Prepare the data to be sent to answer distributed 'queries'
//
// @param oracle   An oracle that can answer local queries
//   Oracle is expected to define
//     - size_type : type of indices
//     - value_type : type of data queried
//   Oracle is expected to answer
//     - comm_size() : size of the communicator
//     - get(queries, results) : local query results
//     - owner(idx) : owining process of index idx
// @param queries  A local array of indices onto rdata
//                 whose corresponding rdata entries are desired
// @return sndcts   query  counts to send to each processor
// @return sndqry   query values ordered by the processes
// @return qryprm   permutation vector for queries->sndqry
//
///
template<typename OracleType, typename SizeType=typename OracleType::size_type>
void prepare_query_send_perm(const OracleType& oracle, //IN
                             std::vector<SizeType>& queries, // IN
                             std::vector<int>& sndcts, //OUT
                             std::vector<SizeType>& sndqry, // OUT
                             std::vector<SizeType>& qryprm // OUT
                             ){
    sndcts.resize(oracle.comm_size());
    sndqry.resize(queries.size());
    qryprm.resize(queries.size());
    //  Count the number of queries targeting each processor
    for(auto x: queries){
        int px = oracle.owner(x);
        sndcts[px] += 1;
    }
    // Order the query data to be sent w.r.t the owining processor
    std::vector<int> sndptr = get_displacements(sndcts);
    for(SizeType i = 0; i < queries.size(); i++){
        int px = oracle.owner(queries[i]);
        sndqry[sndptr[px]] = queries[i];
        qryprm[sndptr[px]] = i;
        sndptr[px] += 1;
    }
}

//
// @brief   Get query results for rdata
//
// @param oracle   An oracle that can answer local queries
//   Oracle is expected to define
//     - size_type : type of indices
//     - value_type : type of data queried
//   Oracle is expected to answer
//     - comm_size() : size of the communicator
//     - get(queries, results) : local query results
//     - owner(idx) : owining process of index idx
//   Oracle can answer all the queries in the range
//    - block_begin + [0, ..., oracle.size() - 1] locally.
//    - [0, ..., oracle.global_size() - 1] at orcale.owner() locally.
// @param queries  A local array of indices onto rdata
//                 whose corresponding rdata entries are desired
// @return results  Results from rdata for the local queries
///
template<typename OracleType,
         typename SizeType = typename OracleType::size_type,
         typename DataType = typename OracleType::value_type>
void get_query_results_perm(const OracleType& oracle, // IN
                            std::vector<SizeType>& queries, // IN
                            std::vector<DataType>& results  // OUT
                            ){
    int nproc = oracle.comm_size();
    std::vector<int> sndcts(nproc), rcvcts(nproc), sndptr, rcvptr;
    std::vector<SizeType> sndqry(queries.size()), qryprm(queries.size());
    // 1. Prepare query data to send:
    prepare_query_send_perm<OracleType>(oracle, queries,
                                         sndcts, sndqry, qryprm);

    // 2. Send/Recieve the targeted queries
    //   2.1 all-to-all to specify how many to send to each proc.
    MPI_Alltoall(&sndcts[0], 1, MPI_INT,
                 &rcvcts[0], 1, MPI_INT, oracle.comm());
    sndptr = get_displacements(sndcts); rcvptr = get_displacements(rcvcts);
    int rcvtotal = std::accumulate(rcvcts.begin(), rcvcts.end(), 0);
    //  2.2 all-to-all to send/recieve data
    std::vector<SizeType> rcvqry(rcvtotal);
    MPI_Alltoallv(&sndqry[0], &sndcts[0], &sndptr[0], get_mpi_dt<SizeType>(),
                  &rcvqry[0], &rcvcts[0], &rcvptr[0], get_mpi_dt<SizeType>(),
                  oracle.comm());
    sndqry.clear();

    // 3. Prepare query results corresponding to the recieved queries
    std::vector<DataType> snddata(rcvtotal);
    oracle.get(rcvqry, snddata);

    // 4. Communicate the results
    rcvcts.swap(sndcts);
    sndptr.swap(rcvptr);
    std::vector<DataType> rcvdata(queries.size());
    MPI_Alltoallv(&snddata[0], &sndcts[0], &sndptr[0], get_mpi_dt<DataType>(),
                  &rcvdata[0], &rcvcts[0], &rcvptr[0], get_mpi_dt<DataType>(),
                  oracle.comm());
    snddata.clear();

    // 5. Update the results in the input query order
    results.resize(queries.size());
    for(SizeType i = 0; i < rcvdata.size(); i++){
        results[qryprm[i]] = rcvdata[i];
    }

}


//
// @brief   Prepare the data to be sent to answer distributed 'queries'
//
// @param oracle   An oracle that can answer local queries
//   Oracle is expected to define
//     - size_type : type of indices
//     - value_type : type of data queried
//   Oracle is expected to answer
//     - comm() : communicator targ
//     - comm_size() : size of the communicator
//     - get(queries, results) : local query results
//     - owner(idx) : owining process of index idx
// @param queries  A local array of indices onto rdata
//                 whose corresponding rdata entries are desired
// @return sndcts   query  counts to send to each processor
// @return sndqry   query values ordered by the processes
//
///
template<typename OracleType,
         typename T=int,
         typename SizeType=typename OracleType::size_type>
void prepare_query_send(const OracleType& oracle, //IN
                        const std::vector<SizeType>& queries, // IN
                        std::vector<T>& sndcts, //OUT
                        std::vector<SizeType>& sndqry // OUT
                        ){
    sndcts.resize(oracle.comm_size());
    sndqry.resize(queries.size());
    // initialize
    for(size_t i = 0; i < sndcts.size(); i++)
      sndcts[i] = 0;

    //  Count the number of queries targeting each processor
    for(auto x: queries){
        int px = oracle.owner(x);
        sndcts[px] += 1;
    }
    // Order the query data to be sent w.r.t the owining processor
    std::vector<T> sndptr = get_displacements(sndcts);
    for(SizeType i = 0; i < queries.size(); i++){
        int px = oracle.owner(queries[i]);
        sndqry[sndptr[px]] = queries[i];
        sndptr[px] += 1;
    }
}

//
// @brief   Get query results for rdata
//
// @param oracle   An oracle that can answer local queries
//   Oracle is expected to define
//     - size_type : type of indices
//     - value_type : type of data queried
//   Oracle is expected to answer
//     - comm() : communicator targ
//     - comm_size() : size of the communicator
//     - get(queries, results) : local query results
//     - owner(idx) : owining process of index idx
//   Oracle can answer all the queries in the range
//    - block_begin + [0, ..., oracle.size() - 1] locally.
//    - [0, ..., oracle.global_size() - 1] at orcale.owner() locally.
// @param queries  A local array of indices onto rdata
//                 whose corresponding rdata entries are desired
// @return results  Results from rdata for the local queries
//
// @return The displacements vector needed by MPI_Alltoallv.
///
#ifdef WITH_MXX

template<typename OracleType,
         typename SizeType = typename OracleType::size_type,
         typename DataType = typename OracleType::value_type>
void get_query_results(const OracleType& oracle, // IN
                       const std::vector<SizeType>& queries, // IN
                       std::vector<DataType>& results  // OUT
                       ){
    int nproc = oracle.comm_size();
    std::vector<SizeType> sndqry(queries.size());
    std::vector<std::size_t>  sndcts(nproc), rcvcts(nproc);

    // 1. Prepare query data to send:
    prepare_query_send<OracleType, std::size_t>(oracle, queries, sndcts, sndqry);

    // 2. Send/Recieve the targetd queries
    //   2.1 all-to-all to specify how many to send to each proc.
    std::vector<SizeType> rcvqry;
    MPI_Alltoall(&sndcts[0], 1, get_mpi_dt<std::size_t>(),
                 &rcvcts[0], 1, get_mpi_dt<std::size_t>(), oracle.comm());
    std::size_t rcvtotal = std::accumulate(rcvcts.begin(), rcvcts.end(), 0);
    rcvqry =  mxx::all2all(sndqry, sndcts, oracle.comm());
    assert(rcvqry.size() == rcvtotal);
    summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    sndqry.clear();
    // 3. Prepare query results corresponding to the recieved queries
    std::vector<DataType> snddata(rcvtotal);
    oracle.get(rcvqry, snddata);

    // 4. Communicate the results
    rcvcts.swap(sndcts);
    std::vector<DataType> rcvdata(queries.size());
    mxx::all2all(snddata.begin(), rcvdata.begin(), sndcts, rcvcts, oracle.comm());

    // 5. Update the results in the input query order
    std::vector<std::size_t> rcvptr = get_displacements(rcvcts);
    results.resize(queries.size());
    for(SizeType i = 0; i < queries.size(); i++){
        SizeType x = queries[i];
        int px = oracle.owner(x);
        results[i] = rcvdata[rcvptr[px]];
        rcvptr[px] += 1;
    }
}

#else

template<typename OracleType,
         typename SizeType = typename OracleType::size_type,
         typename DataType = typename OracleType::value_type>
void get_query_results(const OracleType& oracle, // IN
                       const std::vector<SizeType>& queries, // IN
                       std::vector<DataType>& results  // OUT
                       ){
    int nproc = oracle.comm_size();
    std::vector<int> sndcts(nproc), rcvcts(nproc), sndptr(nproc), rcvptr(nproc);
    std::vector<SizeType> sndqry(queries.size());

    // 1. Prepare query data to send:
    prepare_query_send<OracleType>(oracle, queries, sndcts, sndqry);
    // summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);

    // 2. Send/Recieve the targetd queries
    //   2.1 all-to-all to specify how many to send to each proc.
    A2A_PROFILE({
    MPI_Alltoall(&sndcts[0], 1, MPI_INT,
                 &rcvcts[0], 1, MPI_INT, oracle.comm());
      }, oracle.comm());
    sndptr = get_displacements(sndcts); rcvptr = get_displacements(rcvcts);
    int rcvtotal = std::accumulate(rcvcts.begin(), rcvcts.end(), 0);

#ifdef DEBUG
    print_disp(oracle.comm(), sndcts, sndptr, rcvcts, rcvptr);

    std::size_t rcx = rcvtotal;
    std::string pfx_str;
    pfx_str += "rcvtotal_";
    pfx_str += oracle.prefix_str();
    sum_max_report<std::size_t>(pfx_str.c_str(), rcx,
                                oracle.comm_rank(), oracle.comm(), std::cout);
#endif
    //summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    //  2.2 all-to-all to send/recieve data
    std::vector<SizeType> rcvqry(rcvtotal);
    // summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    A2A_PROFILE({
    MPI_Alltoallv(&sndqry[0], &sndcts[0], &sndptr[0], get_mpi_dt<SizeType>(),
                  &rcvqry[0], &rcvcts[0], &rcvptr[0], get_mpi_dt<SizeType>(),
                  oracle.comm());
      }, oracle.comm());
    // summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    sndqry.clear();

    // 3. Prepare query results corresponding to the recieved queries
    std::vector<DataType> snddata(rcvtotal);
    //summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    oracle.get(rcvqry, snddata);

    // 4. Communicate the results
    rcvcts.swap(sndcts);
    sndptr.swap(rcvptr);
#ifdef DEBUG
    print_disp(oracle.comm(), sndcts, sndptr, rcvcts, rcvptr);
#endif
    std::vector<DataType> rcvdata(queries.size());
    A2A_PROFILE({
    MPI_Alltoallv(&snddata[0], &sndcts[0], &sndptr[0], get_mpi_dt<DataType>(),
                  &rcvdata[0], &rcvcts[0], &rcvptr[0], get_mpi_dt<DataType>(),
                  oracle.comm());
      }, oracle.comm());
    snddata.clear();

    // 5. Update the results in the input query order
    results.resize(queries.size());
    for(SizeType i = 0; i < queries.size(); i++){
        SizeType x = queries[i];
        int px = oracle.owner(x);
        results[i] = rcvdata[rcvptr[px]];
        rcvptr[px] += 1;
    }
    // summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
}

#endif

template<typename OracleType,
         typename SizeType = typename OracleType::size_type,
         typename DataType = typename OracleType::value_type>
void query_results(const OracleType& oracle, // IN
                   std::vector<SizeType>& sndqry, // IN
                   std::vector<int>& sndcts, // IN
                   std::vector<DataType>& rcvdata){
    std::vector<int> sndptr(sndcts.size()), rcvptr(sndcts.size()),
        rcvcts(sndcts.size());
    // 2. Send/Recieve the targetd queries
    //   2.1 all-to-all to specify how many to send to each proc.
    A2A_PROFILE({
    MPI_Alltoall(&sndcts[0], 1, MPI_INT,
                 &rcvcts[0], 1, MPI_INT, oracle.comm());
      }, oracle.comm());

    sndptr = get_displacements(sndcts); rcvptr = get_displacements(rcvcts);
    int rcvtotal = std::accumulate(rcvcts.begin(), rcvcts.end(), 0);

#ifdef DEBUG
    print_disp(oracle.comm(), sndcts, sndptr, rcvcts, rcvptr);

    std::size_t rcx = rcvtotal;
    std::string pfx_str;
    pfx_str += "rcvtotal_";
    pfx_str += oracle.prefix_str();
    sum_max_report<std::size_t>(pfx_str.c_str(), rcx,
                                oracle.comm_rank(), oracle.comm(), std::cout);
#endif
    //summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    //  2.2 all-to-all to send/recieve data
    std::vector<SizeType> rcvqry(rcvtotal);
    // summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    A2A_PROFILE({
    MPI_Alltoallv(&sndqry[0], &sndcts[0], &sndptr[0], get_mpi_dt<SizeType>(),
                  &rcvqry[0], &rcvcts[0], &rcvptr[0], get_mpi_dt<SizeType>(),
                  oracle.comm());
      }, oracle.comm());
    // summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    // sndqry.clear();

    // 3. Prepare query results corresponding to the recieved queries
    std::vector<DataType> snddata(rcvtotal);
    //summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
    oracle.get(rcvqry, snddata);

    // 4. Communicate the results
    rcvcts.swap(sndcts);
    sndptr.swap(rcvptr);
#ifdef DEBUG
    print_disp(oracle.comm(), sndcts, sndptr, rcvcts, rcvptr);
#endif
    A2A_PROFILE({
    MPI_Alltoallv(&snddata[0], &sndcts[0], &sndptr[0], get_mpi_dt<DataType>(),
                  &rcvdata[0], &rcvcts[0], &rcvptr[0], get_mpi_dt<DataType>(),
                  oracle.comm());
      }, oracle.comm());
    snddata.clear();

}

template<typename OracleType,
         typename SizeType = typename OracleType::size_type,
         typename DataType = typename OracleType::value_type>
void get_query_results_srtd(const OracleType& oracle, // IN
                            const std::vector<SizeType>& queries, // IN
                            const std::vector<int>& qp_counts, // IN
                            std::vector<DataType>& results  // OUT
                            ){
    typedef typename std::vector<SizeType>::const_iterator size_iterator_t;

    int nproc = oracle.comm_size();

    // 0. an index vector;
    std::vector<SizeType> srtd_idx(queries.size());
    for(SizeType i = 0; i < queries.size();i++) srtd_idx[i] = i;

    size_iterator_t qtb = queries.cbegin();
    DefaultCompartor<size_iterator_t, SizeType> qp_compare(qtb);
    std::vector<SizeType> srt_queries(queries.size());
    std::vector<int> srtq_counts(queries.size());
    std::vector<int> sndcts(nproc);
    SizeType sidx = 0, qp_ptr = 0;

    // 1. prepare uniq query input
    for(int i = 0;i < nproc;i++) {
        SizeType rcount = sidx;
        // 1.1. sort the index vector among processors
        std::sort(srtd_idx.begin() + qp_ptr,
                  srtd_idx.begin() + (qp_ptr + qp_counts[i]),
                  qp_compare);

        // 1.2. eliminate dupes and maintain the counts array
        if(qp_counts[i] > 0) {
            srt_queries[sidx] = queries[srtd_idx[qp_ptr]];
            srtq_counts[sidx] = 1; sidx++;
        }
        for(int jdx = 1; jdx < qp_counts[i]; jdx++) {
            SizeType cptr = qp_ptr + ((SizeType)jdx);
            if(queries[srtd_idx[cptr]] == queries[srtd_idx[cptr - 1]]){
                srtq_counts[sidx - 1] += 1;
            } else {
                srt_queries[sidx] = queries[srtd_idx[cptr]];
                srtq_counts[sidx] = 1; sidx++;
            }
        }
        sndcts[i] = (sidx > rcount) ? ((int) (sidx - rcount)) : 0;
        qp_ptr += qp_counts[i];
    }
    srt_queries.resize(sidx); srtq_counts.resize(sidx);

    // 2. query and get results
    std::vector<DataType> srtq_results(srt_queries.size());
    query_results(oracle, srt_queries, sndcts, srtq_results);

    // 3. re-arrange the results
    sidx = 0;
    for(SizeType idx = 0; idx < srtq_results.size(); idx++){
        for(int jdx = 0; jdx < srtq_counts[idx];jdx++)
            results[srtd_idx[sidx++]] = srtq_results[idx];
    }
}

template<typename OracleType,
         typename SizeType = typename OracleType::size_type,
         typename DataType = typename OracleType::value_type>
void get_query_results_uniq(const OracleType& oracle, // IN
                            const std::vector<SizeType>& queries, // IN
                            std::vector<DataType>& results  // OUT
                            ){
    int nproc = oracle.comm_size();
    std::vector<int> sndcts(nproc), rcvcts(nproc), rcvptr(nproc);
    std::vector<SizeType> sndqry(queries.size());

    // 1. Prepare query data to send:
    prepare_query_send<OracleType>(oracle, queries, sndcts, sndqry);
    // summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);

    // 2. Get query results after eliminating dupes
    std::vector<DataType> rcvdata(queries.size());
    get_query_results_srtd<OracleType, SizeType, DataType>(oracle, sndqry,
                                                           sndcts, rcvdata);

    rcvcts = sndcts;
    rcvptr = get_displacements(rcvcts);

    // 3. Update the results in the input query order
    results.resize(queries.size());
    for(SizeType i = 0; i < queries.size(); i++){
        SizeType x = queries[i];
        int px = oracle.owner(x);
        results[i] = rcvdata[rcvptr[px]];
        rcvptr[px] += 1;
    }
    // summary_mem_report(oracle.comm_rank(), oracle.comm(), std::cout);
}


template<typename T=double>
void mpi_reduce_timings(T phase_time, MPI_Comm comm,
                        T& min_time, T& max_time, T& avg_time){
    T sum_time;
    int nproc;
    MPI_Comm_size(comm, &nproc);
    MPI_Allreduce(&phase_time, &min_time, 1, get_mpi_dt<T>(), MPI_MIN, comm);
    MPI_Allreduce(&phase_time, &max_time, 1, get_mpi_dt<T>(), MPI_MAX, comm);
    MPI_Allreduce(&phase_time, &sum_time, 1, get_mpi_dt<T>(), MPI_SUM, comm);
    avg_time = sum_time / (1.0 * nproc);
}

template<typename T>
void report_time(const char* phase_name, MPI_Comm comm,
                 T phase_time, T mpi_phase_time, std::ostream& ofs){
    T min_time, max_time, avg_time;
    T mpi_min_time, mpi_max_time, mpi_avg_time;
    mpi_reduce_timings<T>(phase_time, comm, min_time, max_time, avg_time);
    mpi_reduce_timings<T>(mpi_phase_time, comm, mpi_min_time,
                          mpi_max_time, mpi_avg_time);
    int rank;
    MPI_Comm_rank(comm, &rank);
    if(rank == 0){
        std::stringstream oss;
        oss
            << "\"" << phase_name << "\" : ["
            << std::setw(15) << (min_time/1000.0)
            << ","
            << std::setw(15) << (max_time/1000.0)
            << ","
            << std::setw(15) << (avg_time/1000.0)
            << ","
            << std::setw(15) << (mpi_min_time)
            << ","
            << std::setw(15) << (mpi_max_time)
            << ","
            << std::setw(15) << (mpi_avg_time)
            << "], "
            << std::endl;
        ofs << oss.str();
    }
}

template<typename T>
void mpi_report_all_times(const char* phase_name, T rtime,
                          MPI_Comm comm, std::ostream& ofs){
  int rank, nproc;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &nproc);
  std::vector<T> vrtime(nproc);
  MPI_Gather(&rtime, 1, get_mpi_dt<T>(),
             &vrtime[0], 1, get_mpi_dt<T>(), 0, comm);
  if(rank == 0) {
      for(int j = 0; j < nproc; j++) {
          std::stringstream oss;
          oss
              << "\"" << phase_name << "_" << j << "\" : ["
              << std::setw(15) <<  vrtime[j]
              << "], "
            << std::endl;
          ofs << oss.str();
      }
  }
}



#endif // MPI_UTIL_H
