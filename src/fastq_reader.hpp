#ifndef FASTQ_READER_H
#define FASTQ_READER_H

#include <iostream>
#include <string>
#include <cstring>
#include "util.hpp"

template<typename CharType, typename SizeType>
struct ReadStore {
    char _sep;
    std::vector<CharType> readsString;
    std::vector<SizeType> readsOffset;
    std::vector<CharType> qualsString;
    std::vector<SizeType> qualsOffset;
    int readId;

    ReadStore(char c = '0') : _sep(c) {
        // std::cout << "C" ;
    }

    void reset(){
        readsString.resize(0);
        readsOffset.resize(0);
        qualsString.resize(0);
        qualsOffset.resize(0);
    }

    void swap(ReadStore& other){
        std::swap(readsString, other.readsString);
        std::swap(qualsString, other.qualsString);
        std::swap(readsOffset, other.readsOffset);
        std::swap(qualsOffset, other.qualsOffset);
        std::swap(readId, other.readId);
    }

    SizeType size() const{
        return readsOffset.size();
    }

    void update_store(const std::string& in_str,
                      std::vector<CharType>& data_store,
                      std::vector<SizeType>& store_offset,
                      SizeType& position){
        data_store.resize(data_store.size() + in_str.length() + 1);
        memcpy(&data_store[position], in_str.c_str(), in_str.length() + 1);
        store_offset.push_back(position);
        position += in_str.length() + 1;
        data_store[position - 1] = _sep;
    }

    void update_store_rc(const std::string& in_str,
                         std::vector<CharType>& data_store,
                         std::vector<SizeType>& store_offset,
                         SizeType& position){
        data_store.resize(data_store.size() + in_str.length() + 1);
        memcpy(&data_store[position], in_str.c_str(), in_str.length() + 1);
        store_offset.push_back(position);
        position += in_str.length() + 1;
        data_store[position - 1] = _sep;

        std::string rc_str =  reverse_complement(in_str);
        data_store.resize(data_store.size() + rc_str.length() + 1);
        memcpy(&data_store[position], rc_str.c_str(), rc_str.length() + 1);
        store_offset.push_back(position);
        position += rc_str.length() + 1;
        data_store[position - 1] = _sep;
    }

    void reverse_reads(){
        if(readsString.size() <= 1)
            return;
        std::reverse(readsString.begin(),
                     readsString.begin() + (readsString.size() - 1));
        std::reverse(readsOffset.begin(), readsOffset.end());
    }

    void reverse(){
        reverse_reads();
    }
};

template<typename Stream>
class StreamWrapper{
    Stream _fs;
    StreamWrapper(Stream* in):_fs(in){
    }
public:
    StreamWrapper(const char* fname, std::size_t offset = 0)
        : _fs(fname, std::ios::in){
        _fs.seekg(offset, std::ios::beg);
    }


    inline void get_line(std::string& outs){
        std::getline(_fs, outs);
    }

    inline bool good(){
        return _fs.good();
    }

    inline bool is_end(std::size_t offset){
        return _fs.tellg() >= (std::streamoff) offset;
    }
};

template<typename CharType>
inline bool isAlphabet(CharType x){
        return (x == 'A' || x == 'a' ||
                x == 'G' || x == 'g' ||
                x == 'C' || x == 'c' ||
                x == 'T' || x == 't' ||
                x == 'N' || x == 'n');
}


template<typename InputStream>
class FastqReader{
    int _rlength;
    std::string _empty;
    InputStream& fqfs;
public:
    FastqReader(InputStream& fin):_rlength(4), _empty(""), fqfs(fin){}

    virtual void print(std::vector<std::string>& frecord,
                       std::ostream& ots) const {
        ots << readOf(frecord) << std::endl;
        ots << qualOf(frecord) << std::endl;
    }

    bool readRecord(std::vector<std::string>& fRecord) {
        fRecord.resize(_rlength);
        for(int i = 0; i < _rlength; i++) {
            fRecord[i] = "";
        }
        for(int i = 0; i < _rlength; i++) {
            if(!fqfs.good())
                return false;
            fqfs.get_line(fRecord[i]);
            // std::getline(*fqfs, fRecord[i]);
            fRecord[i] = trim(fRecord[i]);
            if(fRecord[i].length() == 0)
                return false;
        }

        return true;
    }


    bool readFirstRecord(std::vector<std::string>& fRecord) {
        fRecord.resize(_rlength);
        // read strings
        for(int i = 0; i < _rlength; i++)
            fRecord[i] = "";

        for(int i = 0; i < _rlength; i++) {
            if(!fqfs.good())
                return false;
            fqfs.get_line(fRecord[i]);
            fRecord[i] = trim(fRecord[i]);
        }
        for(int i = 1; i < _rlength; i++)
            if(fRecord[i].length() == 0)
                return false;

        int sdelta = _rlength, sread = _rlength;
        // unless, the record is good, we can't turst the first line's first char
        if(fRecord[2][0] == '+' && isAlphabet(fRecord[1][0]) && fRecord[0][0] == '@'){
            return true; // record is good!
        } else if(fRecord[1][0] == '@' && isAlphabet(fRecord[2][0]) && fRecord[3][0] == '+'){
            sdelta = 1; sread = 3; // have three lines of a 'good' record
        } else if(fRecord[2][0] == '@' && isAlphabet(fRecord[3][0])) {
            sdelta = 2; sread = 2; // have two lines of a 'good' record
        } else if (fRecord[1][0] == '+' && fRecord[3][0] == '@'){
            sdelta = 3; sread = 1; // have only one line of a 'good' record
        } else if(fRecord[2][0] == '+' && isAlphabet(fRecord[1][0])){
            sdelta = _rlength; sread = 0; // have zero lines of a 'good' record
        } else {
            return false; // bad record!
        }
        // bubble up by swapping
        assert(sdelta >= 0);
        for(int i = 0;(i + sdelta) < _rlength; i++)
            std::swap(fRecord[i], fRecord[i + sdelta]);
        // read extra lines to fill up the record
        for(int i = sread; i < _rlength; i++) {
            fRecord[i] = "";
            if(!fqfs.good())
                return false;
            fqfs.get_line(fRecord[i]);
            fRecord[i] = trim(fRecord[i]);
            if(fRecord[i].length() == 0)
                return false;
        }
        return true;
    }

    inline const std::string& readOf(std::vector<std::string>& fRecord) const{
        if(fRecord.size() > 1)
            return fRecord[1];
        else
            return _empty;
    }

    inline const std::string& qualOf(std::vector<std::string>& fRecord) const{
        if(fRecord.size() > 3)
            return fRecord[3];
        else
            return _empty;
    }

};

template<typename InputStream>
class FastaReader{
    int _rlength;
    std::string _empty;
    InputStream& fafs;
    char _hchar;
public:
    FastaReader(InputStream& fin,
                char hc='>')
        : _rlength(2), _empty(""), fafs(fin), _hchar(hc){}

    virtual void print(std::vector<std::string>& frecord,
                       std::ostream& ots) const {
        ots << readOf(frecord) << std::endl;
        ots << qualOf(frecord) << std::endl;
    }

    bool readRecord(std::vector<std::string>& fRecord) {
        fRecord.resize(_rlength);
        for(int i = 0; i < 2; i++) {
            fRecord[i] = "";
        }
        for(int i = 0; i < 2; i++) {
            if(!fafs.good())
                return false;
            fafs.get_line(fRecord[i]);
            // std::getline(*fafs, fRecord[i]);
            fRecord[i] = trim(fRecord[i]);
            if(fRecord[i].length() == 0)
                return false;
        }

        return true;
    }


    bool readFirstRecord(std::vector<std::string>& fRecord) {
        fRecord.resize(_rlength);

        // read strings
        for(int i = 0; i < _rlength; i++)
            fRecord[i] = "";

        for(int i = 0; i < _rlength; i++) {
            if(!fafs.good())
                return false;
            fafs.get_line(fRecord[i]);
            fRecord[i] = trim(fRecord[i]);
        }
        for(int i = 1; i < _rlength; i++)
            if(fRecord[i].length() == 0)
                return false;

        int sdelta = _rlength, sread = _rlength;
        // unless, the record is good, we can't turst the first line's first char
        if(fRecord[0][0] == _hchar){
            return true; // record is good;
        } else if(fRecord[1][0] == _hchar){
            sdelta = 1; sread = 1; // have one line of a 'good' record
        } else if (isAlphabet(fRecord[1][0])) {
            sdelta = _rlength; sread = 0; // have zero lines of a 'good' record
        }else {
            return false; // bad record!
        }
        // bubble up by swapping
        assert(sdelta >= 0);
        for(int i = 0;(i + sdelta) < _rlength; i++)
            std::swap(fRecord[i], fRecord[i + sdelta]);
        // read extra lines to fill up the record
        for(int i = sread; i < _rlength; i++) {
            fRecord[i] = "";
            if(!fafs.good())
                return false;
            fafs.get_line(fRecord[i]);
            fRecord[i] = trim(fRecord[i]);
            if(fRecord[i].length() == 0)
                return false;
        }
        return true;
    }

    inline const std::string& readOf(std::vector<std::string>& fRecord) const{
        if(fRecord.size() > 1)
            return fRecord[1];
        else
            return _empty;
    }

    inline const std::string& qualOf(std::vector<std::string>& fRecord) const{
        return _empty;
    }

};


template< typename CharType, typename SizeType,
          typename InputStream, typename FileReader>
bool read_batch(InputStream& fqfs, SizeType batchSize, SizeType offsetEnd,
               ReadStore<CharType, SizeType>& rbatch) {
    FileReader fprt(fqfs);
    SizeType rpos = 0, qpos = 0;
    bool lastRead = false;
    std::vector<std::string> fqRecord(4, "");
    if(batchSize == 0)
        return true;

    // 1. read first record : for handling special case
    if(!fprt.readFirstRecord(fqRecord))
        return false;

    //rbatch.update_store_rc(fprt.readOf(fqRecord), rbatch.readsString,
    //                       rbatch.readsOffset, rpos);
    rbatch.update_store(fprt.readOf(fqRecord), rbatch.readsString,
                        rbatch.readsOffset, rpos);
    rbatch.update_store(fprt.qualOf(fqRecord), rbatch.qualsString,
                        rbatch.qualsOffset, qpos);
    rbatch.readId = std::stoi(fqRecord[0].substr(1).c_str());

    // 2. read as much as batch size
    for(SizeType j=1; j < batchSize ; j++){
        for(SizeType i = 0; i < 4; i++)
            fqRecord[i] = "";

        if(!fprt.readRecord(fqRecord)){
            lastRead = true;
            break;
        }

        //rbatch.update_store_rc(fprt.readOf(fqRecord), rbatch.readsString,
        //                       rbatch.readsOffset, rpos);
        rbatch.update_store(fprt.readOf(fqRecord), rbatch.readsString,
                            rbatch.readsOffset, rpos);
        rbatch.update_store(fprt.qualOf(fqRecord), rbatch.qualsString,
                            rbatch.qualsOffset, qpos);

        if(!fqfs.good() || fqfs.is_end(offsetEnd)){
            lastRead = true;
            break;
        }
    }

    return lastRead;
}



#endif /* FASTQ_READER_H */
