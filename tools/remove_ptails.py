import sys
import os

alphabet = ['A', 'C', 'G', 'T', 'N']
base_alpha = ['A', 'C', 'G', 'T']
complementMapping = {'A': 'T', 'C': 'G', 'T': 'A',
                     'G': 'C', '-': '-', 'N': 'N'}
test_reads = []

def valid_read(inRead):
    for x in test_reads:
         if x in inRead:
             return False
    return True

def elim_poly_tails(infile, read_length, outfile):
    out_ptr = open(outfile, 'w')
    for x in alphabet:
        tx = x * (read_length/2)
        test_reads.append(tx)
    with open(infile) as f:
        try:
            while True:
                l1 = f.next()
                l2 = f.next()
                l3 = f.next()
                l4 = f.next()
                # print l1,
                if(valid_read(l2)):
                    out_ptr.write(l1);
                    out_ptr.write(l2);
                    out_ptr.write(l3);
                    out_ptr.write(l4);
        except StopIteration as e:
            print "(end)"
    out_ptr.close()


def main(args):
    infile = args[0]
    read_length = int(args[1])
    outfile = args[2]
    print infile, str(read_length), outfile
    elim_poly_tails(infile, read_length, outfile)


if __name__ == '__main__':
    main(sys.argv[1:])

