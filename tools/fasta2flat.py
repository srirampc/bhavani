import sys
import os

alphabet = ['A', 'C', 'G', 'T', 'N']
base_alpha = ['A', 'C', 'G', 'T']
complementMapping = {'A': 'T', 'C': 'G', 'T': 'A',
                     'G': 'C', '-': '-', 'N': 'N'}
test_reads = []

def valid_read2(inRead):
    for x in test_reads:
         if x in inRead:
             return False
    return True

def valid_read(inRead):
    for x in inRead:
        if x not in base_alpha:
            return False
    return True


def reverse_complement(inRead):
    ls = map(lambda x: complementMapping[x], inRead)
    rs = ''.join(ls)
    return rs[::-1]


def write_rev_file(infile, outfile, buffer_length):
    out_ptr = open(outfile, 'w')
    with open(infile) as f:
        f.seek(0, os.SEEK_END)
        nsize = f.tell()
        if(buffer_length >= nsize):
            buffer_length = nsize/2
        read_max = buffer_length
        idx = 1 + read_max
        while True:
            f.seek(-idx, os.SEEK_END)
            read_buffer = f.read(read_max)
            out_ptr.write(read_buffer[::-1])
            if(idx == nsize):
                break
            elif((idx + read_max) < nsize):
                idx += read_max
            else:
                read_max = nsize - idx
                idx = nsize
    out_ptr.write('$')
    out_ptr.close()


def write_fwd_file(infile, outfile, max_reads):
    count = 1
    rcount = 0
    out_ptr = open(outfile, 'w')
    with open(infile) as f:
        for line in f:
            if((count + 2) % 2 == 0):
                line = line.strip()
                if(valid_read(line) and valid_read2(line)):
                    out_ptr.write(line)
                    out_ptr.write("$")
                    out_ptr.write(reverse_complement(line))
                    out_ptr.write("$")
                    rcount += 1
                    if rcount == max_reads:
                       break
            count += 1
    out_ptr.close()
    print rcount


def main(args):
    infile = args[0]
    outfile = args[1]
    read_length = int(args[2])
    factor = read_length/2
    if(len(args) > 3):
        factor = int(args[3])
    max_reads = 0
    if(len(args) > 4):
        max_reads = int(args[4])
    for x in base_alpha:
        tx = x * factor
        test_reads.append(tx)
    fwd_file = outfile + ".fwd"
    rev_file = outfile + ".rev"
    print infile, outfile + ".fwd", outfile + ".rev", str(factor), str(read_length)
    write_fwd_file(infile, fwd_file, max_reads)
    write_rev_file(fwd_file, rev_file, 1024)


if __name__ == '__main__':
    main(sys.argv[1:])
