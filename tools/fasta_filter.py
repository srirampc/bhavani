#!/usr/bin/python
import sys
import os

alphabet = ['A', 'C', 'G', 'T', 'N']
base_alpha = ['A', 'C', 'G', 'T']
complementMapping = {'A': 'T', 'C': 'G', 'T': 'A',
                     'G': 'C', '-': '-', 'N': 'N'}
test_pat = []

def valid_read2(inRead):
    for x in test_pat:
         if x in inRead:
             return False
    return True

def valid_read(inRead):
    for x in inRead:
        if x not in base_alpha:
            return False
    return True

def write_file(infile, outfile, max_reads):
    count = 1
    rcount = 0
    out_ptr = open(outfile, 'w')
    with open(infile) as f:
        while True:
            try:
                header = f.readline().strip()
                line = f.readline().strip()
                if(len(line) == 0):
                    break
                if(valid_read(line) and valid_read2(line)):
                    out_ptr.write(header)
                    out_ptr.write("\n")
                    out_ptr.write(line)
                    out_ptr.write("\n")
                    rcount += 1
                    if rcount == max_reads:
                        break
                    count += 1
            except IOError as e:
                break
            except:
                break
    out_ptr.close()
    print rcount


def main(args):
    infile = args[0]
    outfile = args[1]
    read_length = int(args[2])
    factor = read_length/2
    if(len(args) > 3):
        factor = int(args[3])
    max_reads = 0
    if(len(args) > 4):
        max_reads = int(args[4])
    for x in base_alpha:
        tx = x * factor
        test_pat.append(tx)
    flt_file = outfile + ".filter.fa"
    print infile, flt_file, str(factor), str(read_length)
    write_file(infile, flt_file, max_reads)


if __name__ == '__main__':
    main(sys.argv[1:])
