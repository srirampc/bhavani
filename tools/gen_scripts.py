#!/usr/bin/python
import sys

script_format = """#!/bin/bash
#PBS  -o %s.t%d.k%d.x%d.p%d.out.log
#PBS  -e %s.t%d.k%d.x%d.p%d.err.log
#PBS -lnodes=%d:ppn=%d:compute,walltime=%d:00:00

source  /work/alurugroup/tpan/gcc48env.sh
MPIRUN=/usr/mpi/gcc/mvapich2-1.7-qlc/bin/mpirun

# Change to directory from which qsub command was issued
cd $PBS_O_WORKDIR
$MPIRUN -np %d %s/build/bhavani.x -f %s/data/%s/full.filter.fa -r %d -o tmp/test_stats.%s.k%d.out -t %d -k %d -b %d -g %d """


def gen_script(root_dir, dataset, rlen, tau, hd,
               np, batch, pct, wt, maximal):
    lnodes = np/16
    script_txt = script_format % (dataset, tau, hd, (1 if maximal else 0), np,
                                  dataset, tau, hd, (1 if maximal else 0), np,
                                  lnodes, 16, wt,
                                  np, root_dir, root_dir, dataset,
                                  rlen, dataset, hd, tau, hd, batch, pct)
    if maximal:
        script_txt += " -x "
    script_txt += " \n"
    script_fname = "run.%s.t%d.k%d.x%d.p%d.sh" % (dataset, tau, hd,
                                                  (1 if maximal else 0), np)
    with open(script_fname, 'w') as f:
        f.write(script_txt)
    # print script_txt


def main(args):
    root_dir = args[0]
    dataset = args[1]
    rlen = int(args[2])
    tau = int(args[3])
    hd_max = int(args[4])
    batch = int(args[5])
    pct = int(args[6])
    wt = int(args[7])
    maximal = False
    if len(args) > 8:
        maximal = True
    for np in [128, 256, 512, 1024]:
        for hd in range(hd_max):
            gen_script(root_dir, dataset, rlen, tau, hd, np,
                       batch, pct, wt, maximal)

if __name__ == '__main__':
    main(sys.argv[1:])

# python gen_scripts.py /work/alurugroup/jnk/bhavani dx 101 15 3 500 100 4
